---- MODULE MC ----
EXTENDS TlaGreenBst, TLC

\* CONSTANT definitions @modelParameterConstants:0N
const_168784610235923000 == 
3
----

\* CONSTANT definitions @modelParameterConstants:1DefaultHasRoot
const_168784610235924000 == 
FALSE
----

\* CONSTANT definitions @modelParameterConstants:2SplitSize
const_168784610236025000 == 
3
----

\* CONSTANT definitions @modelParameterConstants:3DefaultGNodeArr
const_168784610236026000 == 
<<>>
----

\* CONSTANT definitions @modelParameterConstants:4DefaultRoot
const_168784610236027000 == 
0
----

\* CONSTANT definitions @modelParameterConstants:5MaxValue
const_168784610236028000 == 
4
----

\* CONSTANT definitions @modelParameterConstants:6GNodeSize
const_168784610236029000 == 
3
----

\* CONSTANT definitions @modelParameterConstants:7DefaultNodeArr
const_168784610236030000 == 
<<>>
----

\* CONSTANT definitions @modelParameterConstants:8Readers
const_168784610236031000 == 
1
----

\* CONSTANT definitions @modelParameterConstants:9GNodeDepth
const_168784610236032000 == 
2
----

\* INVARIANT definition @modelCorrectnessInvariants:0
inv_168784610236033000 ==
Len(LET F[ii \in 0..Len(nodeArr)] == 
	IF ii = 0
	THEN << >>
	ELSE IF startPhysicalNodehasValueChecker(nodeArr, nodeArr[ii])
	THEN Append(F[ii-1], nodeArr[ii])
	ELSE F[ii-1]
IN F[Len(nodeArr)]) = Len(nodeArr)
----
\* INVARIANT definition @modelCorrectnessInvariants:1
inv_168784610236034000 ==
Len(SelectSeq(nodeArr, physicalNodeHasLeftChecker)) = Len(nodeArr)
----
\* INVARIANT definition @modelCorrectnessInvariants:2
inv_168784610236035000 ==
Len(SelectSeq(nodeArr, physicalNodeHasRightChecker)) = Len(nodeArr)
----
\* INVARIANT definition @modelCorrectnessInvariants:3
inv_168784610236036000 ==
Len(LET F[ii \in 0..Len(nodeArr)] == 
					IF ii = 0
					THEN << >>
					ELSE IF deletedInvariantChecker(nodeArr, nodeArr[ii])
					THEN Append(F[ii-1], nodeArr[ii])
					ELSE F[ii-1]
				IN F[Len(nodeArr)]) = Len(nodeArr)
----
\* INVARIANT definition @modelCorrectnessInvariants:4
inv_168784610236037000 ==
Len(SelectSeq(gNodeArr, physicalGNodeIsLeafChecker)) = Len(gNodeArr)
----
\* INVARIANT definition @modelCorrectnessInvariants:5
inv_168784610236038000 ==
Len(LET F[ii \in 0..Len(nodeArr)] == 
					IF ii = 0
					THEN << >>
					ELSE IF getDepth(nodeArr, nodeArr[ii]) <= GNodeDepth
					THEN Append(F[ii-1], nodeArr[ii])
					ELSE F[ii-1]
				IN F[Len(nodeArr)]) = Len(nodeArr)
----
\* INVARIANT definition @modelCorrectnessInvariants:6
inv_168784610236039000 ==
Len(SelectSeq(gNodeArr, maxSizeInvariantChecker)) = Len(gNodeArr)
----
\* INVARIANT definition @modelCorrectnessInvariants:7
inv_168784610236040000 ==
Len(LET F[ii \in 0..Len(gNodeArr)] == 
					IF ii = 0
					THEN << >>
					ELSE IF validNodeCountInvariantChecker(nodeArr, gNodeArr[ii])
					THEN Append(F[ii-1], gNodeArr[ii])
					ELSE F[ii-1]
				IN F[Len(gNodeArr)]) = Len(gNodeArr)
----
\* INVARIANT definition @modelCorrectnessInvariants:8
inv_168784610236041000 ==
Len(SelectSeq(gNodeArr, hasSiblingInvariantChecker)) = Len(gNodeArr)
----
\* INVARIANT definition @modelCorrectnessInvariants:9
inv_168784610236042000 ==
Len(LET F[ii \in 0..Len(nodeArr)] == 
					IF ii = 0
					THEN << >>
					ELSE IF nodeDfs(nodeArr, ii)[2]
					THEN Append(F[ii-1], ii)
					ELSE F[ii-1]
				IN F[Len(nodeArr)]) = Len(nodeArr)
----
\* INVARIANT definition @modelCorrectnessInvariants:10
inv_168784610236043000 ==
Len(LET F[ii \in 0..Len(gNodeArr)] == 
					IF ii = 0
					THEN << >>
					ELSE IF gNodeDfs(gNodeArr, ii)[3]
					THEN Append(F[ii-1], gNodeArr[ii])
					ELSE F[ii-1]
				IN F[Len(gNodeArr)]) = Len(gNodeArr)
----
=============================================================================
\* Modification History
\* Created Tue Jun 27 09:08:22 MSK 2023 by green-tea
