------------------------- MODULE TlaGreenBst -------------------------
EXTENDS Naturals, Sequences, TLC, Integers, FiniteSets

CONSTANTS N, GNodeSize, GNodeDepth, SplitSize, MaxValue, Readers,
DefaultRoot, DefaultHasRoot, DefaultGNodeArr, DefaultNodeArr

ASSUME N \in Nat /\ N > 0
ASSUME GNodeSize \in Nat /\ GNodeSize > 0
ASSUME GNodeDepth \in Nat /\ GNodeDepth > 0
ASSUME SplitSize \in Nat /\ SplitSize > 0

P == 1..N
R == N+1..Readers + N
VALUES == 1..MaxValue

RECURSIVE shiftR(_,_)
shiftR(n,pos) == 
    IF pos = 0 
    THEN n
    ELSE LET odd(z) == z % 2 = 1
             m == IF odd(n) THEN (n-1) \div 2 ELSE n \div 2
         IN shiftR(m, pos - 1)
         
RECURSIVE shiftL(_,_)
shiftL(n,pos) == 
    IF pos = 0 
    THEN n
    ELSE shiftL(n * 2, pos - 1)
    
RECURSIVE physicalNodehasValueChecker(_, _, _)
physicalNodehasValueChecker(nodeArr, node, isFirst) == IF node.hasValue
                                              THEN 
                                                  IF isFirst
                                                  THEN TRUE
                                                  ELSE FALSE
                                              ELSE
                                                  IF node.hasLeft
                                                  THEN physicalNodehasValueChecker(nodeArr, nodeArr[node.left], FALSE)
                                                  ELSE TRUE
                                                  /\
                                                  IF node.hasRight
                                                  THEN physicalNodehasValueChecker(nodeArr, nodeArr[node.right], FALSE)
                                                  ELSE TRUE
     
startPhysicalNodehasValueChecker(nodeArr, node) == physicalNodehasValueChecker(nodeArr, node, TRUE)

physicalNodeHasLeftChecker(node) == IF node.hasLeft THEN node.left # 0 ELSE TRUE

physicalNodeHasRightChecker(node) == IF node.hasRight THEN node.right # 0 ELSE TRUE

deletedInvariantChecker(nodeArr, node) ==
                IF node.deleted
                THEN
                    IF node.hasLeft
                    THEN ~nodeArr[node.left].hasValue
                    ELSE TRUE
                    /\
                    IF node.hasRight
                    THEN ~nodeArr[node.right].hasValue
                    ELSE TRUE
                ELSE TRUE
             
                
physicalGNodeIsLeafChecker(node) ==
    IF node.isLeaf
    THEN \A ii \in 1..GNodeSize : node.links[ii] = 0
    ELSE TRUE
    
max(a, b) == IF a < b THEN b ELSE a
    
RECURSIVE getDepth(_, _)
getDepth(nodeArr, node) == 
    IF ~node.hasValue
    THEN 0
    ELSE
        IF node.hasLeft
        THEN 
            IF node.hasRight
            THEN max(getDepth(nodeArr, nodeArr[node.left]), getDepth(nodeArr, nodeArr[node.right])) + 1
            ELSE getDepth(nodeArr, nodeArr[node.left]) + 1
        ELSE
            IF node.hasRight
            THEN getDepth(nodeArr, nodeArr[node.right]) + 1
            ELSE 1

maxSizeInvariantChecker(node) == node.countNode <= SplitSize

validSiblingInvariantLinkChecker(gNodeArr, node, i) ==
    IF node.hasLinks[i] /\ i < GNodeSize
    THEN
        IF node.hasLinks[i + 1]
        THEN gNodeArr[node.links[i]].sibling = node.links[i + 1]
        ELSE gNodeArr[node.links[i]].sibling = 0
    ELSE TRUE

validSiblingInvariantChecker(gNodeArr, node) ==
    \A ii \in 1..GNodeSize : validSiblingInvariantLinkChecker(gNodeArr, node, ii)
    
RECURSIVE validNodeCountInvariantRecChecker(_, _)
validNodeCountInvariantRecChecker(nodeArr, node) == 
    IF ~node.hasValue
        THEN 0
        ELSE
            IF node.hasLeft
            THEN 
                IF node.hasRight
                THEN validNodeCountInvariantRecChecker(nodeArr, nodeArr[node.left]) + validNodeCountInvariantRecChecker(nodeArr, nodeArr[node.right]) + 1
                ELSE validNodeCountInvariantRecChecker(nodeArr, nodeArr[node.left]) + 1
            ELSE
                IF node.hasRight
                THEN validNodeCountInvariantRecChecker(nodeArr, nodeArr[node.right]) + 1
                ELSE 1
    
validNodeCountInvariantChecker(nodeArr, node) ==
    node.countNode = validNodeCountInvariantRecChecker(nodeArr, nodeArr[node.root])
    
RECURSIVE maxValue(_, _)
maxValue(nodeArr, node) ==
    IF ~node.hasValue
    THEN 0
    ELSE
        IF node.hasLeft
        THEN
            IF node.hasRight
            THEN max(max(maxValue(nodeArr, nodeArr[node.left]), maxValue(nodeArr, nodeArr[node.right])), node.value)
            ELSE max(maxValue(nodeArr, nodeArr[node.left]), node.value)
        ELSE
            IF node.hasRight
            THEN max(maxValue(nodeArr, nodeArr[node.right]), node.value)
            ELSE node.value
    
highKeyInvariantChecker(nodeArr, node) ==
    node.highKey = maxValue(nodeArr, nodeArr[node.root])
    
hasSiblingInvariantChecker(node) == ~node.hasSibling \/ node.sibling # 0

RECURSIVE nodeDfs(_, _)
nodeDfs(nodeArr, node) == 
    IF nodeArr[node].hasLeft
    THEN
        LET subLeft == nodeDfs(nodeArr, nodeArr[node].left)
        IN 
            IF nodeArr[node].hasRight
            THEN
                LET subRight == nodeDfs(nodeArr, nodeArr[node].right)
                IN
                    <<
                        subLeft[1] \union subRight[1] \union {node},
                        (subLeft[2] /\ subRight[2] /\ Cardinality(subLeft[1] \union subRight[1] \union {node}) = Cardinality(subLeft[1]) + Cardinality(subRight[1]) + 1)
                    >>
            ELSE
                <<
                    subLeft[1] \union {node},
                    subLeft[2] /\ Cardinality(subLeft[1] \union {node}) = Cardinality(subLeft[1]) + 1
                >>
    ELSE
        IF nodeArr[node].hasRight
        THEN
            LET subRight == nodeDfs(nodeArr, nodeArr[node].right)
            IN
                <<
                    subRight[1] \union {node},
                    subRight[2] /\ Cardinality(subRight[1] \union {node}) = Cardinality(subRight[0]) + 1
                >>
        ELSE <<{node}, TRUE>>
    
RECURSIVE gNodeDfs(_, _)
gNodeDfs(nodeArr, node) == 
    LET F[ii \in 0..GNodeSize] ==
        IF ii = 0
        THEN << {node}, 1, TRUE>>
        ELSE 
            IF nodeArr[node].hasLinks[ii]
            THEN 
                LET sub == gNodeDfs(nodeArr, nodeArr[node].links[ii])
                IN
                    <<
                        F[ii - 1][1] \o sub[1],
                        F[ii - 1][2] + sub[2],
                        sub[3] /\ F[ii - 1][3] /\ Cardinality(F[ii - 1][1] \o sub[1]) = F[ii - 1][2] + sub[2]
                    >>
            ELSE
                F[ii - 1]
    IN 
        F[Len(nodeArr)]
        
abs(a) == 
    IF a < 0
    THEN -a
    ELSE a
        
RECURSIVE balanceInvariantChecker(_, _)
balanceInvariantChecker(nodeArr, node) == 
    LET F[ii \in 0..GNodeSize] ==
        IF ii = 0
        THEN <<1, TRUE>>
        ELSE 
            IF nodeArr[node].hasLinks[ii]
            THEN 
                LET sub == balanceInvariantChecker(nodeArr, nodeArr[node].links[ii])
                IN
                    <<
                        max(F[ii - 1][1], sub[1]),
                        F[ii - 1][2] /\ sub[2] /\ abs(F[ii - 1][1] - sub[1]) <= 1
                    >>
            ELSE
                F[ii - 1]
    IN 
        <<
            F[Len(nodeArr)][1] + 1,
            F[Len(nodeArr)][2]
        >>
        
getResultChecker(values, res, statesBefore, statesAfter) ==
    \E ii \in 1..Len(statesBefore) : statesBefore[ii] = "started" \/
    statesBefore # statesAfter  \/
    IF res[1] = -1
    THEN ~values[res[2]]
    ELSE values[res[2]]
    
isQuiscent(processWriterState, processReaderState) == 
    \A p \in P : processWriterState[p] # "started" /\
    \A r \in R : processReaderState[r] # "started"
    
checkNodeIsLeaf(nodeArr, node) ==
    IF nodeArr[node].hasLeft
    THEN 
        IF nodeArr[nodeArr[node].left].hasValue
        THEN FALSE
        ELSE
            IF nodeArr[nodeArr[node].right].hasValue
            THEN FALSE
            ELSE TRUE
    ELSE
        IF nodeArr[node].hasRight
        THEN
            IF nodeArr[nodeArr[node].right].hasValue
            THEN FALSE
            ELSE TRUE
        ELSE TRUE
    

createNode(key) == [hasValue |-> TRUE, value |-> key, hasLeft |-> FALSE, hasRight |-> FALSE, isLeaf |-> TRUE, left |-> 0, right |-> 0, deleted |-> FALSE]

createEmptyNode == [hasValue |-> FALSE, value |-> 0, hasLeft |-> FALSE, hasRight |-> FALSE, isLeaf |-> TRUE, left |-> 0, right |-> 0, deleted |-> FALSE]


(*
--algorithm NewGreenBst {
    variables
        root = DefaultRoot,
        hasRoot = DefaultHasRoot,
        globalLock = TRUE,
        gNodeArr = DefaultGNodeArr,
        nodeArr = DefaultNodeArr,
        insertRes = [s \in P |-> FALSE],
        resultsForGet = [s \in R |-> <<-1, -1>>],
        deleteRes = [s \in P |-> -1],
        processWriterState = [s \in P |-> "not started"],
        processReaderState = [s \in R |-> "not started"],
        values = [s \in VALUES |-> FALSE];
        
    macro AtomicInc(index) {
        gNodeArr[index].rev := gNodeArr[index];
    }
    
    procedure initLeaf() variables i, cur, istack {
initLeaf:    istack := <<>>;
             nodeArr := Append(nodeArr, createNode(key));
             gNodeArr := Append(gNodeArr, [isLeaf |-> TRUE, links |-> [x \in 0..GNodeSize |-> 0], hasLinks |-> [x \in 0..GNodeSize |-> FALSE], root |-> Len(nodeArr), sibling |-> 0, countNode |-> 1, highKey |-> 0, lock |-> TRUE, hasSibling |-> FALSE, rev |-> 0]);
             
             i := 0;
pushStack:   istack := Append(istack, Len(nodeArr));
startInit:   while (i <= GNodeSize) {
                 cur := Head(istack);
                 istack := SubSeq(istack, 1, Len(istack)); 
             
addLeft:         nodeArr := Append(nodeArr, createEmptyNode);
addleft2:        nodeArr[cur].left := Len(nodeArr);
addleft3:        nodeArr[cur].hasLeft := TRUE;
                 istack := Append(istack, Len(nodeArr));
                 i := i + 1;
                 
addRight:        nodeArr := Append(nodeArr, createEmptyNode);
addRight2:       nodeArr[cur].right := Len(nodeArr);
addright3:       nodeArr[cur].hasRight := TRUE;
                 istack := Append(istack, Len(nodeArr));
                 i := i + 1;
             }
    }
    
    
    
    
    
    procedure doInsertLeaf(current, key) variables tmp, tmp1, mid, p, p1, recStack, recStack1, ret, depth, res, calc, maxDepth {
getCurrentRoot:                   p := gNodeArr[current].root;

                                  depth := 0;
        
checkKeyAlreadyExists:            if (key = nodeArr[p].value) {
                                      ret := 0;
                                      goto finishInsertRecInsert;
                                  };
                                  
setZeroRet:                       ret := 0;
                                  
checkLeft4:                       if (key < nodeArr[p].value) {
checkLeftExists:                      if (nodeArr[p].hasLeft) {
                                          recStack1 := Append(recStack1, <<p, depth, ret, 0>>);
getLeftInInsert:                          p := nodeArr[p].left;
                                          goto checkKeyAlreadyExists;
                                      };
                                      
createNewNode:                        nodeArr := Append(nodeArr, createNode(key));
insertInLeft:                         nodeArr[p].left := Len(nodeArr);
insertHasLeft:                        nodeArr[p].hasLeft := TRUE;
                                      
updateCountNode:                      tmp1 := gNodeArr[tmp].countNode;
updateCountNode2:                     tmp1 := tmp1 + 1;
                                      
                                      ret := 0;                                      
                                      
                                      goto finishInsertRecInsert;
                                      
checkRet:                             if (ret > 0) {

checkRightBeforeSum:                      if (~nodeArr[p].hasRight) {
                                              goto finishInsertRecInsert;
                                          };
gotToRightBeforeSum:                       p := nodeArr[p].right;






\*____________________________sum node________________________________
                                          p1 := p;
                                          res := 0;
                                          recStack := <<>>;
                                          
checkLeft12:                              if (nodeArr[p1].hasLeft) {
                                              recStack := Append(recStack, <<p1, 0, FALSE>>);
goToLeft12:                                   p1 := nodeArr[p1].left;
                                              goto checkLeft12;
                                          };
                                          
checkRight12:                             if (nodeArr[p1].hasRight) {
                                              recStack := Append(recStack, <<p1, res, TRUE>>);
goToRight12:                                  p1 := nodeArr[p1].right;
                                              goto checkLeft12;
                                          };
                                          
checkRetValue12:                          if (Len(recStack) = 0) {
                                              goto updateRet;
                                          };
                                          
getRecValue12:                            tmp := Tail(recStack);
                                          recStack := SubSeq(recStack, 0, Len(recStack) - 1);      
                                          
                                          p1 := tmp[0];
                                          if (tmp[2]) {
                                              res := res + tmp[1] + 1; 
                                              goto checkRetValue12;
                                          } else {
                                              goto checkRight12;
                                          };
\*____________________________________________________________________







updateRet:                                ret := ret + res + 1;
                                          depth := depth + 1;
                                          
                                          calc := shiftL(1, depth - 1) - 1;
                                          
                                          if (ret - 1 < calc /\ depth > 2) {
                                              maxDepth := shiftL(1, depth) - 1;
                                              
                                              
                                              
                                              
                                              
                                              
                                              
                                              
                                              
                                              
                                              
\*______________________________fill buf____________________________
                                              array := <<>>;
                                              recStack := <<>>;
                                              answer := <<>>;
startFillBuf2:                                p1 := p;                           

checkLeft14:                                  if (nodeArr[p1].hasLeft) {
                                                  recStack := Append(recStack, <<p1, FALSE>>);
goToLeft14:                                       p1 := nodeArr[p1].left;
                                                  goto checkLeft14;
                                              };

saveAnswer14:                                 if (~nodeArr[p].deleted) {
saveAnswer15:                                     answer := Append(answer, nodeArr[p].value);
                                              };  

checkRight14:                                 if (nodeArr[p1].hasRight) {
                                                  recStack := Append(recStack, <<p1, TRUE>>);
goToRight14:                                      p1 := nodeArr[p1].right;
                                                  goto checkLeft14;
                                              };
                              
checkRetValue14:                              if (Len(recStack) = 0) {
                                                  goto startUpdateCurrent2;
                                              };

getRecValue14:                                tmp := Tail(recStack);
                                              recStack := SubSeq(recStack, 0, Len(recStack - 1));
                              
                                              p1 := tmp[0];
                                              if (tmp[1]) {
                                                  goto checkRetValue14;
                                              } else {
                                                  goto saveAnswer14;
                                              };
\*______________________________-___________________________________  









startUpdateCurrent2:                          AtomicInc(current);










\*_________________________________smart_fill_val_inc_________________________________ 
startSmartFillValInc:                         l := 0;
                                              r := Len(answer) - 1;      
                                              p1 := p;
                                              recStack := <<>>;
                                              
        
startSmartFillValInc2:                        if (l > r)  {
checkLeft6:                                       if (nodeArr[p1].hasLeft) {
                                                      recStack := Append(recStack, <<p1, l, r, 0>>);
goToLeft6:                                            p1 := nodeArr[p1].left;
                                                      goto startSmartFillValInc2;
                                                  };
                                                  
setEmpty:                                         nodeArr[p1].hasValue := FALSE;
        
checkRight6:                                      if (nodeArr[p1].hasRight) {
                                                      recStack := Append(recStack, <<p1, l, r, 1>>);
goToRight6:                                           p1 := nodeArr[p1].right;
                                                      goto startSmartFillValInc2;                                     
                                                  };
                                              } else {
                                                  mid := (l + r) \div 2;
                                                  
checkLeft7:                                       if (nodeArr[p1].hasLeft) {
                                                      recStack := Append(recStack, <<p1, l, r, 2>>);
goToLeft7:                                            p1 := nodeArr[p1].left;
                                                      r := mid - 1;
                                                      goto startSmartFillValInc2;
                                                  };
                                                  
saveAnswer7:                                      nodeArr[p1].value := answer[mid];
saveAnswerHasValue7:                              nodeArr[p1].hasValue := TRUE;
        
checkRight7:                                      if (nodeArr[p1].hasRight) {
                                                      recStack := Append(recStack, <<p1, l, r, 3>>);
goToRight7:                                           p1 := nodeArr[p1].right;
                                                      l := mid + 1;
                                                      goto startSmartFillValInc2;
                                                  };
                                              };
                                              
checkRetValue7:                               if (Len(recStack) = 0) {
                                                  goto finishUpdateCurrent3;
                                              };
                                              
getRetValue7:                                 tmp := Tail(recStack);
                                              recStack := SubSeq(recStack, 0, Len(recStack - 1));
                                              
                                              p1 := tmp[0];
                                              l := tmp[1];
                                              r := tmp[2];
                                              
                                              if (tmp[3] = 0) {
                                                  goto setEmpty;
                                              };
                                              
checkSecond:                                  if (tmp[3] = 1) {
                                                  goto checkRetValue7;
                                              };
                                              
checkThird:                                   if (tmp[3] = 2) {
                                                  goto saveAnswer7;
                                              };
                                              
checkFourth:                                  if (tmp[3] = 3) {
                                                  goto checkRetValue7;
                                              };
\*____________________________________________________________________________________ 












finishUpdateCurrent3:                         AtomicInc(current);
                                              
                                              recStack1 := Append(recStack1, <<p, depth, ret, 1>>);
                                              goto checkKeyAlreadyExists;
                                          } else {
                                              goto finishInsertRecInsert;
                                          }
                                      } else {
                                          goto finishInsertRecInsert;
                                      }                      
                                  } else {
checkRight8:                          if (nodeArr[p].hasRight) {
                                          recStack1 := Append(recStack1, <<p, depth, ret, 2>>);
goToRight8:                               p := nodeArr[p].right;
                                          goto checkKeyAlreadyExists;
                                      };
                                      
createNewNode2:                       nodeArr := Append(nodeArr, createNode(key));
insertInRight:                        nodeArr[p].right := Len(nodeArr);
setHasRight:                          nodeArr[p].hasRight := TRUE;
                                      
updateCountNode3:                     tmp := gNodeArr[tmp].countNode;
updateCountNode4:                     tmp := tmp + 1;
                                      
                                      ret := 0;                                      
                                      
                                      goto finishInsertRecInsert;
                                      
checkRet2:                            if (ret > 0) {
                                      
checkLeftBeforeSum:                       if (~nodeArr[p].hasLeft) {
                                              goto finishInsertRecInsert;
                                          };
gotToLeftBeforeSum:                       p := nodeArr[p].left;
                                          
                                          goto checkLeft12
                                      } else {
                                          goto finishInsertRecInsert;
                                      };
                                  };
                                  
finishInsertRecInsert:            if (Len(recStack1) = 0) {
                                      return;
                                  };
                                  
getRetValue2:                     tmp := Tail(recStack1);
                                  recStack1 := SubSeq(recStack1, 0, Len(recStack1 - 1));
                                  
                                  p := tmp[0];
                                  depth := tmp[1];
                                  ret := tmp[2] + ret;
                                  
                                  if (tmp[3] = 0) {
                                      goto checkRet;
                                  };
                                  
checkSecond3:                    if (tmp[3] = 1) {
                                      goto finishInsertRecInsert;
                                  };
                                  
checkThird3:                      if (tmp[3] = 2) {
                                      goto checkRet2;
                                  };
                                  
checkFourth3:                     if (tmp[3] = 3) {
                                      goto finishInsertRecInsert;
                                  };
\*_________________________________-_________________________________ 
    }
    
    procedure smartFillValParentLo(l, r, gNode, buf, rmax, links) variables recStack, mid, val, child, lastNode, bits, depth, newBits, movedBits, ln, rn {
        
startFillValParentLo:         recStack := <<>>;

checkBounds:                  if (r < l \/ l = rmax) {
                                  goto checkRecSize15;
                              };
        
calcMid:                      mid := (l + r) \div 2;
                              val := buf[mid];
                              child := links[mid];
                              p := gNodeArr[gNode].root;
                         
                         
                         
                         
                         
                         
                         
\*_____________________-___detailed_node_search_________________________________________

detailedNodeSearchStart:      p1 := p;
                              lastNode := p1;
                         
                              bits := 1;
                              depth := 0;
                         
mainCycleStart:               while (TRUE) {
checkHasValue:                    if (~nodeArr[p1].hasValue) {
                                      goto updateBits;
                                  };

detailedNodeSearchCalcs:          depth := depth + 1;
                                  bits := bits * 2;
                                  lastNode := p1;
                             
checkKey:                         if (val < nodeArr[p1].value) {
dnsCheckLeft:                         if (~nodeArr[p1].hasLeft) {
                                          goto updateBits;
                                      };

dnsGetLeft:                           p1 := nodeArr[p1].left;
                                  } else {
dnsCheckRight:                        if (~nodeArr[p1].hasRight) {
                                          goto updateBits;
                                      };

dnsGetRight:                          p1 := nodeArr[p1].right;
                                      bits := bits + 1;
                                  }
                              };
                        
updateBits:                   bits := bits \div 2; 

\*______________________________________________________________________________________











        
        
prepareBitsAndDepth:          newBits := bits;
                              movedBits := bits;
                              bits := shiftL(bits, GNodeDepth - depth);
                              depth := depth + 1;
                         
getLastNodeLeft:              ln := nodeArr[lastNode].left;
getLastNodeRight:             rn := nodeArr[lastNode].right;          
checkLastNodeValue:           if (val < nodeArr[lastNode].value) {
setLnValue:                       nodeArr[ln].value := val;
setLnHasValue:                    nodeArr[ln].hasValue := TRUE;
                             
setRnValue:                       nodeArr[rn].value := nodeArr[lastNode].value;
                                  newBits := newBits * 2;
                                  movedBits := (movedBits * 2) + 1;
                              } else {
setRnValue2:                      nodeArr[rn].value := val;
setRnHasValue:                    nodeArr[rn].hasValue := TRUE;
                             
setLnValue2:                      nodeArr[ln].value := nodeArr[lastNode].value;
                                  newBits := newBits * 2 + 1;
                                  movedBits := movedBits * 2;
                              };
                         
calcBits:                     movedBits := shiftL(movedBits, GNodeDepth - depth);
                              newBits := shiftL(newBits, GNodeDepth - depth);
                         
setLinks1:                    gNodeArr[gNode].links[movedBits] := gNodeArr[gNode].links[bits];
setLinks2:                    gNodeArr[gNode].links[newBits] := child;

callFillInnerNode1:           recStack := Append(recStack, <<p, l, r, FALSE>>);
                              r := mid - 1;
                              goto checkBounds;
                              
callFillInnerNode2:           recStack := Append(recStack, <<p, l, r, TRUE>>);
                              l := mid + 1;
                              goto checkBounds;
                              
                              
checkRecSize15:               if (Len(recStack) = 0) {
                                  return;
                              };
                              
goToRet15:                    tmp := Tail(recStack);
                              recStack := SubSeq(recStack, 0, Len(recStack) - 1);
                              
                              p := tmp[0];
                              l := tmp[1];
                              r := tmp[2];
                              
                              if (tmp[3]) {
                                  goto checkRecSize15;
                              } else {
                                  goto callFillInnerNode2;
                              }
    }
    
    procedure doInsertNode(current, sibling, keyL, keyR, buf, bufLink) variables p, tempL, tempR, bits, depth {
    
    
    
    
    
\*_____________________-___detailed_node_search_________________________________________

detailedNodeSearchStart2:     p := gNodeArr[current].root;
                              lastNode := p;
                         
                              bits := 1;
                              depth := 0;
                         
mainCycleStart2:              while (TRUE) {
checkHasValue2:                   if (~nodeArr[p].hasValue) {
                                      goto updateBits2;
                                  };

detailedNodeSearchCalcs2:         depth := depth + 1;
                                  bits := bits * 2;
                                  lastNode := p;
                             
checkKey2:                        if (keyR < nodeArr[p].value) {
dnsCheckLeft2:                        if (~nodeArr[p].hasLeft) {
                                          goto updateBits2;
                                      };

dnsGetLeft2:                          p := nodeArr[p].left;
                                  } else {
dnsCheckRight2:                       if (~nodeArr[p].hasRight) {
                                          goto updateBits2;
                                      };

dnsGetRight2:                         p := nodeArr[p].right;
                                      bits := bits + 1;
                                  }
                              };
                        
updateBits2:                  bits := bits \div 2; 

\*______________________________________________________________________________________











checkLeftInInsertNode:        if (~nodeArr[lastNode].hasLeft) {






\*________________________________________fill-buf-lo______________________________________

startFillBufLo2:                  p := gNodeArr[current].root;
                                  answer := <<>>;
                              
fboCheckLeftRight4:               if (nodeArr[p].hasLeft /\ nodeArr[p].hasRight) {
fboGetleft2:                          l := nodeArr[p].left;
fboGetRight2:                         r := nodeArr[p].right;           

fboCheckLeftRight5:                   recStack := Append(recStack, <<p, FALSE>>);
                                      p := l;
                                      goto fboCheckLeftRight4;
                                  
fboCheckLeftRight6:                   recStack := Append(recStack, <<p, TRUE>>);
                                      p := r;
                                      goto fboCheckLeftRight4;                                            
                                  } else {
fboIncValue2:                         if (~nodeArr[p].deleted) {
                                          answer := Append(answer, nodeArr[p].value);
                                      }
                                  };
                              
fboCheckRetSize2:                 if (Len(recStack) = 0) {
                                      goto fillLinkBuf;
                                  };
                              
fboGoToRet4:                      tmp := Tail(recStack);
                                  recStack := SubSeq(recStack, 0, Len(recStack) - 1);
                              
                                  p := tmp[0];
                                  if (tmp[1]) {
                                      goto fboCheckRetSize2; 
                                  } else {
                                      goto fboCheckLeftRight6;
                                  };

\*_________________________________________________________________________________________







fillLinkBuf:                      links := gNodeArr[current].links;

startUpdateCurrent:               AtomicInc(current);

                                  mid := Len(answer) \div 2;
                              
updateValues1:                    gNodeArr[current].root := answer[mid];
updateValues2:                    gNodeArr[current].links[0] := links[mid];

smartFillValParentLo1:            call smartFillValParentLo(0, mid - 1, current, answer, Len(answer), links);
smartFillValParentLo2:            call smartFillValParentLo(mid + 1, Len(answer), current, answer, Len(answer), links);

finishUpdateCurrent2:             AtomicInc(current);

updateCountNode:                  gNodeArr[current].countNode := Len(answer) * 2;







\*_____________________-___detailed_node_search_________________________________________

detailedNodeSearchStart3:         p := gNodeArr[current].root;
                                  lastNode := p;
                         
                                  bits := 1;
                                  depth := 0;
                         
mainCycleStart3:                  while (TRUE) {
checkHasValue3:                       if (~nodeArr[p].hasValue) {
                                          goto updateBits3;
                                      };

detailedNodeSearchCalcs3:             depth := depth + 1;
                                      bits := bits * 2;
                                      lastNode := p;
                             
checkKey3:                            if (keyR < nodeArr[p].value) {
dnsCheckLeft3:                            if (~nodeArr[p].hasLeft) {
                                              goto updateBits3;
                                          };

dnsGetLeft3:                              p := nodeArr[p].left;
                                      } else {
dnsCheckRight3:                           if (~nodeArr[p].hasRight) {
                                              goto updateBits3;
                                          };

dnsGetRight3:                             p := nodeArr[p].right;
                                          bits := bits + 1;
                                      }
                                  };
                        
updateBits3:                      bits := bits \div 2; 

\*______________________________________________________________________________________
                              };







getTempL:                     tempL := nodeArr[lastNode].left;
getTempR:                     tempR := nodeArr[lastNode].right;

                              depth := depth + 1;
                              
checkKeyR:                    if (keyR < nodeArr[lastNode].value) {
setKeyR1:                         nodeArr[lastNode].value := keyR;
setKeyL1:                         nodeArr[tempL].value := keyL;
setKeyR2:                         nodeArr[tempR].value := keyR;
                              } else {
setTempL:                         nodeArr[tempL].value := nodeArr[lastNode].value;
setKeyR3:                         nodeArr[lastNode].value := keyR;
setKeyR4:                         nodeArr[tempR].value := keyR;
                              };

updateCountNode2:             gNodeArr[current].countNode := gNodeArr[current].countNode + 2;
                              bits := (bits * 2) + 1;
                              
secondUpdateBits:             bits := shiftL(bits, GNodeDepth - depth);

setCurrentLinks:              gNodeArr[current].links[bits] := sibling;

    }
    
        
    
    \* Стэк - вспомогателная структура данных, которая используется многопоточно, а значит должна быть глобально определена

    procedure insert(key) variables stackData = <<>>, keyRNext, oldCurrent, parent, keyLNext, links, oldSibling, current, rev, bits, depth, p, temp, scanRes, t, array, split, recStack, answer, tmp, tmp2, l, r, mid, sibling, keyL, keyR, recStack1 {
checkHasRoot:     if (~hasRoot) {
hasRootLock:          await globalLock;
                      globalLock := FALSE;
checkHasRoot2:        if (~hasRoot) {
initLeaf:                 call initLeaf();          
setRoot:                  root := Len(gNodeArr);
                          hasRoot := TRUE;

releaseLock:              globalLock := TRUE;
returnInsertRes:          insertRes[self] := TRUE;
                          return;
                      } else {
releaseLock2:             globalLock := TRUE;
                      }
                  };
                  
startFindGNode:   current := root;
        
startFindGNode2:  while (~gNodeArr[current].isLeaf) {
startCycle:           oldCurrent := current;












                      
\*____________________scannode_________________________________                      
startScanNode:        rev := gNodeArr[current].rev;
                      bits := 1;
                      depth := 0;
                      
                      temp := TRUE;
                      
                      if (rev % 2 = 1) {
updating:                 temp := FALSE;
                          scanRes := FALSE;
                      \* return
                      } else {
getRoot:                  p := gNodeArr[current].root;
startSearchInGNode:       while (~checkNodeIsLeaf(nodeArr, p)) {
                              bits := bits * 2;
                              depth := depth + 1;
                              
checkIsLeft:                  if (key < nodeArr[p].value) {
goLeft:                           p := nodeArr[p].left;
                              } else {
goRight:                          p := nodeArr[p].right;
                                  bits := bits + 1;
                              }
                          };
                          
                          bits := bits \div 2;
preparePath:              bits := shiftL(bits, GNodeDepth - depth);
                          
checkHighKey:             if (gNodeArr[current].hasSibling /\ gNodeArr[current].highKey > 0 /\ gNodeArr[current].highKey <= key) {
checkRev:                     if (gNodeArr[current].rev - rev > 0) {
                                  temp := FALSE;
                                  scanRes := FALSE;
                              } else {
                                  temp := gNodeArr[current].hasSibling;
                                  current := gNodeArr[current].sibling;
                                  scanRes := TRUE;
                              }
                          } else {
getChildGNode:                temp := gNodeArr[current].hasLinks[bits];
checkRev2:                    if (gNodeArr[current].rev - rev > 0) {
                                  temp := FALSE;
                                  scanRes := FALSE;
                              } else {
                                  current := gNodeArr[current].links[bits];
                                  scanRes := FALSE;
                              }
                          };
                      };
                      
\*_____________________________________________________________














prepareStack:         if (~scanRes) {
                          if (~temp) {
                              current := oldCurrent;
                          } else {
                              stackData := Append(stackData, oldCurrent);
                          }
                      }   
                  };
                  
                  t := current;
                  
getGNodeLock:     await gNodeArr[t].lock;
                  gNodeArr[t].lock := FALSE;












\*______________________________move right_________________________
startMoveRight:   while (gNodeArr[t].hasSibling /\ gNodeArr[t].highKey > 0 /\ gNodeArr[t].highKey <= key) {
saveSibling:          current := gNodeArr[t].sibling;
getNewLock:           await gNodeArr[current].lock;
                      gNodeArr[current].lock := FALSE;
                      
releaseOldLock:       gNodeArr[t].lock := TRUE;
                      t := current;
                  };
                  
                  current := t;
\*______________________________-___________________________________
















startInsertCycle: while (TRUE) {
checkIsLeaf:          if (gNodeArr[current].isLeaf) {
checkGNodeSize:           if (gNodeArr[current].countNode >= SplitSize) {













\*______________________________fill buf____________________________
                              array := <<>>;
                              recStack := <<>>;
startFillBuf:                 p := gNodeArr[current].root;

checkLeft:                    if (nodeArr[p].hasLeft) {
                                  recStack := Append(recStack, <<p, FALSE>>);
goToLeft:                         p := nodeArr[p].left;
                                  goto checkLeft;
                              };

saveAnswer2:                  if (~nodeArr[p].deleted) {
saveAnswer:                       answer := Append(answer, nodeArr[p].value);
                              };                   

checkRight:                   if (nodeArr[p].hasRight) {
                                  recStack := Append(recStack, <<p, TRUE>>);
goToRight:                        p := nodeArr[p].right;
                                  goto checkLeft;
                              };
                              
checkRecSize:                 if (Len(recStack) = 0) {
                                  goto startUpdateCur;
                              };
goToRet:                      tmp := Tail(recStack);
                              recStack := SubSeq(recStack, 0, Len(recStack - 1));
                              
                              p := tmp[0];
                              if (tmp[1]) {
                                  goto checkRecSize;
                              } else {
                                  goto saveAnswer;
                              };
\*______________________________-___________________________________                         













startUpdateCur:               AtomicInc(current);
                              split := Len(answer) \div 2;
                              













\*______________________________smart_fill_val____________________________                        
                              p := gNodeArr[current].root;
                              l := 0;
                              r := split - 1;
                              recStack := <<>>;
                              
                              
startFillVal:                 if (r < l \/ l = split) {
                                  goto checkRecSize2;
                              };
                              
getMid:                       mid := (l + r) \div 2;
                              
checkLeft2:                  if (nodeArr[p].hasLeft) {
                                  recStack := Append(recStack, <<p, FALSE>>);
goToLeft2       :                 p := nodeArr[p].left;
                                  goto startFillVal;
                              };

updateValue:                  nodeArr[p].value := array[mid];
saveAnswerHasValue7:          nodeArr[p].hasValue := TRUE;

checkRight2:                  if (nodeArr[p].hasRight) {
                                  recStack := Append(recStack, <<p, TRUE>>);
goToRight2:                       p := nodeArr[p].right;
                                  goto startFillVal;
                              };
                              
checkRecSize2:                if (Len(recStack) = 0) {
                                  goto createSibling;
                              };
                              
getRetValue:                  tmp := Tail(recStack);
                              recStack := SubSeq(recStack, 0, Len(recStack) - 1);
                              
                              p := tmp[0];
                              if (tmp[1]) {
                                  goto checkRecSize2;
                              } else {
                                  goto updateValue;
                              };
                              
\*_________________________________-_________________________________   













createSibling:                call initLeaf();
setSibling:                   sibling := Len(gNodeArr);

                              
startUpdateSibling2:          AtomicInc(sibling);
                              
updateSiblingHighKey:         gNodeArr[sibling].highKey := gNodeArr[current].highKey;
updateSibling:                gNodeArr[sibling].sibling := gNodeArr[sibling].sibling;
        
updateCurrentHighKey:         gNodeArr[current].highKey := answer[split];
updateCurrentSibling:         gNodeArr[current].sibling := sibling;
        
finishUpdateCurrent4:         AtomicInc(current);
        
        
        
        
        
        
        
        
        
        
        
        
        

\*______________________________smart_fill_val____________________________                        
                              p := gNodeArr[sibling].root;
                              l := split;
                              r := Len(answer) - 1;
                              recStack := <<>>;
                              
startFillVal2:                if (r < l \/ l = Len(answer)) {
                                  goto checkRecSize3;
                              };
                              
getMid2:                      mid := (l + r) \div 2;
                              
checkLeft3:                   if (nodeArr[p].hasLeft) {
                                  recStack := Append(recStack, <<p, l, r, FALSE>>);
goToLeft3:                        p := nodeArr[p].left;
                                  r := mid - 1;
                                  goto startFillVal2;
                              };

saveAnswer3:                  nodeArr[p].value := array[mid];
saveAnswerHasValue2:          nodeArr[p].hasValue := TRUE;

checkRight3:                  if (nodeArr[p].hasRight) {
                                  recStack := Append(recStack, <<p, l, r, TRUE>>);
goToRight3:                       p := nodeArr[p].right;
                                  l := mid + 1;
                                  goto startFillVal2;
                              };
                              
checkRecSize3:                if (Len(recStack) = 0) {
                                  goto finishUpdateSibling2;
                              };
                              
goToRet3:                     tmp := Tail(recStack);
                              recStack := SubSeq(recStack, 0, Len(recStack) - 1);
                              
                              p := tmp[0];
                              l := tmp[1];
                              r := tmp[2];
                              if (tmp[3]) {
                                  goto checkRecSize3;
                              } else {
                                  goto saveAnswer2;
                              };
\*_________________________________-_________________________________ 


















finishUpdateSibling2:          AtomicInc(sibling);
        
updateCurrentCount:           gNodeArr[current].countNode := split;
updateSiblingCount:           gNodeArr[sibling].countNode := Len(answer) - split;
        
                              keyR := answer[split];
                              keyL := answer[0];
                              
checkCurrentHighKey:          if (key >= gNodeArr[current].highKey) {
insertLeaf:                       current := gNodeArr[current].sibling;
                                  call doInsertLeaf(current, key);
                              } else {
insertLeaf2:                      call doInsertLeaf(current, key);
                              }
                          } else {
insertLeaf3:                  call doInsertLeaf(current, key);
releaseGNodelock2:            gNodeArr[current].lock := TRUE;

rerturnInsert2:               insertRes[self] := TRUE;
                              return
                          }
                      } else {
                          if (gNodeArr[current].countNode >= SplitSize) {
                              oldSibling := sibling;
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
\*________________________________________fill-buf-lo______________________________________

startFillBufLo:               p := gNodeArr[current].root;
                              answer := <<>>;
                              
fboCheckLeftRight:            if (nodeArr[p].hasLeft /\ nodeArr[p].hasRight) {
fboGetleft:                       l := nodeArr[p].left;
fboGetRight:                      r := nodeArr[p].right;           

fboCheckLeftRight2:               recStack := Append(recStack, <<p, FALSE>>);
                                  p := l;
                                  goto fboCheckLeftRight;
                                  
fboCheckLeftRight3:               recStack := Append(recStack, <<p, TRUE>>);
                                  p := r;
                                  goto fboCheckLeftRight;                                            
                              } else {
fboIncValue:                      if (~nodeArr[p].deleted) {
                                      answer := Append(answer, nodeArr[p].value);
                                  }
                              };
                              
fboCheckRetSize:              if (Len(recStack) = 0) {
                                  goto copyLinks;
                              };
                              
fboGoToRet3:                  tmp := Tail(recStack);
                              recStack := SubSeq(recStack, 0, Len(recStack) - 1);
                              
                              p := tmp[0];
                              if (tmp[1]) {
                                  goto fboCheckRetSize; 
                              } else {
                                  goto fboCheckLeftRight3;
                              };

\*_________________________________________________________________________________________

copyLinks:                    links := gNodeArr[current].links;

startUpdateCurrent:           AtomicInc(current);

                              split := Len(answer) \div 2;
                              mid := split \div 2;
                              
insertValues1:                gNodeArr[current].root := answer[mid];
insertValues2:                gNodeArr[current].links[0] := links[mid];

callCurrentFill1:             call smartFillValParentLo(0, mid - 1, current, answer, split, links);
callCurrentFill2:             call smartFillValParentLo(mid + 1, split, current, answer, split, links);

createSibling2:               call initLeaf();
getSibling:                   sibling := Len(gNodeArr);
                              
startUpdateSibling:           AtomicInc(sibling);

updateSiblingValues1:         gNodeArr[sibling].highKey := gNodeArr[current].highKey;
updateSiblingValues2:         gNodeArr[sibling].sibling := gNodeArr[current].sibling;

updateCurrentValues1:         gNodeArr[current].highKey := answer[split];
updateCurrentValues2:         gNodeArr[current].sibling := sibling;

finishUpdateCurrent:          AtomicInc(current);

                              mid := split \div 2 + split;
                              
insertValues3:                gNodeArr[sibling].root := answer[mid];
insertValues4:                gNodeArr[sibling].links[0] := links[mid];                              

callCurrentFill3:             call smartFillValParentLo(split, mid - 1, sibling, answer, Len(answer), links);
callCurrentFill4:             call smartFillValParentLo(mid + 1, Len(answer), sibling, answer, Len(answer), links);

finishUpdateSibling:          AtomicInc(sibling);

updateCountNodes1:            gNodeArr[current].countNode := split * 2;
updateCountNodes2:            gNodeArr[sibling].countNode := (Len(answer) - split) * 2;

                              keyRNext := answer[split];
                              keyLNext := answer[0];

insertNodeIf:                 if (keyR >= gNodeArr[current].highKey) {
doInsertNode1:                    call doInsertNode(gNodeArr[current].sibling, oldSibling, keyL, keyR, answer, links);
                              } else {
doInsertNode2:                    call doInsertNode(current, oldSibling, keyL, keyR, answer, links);
                              };
                              
calcNextKeys:                 keyR := keyRNext;
                              keyL := keyLNext;
                          } else {
doInsertNode3:                call doInsertNode(current, sibling, keyL, keyR, answer, links);
releaseCurrentLock:           gNodeArr[current].lock := TRUE;
insertRes:                    insertRes[self] := TRUE;
                              return;                              
                          }
                      };
                      
                      
rootRebalance:        if (Len(stackData) = 0) {
initNewRoot:              call initLeaf();
initNewRoot2:             parent := Len(gNodeArr);        
                      
setRootValue:             nodeArr[gNodeArr[parent].root].value := keyR;
setRootHasValue:          nodeArr[gNodeArr[parent].root].hasValue := TRUE;
setRootLinks:             gNodeArr[parent].links[0] := current;

getLeftFromRoot:          tempL := nodeArr[gNodeArr[parent].root].left;
getRightFromRoot:         tempR := nodeArr[gNodeArr[parent].root].right;

setTempLValue:            nodeArr[tempL].value := keyL;
setTempRValue:            nodeArr[tempR].value := keyR;

setSecondChild:           gNodeArr[parent].links[shiftL(1, maxDepth - 2)] := sibling;

setParentCountNode:       gNodeArr[parent].countNode := 3;
setNewRoot:               root := parent;

releaseCurrentLock2:      gNodeArr[current].lock := TRUE;

setInsertResult:          insertRes[self] := TRUE;
                          return;
                      } else {
setOldCurrent:            oldCurrent := current;

                          current := Tail(stackData);
                          stackData := SubSeq(stackData, 0, Len(stackData) - 1);   
                      
releaseCurrentLock3:      await gNodeArr[current].lock;
                          gNodeArr[current].lock := FALSE;






\*______________________________move right_________________________
startMoveRight2:          while (gNodeArr[t].hasSibling /\ gNodeArr[t].highKey > 0 /\ gNodeArr[t].highKey <= keyR) {
saveSibling2:                 current := gNodeArr[t].sibling;
getNewLock2:                  await gNodeArr[current].lock;
                              gNodeArr[current].lock := FALSE;
                      
releaseOldLock2:              gNodeArr[t].lock := TRUE;
                              t := current;
                          };
                  
                          current := t;
\*______________________________-___________________________________





                  


relOldCurLock:            gNodeArr[oldCurrent].lock := TRUE;    
                      }
                  };
    }
    
    procedure get(key) variables startValue, depth, bits {
checkRoot:        if (~hasRoot) {
                      resultsForGet[self] := <<-1, key>>;
                      return;
                  };
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
\*___________________________smart_btree_search_lo_______________________________________

startSearchGNode: startValue := root;

sgnStartCycle:    while (gNodeArr[startValue].isLeaf) {
getStartRoot:         p := gNodeArr[startValue].root;
                      
                      bits := 1;
                      depth := 0;
                      
checkNodeCycle:       while (p.hasValue) {
                          depth := depth + 1;
                          bits := bits * 2;
                          
checkKeyValue:            if (key < nodeArr[p].value) {
setLeftP:                     p := nodeArr[p].left;
                          } else {
setRightP:                    p := nodeArr[p].right;
                              bits := bits + 1;                      
                          }
                      };
                      
prepareBits:          bits := bits \div 2;
prepareBits2:         bits := shiftL(bits, GNodeDepth - depth);
                      
checkHighKey2:        if (gNodeArr[startValue].hasSibling /\ gNodeArr[startValue].highKey > 0 /\ gNodeArr[startValue].highKey <= key) {
setSibling:               startValue := gNodeArr[startValue].sibling;                          
                      } else {
                          if (gNodeArr[startValue].hasLinks[bits]) {
                              startValue := gNodeArr[startValue].links[bits];
                          } else {
                              goto startItSearchLo;
                          }
                      }
                  };
                  

\*_______________________________________________________________________________________        













\*___________________________________smart_it_search_lo__________________________________

startItSearchLo:  p := gNodeArr[startValue].root;
                  lastNode := p;

cycleItSearchLo:  while (nodeArr[p].hasValue) {
saveLastNode:         lastNode := p;

smislCheckValue:      if (key < nodeArr[p].value) {
                          if (~nodeArr[p].hasLeft) {
                              goto returnGet;
                          };
smislSetLeft:             p := nodeArr[p].left;
                      } else {
                          if (~nodeArr[p].hasRight) {
                              goto returnGet;
                          };
smislSetRight:            p := nodeArr[p].right;
                      }
                  };

\*________________________________________________________________________________________








returnGet:        resultsForGet[self] := <<nodeArr[lastNode].value, key>>;
    }
    
    procedure delete(key) variables start, p, bits, depth {
                
startDelete:      if (~hasRoot) {
                      deleteRes[self] := 0;
returnDel:            return;
                  };
                  
getStart:         start := root;














\*_____________________________smart-scannode-lo__________________________________________

sslStartCycle:    while (gNodeArr[start].isLeaf) {
getStartRoot:         p := gNodeArr[start].root;
                      bits := 1;
                      depth := 0;
                      
startInnerCycle:      while (nodeArr[p].hasValue) {
                          depth := depth + 1;
                          bits := bits * 2;
                          
sslCheckKey:              if (key < nodeArr[p].value) {
sslSetLeft:                   p := nodeArr[p].left;
                          } else {
sslSetRight:                  p := nodeArr[p].right;
                              bits := bits + 1;                          
                          }
                      };
                      
sslPrepareBits:       bits := bits \div 2;
sslPrepareBits2:      bits := shiftL(bits, GNodeDepth - depth);

sslCheckGNode:        if (gNodeArr[start].hasSibling /\ gNodeArr[start].highKey <= key) {
setStartSibling:          start := gNodeArr[start].sibling;
                      } else {
sslCheckLinks:            if (gNodeArr[start].hasLinks[bits]) {
sslGoToLinks:                 start := gNodeArr[start].links[bits]; 
                          } else {
                              goto lockGettedGNode;
                          }
                      }
                  };

\*________________________________________________________________________________________











lockGettedGNode:  await gNodeArr[start].lock;
                  gNodeArr[start].lock := FALSE;
                  t := start;
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
\*______________________________move right_________________________
startMoveRight3:  while (gNodeArr[t].hasSibling /\ gNodeArr[t].highKey > 0 /\ gNodeArr[t].highKey <= key) {
saveSibling3:         start := gNodeArr[t].sibling;
getNewLock3:          await gNodeArr[start].lock;
                      gNodeArr[start].lock := FALSE;
                      
releaseOldLock3:      gNodeArr[t].lock := TRUE;
                      t := start;
                  };
                  
                  start := t;
\*______________________________-___________________________________













\*___________________________________smart_it_search_lo__________________________________

startItSearchLo2: p := gNodeArr[start].root;
                  lastNode := p;

cycleItSearchLo2: while (nodeArr[p].hasValue) {
saveLastNode2:        lastNode := p;

smislCheckValue2:     if (key < nodeArr[p].value) {
                          if (~nodeArr[p].hasLeft) {
                              goto deleteValue;
                          };
smislSetLeft2:            p := nodeArr[p].left;
                      } else {
                          if (~nodeArr[p].hasRight) {
                              goto deleteValue;
                          };
smislSetRight2:           p := nodeArr[p].right;
                      }
                  };

\*________________________________________________________________________________________







deleteValue:      gNodeArr[lastNode].delete := TRUE;
releaseStartLock2:gNodeArr[start].lock := FALSE;
                  deleteRes[self] := 1;                  
    }
    
    process (procR \in R) variables states {
readerStart:     processReaderState[self] := "started";
startGet:        with (v \in VALUES) {
\*                     states := processWriterState;
                     call get(v);
                 };
                 
\*readerFinish:    assert getResultChecker(values, resultsForGet[self], states, processWriterState);
readerFinish:      processReaderState[self] := "finished";
    }

    process (procW \in P) {
writerStart:     processWriterState[self] := "started";
startInsert:     either {
                     with (v \in VALUES) {
\*                         values[v] := TRUE;
                         call insert(v);                       
                     } 
                 } or {
                     with (v \in VALUES) {
\*                         values[v] := FALSE;
                         call delete(v);                         
                     }
                 };
writerFinish:    processWriterState[self] := "finished";
    }
    
} *)
\* BEGIN TRANSLATION (chksum(pcal) = "8b6f7aed" /\ chksum(tla) = "d8054c5f")
\* Label initLeaf of procedure initLeaf at line 268 col 14 changed to initLeaf_
\* Label updateCountNode of procedure doInsertLeaf at line 319 col 39 changed to updateCountNode_
\* Label updateCountNode2 of procedure doInsertLeaf at line 320 col 39 changed to updateCountNode2_
\* Label saveAnswerHasValue7 of procedure doInsertLeaf at line 483 col 51 changed to saveAnswerHasValue7_
\* Label startUpdateCurrent of procedure doInsertNode at line 264 col 9 changed to startUpdateCurrent_
\* Label initLeaf of procedure insert at line 929 col 27 changed to initLeaf_i
\* Label setSibling of procedure insert at line 1211 col 31 changed to setSibling_
\* Label insertRes of procedure insert at line 1427 col 31 changed to insertRes_
\* Label getStartRoot of procedure get at line 1519 col 23 changed to getStartRoot_
\* Procedure variable tmp of procedure doInsertLeaf at line 296 col 52 changed to tmp_
\* Procedure variable mid of procedure doInsertLeaf at line 296 col 63 changed to mid_
\* Procedure variable p of procedure doInsertLeaf at line 296 col 68 changed to p_
\* Procedure variable recStack of procedure doInsertLeaf at line 296 col 75 changed to recStack_
\* Procedure variable recStack1 of procedure doInsertLeaf at line 296 col 85 changed to recStack1_
\* Procedure variable depth of procedure doInsertLeaf at line 296 col 101 changed to depth_
\* Procedure variable recStack of procedure smartFillValParentLo at line 602 col 77 changed to recStack_s
\* Procedure variable mid of procedure smartFillValParentLo at line 602 col 87 changed to mid_s
\* Procedure variable bits of procedure smartFillValParentLo at line 602 col 114 changed to bits_
\* Procedure variable depth of procedure smartFillValParentLo at line 602 col 120 changed to depth_s
\* Procedure variable p of procedure doInsertNode at line 726 col 82 changed to p_d
\* Procedure variable bits of procedure doInsertNode at line 726 col 99 changed to bits_d
\* Procedure variable depth of procedure doInsertNode at line 726 col 105 changed to depth_d
\* Procedure variable links of procedure insert at line 924 col 95 changed to links_
\* Procedure variable current of procedure insert at line 924 col 114 changed to current_
\* Procedure variable bits of procedure insert at line 924 col 128 changed to bits_i
\* Procedure variable depth of procedure insert at line 924 col 134 changed to depth_i
\* Procedure variable p of procedure insert at line 924 col 141 changed to p_i
\* Procedure variable l of procedure insert at line 924 col 205 changed to l_
\* Procedure variable r of procedure insert at line 924 col 208 changed to r_
\* Procedure variable sibling of procedure insert at line 924 col 216 changed to sibling_
\* Procedure variable keyL of procedure insert at line 924 col 225 changed to keyL_
\* Procedure variable keyR of procedure insert at line 924 col 231 changed to keyR_
\* Procedure variable depth of procedure get at line 1495 col 46 changed to depth_g
\* Procedure variable bits of procedure get at line 1495 col 53 changed to bits_g
\* Parameter current of procedure doInsertLeaf at line 296 col 28 changed to current_d
\* Parameter key of procedure doInsertLeaf at line 296 col 37 changed to key_
\* Parameter buf of procedure smartFillValParentLo at line 602 col 49 changed to buf_
\* Parameter key of procedure insert at line 924 col 22 changed to key_i
\* Parameter key of procedure get at line 1495 col 19 changed to key_g
CONSTANT defaultInitValue
VARIABLES root, hasRoot, globalLock, gNodeArr, nodeArr, insertRes, 
          resultsForGet, deleteRes, processWriterState, processReaderState, 
          values, pc, stack, i, cur, istack, current_d, key_, tmp_, tmp1, 
          mid_, p_, p1, recStack_, recStack1_, ret, depth_, res, calc, 
          maxDepth, l, r, gNode, buf_, rmax, links, recStack_s, mid_s, val, 
          child, lastNode, bits_, depth_s, newBits, movedBits, ln, rn, 
          current, sibling, keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
          bits_d, depth_d, key_i, stackData, keyRNext, oldCurrent, parent, 
          keyLNext, links_, oldSibling, current_, rev, bits_i, depth_i, p_i, 
          temp, scanRes, t, array, split, recStack, answer, tmp, tmp2, l_, r_, 
          mid, sibling_, keyL_, keyR_, recStack1, key_g, startValue, depth_g, 
          bits_g, key, start, p, bits, depth, states

vars == << root, hasRoot, globalLock, gNodeArr, nodeArr, insertRes, 
           resultsForGet, deleteRes, processWriterState, processReaderState, 
           values, pc, stack, i, cur, istack, current_d, key_, tmp_, tmp1, 
           mid_, p_, p1, recStack_, recStack1_, ret, depth_, res, calc, 
           maxDepth, l, r, gNode, buf_, rmax, links, recStack_s, mid_s, val, 
           child, lastNode, bits_, depth_s, newBits, movedBits, ln, rn, 
           current, sibling, keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
           bits_d, depth_d, key_i, stackData, keyRNext, oldCurrent, parent, 
           keyLNext, links_, oldSibling, current_, rev, bits_i, depth_i, p_i, 
           temp, scanRes, t, array, split, recStack, answer, tmp, tmp2, l_, 
           r_, mid, sibling_, keyL_, keyR_, recStack1, key_g, startValue, 
           depth_g, bits_g, key, start, p, bits, depth, states >>

ProcSet == (R) \cup (P)

Init == (* Global variables *)
        /\ root = DefaultRoot
        /\ hasRoot = DefaultHasRoot
        /\ globalLock = TRUE
        /\ gNodeArr = DefaultGNodeArr
        /\ nodeArr = DefaultNodeArr
        /\ insertRes = [s \in P |-> FALSE]
        /\ resultsForGet = [s \in R |-> <<-1, -1>>]
        /\ deleteRes = [s \in P |-> -1]
        /\ processWriterState = [s \in P |-> "not started"]
        /\ processReaderState = [s \in R |-> "not started"]
        /\ values = [s \in VALUES |-> FALSE]
        (* Procedure initLeaf *)
        /\ i = [ self \in ProcSet |-> defaultInitValue]
        /\ cur = [ self \in ProcSet |-> defaultInitValue]
        /\ istack = [ self \in ProcSet |-> defaultInitValue]
        (* Procedure doInsertLeaf *)
        /\ current_d = [ self \in ProcSet |-> defaultInitValue]
        /\ key_ = [ self \in ProcSet |-> defaultInitValue]
        /\ tmp_ = [ self \in ProcSet |-> defaultInitValue]
        /\ tmp1 = [ self \in ProcSet |-> defaultInitValue]
        /\ mid_ = [ self \in ProcSet |-> defaultInitValue]
        /\ p_ = [ self \in ProcSet |-> defaultInitValue]
        /\ p1 = [ self \in ProcSet |-> defaultInitValue]
        /\ recStack_ = [ self \in ProcSet |-> defaultInitValue]
        /\ recStack1_ = [ self \in ProcSet |-> defaultInitValue]
        /\ ret = [ self \in ProcSet |-> defaultInitValue]
        /\ depth_ = [ self \in ProcSet |-> defaultInitValue]
        /\ res = [ self \in ProcSet |-> defaultInitValue]
        /\ calc = [ self \in ProcSet |-> defaultInitValue]
        /\ maxDepth = [ self \in ProcSet |-> defaultInitValue]
        (* Procedure smartFillValParentLo *)
        /\ l = [ self \in ProcSet |-> defaultInitValue]
        /\ r = [ self \in ProcSet |-> defaultInitValue]
        /\ gNode = [ self \in ProcSet |-> defaultInitValue]
        /\ buf_ = [ self \in ProcSet |-> defaultInitValue]
        /\ rmax = [ self \in ProcSet |-> defaultInitValue]
        /\ links = [ self \in ProcSet |-> defaultInitValue]
        /\ recStack_s = [ self \in ProcSet |-> defaultInitValue]
        /\ mid_s = [ self \in ProcSet |-> defaultInitValue]
        /\ val = [ self \in ProcSet |-> defaultInitValue]
        /\ child = [ self \in ProcSet |-> defaultInitValue]
        /\ lastNode = [ self \in ProcSet |-> defaultInitValue]
        /\ bits_ = [ self \in ProcSet |-> defaultInitValue]
        /\ depth_s = [ self \in ProcSet |-> defaultInitValue]
        /\ newBits = [ self \in ProcSet |-> defaultInitValue]
        /\ movedBits = [ self \in ProcSet |-> defaultInitValue]
        /\ ln = [ self \in ProcSet |-> defaultInitValue]
        /\ rn = [ self \in ProcSet |-> defaultInitValue]
        (* Procedure doInsertNode *)
        /\ current = [ self \in ProcSet |-> defaultInitValue]
        /\ sibling = [ self \in ProcSet |-> defaultInitValue]
        /\ keyL = [ self \in ProcSet |-> defaultInitValue]
        /\ keyR = [ self \in ProcSet |-> defaultInitValue]
        /\ buf = [ self \in ProcSet |-> defaultInitValue]
        /\ bufLink = [ self \in ProcSet |-> defaultInitValue]
        /\ p_d = [ self \in ProcSet |-> defaultInitValue]
        /\ tempL = [ self \in ProcSet |-> defaultInitValue]
        /\ tempR = [ self \in ProcSet |-> defaultInitValue]
        /\ bits_d = [ self \in ProcSet |-> defaultInitValue]
        /\ depth_d = [ self \in ProcSet |-> defaultInitValue]
        (* Procedure insert *)
        /\ key_i = [ self \in ProcSet |-> defaultInitValue]
        /\ stackData = [ self \in ProcSet |-> <<>>]
        /\ keyRNext = [ self \in ProcSet |-> defaultInitValue]
        /\ oldCurrent = [ self \in ProcSet |-> defaultInitValue]
        /\ parent = [ self \in ProcSet |-> defaultInitValue]
        /\ keyLNext = [ self \in ProcSet |-> defaultInitValue]
        /\ links_ = [ self \in ProcSet |-> defaultInitValue]
        /\ oldSibling = [ self \in ProcSet |-> defaultInitValue]
        /\ current_ = [ self \in ProcSet |-> defaultInitValue]
        /\ rev = [ self \in ProcSet |-> defaultInitValue]
        /\ bits_i = [ self \in ProcSet |-> defaultInitValue]
        /\ depth_i = [ self \in ProcSet |-> defaultInitValue]
        /\ p_i = [ self \in ProcSet |-> defaultInitValue]
        /\ temp = [ self \in ProcSet |-> defaultInitValue]
        /\ scanRes = [ self \in ProcSet |-> defaultInitValue]
        /\ t = [ self \in ProcSet |-> defaultInitValue]
        /\ array = [ self \in ProcSet |-> defaultInitValue]
        /\ split = [ self \in ProcSet |-> defaultInitValue]
        /\ recStack = [ self \in ProcSet |-> defaultInitValue]
        /\ answer = [ self \in ProcSet |-> defaultInitValue]
        /\ tmp = [ self \in ProcSet |-> defaultInitValue]
        /\ tmp2 = [ self \in ProcSet |-> defaultInitValue]
        /\ l_ = [ self \in ProcSet |-> defaultInitValue]
        /\ r_ = [ self \in ProcSet |-> defaultInitValue]
        /\ mid = [ self \in ProcSet |-> defaultInitValue]
        /\ sibling_ = [ self \in ProcSet |-> defaultInitValue]
        /\ keyL_ = [ self \in ProcSet |-> defaultInitValue]
        /\ keyR_ = [ self \in ProcSet |-> defaultInitValue]
        /\ recStack1 = [ self \in ProcSet |-> defaultInitValue]
        (* Procedure get *)
        /\ key_g = [ self \in ProcSet |-> defaultInitValue]
        /\ startValue = [ self \in ProcSet |-> defaultInitValue]
        /\ depth_g = [ self \in ProcSet |-> defaultInitValue]
        /\ bits_g = [ self \in ProcSet |-> defaultInitValue]
        (* Procedure delete *)
        /\ key = [ self \in ProcSet |-> defaultInitValue]
        /\ start = [ self \in ProcSet |-> defaultInitValue]
        /\ p = [ self \in ProcSet |-> defaultInitValue]
        /\ bits = [ self \in ProcSet |-> defaultInitValue]
        /\ depth = [ self \in ProcSet |-> defaultInitValue]
        (* Process procR *)
        /\ states = [self \in R |-> defaultInitValue]
        /\ stack = [self \in ProcSet |-> << >>]
        /\ pc = [self \in ProcSet |-> CASE self \in R -> "readerStart"
                                        [] self \in P -> "writerStart"]

initLeaf_(self) == /\ pc[self] = "initLeaf_"
                   /\ istack' = [istack EXCEPT ![self] = <<>>]
                   /\ nodeArr' = Append(nodeArr, createNode(key[self]))
                   /\ gNodeArr' = Append(gNodeArr, [isLeaf |-> TRUE, links |-> [x \in 0..GNodeSize |-> 0], hasLinks |-> [x \in 0..GNodeSize |-> FALSE], root |-> Len(nodeArr'), sibling |-> 0, countNode |-> 1, highKey |-> 0, lock |-> TRUE, hasSibling |-> FALSE, rev |-> 0])
                   /\ i' = [i EXCEPT ![self] = 0]
                   /\ pc' = [pc EXCEPT ![self] = "pushStack"]
                   /\ UNCHANGED << root, hasRoot, globalLock, insertRes, 
                                   resultsForGet, deleteRes, 
                                   processWriterState, processReaderState, 
                                   values, stack, cur, current_d, key_, tmp_, 
                                   tmp1, mid_, p_, p1, recStack_, recStack1_, 
                                   ret, depth_, res, calc, maxDepth, l, r, 
                                   gNode, buf_, rmax, links, recStack_s, mid_s, 
                                   val, child, lastNode, bits_, depth_s, 
                                   newBits, movedBits, ln, rn, current, 
                                   sibling, keyL, keyR, buf, bufLink, p_d, 
                                   tempL, tempR, bits_d, depth_d, key_i, 
                                   stackData, keyRNext, oldCurrent, parent, 
                                   keyLNext, links_, oldSibling, current_, rev, 
                                   bits_i, depth_i, p_i, temp, scanRes, t, 
                                   array, split, recStack, answer, tmp, tmp2, 
                                   l_, r_, mid, sibling_, keyL_, keyR_, 
                                   recStack1, key_g, startValue, depth_g, 
                                   bits_g, key, start, p, bits, depth, states >>

pushStack(self) == /\ pc[self] = "pushStack"
                   /\ istack' = [istack EXCEPT ![self] = Append(istack[self], Len(nodeArr))]
                   /\ pc' = [pc EXCEPT ![self] = "startInit"]
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, resultsForGet, 
                                   deleteRes, processWriterState, 
                                   processReaderState, values, stack, i, cur, 
                                   current_d, key_, tmp_, tmp1, mid_, p_, p1, 
                                   recStack_, recStack1_, ret, depth_, res, 
                                   calc, maxDepth, l, r, gNode, buf_, rmax, 
                                   links, recStack_s, mid_s, val, child, 
                                   lastNode, bits_, depth_s, newBits, 
                                   movedBits, ln, rn, current, sibling, keyL, 
                                   keyR, buf, bufLink, p_d, tempL, tempR, 
                                   bits_d, depth_d, key_i, stackData, keyRNext, 
                                   oldCurrent, parent, keyLNext, links_, 
                                   oldSibling, current_, rev, bits_i, depth_i, 
                                   p_i, temp, scanRes, t, array, split, 
                                   recStack, answer, tmp, tmp2, l_, r_, mid, 
                                   sibling_, keyL_, keyR_, recStack1, key_g, 
                                   startValue, depth_g, bits_g, key, start, p, 
                                   bits, depth, states >>

startInit(self) == /\ pc[self] = "startInit"
                   /\ IF i[self] <= GNodeSize
                         THEN /\ cur' = [cur EXCEPT ![self] = Head(istack[self])]
                              /\ istack' = [istack EXCEPT ![self] = SubSeq(istack[self], 1, Len(istack[self]))]
                              /\ pc' = [pc EXCEPT ![self] = "addLeft"]
                         ELSE /\ pc' = [pc EXCEPT ![self] = "Error"]
                              /\ UNCHANGED << cur, istack >>
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, resultsForGet, 
                                   deleteRes, processWriterState, 
                                   processReaderState, values, stack, i, 
                                   current_d, key_, tmp_, tmp1, mid_, p_, p1, 
                                   recStack_, recStack1_, ret, depth_, res, 
                                   calc, maxDepth, l, r, gNode, buf_, rmax, 
                                   links, recStack_s, mid_s, val, child, 
                                   lastNode, bits_, depth_s, newBits, 
                                   movedBits, ln, rn, current, sibling, keyL, 
                                   keyR, buf, bufLink, p_d, tempL, tempR, 
                                   bits_d, depth_d, key_i, stackData, keyRNext, 
                                   oldCurrent, parent, keyLNext, links_, 
                                   oldSibling, current_, rev, bits_i, depth_i, 
                                   p_i, temp, scanRes, t, array, split, 
                                   recStack, answer, tmp, tmp2, l_, r_, mid, 
                                   sibling_, keyL_, keyR_, recStack1, key_g, 
                                   startValue, depth_g, bits_g, key, start, p, 
                                   bits, depth, states >>

addLeft(self) == /\ pc[self] = "addLeft"
                 /\ nodeArr' = Append(nodeArr, createEmptyNode)
                 /\ pc' = [pc EXCEPT ![self] = "addleft2"]
                 /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                 insertRes, resultsForGet, deleteRes, 
                                 processWriterState, processReaderState, 
                                 values, stack, i, cur, istack, current_d, 
                                 key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                 recStack1_, ret, depth_, res, calc, maxDepth, 
                                 l, r, gNode, buf_, rmax, links, recStack_s, 
                                 mid_s, val, child, lastNode, bits_, depth_s, 
                                 newBits, movedBits, ln, rn, current, sibling, 
                                 keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                 bits_d, depth_d, key_i, stackData, keyRNext, 
                                 oldCurrent, parent, keyLNext, links_, 
                                 oldSibling, current_, rev, bits_i, depth_i, 
                                 p_i, temp, scanRes, t, array, split, recStack, 
                                 answer, tmp, tmp2, l_, r_, mid, sibling_, 
                                 keyL_, keyR_, recStack1, key_g, startValue, 
                                 depth_g, bits_g, key, start, p, bits, depth, 
                                 states >>

addleft2(self) == /\ pc[self] = "addleft2"
                  /\ nodeArr' = [nodeArr EXCEPT ![cur[self]].left = Len(nodeArr)]
                  /\ pc' = [pc EXCEPT ![self] = "addleft3"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  p_i, temp, scanRes, t, array, split, 
                                  recStack, answer, tmp, tmp2, l_, r_, mid, 
                                  sibling_, keyL_, keyR_, recStack1, key_g, 
                                  startValue, depth_g, bits_g, key, start, p, 
                                  bits, depth, states >>

addleft3(self) == /\ pc[self] = "addleft3"
                  /\ nodeArr' = [nodeArr EXCEPT ![cur[self]].hasLeft = TRUE]
                  /\ istack' = [istack EXCEPT ![self] = Append(istack[self], Len(nodeArr'))]
                  /\ i' = [i EXCEPT ![self] = i[self] + 1]
                  /\ pc' = [pc EXCEPT ![self] = "addRight"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, cur, current_d, key_, tmp_, 
                                  tmp1, mid_, p_, p1, recStack_, recStack1_, 
                                  ret, depth_, res, calc, maxDepth, l, r, 
                                  gNode, buf_, rmax, links, recStack_s, mid_s, 
                                  val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  p_i, temp, scanRes, t, array, split, 
                                  recStack, answer, tmp, tmp2, l_, r_, mid, 
                                  sibling_, keyL_, keyR_, recStack1, key_g, 
                                  startValue, depth_g, bits_g, key, start, p, 
                                  bits, depth, states >>

addRight(self) == /\ pc[self] = "addRight"
                  /\ nodeArr' = Append(nodeArr, createEmptyNode)
                  /\ pc' = [pc EXCEPT ![self] = "addRight2"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  p_i, temp, scanRes, t, array, split, 
                                  recStack, answer, tmp, tmp2, l_, r_, mid, 
                                  sibling_, keyL_, keyR_, recStack1, key_g, 
                                  startValue, depth_g, bits_g, key, start, p, 
                                  bits, depth, states >>

addRight2(self) == /\ pc[self] = "addRight2"
                   /\ nodeArr' = [nodeArr EXCEPT ![cur[self]].right = Len(nodeArr)]
                   /\ pc' = [pc EXCEPT ![self] = "addright3"]
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   insertRes, resultsForGet, deleteRes, 
                                   processWriterState, processReaderState, 
                                   values, stack, i, cur, istack, current_d, 
                                   key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                   recStack1_, ret, depth_, res, calc, 
                                   maxDepth, l, r, gNode, buf_, rmax, links, 
                                   recStack_s, mid_s, val, child, lastNode, 
                                   bits_, depth_s, newBits, movedBits, ln, rn, 
                                   current, sibling, keyL, keyR, buf, bufLink, 
                                   p_d, tempL, tempR, bits_d, depth_d, key_i, 
                                   stackData, keyRNext, oldCurrent, parent, 
                                   keyLNext, links_, oldSibling, current_, rev, 
                                   bits_i, depth_i, p_i, temp, scanRes, t, 
                                   array, split, recStack, answer, tmp, tmp2, 
                                   l_, r_, mid, sibling_, keyL_, keyR_, 
                                   recStack1, key_g, startValue, depth_g, 
                                   bits_g, key, start, p, bits, depth, states >>

addright3(self) == /\ pc[self] = "addright3"
                   /\ nodeArr' = [nodeArr EXCEPT ![cur[self]].hasRight = TRUE]
                   /\ istack' = [istack EXCEPT ![self] = Append(istack[self], Len(nodeArr'))]
                   /\ i' = [i EXCEPT ![self] = i[self] + 1]
                   /\ pc' = [pc EXCEPT ![self] = "startInit"]
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   insertRes, resultsForGet, deleteRes, 
                                   processWriterState, processReaderState, 
                                   values, stack, cur, current_d, key_, tmp_, 
                                   tmp1, mid_, p_, p1, recStack_, recStack1_, 
                                   ret, depth_, res, calc, maxDepth, l, r, 
                                   gNode, buf_, rmax, links, recStack_s, mid_s, 
                                   val, child, lastNode, bits_, depth_s, 
                                   newBits, movedBits, ln, rn, current, 
                                   sibling, keyL, keyR, buf, bufLink, p_d, 
                                   tempL, tempR, bits_d, depth_d, key_i, 
                                   stackData, keyRNext, oldCurrent, parent, 
                                   keyLNext, links_, oldSibling, current_, rev, 
                                   bits_i, depth_i, p_i, temp, scanRes, t, 
                                   array, split, recStack, answer, tmp, tmp2, 
                                   l_, r_, mid, sibling_, keyL_, keyR_, 
                                   recStack1, key_g, startValue, depth_g, 
                                   bits_g, key, start, p, bits, depth, states >>

initLeaf(self) == initLeaf_(self) \/ pushStack(self) \/ startInit(self)
                     \/ addLeft(self) \/ addleft2(self) \/ addleft3(self)
                     \/ addRight(self) \/ addRight2(self)
                     \/ addright3(self)

getCurrentRoot(self) == /\ pc[self] = "getCurrentRoot"
                        /\ p_' = [p_ EXCEPT ![self] = gNodeArr[current_d[self]].root]
                        /\ depth_' = [depth_ EXCEPT ![self] = 0]
                        /\ pc' = [pc EXCEPT ![self] = "checkKeyAlreadyExists"]
                        /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                        nodeArr, insertRes, resultsForGet, 
                                        deleteRes, processWriterState, 
                                        processReaderState, values, stack, i, 
                                        cur, istack, current_d, key_, tmp_, 
                                        tmp1, mid_, p1, recStack_, recStack1_, 
                                        ret, res, calc, maxDepth, l, r, gNode, 
                                        buf_, rmax, links, recStack_s, mid_s, 
                                        val, child, lastNode, bits_, depth_s, 
                                        newBits, movedBits, ln, rn, current, 
                                        sibling, keyL, keyR, buf, bufLink, p_d, 
                                        tempL, tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        current_, rev, bits_i, depth_i, p_i, 
                                        temp, scanRes, t, array, split, 
                                        recStack, answer, tmp, tmp2, l_, r_, 
                                        mid, sibling_, keyL_, keyR_, recStack1, 
                                        key_g, startValue, depth_g, bits_g, 
                                        key, start, p, bits, depth, states >>

checkKeyAlreadyExists(self) == /\ pc[self] = "checkKeyAlreadyExists"
                               /\ IF key_[self] = nodeArr[p_[self]].value
                                     THEN /\ ret' = [ret EXCEPT ![self] = 0]
                                          /\ pc' = [pc EXCEPT ![self] = "finishInsertRecInsert"]
                                     ELSE /\ pc' = [pc EXCEPT ![self] = "setZeroRet"]
                                          /\ ret' = ret
                               /\ UNCHANGED << root, hasRoot, globalLock, 
                                               gNodeArr, nodeArr, insertRes, 
                                               resultsForGet, deleteRes, 
                                               processWriterState, 
                                               processReaderState, values, 
                                               stack, i, cur, istack, 
                                               current_d, key_, tmp_, tmp1, 
                                               mid_, p_, p1, recStack_, 
                                               recStack1_, depth_, res, calc, 
                                               maxDepth, l, r, gNode, buf_, 
                                               rmax, links, recStack_s, mid_s, 
                                               val, child, lastNode, bits_, 
                                               depth_s, newBits, movedBits, ln, 
                                               rn, current, sibling, keyL, 
                                               keyR, buf, bufLink, p_d, tempL, 
                                               tempR, bits_d, depth_d, key_i, 
                                               stackData, keyRNext, oldCurrent, 
                                               parent, keyLNext, links_, 
                                               oldSibling, current_, rev, 
                                               bits_i, depth_i, p_i, temp, 
                                               scanRes, t, array, split, 
                                               recStack, answer, tmp, tmp2, l_, 
                                               r_, mid, sibling_, keyL_, keyR_, 
                                               recStack1, key_g, startValue, 
                                               depth_g, bits_g, key, start, p, 
                                               bits, depth, states >>

setZeroRet(self) == /\ pc[self] = "setZeroRet"
                    /\ ret' = [ret EXCEPT ![self] = 0]
                    /\ pc' = [pc EXCEPT ![self] = "checkLeft4"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, p1, recStack_, recStack1_, depth_, res, 
                                    calc, maxDepth, l, r, gNode, buf_, rmax, 
                                    links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, recStack, answer, tmp, tmp2, l_, r_, 
                                    mid, sibling_, keyL_, keyR_, recStack1, 
                                    key_g, startValue, depth_g, bits_g, key, 
                                    start, p, bits, depth, states >>

checkLeft4(self) == /\ pc[self] = "checkLeft4"
                    /\ IF key_[self] < nodeArr[p_[self]].value
                          THEN /\ pc' = [pc EXCEPT ![self] = "checkLeftExists"]
                          ELSE /\ pc' = [pc EXCEPT ![self] = "checkRight8"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, p1, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, recStack, answer, tmp, tmp2, l_, r_, 
                                    mid, sibling_, keyL_, keyR_, recStack1, 
                                    key_g, startValue, depth_g, bits_g, key, 
                                    start, p, bits, depth, states >>

checkLeftExists(self) == /\ pc[self] = "checkLeftExists"
                         /\ IF nodeArr[p_[self]].hasLeft
                               THEN /\ recStack1_' = [recStack1_ EXCEPT ![self] = Append(recStack1_[self], <<p_[self], depth_[self], ret[self], 0>>)]
                                    /\ pc' = [pc EXCEPT ![self] = "getLeftInInsert"]
                               ELSE /\ pc' = [pc EXCEPT ![self] = "createNewNode"]
                                    /\ UNCHANGED recStack1_
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, ret, 
                                         depth_, res, calc, maxDepth, l, r, 
                                         gNode, buf_, rmax, links, recStack_s, 
                                         mid_s, val, child, lastNode, bits_, 
                                         depth_s, newBits, movedBits, ln, rn, 
                                         current, sibling, keyL, keyR, buf, 
                                         bufLink, p_d, tempL, tempR, bits_d, 
                                         depth_d, key_i, stackData, keyRNext, 
                                         oldCurrent, parent, keyLNext, links_, 
                                         oldSibling, current_, rev, bits_i, 
                                         depth_i, p_i, temp, scanRes, t, array, 
                                         split, recStack, answer, tmp, tmp2, 
                                         l_, r_, mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, start, p, bits, depth, 
                                         states >>

getLeftInInsert(self) == /\ pc[self] = "getLeftInInsert"
                         /\ p_' = [p_ EXCEPT ![self] = nodeArr[p_[self]].left]
                         /\ pc' = [pc EXCEPT ![self] = "checkKeyAlreadyExists"]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p1, recStack_, recStack1_, 
                                         ret, depth_, res, calc, maxDepth, l, 
                                         r, gNode, buf_, rmax, links, 
                                         recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, answer, tmp, tmp2, l_, r_, 
                                         mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, start, p, bits, depth, 
                                         states >>

createNewNode(self) == /\ pc[self] = "createNewNode"
                       /\ nodeArr' = Append(nodeArr, createNode(key_[self]))
                       /\ pc' = [pc EXCEPT ![self] = "insertInLeft"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       insertRes, resultsForGet, deleteRes, 
                                       processWriterState, processReaderState, 
                                       values, stack, i, cur, istack, 
                                       current_d, key_, tmp_, tmp1, mid_, p_, 
                                       p1, recStack_, recStack1_, ret, depth_, 
                                       res, calc, maxDepth, l, r, gNode, buf_, 
                                       rmax, links, recStack_s, mid_s, val, 
                                       child, lastNode, bits_, depth_s, 
                                       newBits, movedBits, ln, rn, current, 
                                       sibling, keyL, keyR, buf, bufLink, p_d, 
                                       tempL, tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

insertInLeft(self) == /\ pc[self] = "insertInLeft"
                      /\ nodeArr' = [nodeArr EXCEPT ![p_[self]].left = Len(nodeArr)]
                      /\ pc' = [pc EXCEPT ![self] = "insertHasLeft"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      insertRes, resultsForGet, deleteRes, 
                                      processWriterState, processReaderState, 
                                      values, stack, i, cur, istack, current_d, 
                                      key_, tmp_, tmp1, mid_, p_, p1, 
                                      recStack_, recStack1_, ret, depth_, res, 
                                      calc, maxDepth, l, r, gNode, buf_, rmax, 
                                      links, recStack_s, mid_s, val, child, 
                                      lastNode, bits_, depth_s, newBits, 
                                      movedBits, ln, rn, current, sibling, 
                                      keyL, keyR, buf, bufLink, p_d, tempL, 
                                      tempR, bits_d, depth_d, key_i, stackData, 
                                      keyRNext, oldCurrent, parent, keyLNext, 
                                      links_, oldSibling, current_, rev, 
                                      bits_i, depth_i, p_i, temp, scanRes, t, 
                                      array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

insertHasLeft(self) == /\ pc[self] = "insertHasLeft"
                       /\ nodeArr' = [nodeArr EXCEPT ![p_[self]].hasLeft = TRUE]
                       /\ pc' = [pc EXCEPT ![self] = "updateCountNode_"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       insertRes, resultsForGet, deleteRes, 
                                       processWriterState, processReaderState, 
                                       values, stack, i, cur, istack, 
                                       current_d, key_, tmp_, tmp1, mid_, p_, 
                                       p1, recStack_, recStack1_, ret, depth_, 
                                       res, calc, maxDepth, l, r, gNode, buf_, 
                                       rmax, links, recStack_s, mid_s, val, 
                                       child, lastNode, bits_, depth_s, 
                                       newBits, movedBits, ln, rn, current, 
                                       sibling, keyL, keyR, buf, bufLink, p_d, 
                                       tempL, tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

updateCountNode_(self) == /\ pc[self] = "updateCountNode_"
                          /\ tmp1' = [tmp1 EXCEPT ![self] = gNodeArr[tmp_[self]].countNode]
                          /\ pc' = [pc EXCEPT ![self] = "updateCountNode2_"]
                          /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                          nodeArr, insertRes, resultsForGet, 
                                          deleteRes, processWriterState, 
                                          processReaderState, values, stack, i, 
                                          cur, istack, current_d, key_, tmp_, 
                                          mid_, p_, p1, recStack_, recStack1_, 
                                          ret, depth_, res, calc, maxDepth, l, 
                                          r, gNode, buf_, rmax, links, 
                                          recStack_s, mid_s, val, child, 
                                          lastNode, bits_, depth_s, newBits, 
                                          movedBits, ln, rn, current, sibling, 
                                          keyL, keyR, buf, bufLink, p_d, tempL, 
                                          tempR, bits_d, depth_d, key_i, 
                                          stackData, keyRNext, oldCurrent, 
                                          parent, keyLNext, links_, oldSibling, 
                                          current_, rev, bits_i, depth_i, p_i, 
                                          temp, scanRes, t, array, split, 
                                          recStack, answer, tmp, tmp2, l_, r_, 
                                          mid, sibling_, keyL_, keyR_, 
                                          recStack1, key_g, startValue, 
                                          depth_g, bits_g, key, start, p, bits, 
                                          depth, states >>

updateCountNode2_(self) == /\ pc[self] = "updateCountNode2_"
                           /\ tmp1' = [tmp1 EXCEPT ![self] = tmp1[self] + 1]
                           /\ ret' = [ret EXCEPT ![self] = 0]
                           /\ pc' = [pc EXCEPT ![self] = "finishInsertRecInsert"]
                           /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                           nodeArr, insertRes, resultsForGet, 
                                           deleteRes, processWriterState, 
                                           processReaderState, values, stack, 
                                           i, cur, istack, current_d, key_, 
                                           tmp_, mid_, p_, p1, recStack_, 
                                           recStack1_, depth_, res, calc, 
                                           maxDepth, l, r, gNode, buf_, rmax, 
                                           links, recStack_s, mid_s, val, 
                                           child, lastNode, bits_, depth_s, 
                                           newBits, movedBits, ln, rn, current, 
                                           sibling, keyL, keyR, buf, bufLink, 
                                           p_d, tempL, tempR, bits_d, depth_d, 
                                           key_i, stackData, keyRNext, 
                                           oldCurrent, parent, keyLNext, 
                                           links_, oldSibling, current_, rev, 
                                           bits_i, depth_i, p_i, temp, scanRes, 
                                           t, array, split, recStack, answer, 
                                           tmp, tmp2, l_, r_, mid, sibling_, 
                                           keyL_, keyR_, recStack1, key_g, 
                                           startValue, depth_g, bits_g, key, 
                                           start, p, bits, depth, states >>

checkRet(self) == /\ pc[self] = "checkRet"
                  /\ IF ret[self] > 0
                        THEN /\ pc' = [pc EXCEPT ![self] = "checkRightBeforeSum"]
                        ELSE /\ pc' = [pc EXCEPT ![self] = "finishInsertRecInsert"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  p_i, temp, scanRes, t, array, split, 
                                  recStack, answer, tmp, tmp2, l_, r_, mid, 
                                  sibling_, keyL_, keyR_, recStack1, key_g, 
                                  startValue, depth_g, bits_g, key, start, p, 
                                  bits, depth, states >>

checkRightBeforeSum(self) == /\ pc[self] = "checkRightBeforeSum"
                             /\ IF ~nodeArr[p_[self]].hasRight
                                   THEN /\ pc' = [pc EXCEPT ![self] = "finishInsertRecInsert"]
                                   ELSE /\ pc' = [pc EXCEPT ![self] = "gotToRightBeforeSum"]
                             /\ UNCHANGED << root, hasRoot, globalLock, 
                                             gNodeArr, nodeArr, insertRes, 
                                             resultsForGet, deleteRes, 
                                             processWriterState, 
                                             processReaderState, values, stack, 
                                             i, cur, istack, current_d, key_, 
                                             tmp_, tmp1, mid_, p_, p1, 
                                             recStack_, recStack1_, ret, 
                                             depth_, res, calc, maxDepth, l, r, 
                                             gNode, buf_, rmax, links, 
                                             recStack_s, mid_s, val, child, 
                                             lastNode, bits_, depth_s, newBits, 
                                             movedBits, ln, rn, current, 
                                             sibling, keyL, keyR, buf, bufLink, 
                                             p_d, tempL, tempR, bits_d, 
                                             depth_d, key_i, stackData, 
                                             keyRNext, oldCurrent, parent, 
                                             keyLNext, links_, oldSibling, 
                                             current_, rev, bits_i, depth_i, 
                                             p_i, temp, scanRes, t, array, 
                                             split, recStack, answer, tmp, 
                                             tmp2, l_, r_, mid, sibling_, 
                                             keyL_, keyR_, recStack1, key_g, 
                                             startValue, depth_g, bits_g, key, 
                                             start, p, bits, depth, states >>

gotToRightBeforeSum(self) == /\ pc[self] = "gotToRightBeforeSum"
                             /\ p_' = [p_ EXCEPT ![self] = nodeArr[p_[self]].right]
                             /\ p1' = [p1 EXCEPT ![self] = p_'[self]]
                             /\ res' = [res EXCEPT ![self] = 0]
                             /\ recStack_' = [recStack_ EXCEPT ![self] = <<>>]
                             /\ pc' = [pc EXCEPT ![self] = "checkLeft12"]
                             /\ UNCHANGED << root, hasRoot, globalLock, 
                                             gNodeArr, nodeArr, insertRes, 
                                             resultsForGet, deleteRes, 
                                             processWriterState, 
                                             processReaderState, values, stack, 
                                             i, cur, istack, current_d, key_, 
                                             tmp_, tmp1, mid_, recStack1_, ret, 
                                             depth_, calc, maxDepth, l, r, 
                                             gNode, buf_, rmax, links, 
                                             recStack_s, mid_s, val, child, 
                                             lastNode, bits_, depth_s, newBits, 
                                             movedBits, ln, rn, current, 
                                             sibling, keyL, keyR, buf, bufLink, 
                                             p_d, tempL, tempR, bits_d, 
                                             depth_d, key_i, stackData, 
                                             keyRNext, oldCurrent, parent, 
                                             keyLNext, links_, oldSibling, 
                                             current_, rev, bits_i, depth_i, 
                                             p_i, temp, scanRes, t, array, 
                                             split, recStack, answer, tmp, 
                                             tmp2, l_, r_, mid, sibling_, 
                                             keyL_, keyR_, recStack1, key_g, 
                                             startValue, depth_g, bits_g, key, 
                                             start, p, bits, depth, states >>

checkLeft12(self) == /\ pc[self] = "checkLeft12"
                     /\ IF nodeArr[p1[self]].hasLeft
                           THEN /\ recStack_' = [recStack_ EXCEPT ![self] = Append(recStack_[self], <<p1[self], 0, FALSE>>)]
                                /\ pc' = [pc EXCEPT ![self] = "goToLeft12"]
                           ELSE /\ pc' = [pc EXCEPT ![self] = "checkRight12"]
                                /\ UNCHANGED recStack_
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack1_, ret, depth_, res, 
                                     calc, maxDepth, l, r, gNode, buf_, rmax, 
                                     links, recStack_s, mid_s, val, child, 
                                     lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

goToLeft12(self) == /\ pc[self] = "goToLeft12"
                    /\ p1' = [p1 EXCEPT ![self] = nodeArr[p1[self]].left]
                    /\ pc' = [pc EXCEPT ![self] = "checkLeft12"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, recStack, answer, tmp, tmp2, l_, r_, 
                                    mid, sibling_, keyL_, keyR_, recStack1, 
                                    key_g, startValue, depth_g, bits_g, key, 
                                    start, p, bits, depth, states >>

checkRight12(self) == /\ pc[self] = "checkRight12"
                      /\ IF nodeArr[p1[self]].hasRight
                            THEN /\ recStack_' = [recStack_ EXCEPT ![self] = Append(recStack_[self], <<p1[self], res[self], TRUE>>)]
                                 /\ pc' = [pc EXCEPT ![self] = "goToRight12"]
                            ELSE /\ pc' = [pc EXCEPT ![self] = "checkRetValue12"]
                                 /\ UNCHANGED recStack_
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack1_, ret, depth_, 
                                      res, calc, maxDepth, l, r, gNode, buf_, 
                                      rmax, links, recStack_s, mid_s, val, 
                                      child, lastNode, bits_, depth_s, newBits, 
                                      movedBits, ln, rn, current, sibling, 
                                      keyL, keyR, buf, bufLink, p_d, tempL, 
                                      tempR, bits_d, depth_d, key_i, stackData, 
                                      keyRNext, oldCurrent, parent, keyLNext, 
                                      links_, oldSibling, current_, rev, 
                                      bits_i, depth_i, p_i, temp, scanRes, t, 
                                      array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

goToRight12(self) == /\ pc[self] = "goToRight12"
                     /\ p1' = [p1 EXCEPT ![self] = nodeArr[p1[self]].right]
                     /\ pc' = [pc EXCEPT ![self] = "checkLeft12"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, recStack_, recStack1_, ret, depth_, 
                                     res, calc, maxDepth, l, r, gNode, buf_, 
                                     rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

checkRetValue12(self) == /\ pc[self] = "checkRetValue12"
                         /\ IF Len(recStack_[self]) = 0
                               THEN /\ pc' = [pc EXCEPT ![self] = "updateRet"]
                               ELSE /\ pc' = [pc EXCEPT ![self] = "getRecValue12"]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, answer, tmp, tmp2, l_, r_, 
                                         mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, start, p, bits, depth, 
                                         states >>

getRecValue12(self) == /\ pc[self] = "getRecValue12"
                       /\ tmp_' = [tmp_ EXCEPT ![self] = Tail(recStack_[self])]
                       /\ recStack_' = [recStack_ EXCEPT ![self] = SubSeq(recStack_[self], 0, Len(recStack_[self]) - 1)]
                       /\ p1' = [p1 EXCEPT ![self] = tmp_'[self][0]]
                       /\ IF tmp_'[self][2]
                             THEN /\ res' = [res EXCEPT ![self] = res[self] + tmp_'[self][1] + 1]
                                  /\ pc' = [pc EXCEPT ![self] = "checkRetValue12"]
                             ELSE /\ pc' = [pc EXCEPT ![self] = "checkRight12"]
                                  /\ res' = res
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp1, 
                                       mid_, p_, recStack1_, ret, depth_, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

updateRet(self) == /\ pc[self] = "updateRet"
                   /\ ret' = [ret EXCEPT ![self] = ret[self] + res[self] + 1]
                   /\ depth_' = [depth_ EXCEPT ![self] = depth_[self] + 1]
                   /\ calc' = [calc EXCEPT ![self] = shiftL(1, depth_'[self] - 1) - 1]
                   /\ IF ret'[self] - 1 < calc'[self] /\ depth_'[self] > 2
                         THEN /\ maxDepth' = [maxDepth EXCEPT ![self] = shiftL(1, depth_'[self]) - 1]
                              /\ array' = [array EXCEPT ![self] = <<>>]
                              /\ recStack_' = [recStack_ EXCEPT ![self] = <<>>]
                              /\ answer' = [answer EXCEPT ![self] = <<>>]
                              /\ pc' = [pc EXCEPT ![self] = "startFillBuf2"]
                         ELSE /\ pc' = [pc EXCEPT ![self] = "finishInsertRecInsert"]
                              /\ UNCHANGED << recStack_, maxDepth, array, 
                                              answer >>
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, resultsForGet, 
                                   deleteRes, processWriterState, 
                                   processReaderState, values, stack, i, cur, 
                                   istack, current_d, key_, tmp_, tmp1, mid_, 
                                   p_, p1, recStack1_, res, l, r, gNode, buf_, 
                                   rmax, links, recStack_s, mid_s, val, child, 
                                   lastNode, bits_, depth_s, newBits, 
                                   movedBits, ln, rn, current, sibling, keyL, 
                                   keyR, buf, bufLink, p_d, tempL, tempR, 
                                   bits_d, depth_d, key_i, stackData, keyRNext, 
                                   oldCurrent, parent, keyLNext, links_, 
                                   oldSibling, current_, rev, bits_i, depth_i, 
                                   p_i, temp, scanRes, t, split, recStack, tmp, 
                                   tmp2, l_, r_, mid, sibling_, keyL_, keyR_, 
                                   recStack1, key_g, startValue, depth_g, 
                                   bits_g, key, start, p, bits, depth, states >>

startFillBuf2(self) == /\ pc[self] = "startFillBuf2"
                       /\ p1' = [p1 EXCEPT ![self] = p_[self]]
                       /\ pc' = [pc EXCEPT ![self] = "checkLeft14"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, recStack_, recStack1_, 
                                       ret, depth_, res, calc, maxDepth, l, r, 
                                       gNode, buf_, rmax, links, recStack_s, 
                                       mid_s, val, child, lastNode, bits_, 
                                       depth_s, newBits, movedBits, ln, rn, 
                                       current, sibling, keyL, keyR, buf, 
                                       bufLink, p_d, tempL, tempR, bits_d, 
                                       depth_d, key_i, stackData, keyRNext, 
                                       oldCurrent, parent, keyLNext, links_, 
                                       oldSibling, current_, rev, bits_i, 
                                       depth_i, p_i, temp, scanRes, t, array, 
                                       split, recStack, answer, tmp, tmp2, l_, 
                                       r_, mid, sibling_, keyL_, keyR_, 
                                       recStack1, key_g, startValue, depth_g, 
                                       bits_g, key, start, p, bits, depth, 
                                       states >>

checkLeft14(self) == /\ pc[self] = "checkLeft14"
                     /\ IF nodeArr[p1[self]].hasLeft
                           THEN /\ recStack_' = [recStack_ EXCEPT ![self] = Append(recStack_[self], <<p1[self], FALSE>>)]
                                /\ pc' = [pc EXCEPT ![self] = "goToLeft14"]
                           ELSE /\ pc' = [pc EXCEPT ![self] = "saveAnswer14"]
                                /\ UNCHANGED recStack_
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack1_, ret, depth_, res, 
                                     calc, maxDepth, l, r, gNode, buf_, rmax, 
                                     links, recStack_s, mid_s, val, child, 
                                     lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

goToLeft14(self) == /\ pc[self] = "goToLeft14"
                    /\ p1' = [p1 EXCEPT ![self] = nodeArr[p1[self]].left]
                    /\ pc' = [pc EXCEPT ![self] = "checkLeft14"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, recStack, answer, tmp, tmp2, l_, r_, 
                                    mid, sibling_, keyL_, keyR_, recStack1, 
                                    key_g, startValue, depth_g, bits_g, key, 
                                    start, p, bits, depth, states >>

saveAnswer14(self) == /\ pc[self] = "saveAnswer14"
                      /\ IF ~nodeArr[p_[self]].deleted
                            THEN /\ pc' = [pc EXCEPT ![self] = "saveAnswer15"]
                            ELSE /\ pc' = [pc EXCEPT ![self] = "checkRight14"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

saveAnswer15(self) == /\ pc[self] = "saveAnswer15"
                      /\ answer' = [answer EXCEPT ![self] = Append(answer[self], nodeArr[p_[self]].value)]
                      /\ pc' = [pc EXCEPT ![self] = "checkRight14"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, tmp, tmp2, l_, 
                                      r_, mid, sibling_, keyL_, keyR_, 
                                      recStack1, key_g, startValue, depth_g, 
                                      bits_g, key, start, p, bits, depth, 
                                      states >>

checkRight14(self) == /\ pc[self] = "checkRight14"
                      /\ IF nodeArr[p1[self]].hasRight
                            THEN /\ recStack_' = [recStack_ EXCEPT ![self] = Append(recStack_[self], <<p1[self], TRUE>>)]
                                 /\ pc' = [pc EXCEPT ![self] = "goToRight14"]
                            ELSE /\ pc' = [pc EXCEPT ![self] = "checkRetValue14"]
                                 /\ UNCHANGED recStack_
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack1_, ret, depth_, 
                                      res, calc, maxDepth, l, r, gNode, buf_, 
                                      rmax, links, recStack_s, mid_s, val, 
                                      child, lastNode, bits_, depth_s, newBits, 
                                      movedBits, ln, rn, current, sibling, 
                                      keyL, keyR, buf, bufLink, p_d, tempL, 
                                      tempR, bits_d, depth_d, key_i, stackData, 
                                      keyRNext, oldCurrent, parent, keyLNext, 
                                      links_, oldSibling, current_, rev, 
                                      bits_i, depth_i, p_i, temp, scanRes, t, 
                                      array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

goToRight14(self) == /\ pc[self] = "goToRight14"
                     /\ p1' = [p1 EXCEPT ![self] = nodeArr[p1[self]].right]
                     /\ pc' = [pc EXCEPT ![self] = "checkLeft14"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, recStack_, recStack1_, ret, depth_, 
                                     res, calc, maxDepth, l, r, gNode, buf_, 
                                     rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

checkRetValue14(self) == /\ pc[self] = "checkRetValue14"
                         /\ IF Len(recStack_[self]) = 0
                               THEN /\ pc' = [pc EXCEPT ![self] = "startUpdateCurrent2"]
                               ELSE /\ pc' = [pc EXCEPT ![self] = "getRecValue14"]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, answer, tmp, tmp2, l_, r_, 
                                         mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, start, p, bits, depth, 
                                         states >>

getRecValue14(self) == /\ pc[self] = "getRecValue14"
                       /\ tmp_' = [tmp_ EXCEPT ![self] = Tail(recStack_[self])]
                       /\ recStack_' = [recStack_ EXCEPT ![self] = SubSeq(recStack_[self], 0, Len(recStack_[self] - 1))]
                       /\ p1' = [p1 EXCEPT ![self] = tmp_'[self][0]]
                       /\ IF tmp_'[self][1]
                             THEN /\ pc' = [pc EXCEPT ![self] = "checkRetValue14"]
                             ELSE /\ pc' = [pc EXCEPT ![self] = "saveAnswer14"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp1, 
                                       mid_, p_, recStack1_, ret, depth_, res, 
                                       calc, maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

startUpdateCurrent2(self) == /\ pc[self] = "startUpdateCurrent2"
                             /\ gNodeArr' = [gNodeArr EXCEPT ![current_d[self]].rev = gNodeArr[current_d[self]]]
                             /\ pc' = [pc EXCEPT ![self] = "startSmartFillValInc"]
                             /\ UNCHANGED << root, hasRoot, globalLock, 
                                             nodeArr, insertRes, resultsForGet, 
                                             deleteRes, processWriterState, 
                                             processReaderState, values, stack, 
                                             i, cur, istack, current_d, key_, 
                                             tmp_, tmp1, mid_, p_, p1, 
                                             recStack_, recStack1_, ret, 
                                             depth_, res, calc, maxDepth, l, r, 
                                             gNode, buf_, rmax, links, 
                                             recStack_s, mid_s, val, child, 
                                             lastNode, bits_, depth_s, newBits, 
                                             movedBits, ln, rn, current, 
                                             sibling, keyL, keyR, buf, bufLink, 
                                             p_d, tempL, tempR, bits_d, 
                                             depth_d, key_i, stackData, 
                                             keyRNext, oldCurrent, parent, 
                                             keyLNext, links_, oldSibling, 
                                             current_, rev, bits_i, depth_i, 
                                             p_i, temp, scanRes, t, array, 
                                             split, recStack, answer, tmp, 
                                             tmp2, l_, r_, mid, sibling_, 
                                             keyL_, keyR_, recStack1, key_g, 
                                             startValue, depth_g, bits_g, key, 
                                             start, p, bits, depth, states >>

startSmartFillValInc(self) == /\ pc[self] = "startSmartFillValInc"
                              /\ l' = [l EXCEPT ![self] = 0]
                              /\ r' = [r EXCEPT ![self] = Len(answer[self]) - 1]
                              /\ p1' = [p1 EXCEPT ![self] = p_[self]]
                              /\ recStack_' = [recStack_ EXCEPT ![self] = <<>>]
                              /\ pc' = [pc EXCEPT ![self] = "startSmartFillValInc2"]
                              /\ UNCHANGED << root, hasRoot, globalLock, 
                                              gNodeArr, nodeArr, insertRes, 
                                              resultsForGet, deleteRes, 
                                              processWriterState, 
                                              processReaderState, values, 
                                              stack, i, cur, istack, current_d, 
                                              key_, tmp_, tmp1, mid_, p_, 
                                              recStack1_, ret, depth_, res, 
                                              calc, maxDepth, gNode, buf_, 
                                              rmax, links, recStack_s, mid_s, 
                                              val, child, lastNode, bits_, 
                                              depth_s, newBits, movedBits, ln, 
                                              rn, current, sibling, keyL, keyR, 
                                              buf, bufLink, p_d, tempL, tempR, 
                                              bits_d, depth_d, key_i, 
                                              stackData, keyRNext, oldCurrent, 
                                              parent, keyLNext, links_, 
                                              oldSibling, current_, rev, 
                                              bits_i, depth_i, p_i, temp, 
                                              scanRes, t, array, split, 
                                              recStack, answer, tmp, tmp2, l_, 
                                              r_, mid, sibling_, keyL_, keyR_, 
                                              recStack1, key_g, startValue, 
                                              depth_g, bits_g, key, start, p, 
                                              bits, depth, states >>

startSmartFillValInc2(self) == /\ pc[self] = "startSmartFillValInc2"
                               /\ IF l[self] > r[self]
                                     THEN /\ pc' = [pc EXCEPT ![self] = "checkLeft6"]
                                          /\ mid_' = mid_
                                     ELSE /\ mid_' = [mid_ EXCEPT ![self] = (l[self] + r[self]) \div 2]
                                          /\ pc' = [pc EXCEPT ![self] = "checkLeft7"]
                               /\ UNCHANGED << root, hasRoot, globalLock, 
                                               gNodeArr, nodeArr, insertRes, 
                                               resultsForGet, deleteRes, 
                                               processWriterState, 
                                               processReaderState, values, 
                                               stack, i, cur, istack, 
                                               current_d, key_, tmp_, tmp1, p_, 
                                               p1, recStack_, recStack1_, ret, 
                                               depth_, res, calc, maxDepth, l, 
                                               r, gNode, buf_, rmax, links, 
                                               recStack_s, mid_s, val, child, 
                                               lastNode, bits_, depth_s, 
                                               newBits, movedBits, ln, rn, 
                                               current, sibling, keyL, keyR, 
                                               buf, bufLink, p_d, tempL, tempR, 
                                               bits_d, depth_d, key_i, 
                                               stackData, keyRNext, oldCurrent, 
                                               parent, keyLNext, links_, 
                                               oldSibling, current_, rev, 
                                               bits_i, depth_i, p_i, temp, 
                                               scanRes, t, array, split, 
                                               recStack, answer, tmp, tmp2, l_, 
                                               r_, mid, sibling_, keyL_, keyR_, 
                                               recStack1, key_g, startValue, 
                                               depth_g, bits_g, key, start, p, 
                                               bits, depth, states >>

checkLeft6(self) == /\ pc[self] = "checkLeft6"
                    /\ IF nodeArr[p1[self]].hasLeft
                          THEN /\ recStack_' = [recStack_ EXCEPT ![self] = Append(recStack_[self], <<p1[self], l[self], r[self], 0>>)]
                               /\ pc' = [pc EXCEPT ![self] = "goToLeft6"]
                          ELSE /\ pc' = [pc EXCEPT ![self] = "setEmpty"]
                               /\ UNCHANGED recStack_
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, p1, recStack1_, ret, depth_, res, calc, 
                                    maxDepth, l, r, gNode, buf_, rmax, links, 
                                    recStack_s, mid_s, val, child, lastNode, 
                                    bits_, depth_s, newBits, movedBits, ln, rn, 
                                    current, sibling, keyL, keyR, buf, bufLink, 
                                    p_d, tempL, tempR, bits_d, depth_d, key_i, 
                                    stackData, keyRNext, oldCurrent, parent, 
                                    keyLNext, links_, oldSibling, current_, 
                                    rev, bits_i, depth_i, p_i, temp, scanRes, 
                                    t, array, split, recStack, answer, tmp, 
                                    tmp2, l_, r_, mid, sibling_, keyL_, keyR_, 
                                    recStack1, key_g, startValue, depth_g, 
                                    bits_g, key, start, p, bits, depth, states >>

goToLeft6(self) == /\ pc[self] = "goToLeft6"
                   /\ p1' = [p1 EXCEPT ![self] = nodeArr[p1[self]].left]
                   /\ pc' = [pc EXCEPT ![self] = "startSmartFillValInc2"]
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, resultsForGet, 
                                   deleteRes, processWriterState, 
                                   processReaderState, values, stack, i, cur, 
                                   istack, current_d, key_, tmp_, tmp1, mid_, 
                                   p_, recStack_, recStack1_, ret, depth_, res, 
                                   calc, maxDepth, l, r, gNode, buf_, rmax, 
                                   links, recStack_s, mid_s, val, child, 
                                   lastNode, bits_, depth_s, newBits, 
                                   movedBits, ln, rn, current, sibling, keyL, 
                                   keyR, buf, bufLink, p_d, tempL, tempR, 
                                   bits_d, depth_d, key_i, stackData, keyRNext, 
                                   oldCurrent, parent, keyLNext, links_, 
                                   oldSibling, current_, rev, bits_i, depth_i, 
                                   p_i, temp, scanRes, t, array, split, 
                                   recStack, answer, tmp, tmp2, l_, r_, mid, 
                                   sibling_, keyL_, keyR_, recStack1, key_g, 
                                   startValue, depth_g, bits_g, key, start, p, 
                                   bits, depth, states >>

setEmpty(self) == /\ pc[self] = "setEmpty"
                  /\ nodeArr' = [nodeArr EXCEPT ![p1[self]].hasValue = FALSE]
                  /\ pc' = [pc EXCEPT ![self] = "checkRight6"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  p_i, temp, scanRes, t, array, split, 
                                  recStack, answer, tmp, tmp2, l_, r_, mid, 
                                  sibling_, keyL_, keyR_, recStack1, key_g, 
                                  startValue, depth_g, bits_g, key, start, p, 
                                  bits, depth, states >>

checkRight6(self) == /\ pc[self] = "checkRight6"
                     /\ IF nodeArr[p1[self]].hasRight
                           THEN /\ recStack_' = [recStack_ EXCEPT ![self] = Append(recStack_[self], <<p1[self], l[self], r[self], 1>>)]
                                /\ pc' = [pc EXCEPT ![self] = "goToRight6"]
                           ELSE /\ pc' = [pc EXCEPT ![self] = "checkRetValue7"]
                                /\ UNCHANGED recStack_
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack1_, ret, depth_, res, 
                                     calc, maxDepth, l, r, gNode, buf_, rmax, 
                                     links, recStack_s, mid_s, val, child, 
                                     lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

goToRight6(self) == /\ pc[self] = "goToRight6"
                    /\ p1' = [p1 EXCEPT ![self] = nodeArr[p1[self]].right]
                    /\ pc' = [pc EXCEPT ![self] = "startSmartFillValInc2"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, recStack, answer, tmp, tmp2, l_, r_, 
                                    mid, sibling_, keyL_, keyR_, recStack1, 
                                    key_g, startValue, depth_g, bits_g, key, 
                                    start, p, bits, depth, states >>

checkLeft7(self) == /\ pc[self] = "checkLeft7"
                    /\ IF nodeArr[p1[self]].hasLeft
                          THEN /\ recStack_' = [recStack_ EXCEPT ![self] = Append(recStack_[self], <<p1[self], l[self], r[self], 2>>)]
                               /\ pc' = [pc EXCEPT ![self] = "goToLeft7"]
                          ELSE /\ pc' = [pc EXCEPT ![self] = "saveAnswer7"]
                               /\ UNCHANGED recStack_
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, p1, recStack1_, ret, depth_, res, calc, 
                                    maxDepth, l, r, gNode, buf_, rmax, links, 
                                    recStack_s, mid_s, val, child, lastNode, 
                                    bits_, depth_s, newBits, movedBits, ln, rn, 
                                    current, sibling, keyL, keyR, buf, bufLink, 
                                    p_d, tempL, tempR, bits_d, depth_d, key_i, 
                                    stackData, keyRNext, oldCurrent, parent, 
                                    keyLNext, links_, oldSibling, current_, 
                                    rev, bits_i, depth_i, p_i, temp, scanRes, 
                                    t, array, split, recStack, answer, tmp, 
                                    tmp2, l_, r_, mid, sibling_, keyL_, keyR_, 
                                    recStack1, key_g, startValue, depth_g, 
                                    bits_g, key, start, p, bits, depth, states >>

goToLeft7(self) == /\ pc[self] = "goToLeft7"
                   /\ p1' = [p1 EXCEPT ![self] = nodeArr[p1[self]].left]
                   /\ r' = [r EXCEPT ![self] = mid_[self] - 1]
                   /\ pc' = [pc EXCEPT ![self] = "startSmartFillValInc2"]
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, resultsForGet, 
                                   deleteRes, processWriterState, 
                                   processReaderState, values, stack, i, cur, 
                                   istack, current_d, key_, tmp_, tmp1, mid_, 
                                   p_, recStack_, recStack1_, ret, depth_, res, 
                                   calc, maxDepth, l, gNode, buf_, rmax, links, 
                                   recStack_s, mid_s, val, child, lastNode, 
                                   bits_, depth_s, newBits, movedBits, ln, rn, 
                                   current, sibling, keyL, keyR, buf, bufLink, 
                                   p_d, tempL, tempR, bits_d, depth_d, key_i, 
                                   stackData, keyRNext, oldCurrent, parent, 
                                   keyLNext, links_, oldSibling, current_, rev, 
                                   bits_i, depth_i, p_i, temp, scanRes, t, 
                                   array, split, recStack, answer, tmp, tmp2, 
                                   l_, r_, mid, sibling_, keyL_, keyR_, 
                                   recStack1, key_g, startValue, depth_g, 
                                   bits_g, key, start, p, bits, depth, states >>

saveAnswer7(self) == /\ pc[self] = "saveAnswer7"
                     /\ nodeArr' = [nodeArr EXCEPT ![p1[self]].value = answer[self][mid_[self]]]
                     /\ pc' = [pc EXCEPT ![self] = "saveAnswerHasValue7_"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     insertRes, resultsForGet, deleteRes, 
                                     processWriterState, processReaderState, 
                                     values, stack, i, cur, istack, current_d, 
                                     key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                     recStack1_, ret, depth_, res, calc, 
                                     maxDepth, l, r, gNode, buf_, rmax, links, 
                                     recStack_s, mid_s, val, child, lastNode, 
                                     bits_, depth_s, newBits, movedBits, ln, 
                                     rn, current, sibling, keyL, keyR, buf, 
                                     bufLink, p_d, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

saveAnswerHasValue7_(self) == /\ pc[self] = "saveAnswerHasValue7_"
                              /\ nodeArr' = [nodeArr EXCEPT ![p1[self]].hasValue = TRUE]
                              /\ pc' = [pc EXCEPT ![self] = "checkRight7"]
                              /\ UNCHANGED << root, hasRoot, globalLock, 
                                              gNodeArr, insertRes, 
                                              resultsForGet, deleteRes, 
                                              processWriterState, 
                                              processReaderState, values, 
                                              stack, i, cur, istack, current_d, 
                                              key_, tmp_, tmp1, mid_, p_, p1, 
                                              recStack_, recStack1_, ret, 
                                              depth_, res, calc, maxDepth, l, 
                                              r, gNode, buf_, rmax, links, 
                                              recStack_s, mid_s, val, child, 
                                              lastNode, bits_, depth_s, 
                                              newBits, movedBits, ln, rn, 
                                              current, sibling, keyL, keyR, 
                                              buf, bufLink, p_d, tempL, tempR, 
                                              bits_d, depth_d, key_i, 
                                              stackData, keyRNext, oldCurrent, 
                                              parent, keyLNext, links_, 
                                              oldSibling, current_, rev, 
                                              bits_i, depth_i, p_i, temp, 
                                              scanRes, t, array, split, 
                                              recStack, answer, tmp, tmp2, l_, 
                                              r_, mid, sibling_, keyL_, keyR_, 
                                              recStack1, key_g, startValue, 
                                              depth_g, bits_g, key, start, p, 
                                              bits, depth, states >>

checkRight7(self) == /\ pc[self] = "checkRight7"
                     /\ IF nodeArr[p1[self]].hasRight
                           THEN /\ recStack_' = [recStack_ EXCEPT ![self] = Append(recStack_[self], <<p1[self], l[self], r[self], 3>>)]
                                /\ pc' = [pc EXCEPT ![self] = "goToRight7"]
                           ELSE /\ pc' = [pc EXCEPT ![self] = "checkRetValue7"]
                                /\ UNCHANGED recStack_
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack1_, ret, depth_, res, 
                                     calc, maxDepth, l, r, gNode, buf_, rmax, 
                                     links, recStack_s, mid_s, val, child, 
                                     lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

goToRight7(self) == /\ pc[self] = "goToRight7"
                    /\ p1' = [p1 EXCEPT ![self] = nodeArr[p1[self]].right]
                    /\ l' = [l EXCEPT ![self] = mid_[self] + 1]
                    /\ pc' = [pc EXCEPT ![self] = "startSmartFillValInc2"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, r, gNode, buf_, rmax, 
                                    links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, recStack, answer, tmp, tmp2, l_, r_, 
                                    mid, sibling_, keyL_, keyR_, recStack1, 
                                    key_g, startValue, depth_g, bits_g, key, 
                                    start, p, bits, depth, states >>

checkRetValue7(self) == /\ pc[self] = "checkRetValue7"
                        /\ IF Len(recStack_[self]) = 0
                              THEN /\ pc' = [pc EXCEPT ![self] = "finishUpdateCurrent3"]
                              ELSE /\ pc' = [pc EXCEPT ![self] = "getRetValue7"]
                        /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                        nodeArr, insertRes, resultsForGet, 
                                        deleteRes, processWriterState, 
                                        processReaderState, values, stack, i, 
                                        cur, istack, current_d, key_, tmp_, 
                                        tmp1, mid_, p_, p1, recStack_, 
                                        recStack1_, ret, depth_, res, calc, 
                                        maxDepth, l, r, gNode, buf_, rmax, 
                                        links, recStack_s, mid_s, val, child, 
                                        lastNode, bits_, depth_s, newBits, 
                                        movedBits, ln, rn, current, sibling, 
                                        keyL, keyR, buf, bufLink, p_d, tempL, 
                                        tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        current_, rev, bits_i, depth_i, p_i, 
                                        temp, scanRes, t, array, split, 
                                        recStack, answer, tmp, tmp2, l_, r_, 
                                        mid, sibling_, keyL_, keyR_, recStack1, 
                                        key_g, startValue, depth_g, bits_g, 
                                        key, start, p, bits, depth, states >>

getRetValue7(self) == /\ pc[self] = "getRetValue7"
                      /\ tmp_' = [tmp_ EXCEPT ![self] = Tail(recStack_[self])]
                      /\ recStack_' = [recStack_ EXCEPT ![self] = SubSeq(recStack_[self], 0, Len(recStack_[self] - 1))]
                      /\ p1' = [p1 EXCEPT ![self] = tmp_'[self][0]]
                      /\ l' = [l EXCEPT ![self] = tmp_'[self][1]]
                      /\ r' = [r EXCEPT ![self] = tmp_'[self][2]]
                      /\ IF tmp_'[self][3] = 0
                            THEN /\ pc' = [pc EXCEPT ![self] = "setEmpty"]
                            ELSE /\ pc' = [pc EXCEPT ![self] = "checkSecond"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp1, mid_, 
                                      p_, recStack1_, ret, depth_, res, calc, 
                                      maxDepth, gNode, buf_, rmax, links, 
                                      recStack_s, mid_s, val, child, lastNode, 
                                      bits_, depth_s, newBits, movedBits, ln, 
                                      rn, current, sibling, keyL, keyR, buf, 
                                      bufLink, p_d, tempL, tempR, bits_d, 
                                      depth_d, key_i, stackData, keyRNext, 
                                      oldCurrent, parent, keyLNext, links_, 
                                      oldSibling, current_, rev, bits_i, 
                                      depth_i, p_i, temp, scanRes, t, array, 
                                      split, recStack, answer, tmp, tmp2, l_, 
                                      r_, mid, sibling_, keyL_, keyR_, 
                                      recStack1, key_g, startValue, depth_g, 
                                      bits_g, key, start, p, bits, depth, 
                                      states >>

checkSecond(self) == /\ pc[self] = "checkSecond"
                     /\ IF tmp_[self][3] = 1
                           THEN /\ pc' = [pc EXCEPT ![self] = "checkRetValue7"]
                           ELSE /\ pc' = [pc EXCEPT ![self] = "checkThird"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

checkThird(self) == /\ pc[self] = "checkThird"
                    /\ IF tmp_[self][3] = 2
                          THEN /\ pc' = [pc EXCEPT ![self] = "saveAnswer7"]
                          ELSE /\ pc' = [pc EXCEPT ![self] = "checkFourth"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, p1, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, recStack, answer, tmp, tmp2, l_, r_, 
                                    mid, sibling_, keyL_, keyR_, recStack1, 
                                    key_g, startValue, depth_g, bits_g, key, 
                                    start, p, bits, depth, states >>

checkFourth(self) == /\ pc[self] = "checkFourth"
                     /\ IF tmp_[self][3] = 3
                           THEN /\ pc' = [pc EXCEPT ![self] = "checkRetValue7"]
                           ELSE /\ pc' = [pc EXCEPT ![self] = "finishUpdateCurrent3"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

finishUpdateCurrent3(self) == /\ pc[self] = "finishUpdateCurrent3"
                              /\ gNodeArr' = [gNodeArr EXCEPT ![current_d[self]].rev = gNodeArr[current_d[self]]]
                              /\ recStack1_' = [recStack1_ EXCEPT ![self] = Append(recStack1_[self], <<p_[self], depth_[self], ret[self], 1>>)]
                              /\ pc' = [pc EXCEPT ![self] = "checkKeyAlreadyExists"]
                              /\ UNCHANGED << root, hasRoot, globalLock, 
                                              nodeArr, insertRes, 
                                              resultsForGet, deleteRes, 
                                              processWriterState, 
                                              processReaderState, values, 
                                              stack, i, cur, istack, current_d, 
                                              key_, tmp_, tmp1, mid_, p_, p1, 
                                              recStack_, ret, depth_, res, 
                                              calc, maxDepth, l, r, gNode, 
                                              buf_, rmax, links, recStack_s, 
                                              mid_s, val, child, lastNode, 
                                              bits_, depth_s, newBits, 
                                              movedBits, ln, rn, current, 
                                              sibling, keyL, keyR, buf, 
                                              bufLink, p_d, tempL, tempR, 
                                              bits_d, depth_d, key_i, 
                                              stackData, keyRNext, oldCurrent, 
                                              parent, keyLNext, links_, 
                                              oldSibling, current_, rev, 
                                              bits_i, depth_i, p_i, temp, 
                                              scanRes, t, array, split, 
                                              recStack, answer, tmp, tmp2, l_, 
                                              r_, mid, sibling_, keyL_, keyR_, 
                                              recStack1, key_g, startValue, 
                                              depth_g, bits_g, key, start, p, 
                                              bits, depth, states >>

checkRight8(self) == /\ pc[self] = "checkRight8"
                     /\ IF nodeArr[p_[self]].hasRight
                           THEN /\ recStack1_' = [recStack1_ EXCEPT ![self] = Append(recStack1_[self], <<p_[self], depth_[self], ret[self], 2>>)]
                                /\ pc' = [pc EXCEPT ![self] = "goToRight8"]
                           ELSE /\ pc' = [pc EXCEPT ![self] = "createNewNode2"]
                                /\ UNCHANGED recStack1_
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, ret, depth_, res, calc, 
                                     maxDepth, l, r, gNode, buf_, rmax, links, 
                                     recStack_s, mid_s, val, child, lastNode, 
                                     bits_, depth_s, newBits, movedBits, ln, 
                                     rn, current, sibling, keyL, keyR, buf, 
                                     bufLink, p_d, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

goToRight8(self) == /\ pc[self] = "goToRight8"
                    /\ p_' = [p_ EXCEPT ![self] = nodeArr[p_[self]].right]
                    /\ pc' = [pc EXCEPT ![self] = "checkKeyAlreadyExists"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p1, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, recStack, answer, tmp, tmp2, l_, r_, 
                                    mid, sibling_, keyL_, keyR_, recStack1, 
                                    key_g, startValue, depth_g, bits_g, key, 
                                    start, p, bits, depth, states >>

createNewNode2(self) == /\ pc[self] = "createNewNode2"
                        /\ nodeArr' = Append(nodeArr, createNode(key_[self]))
                        /\ pc' = [pc EXCEPT ![self] = "insertInRight"]
                        /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                        insertRes, resultsForGet, deleteRes, 
                                        processWriterState, processReaderState, 
                                        values, stack, i, cur, istack, 
                                        current_d, key_, tmp_, tmp1, mid_, p_, 
                                        p1, recStack_, recStack1_, ret, depth_, 
                                        res, calc, maxDepth, l, r, gNode, buf_, 
                                        rmax, links, recStack_s, mid_s, val, 
                                        child, lastNode, bits_, depth_s, 
                                        newBits, movedBits, ln, rn, current, 
                                        sibling, keyL, keyR, buf, bufLink, p_d, 
                                        tempL, tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        current_, rev, bits_i, depth_i, p_i, 
                                        temp, scanRes, t, array, split, 
                                        recStack, answer, tmp, tmp2, l_, r_, 
                                        mid, sibling_, keyL_, keyR_, recStack1, 
                                        key_g, startValue, depth_g, bits_g, 
                                        key, start, p, bits, depth, states >>

insertInRight(self) == /\ pc[self] = "insertInRight"
                       /\ nodeArr' = [nodeArr EXCEPT ![p_[self]].right = Len(nodeArr)]
                       /\ pc' = [pc EXCEPT ![self] = "setHasRight"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       insertRes, resultsForGet, deleteRes, 
                                       processWriterState, processReaderState, 
                                       values, stack, i, cur, istack, 
                                       current_d, key_, tmp_, tmp1, mid_, p_, 
                                       p1, recStack_, recStack1_, ret, depth_, 
                                       res, calc, maxDepth, l, r, gNode, buf_, 
                                       rmax, links, recStack_s, mid_s, val, 
                                       child, lastNode, bits_, depth_s, 
                                       newBits, movedBits, ln, rn, current, 
                                       sibling, keyL, keyR, buf, bufLink, p_d, 
                                       tempL, tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

setHasRight(self) == /\ pc[self] = "setHasRight"
                     /\ nodeArr' = [nodeArr EXCEPT ![p_[self]].hasRight = TRUE]
                     /\ pc' = [pc EXCEPT ![self] = "updateCountNode3"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     insertRes, resultsForGet, deleteRes, 
                                     processWriterState, processReaderState, 
                                     values, stack, i, cur, istack, current_d, 
                                     key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                     recStack1_, ret, depth_, res, calc, 
                                     maxDepth, l, r, gNode, buf_, rmax, links, 
                                     recStack_s, mid_s, val, child, lastNode, 
                                     bits_, depth_s, newBits, movedBits, ln, 
                                     rn, current, sibling, keyL, keyR, buf, 
                                     bufLink, p_d, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

updateCountNode3(self) == /\ pc[self] = "updateCountNode3"
                          /\ tmp_' = [tmp_ EXCEPT ![self] = gNodeArr[tmp_[self]].countNode]
                          /\ pc' = [pc EXCEPT ![self] = "updateCountNode4"]
                          /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                          nodeArr, insertRes, resultsForGet, 
                                          deleteRes, processWriterState, 
                                          processReaderState, values, stack, i, 
                                          cur, istack, current_d, key_, tmp1, 
                                          mid_, p_, p1, recStack_, recStack1_, 
                                          ret, depth_, res, calc, maxDepth, l, 
                                          r, gNode, buf_, rmax, links, 
                                          recStack_s, mid_s, val, child, 
                                          lastNode, bits_, depth_s, newBits, 
                                          movedBits, ln, rn, current, sibling, 
                                          keyL, keyR, buf, bufLink, p_d, tempL, 
                                          tempR, bits_d, depth_d, key_i, 
                                          stackData, keyRNext, oldCurrent, 
                                          parent, keyLNext, links_, oldSibling, 
                                          current_, rev, bits_i, depth_i, p_i, 
                                          temp, scanRes, t, array, split, 
                                          recStack, answer, tmp, tmp2, l_, r_, 
                                          mid, sibling_, keyL_, keyR_, 
                                          recStack1, key_g, startValue, 
                                          depth_g, bits_g, key, start, p, bits, 
                                          depth, states >>

updateCountNode4(self) == /\ pc[self] = "updateCountNode4"
                          /\ tmp_' = [tmp_ EXCEPT ![self] = tmp_[self] + 1]
                          /\ ret' = [ret EXCEPT ![self] = 0]
                          /\ pc' = [pc EXCEPT ![self] = "finishInsertRecInsert"]
                          /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                          nodeArr, insertRes, resultsForGet, 
                                          deleteRes, processWriterState, 
                                          processReaderState, values, stack, i, 
                                          cur, istack, current_d, key_, tmp1, 
                                          mid_, p_, p1, recStack_, recStack1_, 
                                          depth_, res, calc, maxDepth, l, r, 
                                          gNode, buf_, rmax, links, recStack_s, 
                                          mid_s, val, child, lastNode, bits_, 
                                          depth_s, newBits, movedBits, ln, rn, 
                                          current, sibling, keyL, keyR, buf, 
                                          bufLink, p_d, tempL, tempR, bits_d, 
                                          depth_d, key_i, stackData, keyRNext, 
                                          oldCurrent, parent, keyLNext, links_, 
                                          oldSibling, current_, rev, bits_i, 
                                          depth_i, p_i, temp, scanRes, t, 
                                          array, split, recStack, answer, tmp, 
                                          tmp2, l_, r_, mid, sibling_, keyL_, 
                                          keyR_, recStack1, key_g, startValue, 
                                          depth_g, bits_g, key, start, p, bits, 
                                          depth, states >>

checkRet2(self) == /\ pc[self] = "checkRet2"
                   /\ IF ret[self] > 0
                         THEN /\ pc' = [pc EXCEPT ![self] = "checkLeftBeforeSum"]
                         ELSE /\ pc' = [pc EXCEPT ![self] = "finishInsertRecInsert"]
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, resultsForGet, 
                                   deleteRes, processWriterState, 
                                   processReaderState, values, stack, i, cur, 
                                   istack, current_d, key_, tmp_, tmp1, mid_, 
                                   p_, p1, recStack_, recStack1_, ret, depth_, 
                                   res, calc, maxDepth, l, r, gNode, buf_, 
                                   rmax, links, recStack_s, mid_s, val, child, 
                                   lastNode, bits_, depth_s, newBits, 
                                   movedBits, ln, rn, current, sibling, keyL, 
                                   keyR, buf, bufLink, p_d, tempL, tempR, 
                                   bits_d, depth_d, key_i, stackData, keyRNext, 
                                   oldCurrent, parent, keyLNext, links_, 
                                   oldSibling, current_, rev, bits_i, depth_i, 
                                   p_i, temp, scanRes, t, array, split, 
                                   recStack, answer, tmp, tmp2, l_, r_, mid, 
                                   sibling_, keyL_, keyR_, recStack1, key_g, 
                                   startValue, depth_g, bits_g, key, start, p, 
                                   bits, depth, states >>

checkLeftBeforeSum(self) == /\ pc[self] = "checkLeftBeforeSum"
                            /\ IF ~nodeArr[p_[self]].hasLeft
                                  THEN /\ pc' = [pc EXCEPT ![self] = "finishInsertRecInsert"]
                                  ELSE /\ pc' = [pc EXCEPT ![self] = "gotToLeftBeforeSum"]
                            /\ UNCHANGED << root, hasRoot, globalLock, 
                                            gNodeArr, nodeArr, insertRes, 
                                            resultsForGet, deleteRes, 
                                            processWriterState, 
                                            processReaderState, values, stack, 
                                            i, cur, istack, current_d, key_, 
                                            tmp_, tmp1, mid_, p_, p1, 
                                            recStack_, recStack1_, ret, depth_, 
                                            res, calc, maxDepth, l, r, gNode, 
                                            buf_, rmax, links, recStack_s, 
                                            mid_s, val, child, lastNode, bits_, 
                                            depth_s, newBits, movedBits, ln, 
                                            rn, current, sibling, keyL, keyR, 
                                            buf, bufLink, p_d, tempL, tempR, 
                                            bits_d, depth_d, key_i, stackData, 
                                            keyRNext, oldCurrent, parent, 
                                            keyLNext, links_, oldSibling, 
                                            current_, rev, bits_i, depth_i, 
                                            p_i, temp, scanRes, t, array, 
                                            split, recStack, answer, tmp, tmp2, 
                                            l_, r_, mid, sibling_, keyL_, 
                                            keyR_, recStack1, key_g, 
                                            startValue, depth_g, bits_g, key, 
                                            start, p, bits, depth, states >>

gotToLeftBeforeSum(self) == /\ pc[self] = "gotToLeftBeforeSum"
                            /\ p_' = [p_ EXCEPT ![self] = nodeArr[p_[self]].left]
                            /\ pc' = [pc EXCEPT ![self] = "checkLeft12"]
                            /\ UNCHANGED << root, hasRoot, globalLock, 
                                            gNodeArr, nodeArr, insertRes, 
                                            resultsForGet, deleteRes, 
                                            processWriterState, 
                                            processReaderState, values, stack, 
                                            i, cur, istack, current_d, key_, 
                                            tmp_, tmp1, mid_, p1, recStack_, 
                                            recStack1_, ret, depth_, res, calc, 
                                            maxDepth, l, r, gNode, buf_, rmax, 
                                            links, recStack_s, mid_s, val, 
                                            child, lastNode, bits_, depth_s, 
                                            newBits, movedBits, ln, rn, 
                                            current, sibling, keyL, keyR, buf, 
                                            bufLink, p_d, tempL, tempR, bits_d, 
                                            depth_d, key_i, stackData, 
                                            keyRNext, oldCurrent, parent, 
                                            keyLNext, links_, oldSibling, 
                                            current_, rev, bits_i, depth_i, 
                                            p_i, temp, scanRes, t, array, 
                                            split, recStack, answer, tmp, tmp2, 
                                            l_, r_, mid, sibling_, keyL_, 
                                            keyR_, recStack1, key_g, 
                                            startValue, depth_g, bits_g, key, 
                                            start, p, bits, depth, states >>

finishInsertRecInsert(self) == /\ pc[self] = "finishInsertRecInsert"
                               /\ IF Len(recStack1_[self]) = 0
                                     THEN /\ pc' = [pc EXCEPT ![self] = Head(stack[self]).pc]
                                          /\ tmp_' = [tmp_ EXCEPT ![self] = Head(stack[self]).tmp_]
                                          /\ tmp1' = [tmp1 EXCEPT ![self] = Head(stack[self]).tmp1]
                                          /\ mid_' = [mid_ EXCEPT ![self] = Head(stack[self]).mid_]
                                          /\ p_' = [p_ EXCEPT ![self] = Head(stack[self]).p_]
                                          /\ p1' = [p1 EXCEPT ![self] = Head(stack[self]).p1]
                                          /\ recStack_' = [recStack_ EXCEPT ![self] = Head(stack[self]).recStack_]
                                          /\ recStack1_' = [recStack1_ EXCEPT ![self] = Head(stack[self]).recStack1_]
                                          /\ ret' = [ret EXCEPT ![self] = Head(stack[self]).ret]
                                          /\ depth_' = [depth_ EXCEPT ![self] = Head(stack[self]).depth_]
                                          /\ res' = [res EXCEPT ![self] = Head(stack[self]).res]
                                          /\ calc' = [calc EXCEPT ![self] = Head(stack[self]).calc]
                                          /\ maxDepth' = [maxDepth EXCEPT ![self] = Head(stack[self]).maxDepth]
                                          /\ current_d' = [current_d EXCEPT ![self] = Head(stack[self]).current_d]
                                          /\ key_' = [key_ EXCEPT ![self] = Head(stack[self]).key_]
                                          /\ stack' = [stack EXCEPT ![self] = Tail(stack[self])]
                                     ELSE /\ pc' = [pc EXCEPT ![self] = "getRetValue2"]
                                          /\ UNCHANGED << stack, current_d, 
                                                          key_, tmp_, tmp1, 
                                                          mid_, p_, p1, 
                                                          recStack_, 
                                                          recStack1_, ret, 
                                                          depth_, res, calc, 
                                                          maxDepth >>
                               /\ UNCHANGED << root, hasRoot, globalLock, 
                                               gNodeArr, nodeArr, insertRes, 
                                               resultsForGet, deleteRes, 
                                               processWriterState, 
                                               processReaderState, values, i, 
                                               cur, istack, l, r, gNode, buf_, 
                                               rmax, links, recStack_s, mid_s, 
                                               val, child, lastNode, bits_, 
                                               depth_s, newBits, movedBits, ln, 
                                               rn, current, sibling, keyL, 
                                               keyR, buf, bufLink, p_d, tempL, 
                                               tempR, bits_d, depth_d, key_i, 
                                               stackData, keyRNext, oldCurrent, 
                                               parent, keyLNext, links_, 
                                               oldSibling, current_, rev, 
                                               bits_i, depth_i, p_i, temp, 
                                               scanRes, t, array, split, 
                                               recStack, answer, tmp, tmp2, l_, 
                                               r_, mid, sibling_, keyL_, keyR_, 
                                               recStack1, key_g, startValue, 
                                               depth_g, bits_g, key, start, p, 
                                               bits, depth, states >>

getRetValue2(self) == /\ pc[self] = "getRetValue2"
                      /\ tmp_' = [tmp_ EXCEPT ![self] = Tail(recStack1_[self])]
                      /\ recStack1_' = [recStack1_ EXCEPT ![self] = SubSeq(recStack1_[self], 0, Len(recStack1_[self] - 1))]
                      /\ p_' = [p_ EXCEPT ![self] = tmp_'[self][0]]
                      /\ depth_' = [depth_ EXCEPT ![self] = tmp_'[self][1]]
                      /\ ret' = [ret EXCEPT ![self] = tmp_'[self][2] + ret[self]]
                      /\ IF tmp_'[self][3] = 0
                            THEN /\ pc' = [pc EXCEPT ![self] = "checkRet"]
                            ELSE /\ pc' = [pc EXCEPT ![self] = "checkSecond3"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp1, mid_, 
                                      p1, recStack_, res, calc, maxDepth, l, r, 
                                      gNode, buf_, rmax, links, recStack_s, 
                                      mid_s, val, child, lastNode, bits_, 
                                      depth_s, newBits, movedBits, ln, rn, 
                                      current, sibling, keyL, keyR, buf, 
                                      bufLink, p_d, tempL, tempR, bits_d, 
                                      depth_d, key_i, stackData, keyRNext, 
                                      oldCurrent, parent, keyLNext, links_, 
                                      oldSibling, current_, rev, bits_i, 
                                      depth_i, p_i, temp, scanRes, t, array, 
                                      split, recStack, answer, tmp, tmp2, l_, 
                                      r_, mid, sibling_, keyL_, keyR_, 
                                      recStack1, key_g, startValue, depth_g, 
                                      bits_g, key, start, p, bits, depth, 
                                      states >>

checkSecond3(self) == /\ pc[self] = "checkSecond3"
                      /\ IF tmp_[self][3] = 1
                            THEN /\ pc' = [pc EXCEPT ![self] = "finishInsertRecInsert"]
                            ELSE /\ pc' = [pc EXCEPT ![self] = "checkThird3"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

checkThird3(self) == /\ pc[self] = "checkThird3"
                     /\ IF tmp_[self][3] = 2
                           THEN /\ pc' = [pc EXCEPT ![self] = "checkRet2"]
                           ELSE /\ pc' = [pc EXCEPT ![self] = "checkFourth3"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

checkFourth3(self) == /\ pc[self] = "checkFourth3"
                      /\ IF tmp_[self][3] = 3
                            THEN /\ pc' = [pc EXCEPT ![self] = "finishInsertRecInsert"]
                            ELSE /\ pc' = [pc EXCEPT ![self] = "Error"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

doInsertLeaf(self) == getCurrentRoot(self) \/ checkKeyAlreadyExists(self)
                         \/ setZeroRet(self) \/ checkLeft4(self)
                         \/ checkLeftExists(self) \/ getLeftInInsert(self)
                         \/ createNewNode(self) \/ insertInLeft(self)
                         \/ insertHasLeft(self) \/ updateCountNode_(self)
                         \/ updateCountNode2_(self) \/ checkRet(self)
                         \/ checkRightBeforeSum(self)
                         \/ gotToRightBeforeSum(self) \/ checkLeft12(self)
                         \/ goToLeft12(self) \/ checkRight12(self)
                         \/ goToRight12(self) \/ checkRetValue12(self)
                         \/ getRecValue12(self) \/ updateRet(self)
                         \/ startFillBuf2(self) \/ checkLeft14(self)
                         \/ goToLeft14(self) \/ saveAnswer14(self)
                         \/ saveAnswer15(self) \/ checkRight14(self)
                         \/ goToRight14(self) \/ checkRetValue14(self)
                         \/ getRecValue14(self)
                         \/ startUpdateCurrent2(self)
                         \/ startSmartFillValInc(self)
                         \/ startSmartFillValInc2(self) \/ checkLeft6(self)
                         \/ goToLeft6(self) \/ setEmpty(self)
                         \/ checkRight6(self) \/ goToRight6(self)
                         \/ checkLeft7(self) \/ goToLeft7(self)
                         \/ saveAnswer7(self) \/ saveAnswerHasValue7_(self)
                         \/ checkRight7(self) \/ goToRight7(self)
                         \/ checkRetValue7(self) \/ getRetValue7(self)
                         \/ checkSecond(self) \/ checkThird(self)
                         \/ checkFourth(self) \/ finishUpdateCurrent3(self)
                         \/ checkRight8(self) \/ goToRight8(self)
                         \/ createNewNode2(self) \/ insertInRight(self)
                         \/ setHasRight(self) \/ updateCountNode3(self)
                         \/ updateCountNode4(self) \/ checkRet2(self)
                         \/ checkLeftBeforeSum(self)
                         \/ gotToLeftBeforeSum(self)
                         \/ finishInsertRecInsert(self)
                         \/ getRetValue2(self) \/ checkSecond3(self)
                         \/ checkThird3(self) \/ checkFourth3(self)

startFillValParentLo(self) == /\ pc[self] = "startFillValParentLo"
                              /\ recStack_s' = [recStack_s EXCEPT ![self] = <<>>]
                              /\ pc' = [pc EXCEPT ![self] = "checkBounds"]
                              /\ UNCHANGED << root, hasRoot, globalLock, 
                                              gNodeArr, nodeArr, insertRes, 
                                              resultsForGet, deleteRes, 
                                              processWriterState, 
                                              processReaderState, values, 
                                              stack, i, cur, istack, current_d, 
                                              key_, tmp_, tmp1, mid_, p_, p1, 
                                              recStack_, recStack1_, ret, 
                                              depth_, res, calc, maxDepth, l, 
                                              r, gNode, buf_, rmax, links, 
                                              mid_s, val, child, lastNode, 
                                              bits_, depth_s, newBits, 
                                              movedBits, ln, rn, current, 
                                              sibling, keyL, keyR, buf, 
                                              bufLink, p_d, tempL, tempR, 
                                              bits_d, depth_d, key_i, 
                                              stackData, keyRNext, oldCurrent, 
                                              parent, keyLNext, links_, 
                                              oldSibling, current_, rev, 
                                              bits_i, depth_i, p_i, temp, 
                                              scanRes, t, array, split, 
                                              recStack, answer, tmp, tmp2, l_, 
                                              r_, mid, sibling_, keyL_, keyR_, 
                                              recStack1, key_g, startValue, 
                                              depth_g, bits_g, key, start, p, 
                                              bits, depth, states >>

checkBounds(self) == /\ pc[self] = "checkBounds"
                     /\ IF r[self] < l[self] \/ l[self] = rmax[self]
                           THEN /\ pc' = [pc EXCEPT ![self] = "checkRecSize15"]
                           ELSE /\ pc' = [pc EXCEPT ![self] = "calcMid"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

calcMid(self) == /\ pc[self] = "calcMid"
                 /\ mid_s' = [mid_s EXCEPT ![self] = (l[self] + r[self]) \div 2]
                 /\ val' = [val EXCEPT ![self] = buf_[self][mid_s'[self]]]
                 /\ child' = [child EXCEPT ![self] = links[self][mid_s'[self]]]
                 /\ p' = [p EXCEPT ![self] = gNodeArr[gNode[self]].root]
                 /\ pc' = [pc EXCEPT ![self] = "detailedNodeSearchStart"]
                 /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                 insertRes, resultsForGet, deleteRes, 
                                 processWriterState, processReaderState, 
                                 values, stack, i, cur, istack, current_d, 
                                 key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                 recStack1_, ret, depth_, res, calc, maxDepth, 
                                 l, r, gNode, buf_, rmax, links, recStack_s, 
                                 lastNode, bits_, depth_s, newBits, movedBits, 
                                 ln, rn, current, sibling, keyL, keyR, buf, 
                                 bufLink, p_d, tempL, tempR, bits_d, depth_d, 
                                 key_i, stackData, keyRNext, oldCurrent, 
                                 parent, keyLNext, links_, oldSibling, 
                                 current_, rev, bits_i, depth_i, p_i, temp, 
                                 scanRes, t, array, split, recStack, answer, 
                                 tmp, tmp2, l_, r_, mid, sibling_, keyL_, 
                                 keyR_, recStack1, key_g, startValue, depth_g, 
                                 bits_g, key, start, bits, depth, states >>

detailedNodeSearchStart(self) == /\ pc[self] = "detailedNodeSearchStart"
                                 /\ p1' = [p1 EXCEPT ![self] = p[self]]
                                 /\ lastNode' = [lastNode EXCEPT ![self] = p1'[self]]
                                 /\ bits_' = [bits_ EXCEPT ![self] = 1]
                                 /\ depth_s' = [depth_s EXCEPT ![self] = 0]
                                 /\ pc' = [pc EXCEPT ![self] = "mainCycleStart"]
                                 /\ UNCHANGED << root, hasRoot, globalLock, 
                                                 gNodeArr, nodeArr, insertRes, 
                                                 resultsForGet, deleteRes, 
                                                 processWriterState, 
                                                 processReaderState, values, 
                                                 stack, i, cur, istack, 
                                                 current_d, key_, tmp_, tmp1, 
                                                 mid_, p_, recStack_, 
                                                 recStack1_, ret, depth_, res, 
                                                 calc, maxDepth, l, r, gNode, 
                                                 buf_, rmax, links, recStack_s, 
                                                 mid_s, val, child, newBits, 
                                                 movedBits, ln, rn, current, 
                                                 sibling, keyL, keyR, buf, 
                                                 bufLink, p_d, tempL, tempR, 
                                                 bits_d, depth_d, key_i, 
                                                 stackData, keyRNext, 
                                                 oldCurrent, parent, keyLNext, 
                                                 links_, oldSibling, current_, 
                                                 rev, bits_i, depth_i, p_i, 
                                                 temp, scanRes, t, array, 
                                                 split, recStack, answer, tmp, 
                                                 tmp2, l_, r_, mid, sibling_, 
                                                 keyL_, keyR_, recStack1, 
                                                 key_g, startValue, depth_g, 
                                                 bits_g, key, start, p, bits, 
                                                 depth, states >>

mainCycleStart(self) == /\ pc[self] = "mainCycleStart"
                        /\ pc' = [pc EXCEPT ![self] = "checkHasValue"]
                        /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                        nodeArr, insertRes, resultsForGet, 
                                        deleteRes, processWriterState, 
                                        processReaderState, values, stack, i, 
                                        cur, istack, current_d, key_, tmp_, 
                                        tmp1, mid_, p_, p1, recStack_, 
                                        recStack1_, ret, depth_, res, calc, 
                                        maxDepth, l, r, gNode, buf_, rmax, 
                                        links, recStack_s, mid_s, val, child, 
                                        lastNode, bits_, depth_s, newBits, 
                                        movedBits, ln, rn, current, sibling, 
                                        keyL, keyR, buf, bufLink, p_d, tempL, 
                                        tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        current_, rev, bits_i, depth_i, p_i, 
                                        temp, scanRes, t, array, split, 
                                        recStack, answer, tmp, tmp2, l_, r_, 
                                        mid, sibling_, keyL_, keyR_, recStack1, 
                                        key_g, startValue, depth_g, bits_g, 
                                        key, start, p, bits, depth, states >>

checkHasValue(self) == /\ pc[self] = "checkHasValue"
                       /\ IF ~nodeArr[p1[self]].hasValue
                             THEN /\ pc' = [pc EXCEPT ![self] = "updateBits"]
                             ELSE /\ pc' = [pc EXCEPT ![self] = "detailedNodeSearchCalcs"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

detailedNodeSearchCalcs(self) == /\ pc[self] = "detailedNodeSearchCalcs"
                                 /\ depth_s' = [depth_s EXCEPT ![self] = depth_s[self] + 1]
                                 /\ bits_' = [bits_ EXCEPT ![self] = bits_[self] * 2]
                                 /\ lastNode' = [lastNode EXCEPT ![self] = p1[self]]
                                 /\ pc' = [pc EXCEPT ![self] = "checkKey"]
                                 /\ UNCHANGED << root, hasRoot, globalLock, 
                                                 gNodeArr, nodeArr, insertRes, 
                                                 resultsForGet, deleteRes, 
                                                 processWriterState, 
                                                 processReaderState, values, 
                                                 stack, i, cur, istack, 
                                                 current_d, key_, tmp_, tmp1, 
                                                 mid_, p_, p1, recStack_, 
                                                 recStack1_, ret, depth_, res, 
                                                 calc, maxDepth, l, r, gNode, 
                                                 buf_, rmax, links, recStack_s, 
                                                 mid_s, val, child, newBits, 
                                                 movedBits, ln, rn, current, 
                                                 sibling, keyL, keyR, buf, 
                                                 bufLink, p_d, tempL, tempR, 
                                                 bits_d, depth_d, key_i, 
                                                 stackData, keyRNext, 
                                                 oldCurrent, parent, keyLNext, 
                                                 links_, oldSibling, current_, 
                                                 rev, bits_i, depth_i, p_i, 
                                                 temp, scanRes, t, array, 
                                                 split, recStack, answer, tmp, 
                                                 tmp2, l_, r_, mid, sibling_, 
                                                 keyL_, keyR_, recStack1, 
                                                 key_g, startValue, depth_g, 
                                                 bits_g, key, start, p, bits, 
                                                 depth, states >>

checkKey(self) == /\ pc[self] = "checkKey"
                  /\ IF val[self] < nodeArr[p1[self]].value
                        THEN /\ pc' = [pc EXCEPT ![self] = "dnsCheckLeft"]
                        ELSE /\ pc' = [pc EXCEPT ![self] = "dnsCheckRight"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  p_i, temp, scanRes, t, array, split, 
                                  recStack, answer, tmp, tmp2, l_, r_, mid, 
                                  sibling_, keyL_, keyR_, recStack1, key_g, 
                                  startValue, depth_g, bits_g, key, start, p, 
                                  bits, depth, states >>

dnsCheckLeft(self) == /\ pc[self] = "dnsCheckLeft"
                      /\ IF ~nodeArr[p1[self]].hasLeft
                            THEN /\ pc' = [pc EXCEPT ![self] = "updateBits"]
                            ELSE /\ pc' = [pc EXCEPT ![self] = "dnsGetLeft"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

dnsGetLeft(self) == /\ pc[self] = "dnsGetLeft"
                    /\ p1' = [p1 EXCEPT ![self] = nodeArr[p1[self]].left]
                    /\ pc' = [pc EXCEPT ![self] = "mainCycleStart"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, recStack, answer, tmp, tmp2, l_, r_, 
                                    mid, sibling_, keyL_, keyR_, recStack1, 
                                    key_g, startValue, depth_g, bits_g, key, 
                                    start, p, bits, depth, states >>

dnsCheckRight(self) == /\ pc[self] = "dnsCheckRight"
                       /\ IF ~nodeArr[p1[self]].hasRight
                             THEN /\ pc' = [pc EXCEPT ![self] = "updateBits"]
                             ELSE /\ pc' = [pc EXCEPT ![self] = "dnsGetRight"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

dnsGetRight(self) == /\ pc[self] = "dnsGetRight"
                     /\ p1' = [p1 EXCEPT ![self] = nodeArr[p1[self]].right]
                     /\ bits_' = [bits_ EXCEPT ![self] = bits_[self] + 1]
                     /\ pc' = [pc EXCEPT ![self] = "mainCycleStart"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, recStack_, recStack1_, ret, depth_, 
                                     res, calc, maxDepth, l, r, gNode, buf_, 
                                     rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

updateBits(self) == /\ pc[self] = "updateBits"
                    /\ bits_' = [bits_ EXCEPT ![self] = bits_[self] \div 2]
                    /\ pc' = [pc EXCEPT ![self] = "prepareBitsAndDepth"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, p1, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, depth_s, newBits, movedBits, ln, 
                                    rn, current, sibling, keyL, keyR, buf, 
                                    bufLink, p_d, tempL, tempR, bits_d, 
                                    depth_d, key_i, stackData, keyRNext, 
                                    oldCurrent, parent, keyLNext, links_, 
                                    oldSibling, current_, rev, bits_i, depth_i, 
                                    p_i, temp, scanRes, t, array, split, 
                                    recStack, answer, tmp, tmp2, l_, r_, mid, 
                                    sibling_, keyL_, keyR_, recStack1, key_g, 
                                    startValue, depth_g, bits_g, key, start, p, 
                                    bits, depth, states >>

prepareBitsAndDepth(self) == /\ pc[self] = "prepareBitsAndDepth"
                             /\ newBits' = [newBits EXCEPT ![self] = bits_[self]]
                             /\ movedBits' = [movedBits EXCEPT ![self] = bits_[self]]
                             /\ bits_' = [bits_ EXCEPT ![self] = shiftL(bits_[self], GNodeDepth - depth_s[self])]
                             /\ depth_s' = [depth_s EXCEPT ![self] = depth_s[self] + 1]
                             /\ pc' = [pc EXCEPT ![self] = "getLastNodeLeft"]
                             /\ UNCHANGED << root, hasRoot, globalLock, 
                                             gNodeArr, nodeArr, insertRes, 
                                             resultsForGet, deleteRes, 
                                             processWriterState, 
                                             processReaderState, values, stack, 
                                             i, cur, istack, current_d, key_, 
                                             tmp_, tmp1, mid_, p_, p1, 
                                             recStack_, recStack1_, ret, 
                                             depth_, res, calc, maxDepth, l, r, 
                                             gNode, buf_, rmax, links, 
                                             recStack_s, mid_s, val, child, 
                                             lastNode, ln, rn, current, 
                                             sibling, keyL, keyR, buf, bufLink, 
                                             p_d, tempL, tempR, bits_d, 
                                             depth_d, key_i, stackData, 
                                             keyRNext, oldCurrent, parent, 
                                             keyLNext, links_, oldSibling, 
                                             current_, rev, bits_i, depth_i, 
                                             p_i, temp, scanRes, t, array, 
                                             split, recStack, answer, tmp, 
                                             tmp2, l_, r_, mid, sibling_, 
                                             keyL_, keyR_, recStack1, key_g, 
                                             startValue, depth_g, bits_g, key, 
                                             start, p, bits, depth, states >>

getLastNodeLeft(self) == /\ pc[self] = "getLastNodeLeft"
                         /\ ln' = [ln EXCEPT ![self] = nodeArr[lastNode[self]].left]
                         /\ pc' = [pc EXCEPT ![self] = "getLastNodeRight"]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, rn, current, sibling, keyL, 
                                         keyR, buf, bufLink, p_d, tempL, tempR, 
                                         bits_d, depth_d, key_i, stackData, 
                                         keyRNext, oldCurrent, parent, 
                                         keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, answer, tmp, tmp2, l_, r_, 
                                         mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, start, p, bits, depth, 
                                         states >>

getLastNodeRight(self) == /\ pc[self] = "getLastNodeRight"
                          /\ rn' = [rn EXCEPT ![self] = nodeArr[lastNode[self]].right]
                          /\ pc' = [pc EXCEPT ![self] = "checkLastNodeValue"]
                          /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                          nodeArr, insertRes, resultsForGet, 
                                          deleteRes, processWriterState, 
                                          processReaderState, values, stack, i, 
                                          cur, istack, current_d, key_, tmp_, 
                                          tmp1, mid_, p_, p1, recStack_, 
                                          recStack1_, ret, depth_, res, calc, 
                                          maxDepth, l, r, gNode, buf_, rmax, 
                                          links, recStack_s, mid_s, val, child, 
                                          lastNode, bits_, depth_s, newBits, 
                                          movedBits, ln, current, sibling, 
                                          keyL, keyR, buf, bufLink, p_d, tempL, 
                                          tempR, bits_d, depth_d, key_i, 
                                          stackData, keyRNext, oldCurrent, 
                                          parent, keyLNext, links_, oldSibling, 
                                          current_, rev, bits_i, depth_i, p_i, 
                                          temp, scanRes, t, array, split, 
                                          recStack, answer, tmp, tmp2, l_, r_, 
                                          mid, sibling_, keyL_, keyR_, 
                                          recStack1, key_g, startValue, 
                                          depth_g, bits_g, key, start, p, bits, 
                                          depth, states >>

checkLastNodeValue(self) == /\ pc[self] = "checkLastNodeValue"
                            /\ IF val[self] < nodeArr[lastNode[self]].value
                                  THEN /\ pc' = [pc EXCEPT ![self] = "setLnValue"]
                                  ELSE /\ pc' = [pc EXCEPT ![self] = "setRnValue2"]
                            /\ UNCHANGED << root, hasRoot, globalLock, 
                                            gNodeArr, nodeArr, insertRes, 
                                            resultsForGet, deleteRes, 
                                            processWriterState, 
                                            processReaderState, values, stack, 
                                            i, cur, istack, current_d, key_, 
                                            tmp_, tmp1, mid_, p_, p1, 
                                            recStack_, recStack1_, ret, depth_, 
                                            res, calc, maxDepth, l, r, gNode, 
                                            buf_, rmax, links, recStack_s, 
                                            mid_s, val, child, lastNode, bits_, 
                                            depth_s, newBits, movedBits, ln, 
                                            rn, current, sibling, keyL, keyR, 
                                            buf, bufLink, p_d, tempL, tempR, 
                                            bits_d, depth_d, key_i, stackData, 
                                            keyRNext, oldCurrent, parent, 
                                            keyLNext, links_, oldSibling, 
                                            current_, rev, bits_i, depth_i, 
                                            p_i, temp, scanRes, t, array, 
                                            split, recStack, answer, tmp, tmp2, 
                                            l_, r_, mid, sibling_, keyL_, 
                                            keyR_, recStack1, key_g, 
                                            startValue, depth_g, bits_g, key, 
                                            start, p, bits, depth, states >>

setLnValue(self) == /\ pc[self] = "setLnValue"
                    /\ nodeArr' = [nodeArr EXCEPT ![ln[self]].value = val[self]]
                    /\ pc' = [pc EXCEPT ![self] = "setLnHasValue"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    insertRes, resultsForGet, deleteRes, 
                                    processWriterState, processReaderState, 
                                    values, stack, i, cur, istack, current_d, 
                                    key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                    recStack1_, ret, depth_, res, calc, 
                                    maxDepth, l, r, gNode, buf_, rmax, links, 
                                    recStack_s, mid_s, val, child, lastNode, 
                                    bits_, depth_s, newBits, movedBits, ln, rn, 
                                    current, sibling, keyL, keyR, buf, bufLink, 
                                    p_d, tempL, tempR, bits_d, depth_d, key_i, 
                                    stackData, keyRNext, oldCurrent, parent, 
                                    keyLNext, links_, oldSibling, current_, 
                                    rev, bits_i, depth_i, p_i, temp, scanRes, 
                                    t, array, split, recStack, answer, tmp, 
                                    tmp2, l_, r_, mid, sibling_, keyL_, keyR_, 
                                    recStack1, key_g, startValue, depth_g, 
                                    bits_g, key, start, p, bits, depth, states >>

setLnHasValue(self) == /\ pc[self] = "setLnHasValue"
                       /\ nodeArr' = [nodeArr EXCEPT ![ln[self]].hasValue = TRUE]
                       /\ pc' = [pc EXCEPT ![self] = "setRnValue"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       insertRes, resultsForGet, deleteRes, 
                                       processWriterState, processReaderState, 
                                       values, stack, i, cur, istack, 
                                       current_d, key_, tmp_, tmp1, mid_, p_, 
                                       p1, recStack_, recStack1_, ret, depth_, 
                                       res, calc, maxDepth, l, r, gNode, buf_, 
                                       rmax, links, recStack_s, mid_s, val, 
                                       child, lastNode, bits_, depth_s, 
                                       newBits, movedBits, ln, rn, current, 
                                       sibling, keyL, keyR, buf, bufLink, p_d, 
                                       tempL, tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

setRnValue(self) == /\ pc[self] = "setRnValue"
                    /\ nodeArr' = [nodeArr EXCEPT ![rn[self]].value = nodeArr[lastNode[self]].value]
                    /\ newBits' = [newBits EXCEPT ![self] = newBits[self] * 2]
                    /\ movedBits' = [movedBits EXCEPT ![self] = (movedBits[self] * 2) + 1]
                    /\ pc' = [pc EXCEPT ![self] = "calcBits"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    insertRes, resultsForGet, deleteRes, 
                                    processWriterState, processReaderState, 
                                    values, stack, i, cur, istack, current_d, 
                                    key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                    recStack1_, ret, depth_, res, calc, 
                                    maxDepth, l, r, gNode, buf_, rmax, links, 
                                    recStack_s, mid_s, val, child, lastNode, 
                                    bits_, depth_s, ln, rn, current, sibling, 
                                    keyL, keyR, buf, bufLink, p_d, tempL, 
                                    tempR, bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, recStack, answer, tmp, tmp2, l_, r_, 
                                    mid, sibling_, keyL_, keyR_, recStack1, 
                                    key_g, startValue, depth_g, bits_g, key, 
                                    start, p, bits, depth, states >>

setRnValue2(self) == /\ pc[self] = "setRnValue2"
                     /\ nodeArr' = [nodeArr EXCEPT ![rn[self]].value = val[self]]
                     /\ pc' = [pc EXCEPT ![self] = "setRnHasValue"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     insertRes, resultsForGet, deleteRes, 
                                     processWriterState, processReaderState, 
                                     values, stack, i, cur, istack, current_d, 
                                     key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                     recStack1_, ret, depth_, res, calc, 
                                     maxDepth, l, r, gNode, buf_, rmax, links, 
                                     recStack_s, mid_s, val, child, lastNode, 
                                     bits_, depth_s, newBits, movedBits, ln, 
                                     rn, current, sibling, keyL, keyR, buf, 
                                     bufLink, p_d, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

setRnHasValue(self) == /\ pc[self] = "setRnHasValue"
                       /\ nodeArr' = [nodeArr EXCEPT ![rn[self]].hasValue = TRUE]
                       /\ pc' = [pc EXCEPT ![self] = "setLnValue2"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       insertRes, resultsForGet, deleteRes, 
                                       processWriterState, processReaderState, 
                                       values, stack, i, cur, istack, 
                                       current_d, key_, tmp_, tmp1, mid_, p_, 
                                       p1, recStack_, recStack1_, ret, depth_, 
                                       res, calc, maxDepth, l, r, gNode, buf_, 
                                       rmax, links, recStack_s, mid_s, val, 
                                       child, lastNode, bits_, depth_s, 
                                       newBits, movedBits, ln, rn, current, 
                                       sibling, keyL, keyR, buf, bufLink, p_d, 
                                       tempL, tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

setLnValue2(self) == /\ pc[self] = "setLnValue2"
                     /\ nodeArr' = [nodeArr EXCEPT ![ln[self]].value = nodeArr[lastNode[self]].value]
                     /\ newBits' = [newBits EXCEPT ![self] = newBits[self] * 2 + 1]
                     /\ movedBits' = [movedBits EXCEPT ![self] = movedBits[self] * 2]
                     /\ pc' = [pc EXCEPT ![self] = "calcBits"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     insertRes, resultsForGet, deleteRes, 
                                     processWriterState, processReaderState, 
                                     values, stack, i, cur, istack, current_d, 
                                     key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                     recStack1_, ret, depth_, res, calc, 
                                     maxDepth, l, r, gNode, buf_, rmax, links, 
                                     recStack_s, mid_s, val, child, lastNode, 
                                     bits_, depth_s, ln, rn, current, sibling, 
                                     keyL, keyR, buf, bufLink, p_d, tempL, 
                                     tempR, bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

calcBits(self) == /\ pc[self] = "calcBits"
                  /\ movedBits' = [movedBits EXCEPT ![self] = shiftL(movedBits[self], GNodeDepth - depth_s[self])]
                  /\ newBits' = [newBits EXCEPT ![self] = shiftL(newBits[self], GNodeDepth - depth_s[self])]
                  /\ pc' = [pc EXCEPT ![self] = "setLinks1"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  ln, rn, current, sibling, keyL, keyR, buf, 
                                  bufLink, p_d, tempL, tempR, bits_d, depth_d, 
                                  key_i, stackData, keyRNext, oldCurrent, 
                                  parent, keyLNext, links_, oldSibling, 
                                  current_, rev, bits_i, depth_i, p_i, temp, 
                                  scanRes, t, array, split, recStack, answer, 
                                  tmp, tmp2, l_, r_, mid, sibling_, keyL_, 
                                  keyR_, recStack1, key_g, startValue, depth_g, 
                                  bits_g, key, start, p, bits, depth, states >>

setLinks1(self) == /\ pc[self] = "setLinks1"
                   /\ gNodeArr' = [gNodeArr EXCEPT ![gNode[self]].links[movedBits[self]] = gNodeArr[gNode[self]].links[bits_[self]]]
                   /\ pc' = [pc EXCEPT ![self] = "setLinks2"]
                   /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                   insertRes, resultsForGet, deleteRes, 
                                   processWriterState, processReaderState, 
                                   values, stack, i, cur, istack, current_d, 
                                   key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                   recStack1_, ret, depth_, res, calc, 
                                   maxDepth, l, r, gNode, buf_, rmax, links, 
                                   recStack_s, mid_s, val, child, lastNode, 
                                   bits_, depth_s, newBits, movedBits, ln, rn, 
                                   current, sibling, keyL, keyR, buf, bufLink, 
                                   p_d, tempL, tempR, bits_d, depth_d, key_i, 
                                   stackData, keyRNext, oldCurrent, parent, 
                                   keyLNext, links_, oldSibling, current_, rev, 
                                   bits_i, depth_i, p_i, temp, scanRes, t, 
                                   array, split, recStack, answer, tmp, tmp2, 
                                   l_, r_, mid, sibling_, keyL_, keyR_, 
                                   recStack1, key_g, startValue, depth_g, 
                                   bits_g, key, start, p, bits, depth, states >>

setLinks2(self) == /\ pc[self] = "setLinks2"
                   /\ gNodeArr' = [gNodeArr EXCEPT ![gNode[self]].links[newBits[self]] = child[self]]
                   /\ pc' = [pc EXCEPT ![self] = "callFillInnerNode1"]
                   /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                   insertRes, resultsForGet, deleteRes, 
                                   processWriterState, processReaderState, 
                                   values, stack, i, cur, istack, current_d, 
                                   key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                   recStack1_, ret, depth_, res, calc, 
                                   maxDepth, l, r, gNode, buf_, rmax, links, 
                                   recStack_s, mid_s, val, child, lastNode, 
                                   bits_, depth_s, newBits, movedBits, ln, rn, 
                                   current, sibling, keyL, keyR, buf, bufLink, 
                                   p_d, tempL, tempR, bits_d, depth_d, key_i, 
                                   stackData, keyRNext, oldCurrent, parent, 
                                   keyLNext, links_, oldSibling, current_, rev, 
                                   bits_i, depth_i, p_i, temp, scanRes, t, 
                                   array, split, recStack, answer, tmp, tmp2, 
                                   l_, r_, mid, sibling_, keyL_, keyR_, 
                                   recStack1, key_g, startValue, depth_g, 
                                   bits_g, key, start, p, bits, depth, states >>

callFillInnerNode1(self) == /\ pc[self] = "callFillInnerNode1"
                            /\ recStack_s' = [recStack_s EXCEPT ![self] = Append(recStack_s[self], <<p[self], l[self], r[self], FALSE>>)]
                            /\ r' = [r EXCEPT ![self] = mid_s[self] - 1]
                            /\ pc' = [pc EXCEPT ![self] = "checkBounds"]
                            /\ UNCHANGED << root, hasRoot, globalLock, 
                                            gNodeArr, nodeArr, insertRes, 
                                            resultsForGet, deleteRes, 
                                            processWriterState, 
                                            processReaderState, values, stack, 
                                            i, cur, istack, current_d, key_, 
                                            tmp_, tmp1, mid_, p_, p1, 
                                            recStack_, recStack1_, ret, depth_, 
                                            res, calc, maxDepth, l, gNode, 
                                            buf_, rmax, links, mid_s, val, 
                                            child, lastNode, bits_, depth_s, 
                                            newBits, movedBits, ln, rn, 
                                            current, sibling, keyL, keyR, buf, 
                                            bufLink, p_d, tempL, tempR, bits_d, 
                                            depth_d, key_i, stackData, 
                                            keyRNext, oldCurrent, parent, 
                                            keyLNext, links_, oldSibling, 
                                            current_, rev, bits_i, depth_i, 
                                            p_i, temp, scanRes, t, array, 
                                            split, recStack, answer, tmp, tmp2, 
                                            l_, r_, mid, sibling_, keyL_, 
                                            keyR_, recStack1, key_g, 
                                            startValue, depth_g, bits_g, key, 
                                            start, p, bits, depth, states >>

callFillInnerNode2(self) == /\ pc[self] = "callFillInnerNode2"
                            /\ recStack_s' = [recStack_s EXCEPT ![self] = Append(recStack_s[self], <<p[self], l[self], r[self], TRUE>>)]
                            /\ l' = [l EXCEPT ![self] = mid_s[self] + 1]
                            /\ pc' = [pc EXCEPT ![self] = "checkBounds"]
                            /\ UNCHANGED << root, hasRoot, globalLock, 
                                            gNodeArr, nodeArr, insertRes, 
                                            resultsForGet, deleteRes, 
                                            processWriterState, 
                                            processReaderState, values, stack, 
                                            i, cur, istack, current_d, key_, 
                                            tmp_, tmp1, mid_, p_, p1, 
                                            recStack_, recStack1_, ret, depth_, 
                                            res, calc, maxDepth, r, gNode, 
                                            buf_, rmax, links, mid_s, val, 
                                            child, lastNode, bits_, depth_s, 
                                            newBits, movedBits, ln, rn, 
                                            current, sibling, keyL, keyR, buf, 
                                            bufLink, p_d, tempL, tempR, bits_d, 
                                            depth_d, key_i, stackData, 
                                            keyRNext, oldCurrent, parent, 
                                            keyLNext, links_, oldSibling, 
                                            current_, rev, bits_i, depth_i, 
                                            p_i, temp, scanRes, t, array, 
                                            split, recStack, answer, tmp, tmp2, 
                                            l_, r_, mid, sibling_, keyL_, 
                                            keyR_, recStack1, key_g, 
                                            startValue, depth_g, bits_g, key, 
                                            start, p, bits, depth, states >>

checkRecSize15(self) == /\ pc[self] = "checkRecSize15"
                        /\ IF Len(recStack_s[self]) = 0
                              THEN /\ pc' = [pc EXCEPT ![self] = Head(stack[self]).pc]
                                   /\ recStack_s' = [recStack_s EXCEPT ![self] = Head(stack[self]).recStack_s]
                                   /\ mid_s' = [mid_s EXCEPT ![self] = Head(stack[self]).mid_s]
                                   /\ val' = [val EXCEPT ![self] = Head(stack[self]).val]
                                   /\ child' = [child EXCEPT ![self] = Head(stack[self]).child]
                                   /\ lastNode' = [lastNode EXCEPT ![self] = Head(stack[self]).lastNode]
                                   /\ bits_' = [bits_ EXCEPT ![self] = Head(stack[self]).bits_]
                                   /\ depth_s' = [depth_s EXCEPT ![self] = Head(stack[self]).depth_s]
                                   /\ newBits' = [newBits EXCEPT ![self] = Head(stack[self]).newBits]
                                   /\ movedBits' = [movedBits EXCEPT ![self] = Head(stack[self]).movedBits]
                                   /\ ln' = [ln EXCEPT ![self] = Head(stack[self]).ln]
                                   /\ rn' = [rn EXCEPT ![self] = Head(stack[self]).rn]
                                   /\ l' = [l EXCEPT ![self] = Head(stack[self]).l]
                                   /\ r' = [r EXCEPT ![self] = Head(stack[self]).r]
                                   /\ gNode' = [gNode EXCEPT ![self] = Head(stack[self]).gNode]
                                   /\ buf_' = [buf_ EXCEPT ![self] = Head(stack[self]).buf_]
                                   /\ rmax' = [rmax EXCEPT ![self] = Head(stack[self]).rmax]
                                   /\ links' = [links EXCEPT ![self] = Head(stack[self]).links]
                                   /\ stack' = [stack EXCEPT ![self] = Tail(stack[self])]
                              ELSE /\ pc' = [pc EXCEPT ![self] = "goToRet15"]
                                   /\ UNCHANGED << stack, l, r, gNode, buf_, 
                                                   rmax, links, recStack_s, 
                                                   mid_s, val, child, lastNode, 
                                                   bits_, depth_s, newBits, 
                                                   movedBits, ln, rn >>
                        /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                        nodeArr, insertRes, resultsForGet, 
                                        deleteRes, processWriterState, 
                                        processReaderState, values, i, cur, 
                                        istack, current_d, key_, tmp_, tmp1, 
                                        mid_, p_, p1, recStack_, recStack1_, 
                                        ret, depth_, res, calc, maxDepth, 
                                        current, sibling, keyL, keyR, buf, 
                                        bufLink, p_d, tempL, tempR, bits_d, 
                                        depth_d, key_i, stackData, keyRNext, 
                                        oldCurrent, parent, keyLNext, links_, 
                                        oldSibling, current_, rev, bits_i, 
                                        depth_i, p_i, temp, scanRes, t, array, 
                                        split, recStack, answer, tmp, tmp2, l_, 
                                        r_, mid, sibling_, keyL_, keyR_, 
                                        recStack1, key_g, startValue, depth_g, 
                                        bits_g, key, start, p, bits, depth, 
                                        states >>

goToRet15(self) == /\ pc[self] = "goToRet15"
                   /\ tmp' = [tmp EXCEPT ![self] = Tail(recStack_s[self])]
                   /\ recStack_s' = [recStack_s EXCEPT ![self] = SubSeq(recStack_s[self], 0, Len(recStack_s[self]) - 1)]
                   /\ p' = [p EXCEPT ![self] = tmp'[self][0]]
                   /\ l' = [l EXCEPT ![self] = tmp'[self][1]]
                   /\ r' = [r EXCEPT ![self] = tmp'[self][2]]
                   /\ IF tmp'[self][3]
                         THEN /\ pc' = [pc EXCEPT ![self] = "checkRecSize15"]
                         ELSE /\ pc' = [pc EXCEPT ![self] = "callFillInnerNode2"]
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, resultsForGet, 
                                   deleteRes, processWriterState, 
                                   processReaderState, values, stack, i, cur, 
                                   istack, current_d, key_, tmp_, tmp1, mid_, 
                                   p_, p1, recStack_, recStack1_, ret, depth_, 
                                   res, calc, maxDepth, gNode, buf_, rmax, 
                                   links, mid_s, val, child, lastNode, bits_, 
                                   depth_s, newBits, movedBits, ln, rn, 
                                   current, sibling, keyL, keyR, buf, bufLink, 
                                   p_d, tempL, tempR, bits_d, depth_d, key_i, 
                                   stackData, keyRNext, oldCurrent, parent, 
                                   keyLNext, links_, oldSibling, current_, rev, 
                                   bits_i, depth_i, p_i, temp, scanRes, t, 
                                   array, split, recStack, answer, tmp2, l_, 
                                   r_, mid, sibling_, keyL_, keyR_, recStack1, 
                                   key_g, startValue, depth_g, bits_g, key, 
                                   start, bits, depth, states >>

smartFillValParentLo(self) == startFillValParentLo(self)
                                 \/ checkBounds(self) \/ calcMid(self)
                                 \/ detailedNodeSearchStart(self)
                                 \/ mainCycleStart(self)
                                 \/ checkHasValue(self)
                                 \/ detailedNodeSearchCalcs(self)
                                 \/ checkKey(self) \/ dnsCheckLeft(self)
                                 \/ dnsGetLeft(self) \/ dnsCheckRight(self)
                                 \/ dnsGetRight(self) \/ updateBits(self)
                                 \/ prepareBitsAndDepth(self)
                                 \/ getLastNodeLeft(self)
                                 \/ getLastNodeRight(self)
                                 \/ checkLastNodeValue(self)
                                 \/ setLnValue(self) \/ setLnHasValue(self)
                                 \/ setRnValue(self) \/ setRnValue2(self)
                                 \/ setRnHasValue(self)
                                 \/ setLnValue2(self) \/ calcBits(self)
                                 \/ setLinks1(self) \/ setLinks2(self)
                                 \/ callFillInnerNode1(self)
                                 \/ callFillInnerNode2(self)
                                 \/ checkRecSize15(self) \/ goToRet15(self)

detailedNodeSearchStart2(self) == /\ pc[self] = "detailedNodeSearchStart2"
                                  /\ p_d' = [p_d EXCEPT ![self] = gNodeArr[current[self]].root]
                                  /\ lastNode' = [lastNode EXCEPT ![self] = p_d'[self]]
                                  /\ bits_d' = [bits_d EXCEPT ![self] = 1]
                                  /\ depth_d' = [depth_d EXCEPT ![self] = 0]
                                  /\ pc' = [pc EXCEPT ![self] = "mainCycleStart2"]
                                  /\ UNCHANGED << root, hasRoot, globalLock, 
                                                  gNodeArr, nodeArr, insertRes, 
                                                  resultsForGet, deleteRes, 
                                                  processWriterState, 
                                                  processReaderState, values, 
                                                  stack, i, cur, istack, 
                                                  current_d, key_, tmp_, tmp1, 
                                                  mid_, p_, p1, recStack_, 
                                                  recStack1_, ret, depth_, res, 
                                                  calc, maxDepth, l, r, gNode, 
                                                  buf_, rmax, links, 
                                                  recStack_s, mid_s, val, 
                                                  child, bits_, depth_s, 
                                                  newBits, movedBits, ln, rn, 
                                                  current, sibling, keyL, keyR, 
                                                  buf, bufLink, tempL, tempR, 
                                                  key_i, stackData, keyRNext, 
                                                  oldCurrent, parent, keyLNext, 
                                                  links_, oldSibling, current_, 
                                                  rev, bits_i, depth_i, p_i, 
                                                  temp, scanRes, t, array, 
                                                  split, recStack, answer, tmp, 
                                                  tmp2, l_, r_, mid, sibling_, 
                                                  keyL_, keyR_, recStack1, 
                                                  key_g, startValue, depth_g, 
                                                  bits_g, key, start, p, bits, 
                                                  depth, states >>

mainCycleStart2(self) == /\ pc[self] = "mainCycleStart2"
                         /\ pc' = [pc EXCEPT ![self] = "checkHasValue2"]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, answer, tmp, tmp2, l_, r_, 
                                         mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, start, p, bits, depth, 
                                         states >>

checkHasValue2(self) == /\ pc[self] = "checkHasValue2"
                        /\ IF ~nodeArr[p_d[self]].hasValue
                              THEN /\ pc' = [pc EXCEPT ![self] = "updateBits2"]
                              ELSE /\ pc' = [pc EXCEPT ![self] = "detailedNodeSearchCalcs2"]
                        /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                        nodeArr, insertRes, resultsForGet, 
                                        deleteRes, processWriterState, 
                                        processReaderState, values, stack, i, 
                                        cur, istack, current_d, key_, tmp_, 
                                        tmp1, mid_, p_, p1, recStack_, 
                                        recStack1_, ret, depth_, res, calc, 
                                        maxDepth, l, r, gNode, buf_, rmax, 
                                        links, recStack_s, mid_s, val, child, 
                                        lastNode, bits_, depth_s, newBits, 
                                        movedBits, ln, rn, current, sibling, 
                                        keyL, keyR, buf, bufLink, p_d, tempL, 
                                        tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        current_, rev, bits_i, depth_i, p_i, 
                                        temp, scanRes, t, array, split, 
                                        recStack, answer, tmp, tmp2, l_, r_, 
                                        mid, sibling_, keyL_, keyR_, recStack1, 
                                        key_g, startValue, depth_g, bits_g, 
                                        key, start, p, bits, depth, states >>

detailedNodeSearchCalcs2(self) == /\ pc[self] = "detailedNodeSearchCalcs2"
                                  /\ depth_d' = [depth_d EXCEPT ![self] = depth_d[self] + 1]
                                  /\ bits_d' = [bits_d EXCEPT ![self] = bits_d[self] * 2]
                                  /\ lastNode' = [lastNode EXCEPT ![self] = p_d[self]]
                                  /\ pc' = [pc EXCEPT ![self] = "checkKey2"]
                                  /\ UNCHANGED << root, hasRoot, globalLock, 
                                                  gNodeArr, nodeArr, insertRes, 
                                                  resultsForGet, deleteRes, 
                                                  processWriterState, 
                                                  processReaderState, values, 
                                                  stack, i, cur, istack, 
                                                  current_d, key_, tmp_, tmp1, 
                                                  mid_, p_, p1, recStack_, 
                                                  recStack1_, ret, depth_, res, 
                                                  calc, maxDepth, l, r, gNode, 
                                                  buf_, rmax, links, 
                                                  recStack_s, mid_s, val, 
                                                  child, bits_, depth_s, 
                                                  newBits, movedBits, ln, rn, 
                                                  current, sibling, keyL, keyR, 
                                                  buf, bufLink, p_d, tempL, 
                                                  tempR, key_i, stackData, 
                                                  keyRNext, oldCurrent, parent, 
                                                  keyLNext, links_, oldSibling, 
                                                  current_, rev, bits_i, 
                                                  depth_i, p_i, temp, scanRes, 
                                                  t, array, split, recStack, 
                                                  answer, tmp, tmp2, l_, r_, 
                                                  mid, sibling_, keyL_, keyR_, 
                                                  recStack1, key_g, startValue, 
                                                  depth_g, bits_g, key, start, 
                                                  p, bits, depth, states >>

checkKey2(self) == /\ pc[self] = "checkKey2"
                   /\ IF keyR[self] < nodeArr[p_d[self]].value
                         THEN /\ pc' = [pc EXCEPT ![self] = "dnsCheckLeft2"]
                         ELSE /\ pc' = [pc EXCEPT ![self] = "dnsCheckRight2"]
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, resultsForGet, 
                                   deleteRes, processWriterState, 
                                   processReaderState, values, stack, i, cur, 
                                   istack, current_d, key_, tmp_, tmp1, mid_, 
                                   p_, p1, recStack_, recStack1_, ret, depth_, 
                                   res, calc, maxDepth, l, r, gNode, buf_, 
                                   rmax, links, recStack_s, mid_s, val, child, 
                                   lastNode, bits_, depth_s, newBits, 
                                   movedBits, ln, rn, current, sibling, keyL, 
                                   keyR, buf, bufLink, p_d, tempL, tempR, 
                                   bits_d, depth_d, key_i, stackData, keyRNext, 
                                   oldCurrent, parent, keyLNext, links_, 
                                   oldSibling, current_, rev, bits_i, depth_i, 
                                   p_i, temp, scanRes, t, array, split, 
                                   recStack, answer, tmp, tmp2, l_, r_, mid, 
                                   sibling_, keyL_, keyR_, recStack1, key_g, 
                                   startValue, depth_g, bits_g, key, start, p, 
                                   bits, depth, states >>

dnsCheckLeft2(self) == /\ pc[self] = "dnsCheckLeft2"
                       /\ IF ~nodeArr[p_d[self]].hasLeft
                             THEN /\ pc' = [pc EXCEPT ![self] = "updateBits2"]
                             ELSE /\ pc' = [pc EXCEPT ![self] = "dnsGetLeft2"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

dnsGetLeft2(self) == /\ pc[self] = "dnsGetLeft2"
                     /\ p_d' = [p_d EXCEPT ![self] = nodeArr[p_d[self]].left]
                     /\ pc' = [pc EXCEPT ![self] = "mainCycleStart2"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

dnsCheckRight2(self) == /\ pc[self] = "dnsCheckRight2"
                        /\ IF ~nodeArr[p_d[self]].hasRight
                              THEN /\ pc' = [pc EXCEPT ![self] = "updateBits2"]
                              ELSE /\ pc' = [pc EXCEPT ![self] = "dnsGetRight2"]
                        /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                        nodeArr, insertRes, resultsForGet, 
                                        deleteRes, processWriterState, 
                                        processReaderState, values, stack, i, 
                                        cur, istack, current_d, key_, tmp_, 
                                        tmp1, mid_, p_, p1, recStack_, 
                                        recStack1_, ret, depth_, res, calc, 
                                        maxDepth, l, r, gNode, buf_, rmax, 
                                        links, recStack_s, mid_s, val, child, 
                                        lastNode, bits_, depth_s, newBits, 
                                        movedBits, ln, rn, current, sibling, 
                                        keyL, keyR, buf, bufLink, p_d, tempL, 
                                        tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        current_, rev, bits_i, depth_i, p_i, 
                                        temp, scanRes, t, array, split, 
                                        recStack, answer, tmp, tmp2, l_, r_, 
                                        mid, sibling_, keyL_, keyR_, recStack1, 
                                        key_g, startValue, depth_g, bits_g, 
                                        key, start, p, bits, depth, states >>

dnsGetRight2(self) == /\ pc[self] = "dnsGetRight2"
                      /\ p_d' = [p_d EXCEPT ![self] = nodeArr[p_d[self]].right]
                      /\ bits_d' = [bits_d EXCEPT ![self] = bits_d[self] + 1]
                      /\ pc' = [pc EXCEPT ![self] = "mainCycleStart2"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, tempL, 
                                      tempR, depth_d, key_i, stackData, 
                                      keyRNext, oldCurrent, parent, keyLNext, 
                                      links_, oldSibling, current_, rev, 
                                      bits_i, depth_i, p_i, temp, scanRes, t, 
                                      array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

updateBits2(self) == /\ pc[self] = "updateBits2"
                     /\ bits_d' = [bits_d EXCEPT ![self] = bits_d[self] \div 2]
                     /\ pc' = [pc EXCEPT ![self] = "checkLeftInInsertNode"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

checkLeftInInsertNode(self) == /\ pc[self] = "checkLeftInInsertNode"
                               /\ IF ~nodeArr[lastNode[self]].hasLeft
                                     THEN /\ pc' = [pc EXCEPT ![self] = "startFillBufLo2"]
                                     ELSE /\ pc' = [pc EXCEPT ![self] = "getTempL"]
                               /\ UNCHANGED << root, hasRoot, globalLock, 
                                               gNodeArr, nodeArr, insertRes, 
                                               resultsForGet, deleteRes, 
                                               processWriterState, 
                                               processReaderState, values, 
                                               stack, i, cur, istack, 
                                               current_d, key_, tmp_, tmp1, 
                                               mid_, p_, p1, recStack_, 
                                               recStack1_, ret, depth_, res, 
                                               calc, maxDepth, l, r, gNode, 
                                               buf_, rmax, links, recStack_s, 
                                               mid_s, val, child, lastNode, 
                                               bits_, depth_s, newBits, 
                                               movedBits, ln, rn, current, 
                                               sibling, keyL, keyR, buf, 
                                               bufLink, p_d, tempL, tempR, 
                                               bits_d, depth_d, key_i, 
                                               stackData, keyRNext, oldCurrent, 
                                               parent, keyLNext, links_, 
                                               oldSibling, current_, rev, 
                                               bits_i, depth_i, p_i, temp, 
                                               scanRes, t, array, split, 
                                               recStack, answer, tmp, tmp2, l_, 
                                               r_, mid, sibling_, keyL_, keyR_, 
                                               recStack1, key_g, startValue, 
                                               depth_g, bits_g, key, start, p, 
                                               bits, depth, states >>

startFillBufLo2(self) == /\ pc[self] = "startFillBufLo2"
                         /\ p_d' = [p_d EXCEPT ![self] = gNodeArr[current[self]].root]
                         /\ answer' = [answer EXCEPT ![self] = <<>>]
                         /\ pc' = [pc EXCEPT ![self] = "fboCheckLeftRight4"]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, tmp, tmp2, l_, r_, mid, 
                                         sibling_, keyL_, keyR_, recStack1, 
                                         key_g, startValue, depth_g, bits_g, 
                                         key, start, p, bits, depth, states >>

fboCheckLeftRight4(self) == /\ pc[self] = "fboCheckLeftRight4"
                            /\ IF nodeArr[p_d[self]].hasLeft /\ nodeArr[p_d[self]].hasRight
                                  THEN /\ pc' = [pc EXCEPT ![self] = "fboGetleft2"]
                                  ELSE /\ pc' = [pc EXCEPT ![self] = "fboIncValue2"]
                            /\ UNCHANGED << root, hasRoot, globalLock, 
                                            gNodeArr, nodeArr, insertRes, 
                                            resultsForGet, deleteRes, 
                                            processWriterState, 
                                            processReaderState, values, stack, 
                                            i, cur, istack, current_d, key_, 
                                            tmp_, tmp1, mid_, p_, p1, 
                                            recStack_, recStack1_, ret, depth_, 
                                            res, calc, maxDepth, l, r, gNode, 
                                            buf_, rmax, links, recStack_s, 
                                            mid_s, val, child, lastNode, bits_, 
                                            depth_s, newBits, movedBits, ln, 
                                            rn, current, sibling, keyL, keyR, 
                                            buf, bufLink, p_d, tempL, tempR, 
                                            bits_d, depth_d, key_i, stackData, 
                                            keyRNext, oldCurrent, parent, 
                                            keyLNext, links_, oldSibling, 
                                            current_, rev, bits_i, depth_i, 
                                            p_i, temp, scanRes, t, array, 
                                            split, recStack, answer, tmp, tmp2, 
                                            l_, r_, mid, sibling_, keyL_, 
                                            keyR_, recStack1, key_g, 
                                            startValue, depth_g, bits_g, key, 
                                            start, p, bits, depth, states >>

fboGetleft2(self) == /\ pc[self] = "fboGetleft2"
                     /\ l' = [l EXCEPT ![self] = nodeArr[p_d[self]].left]
                     /\ pc' = [pc EXCEPT ![self] = "fboGetRight2"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

fboGetRight2(self) == /\ pc[self] = "fboGetRight2"
                      /\ r' = [r EXCEPT ![self] = nodeArr[p_d[self]].right]
                      /\ pc' = [pc EXCEPT ![self] = "fboCheckLeftRight5"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

fboCheckLeftRight5(self) == /\ pc[self] = "fboCheckLeftRight5"
                            /\ recStack' = [recStack EXCEPT ![self] = Append(recStack[self], <<p_d[self], FALSE>>)]
                            /\ p_d' = [p_d EXCEPT ![self] = l[self]]
                            /\ pc' = [pc EXCEPT ![self] = "fboCheckLeftRight4"]
                            /\ UNCHANGED << root, hasRoot, globalLock, 
                                            gNodeArr, nodeArr, insertRes, 
                                            resultsForGet, deleteRes, 
                                            processWriterState, 
                                            processReaderState, values, stack, 
                                            i, cur, istack, current_d, key_, 
                                            tmp_, tmp1, mid_, p_, p1, 
                                            recStack_, recStack1_, ret, depth_, 
                                            res, calc, maxDepth, l, r, gNode, 
                                            buf_, rmax, links, recStack_s, 
                                            mid_s, val, child, lastNode, bits_, 
                                            depth_s, newBits, movedBits, ln, 
                                            rn, current, sibling, keyL, keyR, 
                                            buf, bufLink, tempL, tempR, bits_d, 
                                            depth_d, key_i, stackData, 
                                            keyRNext, oldCurrent, parent, 
                                            keyLNext, links_, oldSibling, 
                                            current_, rev, bits_i, depth_i, 
                                            p_i, temp, scanRes, t, array, 
                                            split, answer, tmp, tmp2, l_, r_, 
                                            mid, sibling_, keyL_, keyR_, 
                                            recStack1, key_g, startValue, 
                                            depth_g, bits_g, key, start, p, 
                                            bits, depth, states >>

fboCheckLeftRight6(self) == /\ pc[self] = "fboCheckLeftRight6"
                            /\ recStack' = [recStack EXCEPT ![self] = Append(recStack[self], <<p_d[self], TRUE>>)]
                            /\ p_d' = [p_d EXCEPT ![self] = r[self]]
                            /\ pc' = [pc EXCEPT ![self] = "fboCheckLeftRight4"]
                            /\ UNCHANGED << root, hasRoot, globalLock, 
                                            gNodeArr, nodeArr, insertRes, 
                                            resultsForGet, deleteRes, 
                                            processWriterState, 
                                            processReaderState, values, stack, 
                                            i, cur, istack, current_d, key_, 
                                            tmp_, tmp1, mid_, p_, p1, 
                                            recStack_, recStack1_, ret, depth_, 
                                            res, calc, maxDepth, l, r, gNode, 
                                            buf_, rmax, links, recStack_s, 
                                            mid_s, val, child, lastNode, bits_, 
                                            depth_s, newBits, movedBits, ln, 
                                            rn, current, sibling, keyL, keyR, 
                                            buf, bufLink, tempL, tempR, bits_d, 
                                            depth_d, key_i, stackData, 
                                            keyRNext, oldCurrent, parent, 
                                            keyLNext, links_, oldSibling, 
                                            current_, rev, bits_i, depth_i, 
                                            p_i, temp, scanRes, t, array, 
                                            split, answer, tmp, tmp2, l_, r_, 
                                            mid, sibling_, keyL_, keyR_, 
                                            recStack1, key_g, startValue, 
                                            depth_g, bits_g, key, start, p, 
                                            bits, depth, states >>

fboIncValue2(self) == /\ pc[self] = "fboIncValue2"
                      /\ IF ~nodeArr[p_d[self]].deleted
                            THEN /\ answer' = [answer EXCEPT ![self] = Append(answer[self], nodeArr[p_d[self]].value)]
                            ELSE /\ TRUE
                                 /\ UNCHANGED answer
                      /\ pc' = [pc EXCEPT ![self] = "fboCheckRetSize2"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, tmp, tmp2, l_, 
                                      r_, mid, sibling_, keyL_, keyR_, 
                                      recStack1, key_g, startValue, depth_g, 
                                      bits_g, key, start, p, bits, depth, 
                                      states >>

fboCheckRetSize2(self) == /\ pc[self] = "fboCheckRetSize2"
                          /\ IF Len(recStack[self]) = 0
                                THEN /\ pc' = [pc EXCEPT ![self] = "fillLinkBuf"]
                                ELSE /\ pc' = [pc EXCEPT ![self] = "fboGoToRet4"]
                          /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                          nodeArr, insertRes, resultsForGet, 
                                          deleteRes, processWriterState, 
                                          processReaderState, values, stack, i, 
                                          cur, istack, current_d, key_, tmp_, 
                                          tmp1, mid_, p_, p1, recStack_, 
                                          recStack1_, ret, depth_, res, calc, 
                                          maxDepth, l, r, gNode, buf_, rmax, 
                                          links, recStack_s, mid_s, val, child, 
                                          lastNode, bits_, depth_s, newBits, 
                                          movedBits, ln, rn, current, sibling, 
                                          keyL, keyR, buf, bufLink, p_d, tempL, 
                                          tempR, bits_d, depth_d, key_i, 
                                          stackData, keyRNext, oldCurrent, 
                                          parent, keyLNext, links_, oldSibling, 
                                          current_, rev, bits_i, depth_i, p_i, 
                                          temp, scanRes, t, array, split, 
                                          recStack, answer, tmp, tmp2, l_, r_, 
                                          mid, sibling_, keyL_, keyR_, 
                                          recStack1, key_g, startValue, 
                                          depth_g, bits_g, key, start, p, bits, 
                                          depth, states >>

fboGoToRet4(self) == /\ pc[self] = "fboGoToRet4"
                     /\ tmp' = [tmp EXCEPT ![self] = Tail(recStack[self])]
                     /\ recStack' = [recStack EXCEPT ![self] = SubSeq(recStack[self], 0, Len(recStack[self]) - 1)]
                     /\ p_d' = [p_d EXCEPT ![self] = tmp'[self][0]]
                     /\ IF tmp'[self][1]
                           THEN /\ pc' = [pc EXCEPT ![self] = "fboCheckRetSize2"]
                           ELSE /\ pc' = [pc EXCEPT ![self] = "fboCheckLeftRight6"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, answer, tmp2, l_, r_, mid, 
                                     sibling_, keyL_, keyR_, recStack1, key_g, 
                                     startValue, depth_g, bits_g, key, start, 
                                     p, bits, depth, states >>

fillLinkBuf(self) == /\ pc[self] = "fillLinkBuf"
                     /\ links' = [links EXCEPT ![self] = gNodeArr[current[self]].links]
                     /\ pc' = [pc EXCEPT ![self] = "startUpdateCurrent_"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, recStack_s, mid_s, val, child, 
                                     lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

startUpdateCurrent_(self) == /\ pc[self] = "startUpdateCurrent_"
                             /\ gNodeArr' = [gNodeArr EXCEPT ![current[self]].rev = gNodeArr[current[self]]]
                             /\ mid' = [mid EXCEPT ![self] = Len(answer[self]) \div 2]
                             /\ pc' = [pc EXCEPT ![self] = "updateValues1"]
                             /\ UNCHANGED << root, hasRoot, globalLock, 
                                             nodeArr, insertRes, resultsForGet, 
                                             deleteRes, processWriterState, 
                                             processReaderState, values, stack, 
                                             i, cur, istack, current_d, key_, 
                                             tmp_, tmp1, mid_, p_, p1, 
                                             recStack_, recStack1_, ret, 
                                             depth_, res, calc, maxDepth, l, r, 
                                             gNode, buf_, rmax, links, 
                                             recStack_s, mid_s, val, child, 
                                             lastNode, bits_, depth_s, newBits, 
                                             movedBits, ln, rn, current, 
                                             sibling, keyL, keyR, buf, bufLink, 
                                             p_d, tempL, tempR, bits_d, 
                                             depth_d, key_i, stackData, 
                                             keyRNext, oldCurrent, parent, 
                                             keyLNext, links_, oldSibling, 
                                             current_, rev, bits_i, depth_i, 
                                             p_i, temp, scanRes, t, array, 
                                             split, recStack, answer, tmp, 
                                             tmp2, l_, r_, sibling_, keyL_, 
                                             keyR_, recStack1, key_g, 
                                             startValue, depth_g, bits_g, key, 
                                             start, p, bits, depth, states >>

updateValues1(self) == /\ pc[self] = "updateValues1"
                       /\ gNodeArr' = [gNodeArr EXCEPT ![current[self]].root = answer[self][mid[self]]]
                       /\ pc' = [pc EXCEPT ![self] = "updateValues2"]
                       /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                       insertRes, resultsForGet, deleteRes, 
                                       processWriterState, processReaderState, 
                                       values, stack, i, cur, istack, 
                                       current_d, key_, tmp_, tmp1, mid_, p_, 
                                       p1, recStack_, recStack1_, ret, depth_, 
                                       res, calc, maxDepth, l, r, gNode, buf_, 
                                       rmax, links, recStack_s, mid_s, val, 
                                       child, lastNode, bits_, depth_s, 
                                       newBits, movedBits, ln, rn, current, 
                                       sibling, keyL, keyR, buf, bufLink, p_d, 
                                       tempL, tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

updateValues2(self) == /\ pc[self] = "updateValues2"
                       /\ gNodeArr' = [gNodeArr EXCEPT ![current[self]].links[0] = links[self][mid[self]]]
                       /\ pc' = [pc EXCEPT ![self] = "smartFillValParentLo1"]
                       /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                       insertRes, resultsForGet, deleteRes, 
                                       processWriterState, processReaderState, 
                                       values, stack, i, cur, istack, 
                                       current_d, key_, tmp_, tmp1, mid_, p_, 
                                       p1, recStack_, recStack1_, ret, depth_, 
                                       res, calc, maxDepth, l, r, gNode, buf_, 
                                       rmax, links, recStack_s, mid_s, val, 
                                       child, lastNode, bits_, depth_s, 
                                       newBits, movedBits, ln, rn, current, 
                                       sibling, keyL, keyR, buf, bufLink, p_d, 
                                       tempL, tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

smartFillValParentLo1(self) == /\ pc[self] = "smartFillValParentLo1"
                               /\ /\ buf_' = [buf_ EXCEPT ![self] = answer[self]]
                                  /\ gNode' = [gNode EXCEPT ![self] = current[self]]
                                  /\ l' = [l EXCEPT ![self] = 0]
                                  /\ links' = [links EXCEPT ![self] = links[self]]
                                  /\ r' = [r EXCEPT ![self] = mid[self] - 1]
                                  /\ rmax' = [rmax EXCEPT ![self] = Len(answer[self])]
                                  /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "smartFillValParentLo",
                                                                           pc        |->  "smartFillValParentLo2",
                                                                           recStack_s |->  recStack_s[self],
                                                                           mid_s     |->  mid_s[self],
                                                                           val       |->  val[self],
                                                                           child     |->  child[self],
                                                                           lastNode  |->  lastNode[self],
                                                                           bits_     |->  bits_[self],
                                                                           depth_s   |->  depth_s[self],
                                                                           newBits   |->  newBits[self],
                                                                           movedBits |->  movedBits[self],
                                                                           ln        |->  ln[self],
                                                                           rn        |->  rn[self],
                                                                           l         |->  l[self],
                                                                           r         |->  r[self],
                                                                           gNode     |->  gNode[self],
                                                                           buf_      |->  buf_[self],
                                                                           rmax      |->  rmax[self],
                                                                           links     |->  links[self] ] >>
                                                                       \o stack[self]]
                               /\ recStack_s' = [recStack_s EXCEPT ![self] = defaultInitValue]
                               /\ mid_s' = [mid_s EXCEPT ![self] = defaultInitValue]
                               /\ val' = [val EXCEPT ![self] = defaultInitValue]
                               /\ child' = [child EXCEPT ![self] = defaultInitValue]
                               /\ lastNode' = [lastNode EXCEPT ![self] = defaultInitValue]
                               /\ bits_' = [bits_ EXCEPT ![self] = defaultInitValue]
                               /\ depth_s' = [depth_s EXCEPT ![self] = defaultInitValue]
                               /\ newBits' = [newBits EXCEPT ![self] = defaultInitValue]
                               /\ movedBits' = [movedBits EXCEPT ![self] = defaultInitValue]
                               /\ ln' = [ln EXCEPT ![self] = defaultInitValue]
                               /\ rn' = [rn EXCEPT ![self] = defaultInitValue]
                               /\ pc' = [pc EXCEPT ![self] = "startFillValParentLo"]
                               /\ UNCHANGED << root, hasRoot, globalLock, 
                                               gNodeArr, nodeArr, insertRes, 
                                               resultsForGet, deleteRes, 
                                               processWriterState, 
                                               processReaderState, values, i, 
                                               cur, istack, current_d, key_, 
                                               tmp_, tmp1, mid_, p_, p1, 
                                               recStack_, recStack1_, ret, 
                                               depth_, res, calc, maxDepth, 
                                               current, sibling, keyL, keyR, 
                                               buf, bufLink, p_d, tempL, tempR, 
                                               bits_d, depth_d, key_i, 
                                               stackData, keyRNext, oldCurrent, 
                                               parent, keyLNext, links_, 
                                               oldSibling, current_, rev, 
                                               bits_i, depth_i, p_i, temp, 
                                               scanRes, t, array, split, 
                                               recStack, answer, tmp, tmp2, l_, 
                                               r_, mid, sibling_, keyL_, keyR_, 
                                               recStack1, key_g, startValue, 
                                               depth_g, bits_g, key, start, p, 
                                               bits, depth, states >>

smartFillValParentLo2(self) == /\ pc[self] = "smartFillValParentLo2"
                               /\ /\ buf_' = [buf_ EXCEPT ![self] = answer[self]]
                                  /\ gNode' = [gNode EXCEPT ![self] = current[self]]
                                  /\ l' = [l EXCEPT ![self] = mid[self] + 1]
                                  /\ links' = [links EXCEPT ![self] = links[self]]
                                  /\ r' = [r EXCEPT ![self] = Len(answer[self])]
                                  /\ rmax' = [rmax EXCEPT ![self] = Len(answer[self])]
                                  /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "smartFillValParentLo",
                                                                           pc        |->  "finishUpdateCurrent2",
                                                                           recStack_s |->  recStack_s[self],
                                                                           mid_s     |->  mid_s[self],
                                                                           val       |->  val[self],
                                                                           child     |->  child[self],
                                                                           lastNode  |->  lastNode[self],
                                                                           bits_     |->  bits_[self],
                                                                           depth_s   |->  depth_s[self],
                                                                           newBits   |->  newBits[self],
                                                                           movedBits |->  movedBits[self],
                                                                           ln        |->  ln[self],
                                                                           rn        |->  rn[self],
                                                                           l         |->  l[self],
                                                                           r         |->  r[self],
                                                                           gNode     |->  gNode[self],
                                                                           buf_      |->  buf_[self],
                                                                           rmax      |->  rmax[self],
                                                                           links     |->  links[self] ] >>
                                                                       \o stack[self]]
                               /\ recStack_s' = [recStack_s EXCEPT ![self] = defaultInitValue]
                               /\ mid_s' = [mid_s EXCEPT ![self] = defaultInitValue]
                               /\ val' = [val EXCEPT ![self] = defaultInitValue]
                               /\ child' = [child EXCEPT ![self] = defaultInitValue]
                               /\ lastNode' = [lastNode EXCEPT ![self] = defaultInitValue]
                               /\ bits_' = [bits_ EXCEPT ![self] = defaultInitValue]
                               /\ depth_s' = [depth_s EXCEPT ![self] = defaultInitValue]
                               /\ newBits' = [newBits EXCEPT ![self] = defaultInitValue]
                               /\ movedBits' = [movedBits EXCEPT ![self] = defaultInitValue]
                               /\ ln' = [ln EXCEPT ![self] = defaultInitValue]
                               /\ rn' = [rn EXCEPT ![self] = defaultInitValue]
                               /\ pc' = [pc EXCEPT ![self] = "startFillValParentLo"]
                               /\ UNCHANGED << root, hasRoot, globalLock, 
                                               gNodeArr, nodeArr, insertRes, 
                                               resultsForGet, deleteRes, 
                                               processWriterState, 
                                               processReaderState, values, i, 
                                               cur, istack, current_d, key_, 
                                               tmp_, tmp1, mid_, p_, p1, 
                                               recStack_, recStack1_, ret, 
                                               depth_, res, calc, maxDepth, 
                                               current, sibling, keyL, keyR, 
                                               buf, bufLink, p_d, tempL, tempR, 
                                               bits_d, depth_d, key_i, 
                                               stackData, keyRNext, oldCurrent, 
                                               parent, keyLNext, links_, 
                                               oldSibling, current_, rev, 
                                               bits_i, depth_i, p_i, temp, 
                                               scanRes, t, array, split, 
                                               recStack, answer, tmp, tmp2, l_, 
                                               r_, mid, sibling_, keyL_, keyR_, 
                                               recStack1, key_g, startValue, 
                                               depth_g, bits_g, key, start, p, 
                                               bits, depth, states >>

finishUpdateCurrent2(self) == /\ pc[self] = "finishUpdateCurrent2"
                              /\ gNodeArr' = [gNodeArr EXCEPT ![current[self]].rev = gNodeArr[current[self]]]
                              /\ pc' = [pc EXCEPT ![self] = "updateCountNode"]
                              /\ UNCHANGED << root, hasRoot, globalLock, 
                                              nodeArr, insertRes, 
                                              resultsForGet, deleteRes, 
                                              processWriterState, 
                                              processReaderState, values, 
                                              stack, i, cur, istack, current_d, 
                                              key_, tmp_, tmp1, mid_, p_, p1, 
                                              recStack_, recStack1_, ret, 
                                              depth_, res, calc, maxDepth, l, 
                                              r, gNode, buf_, rmax, links, 
                                              recStack_s, mid_s, val, child, 
                                              lastNode, bits_, depth_s, 
                                              newBits, movedBits, ln, rn, 
                                              current, sibling, keyL, keyR, 
                                              buf, bufLink, p_d, tempL, tempR, 
                                              bits_d, depth_d, key_i, 
                                              stackData, keyRNext, oldCurrent, 
                                              parent, keyLNext, links_, 
                                              oldSibling, current_, rev, 
                                              bits_i, depth_i, p_i, temp, 
                                              scanRes, t, array, split, 
                                              recStack, answer, tmp, tmp2, l_, 
                                              r_, mid, sibling_, keyL_, keyR_, 
                                              recStack1, key_g, startValue, 
                                              depth_g, bits_g, key, start, p, 
                                              bits, depth, states >>

updateCountNode(self) == /\ pc[self] = "updateCountNode"
                         /\ gNodeArr' = [gNodeArr EXCEPT ![current[self]].countNode = Len(answer[self]) * 2]
                         /\ pc' = [pc EXCEPT ![self] = "detailedNodeSearchStart3"]
                         /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                         insertRes, resultsForGet, deleteRes, 
                                         processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, answer, tmp, tmp2, l_, r_, 
                                         mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, start, p, bits, depth, 
                                         states >>

detailedNodeSearchStart3(self) == /\ pc[self] = "detailedNodeSearchStart3"
                                  /\ p_d' = [p_d EXCEPT ![self] = gNodeArr[current[self]].root]
                                  /\ lastNode' = [lastNode EXCEPT ![self] = p_d'[self]]
                                  /\ bits_d' = [bits_d EXCEPT ![self] = 1]
                                  /\ depth_d' = [depth_d EXCEPT ![self] = 0]
                                  /\ pc' = [pc EXCEPT ![self] = "mainCycleStart3"]
                                  /\ UNCHANGED << root, hasRoot, globalLock, 
                                                  gNodeArr, nodeArr, insertRes, 
                                                  resultsForGet, deleteRes, 
                                                  processWriterState, 
                                                  processReaderState, values, 
                                                  stack, i, cur, istack, 
                                                  current_d, key_, tmp_, tmp1, 
                                                  mid_, p_, p1, recStack_, 
                                                  recStack1_, ret, depth_, res, 
                                                  calc, maxDepth, l, r, gNode, 
                                                  buf_, rmax, links, 
                                                  recStack_s, mid_s, val, 
                                                  child, bits_, depth_s, 
                                                  newBits, movedBits, ln, rn, 
                                                  current, sibling, keyL, keyR, 
                                                  buf, bufLink, tempL, tempR, 
                                                  key_i, stackData, keyRNext, 
                                                  oldCurrent, parent, keyLNext, 
                                                  links_, oldSibling, current_, 
                                                  rev, bits_i, depth_i, p_i, 
                                                  temp, scanRes, t, array, 
                                                  split, recStack, answer, tmp, 
                                                  tmp2, l_, r_, mid, sibling_, 
                                                  keyL_, keyR_, recStack1, 
                                                  key_g, startValue, depth_g, 
                                                  bits_g, key, start, p, bits, 
                                                  depth, states >>

mainCycleStart3(self) == /\ pc[self] = "mainCycleStart3"
                         /\ pc' = [pc EXCEPT ![self] = "checkHasValue3"]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, answer, tmp, tmp2, l_, r_, 
                                         mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, start, p, bits, depth, 
                                         states >>

checkHasValue3(self) == /\ pc[self] = "checkHasValue3"
                        /\ IF ~nodeArr[p_d[self]].hasValue
                              THEN /\ pc' = [pc EXCEPT ![self] = "updateBits3"]
                              ELSE /\ pc' = [pc EXCEPT ![self] = "detailedNodeSearchCalcs3"]
                        /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                        nodeArr, insertRes, resultsForGet, 
                                        deleteRes, processWriterState, 
                                        processReaderState, values, stack, i, 
                                        cur, istack, current_d, key_, tmp_, 
                                        tmp1, mid_, p_, p1, recStack_, 
                                        recStack1_, ret, depth_, res, calc, 
                                        maxDepth, l, r, gNode, buf_, rmax, 
                                        links, recStack_s, mid_s, val, child, 
                                        lastNode, bits_, depth_s, newBits, 
                                        movedBits, ln, rn, current, sibling, 
                                        keyL, keyR, buf, bufLink, p_d, tempL, 
                                        tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        current_, rev, bits_i, depth_i, p_i, 
                                        temp, scanRes, t, array, split, 
                                        recStack, answer, tmp, tmp2, l_, r_, 
                                        mid, sibling_, keyL_, keyR_, recStack1, 
                                        key_g, startValue, depth_g, bits_g, 
                                        key, start, p, bits, depth, states >>

detailedNodeSearchCalcs3(self) == /\ pc[self] = "detailedNodeSearchCalcs3"
                                  /\ depth_d' = [depth_d EXCEPT ![self] = depth_d[self] + 1]
                                  /\ bits_d' = [bits_d EXCEPT ![self] = bits_d[self] * 2]
                                  /\ lastNode' = [lastNode EXCEPT ![self] = p_d[self]]
                                  /\ pc' = [pc EXCEPT ![self] = "checkKey3"]
                                  /\ UNCHANGED << root, hasRoot, globalLock, 
                                                  gNodeArr, nodeArr, insertRes, 
                                                  resultsForGet, deleteRes, 
                                                  processWriterState, 
                                                  processReaderState, values, 
                                                  stack, i, cur, istack, 
                                                  current_d, key_, tmp_, tmp1, 
                                                  mid_, p_, p1, recStack_, 
                                                  recStack1_, ret, depth_, res, 
                                                  calc, maxDepth, l, r, gNode, 
                                                  buf_, rmax, links, 
                                                  recStack_s, mid_s, val, 
                                                  child, bits_, depth_s, 
                                                  newBits, movedBits, ln, rn, 
                                                  current, sibling, keyL, keyR, 
                                                  buf, bufLink, p_d, tempL, 
                                                  tempR, key_i, stackData, 
                                                  keyRNext, oldCurrent, parent, 
                                                  keyLNext, links_, oldSibling, 
                                                  current_, rev, bits_i, 
                                                  depth_i, p_i, temp, scanRes, 
                                                  t, array, split, recStack, 
                                                  answer, tmp, tmp2, l_, r_, 
                                                  mid, sibling_, keyL_, keyR_, 
                                                  recStack1, key_g, startValue, 
                                                  depth_g, bits_g, key, start, 
                                                  p, bits, depth, states >>

checkKey3(self) == /\ pc[self] = "checkKey3"
                   /\ IF keyR[self] < nodeArr[p_d[self]].value
                         THEN /\ pc' = [pc EXCEPT ![self] = "dnsCheckLeft3"]
                         ELSE /\ pc' = [pc EXCEPT ![self] = "dnsCheckRight3"]
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, resultsForGet, 
                                   deleteRes, processWriterState, 
                                   processReaderState, values, stack, i, cur, 
                                   istack, current_d, key_, tmp_, tmp1, mid_, 
                                   p_, p1, recStack_, recStack1_, ret, depth_, 
                                   res, calc, maxDepth, l, r, gNode, buf_, 
                                   rmax, links, recStack_s, mid_s, val, child, 
                                   lastNode, bits_, depth_s, newBits, 
                                   movedBits, ln, rn, current, sibling, keyL, 
                                   keyR, buf, bufLink, p_d, tempL, tempR, 
                                   bits_d, depth_d, key_i, stackData, keyRNext, 
                                   oldCurrent, parent, keyLNext, links_, 
                                   oldSibling, current_, rev, bits_i, depth_i, 
                                   p_i, temp, scanRes, t, array, split, 
                                   recStack, answer, tmp, tmp2, l_, r_, mid, 
                                   sibling_, keyL_, keyR_, recStack1, key_g, 
                                   startValue, depth_g, bits_g, key, start, p, 
                                   bits, depth, states >>

dnsCheckLeft3(self) == /\ pc[self] = "dnsCheckLeft3"
                       /\ IF ~nodeArr[p_d[self]].hasLeft
                             THEN /\ pc' = [pc EXCEPT ![self] = "updateBits3"]
                             ELSE /\ pc' = [pc EXCEPT ![self] = "dnsGetLeft3"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

dnsGetLeft3(self) == /\ pc[self] = "dnsGetLeft3"
                     /\ p_d' = [p_d EXCEPT ![self] = nodeArr[p_d[self]].left]
                     /\ pc' = [pc EXCEPT ![self] = "mainCycleStart3"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

dnsCheckRight3(self) == /\ pc[self] = "dnsCheckRight3"
                        /\ IF ~nodeArr[p_d[self]].hasRight
                              THEN /\ pc' = [pc EXCEPT ![self] = "updateBits3"]
                              ELSE /\ pc' = [pc EXCEPT ![self] = "dnsGetRight3"]
                        /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                        nodeArr, insertRes, resultsForGet, 
                                        deleteRes, processWriterState, 
                                        processReaderState, values, stack, i, 
                                        cur, istack, current_d, key_, tmp_, 
                                        tmp1, mid_, p_, p1, recStack_, 
                                        recStack1_, ret, depth_, res, calc, 
                                        maxDepth, l, r, gNode, buf_, rmax, 
                                        links, recStack_s, mid_s, val, child, 
                                        lastNode, bits_, depth_s, newBits, 
                                        movedBits, ln, rn, current, sibling, 
                                        keyL, keyR, buf, bufLink, p_d, tempL, 
                                        tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        current_, rev, bits_i, depth_i, p_i, 
                                        temp, scanRes, t, array, split, 
                                        recStack, answer, tmp, tmp2, l_, r_, 
                                        mid, sibling_, keyL_, keyR_, recStack1, 
                                        key_g, startValue, depth_g, bits_g, 
                                        key, start, p, bits, depth, states >>

dnsGetRight3(self) == /\ pc[self] = "dnsGetRight3"
                      /\ p_d' = [p_d EXCEPT ![self] = nodeArr[p_d[self]].right]
                      /\ bits_d' = [bits_d EXCEPT ![self] = bits_d[self] + 1]
                      /\ pc' = [pc EXCEPT ![self] = "mainCycleStart3"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, tempL, 
                                      tempR, depth_d, key_i, stackData, 
                                      keyRNext, oldCurrent, parent, keyLNext, 
                                      links_, oldSibling, current_, rev, 
                                      bits_i, depth_i, p_i, temp, scanRes, t, 
                                      array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

updateBits3(self) == /\ pc[self] = "updateBits3"
                     /\ bits_d' = [bits_d EXCEPT ![self] = bits_d[self] \div 2]
                     /\ pc' = [pc EXCEPT ![self] = "getTempL"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

getTempL(self) == /\ pc[self] = "getTempL"
                  /\ tempL' = [tempL EXCEPT ![self] = nodeArr[lastNode[self]].left]
                  /\ pc' = [pc EXCEPT ![self] = "getTempR"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempR, bits_d, 
                                  depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  p_i, temp, scanRes, t, array, split, 
                                  recStack, answer, tmp, tmp2, l_, r_, mid, 
                                  sibling_, keyL_, keyR_, recStack1, key_g, 
                                  startValue, depth_g, bits_g, key, start, p, 
                                  bits, depth, states >>

getTempR(self) == /\ pc[self] = "getTempR"
                  /\ tempR' = [tempR EXCEPT ![self] = nodeArr[lastNode[self]].right]
                  /\ depth_d' = [depth_d EXCEPT ![self] = depth_d[self] + 1]
                  /\ pc' = [pc EXCEPT ![self] = "checkKeyR"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, bits_d, 
                                  key_i, stackData, keyRNext, oldCurrent, 
                                  parent, keyLNext, links_, oldSibling, 
                                  current_, rev, bits_i, depth_i, p_i, temp, 
                                  scanRes, t, array, split, recStack, answer, 
                                  tmp, tmp2, l_, r_, mid, sibling_, keyL_, 
                                  keyR_, recStack1, key_g, startValue, depth_g, 
                                  bits_g, key, start, p, bits, depth, states >>

checkKeyR(self) == /\ pc[self] = "checkKeyR"
                   /\ IF keyR[self] < nodeArr[lastNode[self]].value
                         THEN /\ pc' = [pc EXCEPT ![self] = "setKeyR1"]
                         ELSE /\ pc' = [pc EXCEPT ![self] = "setTempL"]
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, resultsForGet, 
                                   deleteRes, processWriterState, 
                                   processReaderState, values, stack, i, cur, 
                                   istack, current_d, key_, tmp_, tmp1, mid_, 
                                   p_, p1, recStack_, recStack1_, ret, depth_, 
                                   res, calc, maxDepth, l, r, gNode, buf_, 
                                   rmax, links, recStack_s, mid_s, val, child, 
                                   lastNode, bits_, depth_s, newBits, 
                                   movedBits, ln, rn, current, sibling, keyL, 
                                   keyR, buf, bufLink, p_d, tempL, tempR, 
                                   bits_d, depth_d, key_i, stackData, keyRNext, 
                                   oldCurrent, parent, keyLNext, links_, 
                                   oldSibling, current_, rev, bits_i, depth_i, 
                                   p_i, temp, scanRes, t, array, split, 
                                   recStack, answer, tmp, tmp2, l_, r_, mid, 
                                   sibling_, keyL_, keyR_, recStack1, key_g, 
                                   startValue, depth_g, bits_g, key, start, p, 
                                   bits, depth, states >>

setKeyR1(self) == /\ pc[self] = "setKeyR1"
                  /\ nodeArr' = [nodeArr EXCEPT ![lastNode[self]].value = keyR[self]]
                  /\ pc' = [pc EXCEPT ![self] = "setKeyL1"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  p_i, temp, scanRes, t, array, split, 
                                  recStack, answer, tmp, tmp2, l_, r_, mid, 
                                  sibling_, keyL_, keyR_, recStack1, key_g, 
                                  startValue, depth_g, bits_g, key, start, p, 
                                  bits, depth, states >>

setKeyL1(self) == /\ pc[self] = "setKeyL1"
                  /\ nodeArr' = [nodeArr EXCEPT ![tempL[self]].value = keyL[self]]
                  /\ pc' = [pc EXCEPT ![self] = "setKeyR2"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  p_i, temp, scanRes, t, array, split, 
                                  recStack, answer, tmp, tmp2, l_, r_, mid, 
                                  sibling_, keyL_, keyR_, recStack1, key_g, 
                                  startValue, depth_g, bits_g, key, start, p, 
                                  bits, depth, states >>

setKeyR2(self) == /\ pc[self] = "setKeyR2"
                  /\ nodeArr' = [nodeArr EXCEPT ![tempR[self]].value = keyR[self]]
                  /\ pc' = [pc EXCEPT ![self] = "updateCountNode2"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  p_i, temp, scanRes, t, array, split, 
                                  recStack, answer, tmp, tmp2, l_, r_, mid, 
                                  sibling_, keyL_, keyR_, recStack1, key_g, 
                                  startValue, depth_g, bits_g, key, start, p, 
                                  bits, depth, states >>

setTempL(self) == /\ pc[self] = "setTempL"
                  /\ nodeArr' = [nodeArr EXCEPT ![tempL[self]].value = nodeArr[lastNode[self]].value]
                  /\ pc' = [pc EXCEPT ![self] = "setKeyR3"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  p_i, temp, scanRes, t, array, split, 
                                  recStack, answer, tmp, tmp2, l_, r_, mid, 
                                  sibling_, keyL_, keyR_, recStack1, key_g, 
                                  startValue, depth_g, bits_g, key, start, p, 
                                  bits, depth, states >>

setKeyR3(self) == /\ pc[self] = "setKeyR3"
                  /\ nodeArr' = [nodeArr EXCEPT ![lastNode[self]].value = keyR[self]]
                  /\ pc' = [pc EXCEPT ![self] = "setKeyR4"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  p_i, temp, scanRes, t, array, split, 
                                  recStack, answer, tmp, tmp2, l_, r_, mid, 
                                  sibling_, keyL_, keyR_, recStack1, key_g, 
                                  startValue, depth_g, bits_g, key, start, p, 
                                  bits, depth, states >>

setKeyR4(self) == /\ pc[self] = "setKeyR4"
                  /\ nodeArr' = [nodeArr EXCEPT ![tempR[self]].value = keyR[self]]
                  /\ pc' = [pc EXCEPT ![self] = "updateCountNode2"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  p_i, temp, scanRes, t, array, split, 
                                  recStack, answer, tmp, tmp2, l_, r_, mid, 
                                  sibling_, keyL_, keyR_, recStack1, key_g, 
                                  startValue, depth_g, bits_g, key, start, p, 
                                  bits, depth, states >>

updateCountNode2(self) == /\ pc[self] = "updateCountNode2"
                          /\ gNodeArr' = [gNodeArr EXCEPT ![current[self]].countNode = gNodeArr[current[self]].countNode + 2]
                          /\ bits_d' = [bits_d EXCEPT ![self] = (bits_d[self] * 2) + 1]
                          /\ pc' = [pc EXCEPT ![self] = "secondUpdateBits"]
                          /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                          insertRes, resultsForGet, deleteRes, 
                                          processWriterState, 
                                          processReaderState, values, stack, i, 
                                          cur, istack, current_d, key_, tmp_, 
                                          tmp1, mid_, p_, p1, recStack_, 
                                          recStack1_, ret, depth_, res, calc, 
                                          maxDepth, l, r, gNode, buf_, rmax, 
                                          links, recStack_s, mid_s, val, child, 
                                          lastNode, bits_, depth_s, newBits, 
                                          movedBits, ln, rn, current, sibling, 
                                          keyL, keyR, buf, bufLink, p_d, tempL, 
                                          tempR, depth_d, key_i, stackData, 
                                          keyRNext, oldCurrent, parent, 
                                          keyLNext, links_, oldSibling, 
                                          current_, rev, bits_i, depth_i, p_i, 
                                          temp, scanRes, t, array, split, 
                                          recStack, answer, tmp, tmp2, l_, r_, 
                                          mid, sibling_, keyL_, keyR_, 
                                          recStack1, key_g, startValue, 
                                          depth_g, bits_g, key, start, p, bits, 
                                          depth, states >>

secondUpdateBits(self) == /\ pc[self] = "secondUpdateBits"
                          /\ bits_d' = [bits_d EXCEPT ![self] = shiftL(bits_d[self], GNodeDepth - depth_d[self])]
                          /\ pc' = [pc EXCEPT ![self] = "setCurrentLinks"]
                          /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                          nodeArr, insertRes, resultsForGet, 
                                          deleteRes, processWriterState, 
                                          processReaderState, values, stack, i, 
                                          cur, istack, current_d, key_, tmp_, 
                                          tmp1, mid_, p_, p1, recStack_, 
                                          recStack1_, ret, depth_, res, calc, 
                                          maxDepth, l, r, gNode, buf_, rmax, 
                                          links, recStack_s, mid_s, val, child, 
                                          lastNode, bits_, depth_s, newBits, 
                                          movedBits, ln, rn, current, sibling, 
                                          keyL, keyR, buf, bufLink, p_d, tempL, 
                                          tempR, depth_d, key_i, stackData, 
                                          keyRNext, oldCurrent, parent, 
                                          keyLNext, links_, oldSibling, 
                                          current_, rev, bits_i, depth_i, p_i, 
                                          temp, scanRes, t, array, split, 
                                          recStack, answer, tmp, tmp2, l_, r_, 
                                          mid, sibling_, keyL_, keyR_, 
                                          recStack1, key_g, startValue, 
                                          depth_g, bits_g, key, start, p, bits, 
                                          depth, states >>

setCurrentLinks(self) == /\ pc[self] = "setCurrentLinks"
                         /\ gNodeArr' = [gNodeArr EXCEPT ![current[self]].links[bits_d[self]] = sibling[self]]
                         /\ pc' = [pc EXCEPT ![self] = "Error"]
                         /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                         insertRes, resultsForGet, deleteRes, 
                                         processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, answer, tmp, tmp2, l_, r_, 
                                         mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, start, p, bits, depth, 
                                         states >>

doInsertNode(self) == detailedNodeSearchStart2(self)
                         \/ mainCycleStart2(self) \/ checkHasValue2(self)
                         \/ detailedNodeSearchCalcs2(self)
                         \/ checkKey2(self) \/ dnsCheckLeft2(self)
                         \/ dnsGetLeft2(self) \/ dnsCheckRight2(self)
                         \/ dnsGetRight2(self) \/ updateBits2(self)
                         \/ checkLeftInInsertNode(self)
                         \/ startFillBufLo2(self)
                         \/ fboCheckLeftRight4(self) \/ fboGetleft2(self)
                         \/ fboGetRight2(self) \/ fboCheckLeftRight5(self)
                         \/ fboCheckLeftRight6(self) \/ fboIncValue2(self)
                         \/ fboCheckRetSize2(self) \/ fboGoToRet4(self)
                         \/ fillLinkBuf(self) \/ startUpdateCurrent_(self)
                         \/ updateValues1(self) \/ updateValues2(self)
                         \/ smartFillValParentLo1(self)
                         \/ smartFillValParentLo2(self)
                         \/ finishUpdateCurrent2(self)
                         \/ updateCountNode(self)
                         \/ detailedNodeSearchStart3(self)
                         \/ mainCycleStart3(self) \/ checkHasValue3(self)
                         \/ detailedNodeSearchCalcs3(self)
                         \/ checkKey3(self) \/ dnsCheckLeft3(self)
                         \/ dnsGetLeft3(self) \/ dnsCheckRight3(self)
                         \/ dnsGetRight3(self) \/ updateBits3(self)
                         \/ getTempL(self) \/ getTempR(self)
                         \/ checkKeyR(self) \/ setKeyR1(self)
                         \/ setKeyL1(self) \/ setKeyR2(self)
                         \/ setTempL(self) \/ setKeyR3(self)
                         \/ setKeyR4(self) \/ updateCountNode2(self)
                         \/ secondUpdateBits(self) \/ setCurrentLinks(self)

checkHasRoot(self) == /\ pc[self] = "checkHasRoot"
                      /\ IF ~hasRoot
                            THEN /\ pc' = [pc EXCEPT ![self] = "hasRootLock"]
                            ELSE /\ pc' = [pc EXCEPT ![self] = "startFindGNode"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

hasRootLock(self) == /\ pc[self] = "hasRootLock"
                     /\ globalLock
                     /\ globalLock' = FALSE
                     /\ pc' = [pc EXCEPT ![self] = "checkHasRoot2"]
                     /\ UNCHANGED << root, hasRoot, gNodeArr, nodeArr, 
                                     insertRes, resultsForGet, deleteRes, 
                                     processWriterState, processReaderState, 
                                     values, stack, i, cur, istack, current_d, 
                                     key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                     recStack1_, ret, depth_, res, calc, 
                                     maxDepth, l, r, gNode, buf_, rmax, links, 
                                     recStack_s, mid_s, val, child, lastNode, 
                                     bits_, depth_s, newBits, movedBits, ln, 
                                     rn, current, sibling, keyL, keyR, buf, 
                                     bufLink, p_d, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

checkHasRoot2(self) == /\ pc[self] = "checkHasRoot2"
                       /\ IF ~hasRoot
                             THEN /\ pc' = [pc EXCEPT ![self] = "initLeaf_i"]
                             ELSE /\ pc' = [pc EXCEPT ![self] = "releaseLock2"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

initLeaf_i(self) == /\ pc[self] = "initLeaf_i"
                    /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "initLeaf",
                                                             pc        |->  "setRoot",
                                                             i         |->  i[self],
                                                             cur       |->  cur[self],
                                                             istack    |->  istack[self] ] >>
                                                         \o stack[self]]
                    /\ i' = [i EXCEPT ![self] = defaultInitValue]
                    /\ cur' = [cur EXCEPT ![self] = defaultInitValue]
                    /\ istack' = [istack EXCEPT ![self] = defaultInitValue]
                    /\ pc' = [pc EXCEPT ![self] = "initLeaf_"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, current_d, 
                                    key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                    recStack1_, ret, depth_, res, calc, 
                                    maxDepth, l, r, gNode, buf_, rmax, links, 
                                    recStack_s, mid_s, val, child, lastNode, 
                                    bits_, depth_s, newBits, movedBits, ln, rn, 
                                    current, sibling, keyL, keyR, buf, bufLink, 
                                    p_d, tempL, tempR, bits_d, depth_d, key_i, 
                                    stackData, keyRNext, oldCurrent, parent, 
                                    keyLNext, links_, oldSibling, current_, 
                                    rev, bits_i, depth_i, p_i, temp, scanRes, 
                                    t, array, split, recStack, answer, tmp, 
                                    tmp2, l_, r_, mid, sibling_, keyL_, keyR_, 
                                    recStack1, key_g, startValue, depth_g, 
                                    bits_g, key, start, p, bits, depth, states >>

setRoot(self) == /\ pc[self] = "setRoot"
                 /\ root' = Len(gNodeArr)
                 /\ hasRoot' = TRUE
                 /\ pc' = [pc EXCEPT ![self] = "releaseLock"]
                 /\ UNCHANGED << globalLock, gNodeArr, nodeArr, insertRes, 
                                 resultsForGet, deleteRes, processWriterState, 
                                 processReaderState, values, stack, i, cur, 
                                 istack, current_d, key_, tmp_, tmp1, mid_, p_, 
                                 p1, recStack_, recStack1_, ret, depth_, res, 
                                 calc, maxDepth, l, r, gNode, buf_, rmax, 
                                 links, recStack_s, mid_s, val, child, 
                                 lastNode, bits_, depth_s, newBits, movedBits, 
                                 ln, rn, current, sibling, keyL, keyR, buf, 
                                 bufLink, p_d, tempL, tempR, bits_d, depth_d, 
                                 key_i, stackData, keyRNext, oldCurrent, 
                                 parent, keyLNext, links_, oldSibling, 
                                 current_, rev, bits_i, depth_i, p_i, temp, 
                                 scanRes, t, array, split, recStack, answer, 
                                 tmp, tmp2, l_, r_, mid, sibling_, keyL_, 
                                 keyR_, recStack1, key_g, startValue, depth_g, 
                                 bits_g, key, start, p, bits, depth, states >>

releaseLock(self) == /\ pc[self] = "releaseLock"
                     /\ globalLock' = TRUE
                     /\ pc' = [pc EXCEPT ![self] = "returnInsertRes"]
                     /\ UNCHANGED << root, hasRoot, gNodeArr, nodeArr, 
                                     insertRes, resultsForGet, deleteRes, 
                                     processWriterState, processReaderState, 
                                     values, stack, i, cur, istack, current_d, 
                                     key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                     recStack1_, ret, depth_, res, calc, 
                                     maxDepth, l, r, gNode, buf_, rmax, links, 
                                     recStack_s, mid_s, val, child, lastNode, 
                                     bits_, depth_s, newBits, movedBits, ln, 
                                     rn, current, sibling, keyL, keyR, buf, 
                                     bufLink, p_d, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

returnInsertRes(self) == /\ pc[self] = "returnInsertRes"
                         /\ insertRes' = [insertRes EXCEPT ![self] = TRUE]
                         /\ pc' = [pc EXCEPT ![self] = Head(stack[self]).pc]
                         /\ stackData' = [stackData EXCEPT ![self] = Head(stack[self]).stackData]
                         /\ keyRNext' = [keyRNext EXCEPT ![self] = Head(stack[self]).keyRNext]
                         /\ oldCurrent' = [oldCurrent EXCEPT ![self] = Head(stack[self]).oldCurrent]
                         /\ parent' = [parent EXCEPT ![self] = Head(stack[self]).parent]
                         /\ keyLNext' = [keyLNext EXCEPT ![self] = Head(stack[self]).keyLNext]
                         /\ links_' = [links_ EXCEPT ![self] = Head(stack[self]).links_]
                         /\ oldSibling' = [oldSibling EXCEPT ![self] = Head(stack[self]).oldSibling]
                         /\ current_' = [current_ EXCEPT ![self] = Head(stack[self]).current_]
                         /\ rev' = [rev EXCEPT ![self] = Head(stack[self]).rev]
                         /\ bits_i' = [bits_i EXCEPT ![self] = Head(stack[self]).bits_i]
                         /\ depth_i' = [depth_i EXCEPT ![self] = Head(stack[self]).depth_i]
                         /\ p_i' = [p_i EXCEPT ![self] = Head(stack[self]).p_i]
                         /\ temp' = [temp EXCEPT ![self] = Head(stack[self]).temp]
                         /\ scanRes' = [scanRes EXCEPT ![self] = Head(stack[self]).scanRes]
                         /\ t' = [t EXCEPT ![self] = Head(stack[self]).t]
                         /\ array' = [array EXCEPT ![self] = Head(stack[self]).array]
                         /\ split' = [split EXCEPT ![self] = Head(stack[self]).split]
                         /\ recStack' = [recStack EXCEPT ![self] = Head(stack[self]).recStack]
                         /\ answer' = [answer EXCEPT ![self] = Head(stack[self]).answer]
                         /\ tmp' = [tmp EXCEPT ![self] = Head(stack[self]).tmp]
                         /\ tmp2' = [tmp2 EXCEPT ![self] = Head(stack[self]).tmp2]
                         /\ l_' = [l_ EXCEPT ![self] = Head(stack[self]).l_]
                         /\ r_' = [r_ EXCEPT ![self] = Head(stack[self]).r_]
                         /\ mid' = [mid EXCEPT ![self] = Head(stack[self]).mid]
                         /\ sibling_' = [sibling_ EXCEPT ![self] = Head(stack[self]).sibling_]
                         /\ keyL_' = [keyL_ EXCEPT ![self] = Head(stack[self]).keyL_]
                         /\ keyR_' = [keyR_ EXCEPT ![self] = Head(stack[self]).keyR_]
                         /\ recStack1' = [recStack1 EXCEPT ![self] = Head(stack[self]).recStack1]
                         /\ key_i' = [key_i EXCEPT ![self] = Head(stack[self]).key_i]
                         /\ stack' = [stack EXCEPT ![self] = Tail(stack[self])]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, resultsForGet, deleteRes, 
                                         processWriterState, 
                                         processReaderState, values, i, cur, 
                                         istack, current_d, key_, tmp_, tmp1, 
                                         mid_, p_, p1, recStack_, recStack1_, 
                                         ret, depth_, res, calc, maxDepth, l, 
                                         r, gNode, buf_, rmax, links, 
                                         recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_g, 
                                         startValue, depth_g, bits_g, key, 
                                         start, p, bits, depth, states >>

releaseLock2(self) == /\ pc[self] = "releaseLock2"
                      /\ globalLock' = TRUE
                      /\ pc' = [pc EXCEPT ![self] = "startFindGNode"]
                      /\ UNCHANGED << root, hasRoot, gNodeArr, nodeArr, 
                                      insertRes, resultsForGet, deleteRes, 
                                      processWriterState, processReaderState, 
                                      values, stack, i, cur, istack, current_d, 
                                      key_, tmp_, tmp1, mid_, p_, p1, 
                                      recStack_, recStack1_, ret, depth_, res, 
                                      calc, maxDepth, l, r, gNode, buf_, rmax, 
                                      links, recStack_s, mid_s, val, child, 
                                      lastNode, bits_, depth_s, newBits, 
                                      movedBits, ln, rn, current, sibling, 
                                      keyL, keyR, buf, bufLink, p_d, tempL, 
                                      tempR, bits_d, depth_d, key_i, stackData, 
                                      keyRNext, oldCurrent, parent, keyLNext, 
                                      links_, oldSibling, current_, rev, 
                                      bits_i, depth_i, p_i, temp, scanRes, t, 
                                      array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

startFindGNode(self) == /\ pc[self] = "startFindGNode"
                        /\ current_' = [current_ EXCEPT ![self] = root]
                        /\ pc' = [pc EXCEPT ![self] = "startFindGNode2"]
                        /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                        nodeArr, insertRes, resultsForGet, 
                                        deleteRes, processWriterState, 
                                        processReaderState, values, stack, i, 
                                        cur, istack, current_d, key_, tmp_, 
                                        tmp1, mid_, p_, p1, recStack_, 
                                        recStack1_, ret, depth_, res, calc, 
                                        maxDepth, l, r, gNode, buf_, rmax, 
                                        links, recStack_s, mid_s, val, child, 
                                        lastNode, bits_, depth_s, newBits, 
                                        movedBits, ln, rn, current, sibling, 
                                        keyL, keyR, buf, bufLink, p_d, tempL, 
                                        tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        rev, bits_i, depth_i, p_i, temp, 
                                        scanRes, t, array, split, recStack, 
                                        answer, tmp, tmp2, l_, r_, mid, 
                                        sibling_, keyL_, keyR_, recStack1, 
                                        key_g, startValue, depth_g, bits_g, 
                                        key, start, p, bits, depth, states >>

startFindGNode2(self) == /\ pc[self] = "startFindGNode2"
                         /\ IF ~gNodeArr[current_[self]].isLeaf
                               THEN /\ pc' = [pc EXCEPT ![self] = "startCycle"]
                                    /\ t' = t
                               ELSE /\ t' = [t EXCEPT ![self] = current_[self]]
                                    /\ pc' = [pc EXCEPT ![self] = "getGNodeLock"]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, array, split, recStack, 
                                         answer, tmp, tmp2, l_, r_, mid, 
                                         sibling_, keyL_, keyR_, recStack1, 
                                         key_g, startValue, depth_g, bits_g, 
                                         key, start, p, bits, depth, states >>

startCycle(self) == /\ pc[self] = "startCycle"
                    /\ oldCurrent' = [oldCurrent EXCEPT ![self] = current_[self]]
                    /\ pc' = [pc EXCEPT ![self] = "startScanNode"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, p1, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, parent, keyLNext, links_, 
                                    oldSibling, current_, rev, bits_i, depth_i, 
                                    p_i, temp, scanRes, t, array, split, 
                                    recStack, answer, tmp, tmp2, l_, r_, mid, 
                                    sibling_, keyL_, keyR_, recStack1, key_g, 
                                    startValue, depth_g, bits_g, key, start, p, 
                                    bits, depth, states >>

startScanNode(self) == /\ pc[self] = "startScanNode"
                       /\ rev' = [rev EXCEPT ![self] = gNodeArr[current_[self]].rev]
                       /\ bits_i' = [bits_i EXCEPT ![self] = 1]
                       /\ depth_i' = [depth_i EXCEPT ![self] = 0]
                       /\ temp' = [temp EXCEPT ![self] = TRUE]
                       /\ IF rev'[self] % 2 = 1
                             THEN /\ pc' = [pc EXCEPT ![self] = "updating"]
                             ELSE /\ pc' = [pc EXCEPT ![self] = "getRoot"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       p_i, scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

updating(self) == /\ pc[self] = "updating"
                  /\ temp' = [temp EXCEPT ![self] = FALSE]
                  /\ scanRes' = [scanRes EXCEPT ![self] = FALSE]
                  /\ pc' = [pc EXCEPT ![self] = "prepareStack"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  p_i, t, array, split, recStack, answer, tmp, 
                                  tmp2, l_, r_, mid, sibling_, keyL_, keyR_, 
                                  recStack1, key_g, startValue, depth_g, 
                                  bits_g, key, start, p, bits, depth, states >>

getRoot(self) == /\ pc[self] = "getRoot"
                 /\ p_i' = [p_i EXCEPT ![self] = gNodeArr[current_[self]].root]
                 /\ pc' = [pc EXCEPT ![self] = "startSearchInGNode"]
                 /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                 insertRes, resultsForGet, deleteRes, 
                                 processWriterState, processReaderState, 
                                 values, stack, i, cur, istack, current_d, 
                                 key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                 recStack1_, ret, depth_, res, calc, maxDepth, 
                                 l, r, gNode, buf_, rmax, links, recStack_s, 
                                 mid_s, val, child, lastNode, bits_, depth_s, 
                                 newBits, movedBits, ln, rn, current, sibling, 
                                 keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                 bits_d, depth_d, key_i, stackData, keyRNext, 
                                 oldCurrent, parent, keyLNext, links_, 
                                 oldSibling, current_, rev, bits_i, depth_i, 
                                 temp, scanRes, t, array, split, recStack, 
                                 answer, tmp, tmp2, l_, r_, mid, sibling_, 
                                 keyL_, keyR_, recStack1, key_g, startValue, 
                                 depth_g, bits_g, key, start, p, bits, depth, 
                                 states >>

startSearchInGNode(self) == /\ pc[self] = "startSearchInGNode"
                            /\ IF ~checkNodeIsLeaf(nodeArr, p_i[self])
                                  THEN /\ bits_i' = [bits_i EXCEPT ![self] = bits_i[self] * 2]
                                       /\ depth_i' = [depth_i EXCEPT ![self] = depth_i[self] + 1]
                                       /\ pc' = [pc EXCEPT ![self] = "checkIsLeft"]
                                  ELSE /\ bits_i' = [bits_i EXCEPT ![self] = bits_i[self] \div 2]
                                       /\ pc' = [pc EXCEPT ![self] = "preparePath"]
                                       /\ UNCHANGED depth_i
                            /\ UNCHANGED << root, hasRoot, globalLock, 
                                            gNodeArr, nodeArr, insertRes, 
                                            resultsForGet, deleteRes, 
                                            processWriterState, 
                                            processReaderState, values, stack, 
                                            i, cur, istack, current_d, key_, 
                                            tmp_, tmp1, mid_, p_, p1, 
                                            recStack_, recStack1_, ret, depth_, 
                                            res, calc, maxDepth, l, r, gNode, 
                                            buf_, rmax, links, recStack_s, 
                                            mid_s, val, child, lastNode, bits_, 
                                            depth_s, newBits, movedBits, ln, 
                                            rn, current, sibling, keyL, keyR, 
                                            buf, bufLink, p_d, tempL, tempR, 
                                            bits_d, depth_d, key_i, stackData, 
                                            keyRNext, oldCurrent, parent, 
                                            keyLNext, links_, oldSibling, 
                                            current_, rev, p_i, temp, scanRes, 
                                            t, array, split, recStack, answer, 
                                            tmp, tmp2, l_, r_, mid, sibling_, 
                                            keyL_, keyR_, recStack1, key_g, 
                                            startValue, depth_g, bits_g, key, 
                                            start, p, bits, depth, states >>

checkIsLeft(self) == /\ pc[self] = "checkIsLeft"
                     /\ IF key_i[self] < nodeArr[p_i[self]].value
                           THEN /\ pc' = [pc EXCEPT ![self] = "goLeft"]
                           ELSE /\ pc' = [pc EXCEPT ![self] = "goRight"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

goLeft(self) == /\ pc[self] = "goLeft"
                /\ p_i' = [p_i EXCEPT ![self] = nodeArr[p_i[self]].left]
                /\ pc' = [pc EXCEPT ![self] = "startSearchInGNode"]
                /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                insertRes, resultsForGet, deleteRes, 
                                processWriterState, processReaderState, values, 
                                stack, i, cur, istack, current_d, key_, tmp_, 
                                tmp1, mid_, p_, p1, recStack_, recStack1_, ret, 
                                depth_, res, calc, maxDepth, l, r, gNode, buf_, 
                                rmax, links, recStack_s, mid_s, val, child, 
                                lastNode, bits_, depth_s, newBits, movedBits, 
                                ln, rn, current, sibling, keyL, keyR, buf, 
                                bufLink, p_d, tempL, tempR, bits_d, depth_d, 
                                key_i, stackData, keyRNext, oldCurrent, parent, 
                                keyLNext, links_, oldSibling, current_, rev, 
                                bits_i, depth_i, temp, scanRes, t, array, 
                                split, recStack, answer, tmp, tmp2, l_, r_, 
                                mid, sibling_, keyL_, keyR_, recStack1, key_g, 
                                startValue, depth_g, bits_g, key, start, p, 
                                bits, depth, states >>

goRight(self) == /\ pc[self] = "goRight"
                 /\ p_i' = [p_i EXCEPT ![self] = nodeArr[p_i[self]].right]
                 /\ bits_i' = [bits_i EXCEPT ![self] = bits_i[self] + 1]
                 /\ pc' = [pc EXCEPT ![self] = "startSearchInGNode"]
                 /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                 insertRes, resultsForGet, deleteRes, 
                                 processWriterState, processReaderState, 
                                 values, stack, i, cur, istack, current_d, 
                                 key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                 recStack1_, ret, depth_, res, calc, maxDepth, 
                                 l, r, gNode, buf_, rmax, links, recStack_s, 
                                 mid_s, val, child, lastNode, bits_, depth_s, 
                                 newBits, movedBits, ln, rn, current, sibling, 
                                 keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                 bits_d, depth_d, key_i, stackData, keyRNext, 
                                 oldCurrent, parent, keyLNext, links_, 
                                 oldSibling, current_, rev, depth_i, temp, 
                                 scanRes, t, array, split, recStack, answer, 
                                 tmp, tmp2, l_, r_, mid, sibling_, keyL_, 
                                 keyR_, recStack1, key_g, startValue, depth_g, 
                                 bits_g, key, start, p, bits, depth, states >>

preparePath(self) == /\ pc[self] = "preparePath"
                     /\ bits_i' = [bits_i EXCEPT ![self] = shiftL(bits_i[self], GNodeDepth - depth_i[self])]
                     /\ pc' = [pc EXCEPT ![self] = "checkHighKey"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

checkHighKey(self) == /\ pc[self] = "checkHighKey"
                      /\ IF gNodeArr[current_[self]].hasSibling /\ gNodeArr[current_[self]].highKey > 0 /\ gNodeArr[current_[self]].highKey <= key_i[self]
                            THEN /\ pc' = [pc EXCEPT ![self] = "checkRev"]
                            ELSE /\ pc' = [pc EXCEPT ![self] = "getChildGNode"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

checkRev(self) == /\ pc[self] = "checkRev"
                  /\ IF gNodeArr[current_[self]].rev - rev[self] > 0
                        THEN /\ temp' = [temp EXCEPT ![self] = FALSE]
                             /\ scanRes' = [scanRes EXCEPT ![self] = FALSE]
                             /\ UNCHANGED current_
                        ELSE /\ temp' = [temp EXCEPT ![self] = gNodeArr[current_[self]].hasSibling]
                             /\ current_' = [current_ EXCEPT ![self] = gNodeArr[current_[self]].sibling]
                             /\ scanRes' = [scanRes EXCEPT ![self] = TRUE]
                  /\ pc' = [pc EXCEPT ![self] = "prepareStack"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, rev, bits_i, depth_i, p_i, t, 
                                  array, split, recStack, answer, tmp, tmp2, 
                                  l_, r_, mid, sibling_, keyL_, keyR_, 
                                  recStack1, key_g, startValue, depth_g, 
                                  bits_g, key, start, p, bits, depth, states >>

getChildGNode(self) == /\ pc[self] = "getChildGNode"
                       /\ temp' = [temp EXCEPT ![self] = gNodeArr[current_[self]].hasLinks[bits_i[self]]]
                       /\ pc' = [pc EXCEPT ![self] = "checkRev2"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, scanRes, t, 
                                       array, split, recStack, answer, tmp, 
                                       tmp2, l_, r_, mid, sibling_, keyL_, 
                                       keyR_, recStack1, key_g, startValue, 
                                       depth_g, bits_g, key, start, p, bits, 
                                       depth, states >>

checkRev2(self) == /\ pc[self] = "checkRev2"
                   /\ IF gNodeArr[current_[self]].rev - rev[self] > 0
                         THEN /\ temp' = [temp EXCEPT ![self] = FALSE]
                              /\ scanRes' = [scanRes EXCEPT ![self] = FALSE]
                              /\ UNCHANGED current_
                         ELSE /\ current_' = [current_ EXCEPT ![self] = gNodeArr[current_[self]].links[bits_i[self]]]
                              /\ scanRes' = [scanRes EXCEPT ![self] = FALSE]
                              /\ temp' = temp
                   /\ pc' = [pc EXCEPT ![self] = "prepareStack"]
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, resultsForGet, 
                                   deleteRes, processWriterState, 
                                   processReaderState, values, stack, i, cur, 
                                   istack, current_d, key_, tmp_, tmp1, mid_, 
                                   p_, p1, recStack_, recStack1_, ret, depth_, 
                                   res, calc, maxDepth, l, r, gNode, buf_, 
                                   rmax, links, recStack_s, mid_s, val, child, 
                                   lastNode, bits_, depth_s, newBits, 
                                   movedBits, ln, rn, current, sibling, keyL, 
                                   keyR, buf, bufLink, p_d, tempL, tempR, 
                                   bits_d, depth_d, key_i, stackData, keyRNext, 
                                   oldCurrent, parent, keyLNext, links_, 
                                   oldSibling, rev, bits_i, depth_i, p_i, t, 
                                   array, split, recStack, answer, tmp, tmp2, 
                                   l_, r_, mid, sibling_, keyL_, keyR_, 
                                   recStack1, key_g, startValue, depth_g, 
                                   bits_g, key, start, p, bits, depth, states >>

prepareStack(self) == /\ pc[self] = "prepareStack"
                      /\ IF ~scanRes[self]
                            THEN /\ IF ~temp[self]
                                       THEN /\ current_' = [current_ EXCEPT ![self] = oldCurrent[self]]
                                            /\ UNCHANGED stackData
                                       ELSE /\ stackData' = [stackData EXCEPT ![self] = Append(stackData[self], oldCurrent[self])]
                                            /\ UNCHANGED current_
                            ELSE /\ TRUE
                                 /\ UNCHANGED << stackData, current_ >>
                      /\ pc' = [pc EXCEPT ![self] = "startFindGNode2"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      keyRNext, oldCurrent, parent, keyLNext, 
                                      links_, oldSibling, rev, bits_i, depth_i, 
                                      p_i, temp, scanRes, t, array, split, 
                                      recStack, answer, tmp, tmp2, l_, r_, mid, 
                                      sibling_, keyL_, keyR_, recStack1, key_g, 
                                      startValue, depth_g, bits_g, key, start, 
                                      p, bits, depth, states >>

getGNodeLock(self) == /\ pc[self] = "getGNodeLock"
                      /\ gNodeArr[t[self]].lock
                      /\ gNodeArr' = [gNodeArr EXCEPT ![t[self]].lock = FALSE]
                      /\ pc' = [pc EXCEPT ![self] = "startMoveRight"]
                      /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                      insertRes, resultsForGet, deleteRes, 
                                      processWriterState, processReaderState, 
                                      values, stack, i, cur, istack, current_d, 
                                      key_, tmp_, tmp1, mid_, p_, p1, 
                                      recStack_, recStack1_, ret, depth_, res, 
                                      calc, maxDepth, l, r, gNode, buf_, rmax, 
                                      links, recStack_s, mid_s, val, child, 
                                      lastNode, bits_, depth_s, newBits, 
                                      movedBits, ln, rn, current, sibling, 
                                      keyL, keyR, buf, bufLink, p_d, tempL, 
                                      tempR, bits_d, depth_d, key_i, stackData, 
                                      keyRNext, oldCurrent, parent, keyLNext, 
                                      links_, oldSibling, current_, rev, 
                                      bits_i, depth_i, p_i, temp, scanRes, t, 
                                      array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

startMoveRight(self) == /\ pc[self] = "startMoveRight"
                        /\ IF gNodeArr[t[self]].hasSibling /\ gNodeArr[t[self]].highKey > 0 /\ gNodeArr[t[self]].highKey <= key_i[self]
                              THEN /\ pc' = [pc EXCEPT ![self] = "saveSibling"]
                                   /\ UNCHANGED current_
                              ELSE /\ current_' = [current_ EXCEPT ![self] = t[self]]
                                   /\ pc' = [pc EXCEPT ![self] = "startInsertCycle"]
                        /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                        nodeArr, insertRes, resultsForGet, 
                                        deleteRes, processWriterState, 
                                        processReaderState, values, stack, i, 
                                        cur, istack, current_d, key_, tmp_, 
                                        tmp1, mid_, p_, p1, recStack_, 
                                        recStack1_, ret, depth_, res, calc, 
                                        maxDepth, l, r, gNode, buf_, rmax, 
                                        links, recStack_s, mid_s, val, child, 
                                        lastNode, bits_, depth_s, newBits, 
                                        movedBits, ln, rn, current, sibling, 
                                        keyL, keyR, buf, bufLink, p_d, tempL, 
                                        tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        rev, bits_i, depth_i, p_i, temp, 
                                        scanRes, t, array, split, recStack, 
                                        answer, tmp, tmp2, l_, r_, mid, 
                                        sibling_, keyL_, keyR_, recStack1, 
                                        key_g, startValue, depth_g, bits_g, 
                                        key, start, p, bits, depth, states >>

saveSibling(self) == /\ pc[self] = "saveSibling"
                     /\ current_' = [current_ EXCEPT ![self] = gNodeArr[t[self]].sibling]
                     /\ pc' = [pc EXCEPT ![self] = "getNewLock"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, rev, bits_i, depth_i, 
                                     p_i, temp, scanRes, t, array, split, 
                                     recStack, answer, tmp, tmp2, l_, r_, mid, 
                                     sibling_, keyL_, keyR_, recStack1, key_g, 
                                     startValue, depth_g, bits_g, key, start, 
                                     p, bits, depth, states >>

getNewLock(self) == /\ pc[self] = "getNewLock"
                    /\ gNodeArr[current_[self]].lock
                    /\ gNodeArr' = [gNodeArr EXCEPT ![current_[self]].lock = FALSE]
                    /\ pc' = [pc EXCEPT ![self] = "releaseOldLock"]
                    /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                    insertRes, resultsForGet, deleteRes, 
                                    processWriterState, processReaderState, 
                                    values, stack, i, cur, istack, current_d, 
                                    key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                    recStack1_, ret, depth_, res, calc, 
                                    maxDepth, l, r, gNode, buf_, rmax, links, 
                                    recStack_s, mid_s, val, child, lastNode, 
                                    bits_, depth_s, newBits, movedBits, ln, rn, 
                                    current, sibling, keyL, keyR, buf, bufLink, 
                                    p_d, tempL, tempR, bits_d, depth_d, key_i, 
                                    stackData, keyRNext, oldCurrent, parent, 
                                    keyLNext, links_, oldSibling, current_, 
                                    rev, bits_i, depth_i, p_i, temp, scanRes, 
                                    t, array, split, recStack, answer, tmp, 
                                    tmp2, l_, r_, mid, sibling_, keyL_, keyR_, 
                                    recStack1, key_g, startValue, depth_g, 
                                    bits_g, key, start, p, bits, depth, states >>

releaseOldLock(self) == /\ pc[self] = "releaseOldLock"
                        /\ gNodeArr' = [gNodeArr EXCEPT ![t[self]].lock = TRUE]
                        /\ t' = [t EXCEPT ![self] = current_[self]]
                        /\ pc' = [pc EXCEPT ![self] = "startMoveRight"]
                        /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                        insertRes, resultsForGet, deleteRes, 
                                        processWriterState, processReaderState, 
                                        values, stack, i, cur, istack, 
                                        current_d, key_, tmp_, tmp1, mid_, p_, 
                                        p1, recStack_, recStack1_, ret, depth_, 
                                        res, calc, maxDepth, l, r, gNode, buf_, 
                                        rmax, links, recStack_s, mid_s, val, 
                                        child, lastNode, bits_, depth_s, 
                                        newBits, movedBits, ln, rn, current, 
                                        sibling, keyL, keyR, buf, bufLink, p_d, 
                                        tempL, tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        current_, rev, bits_i, depth_i, p_i, 
                                        temp, scanRes, array, split, recStack, 
                                        answer, tmp, tmp2, l_, r_, mid, 
                                        sibling_, keyL_, keyR_, recStack1, 
                                        key_g, startValue, depth_g, bits_g, 
                                        key, start, p, bits, depth, states >>

startInsertCycle(self) == /\ pc[self] = "startInsertCycle"
                          /\ pc' = [pc EXCEPT ![self] = "checkIsLeaf"]
                          /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                          nodeArr, insertRes, resultsForGet, 
                                          deleteRes, processWriterState, 
                                          processReaderState, values, stack, i, 
                                          cur, istack, current_d, key_, tmp_, 
                                          tmp1, mid_, p_, p1, recStack_, 
                                          recStack1_, ret, depth_, res, calc, 
                                          maxDepth, l, r, gNode, buf_, rmax, 
                                          links, recStack_s, mid_s, val, child, 
                                          lastNode, bits_, depth_s, newBits, 
                                          movedBits, ln, rn, current, sibling, 
                                          keyL, keyR, buf, bufLink, p_d, tempL, 
                                          tempR, bits_d, depth_d, key_i, 
                                          stackData, keyRNext, oldCurrent, 
                                          parent, keyLNext, links_, oldSibling, 
                                          current_, rev, bits_i, depth_i, p_i, 
                                          temp, scanRes, t, array, split, 
                                          recStack, answer, tmp, tmp2, l_, r_, 
                                          mid, sibling_, keyL_, keyR_, 
                                          recStack1, key_g, startValue, 
                                          depth_g, bits_g, key, start, p, bits, 
                                          depth, states >>

checkIsLeaf(self) == /\ pc[self] = "checkIsLeaf"
                     /\ IF gNodeArr[current_[self]].isLeaf
                           THEN /\ pc' = [pc EXCEPT ![self] = "checkGNodeSize"]
                                /\ UNCHANGED oldSibling
                           ELSE /\ IF gNodeArr[current_[self]].countNode >= SplitSize
                                      THEN /\ oldSibling' = [oldSibling EXCEPT ![self] = sibling_[self]]
                                           /\ pc' = [pc EXCEPT ![self] = "startFillBufLo"]
                                      ELSE /\ pc' = [pc EXCEPT ![self] = "doInsertNode3"]
                                           /\ UNCHANGED oldSibling
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, current_, rev, bits_i, depth_i, 
                                     p_i, temp, scanRes, t, array, split, 
                                     recStack, answer, tmp, tmp2, l_, r_, mid, 
                                     sibling_, keyL_, keyR_, recStack1, key_g, 
                                     startValue, depth_g, bits_g, key, start, 
                                     p, bits, depth, states >>

checkGNodeSize(self) == /\ pc[self] = "checkGNodeSize"
                        /\ IF gNodeArr[current_[self]].countNode >= SplitSize
                              THEN /\ array' = [array EXCEPT ![self] = <<>>]
                                   /\ recStack' = [recStack EXCEPT ![self] = <<>>]
                                   /\ pc' = [pc EXCEPT ![self] = "startFillBuf"]
                              ELSE /\ pc' = [pc EXCEPT ![self] = "insertLeaf3"]
                                   /\ UNCHANGED << array, recStack >>
                        /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                        nodeArr, insertRes, resultsForGet, 
                                        deleteRes, processWriterState, 
                                        processReaderState, values, stack, i, 
                                        cur, istack, current_d, key_, tmp_, 
                                        tmp1, mid_, p_, p1, recStack_, 
                                        recStack1_, ret, depth_, res, calc, 
                                        maxDepth, l, r, gNode, buf_, rmax, 
                                        links, recStack_s, mid_s, val, child, 
                                        lastNode, bits_, depth_s, newBits, 
                                        movedBits, ln, rn, current, sibling, 
                                        keyL, keyR, buf, bufLink, p_d, tempL, 
                                        tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        current_, rev, bits_i, depth_i, p_i, 
                                        temp, scanRes, t, split, answer, tmp, 
                                        tmp2, l_, r_, mid, sibling_, keyL_, 
                                        keyR_, recStack1, key_g, startValue, 
                                        depth_g, bits_g, key, start, p, bits, 
                                        depth, states >>

startFillBuf(self) == /\ pc[self] = "startFillBuf"
                      /\ p_i' = [p_i EXCEPT ![self] = gNodeArr[current_[self]].root]
                      /\ pc' = [pc EXCEPT ![self] = "checkLeft"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, temp, scanRes, t, 
                                      array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

checkLeft(self) == /\ pc[self] = "checkLeft"
                   /\ IF nodeArr[p_i[self]].hasLeft
                         THEN /\ recStack' = [recStack EXCEPT ![self] = Append(recStack[self], <<p_i[self], FALSE>>)]
                              /\ pc' = [pc EXCEPT ![self] = "goToLeft"]
                         ELSE /\ pc' = [pc EXCEPT ![self] = "saveAnswer2"]
                              /\ UNCHANGED recStack
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, resultsForGet, 
                                   deleteRes, processWriterState, 
                                   processReaderState, values, stack, i, cur, 
                                   istack, current_d, key_, tmp_, tmp1, mid_, 
                                   p_, p1, recStack_, recStack1_, ret, depth_, 
                                   res, calc, maxDepth, l, r, gNode, buf_, 
                                   rmax, links, recStack_s, mid_s, val, child, 
                                   lastNode, bits_, depth_s, newBits, 
                                   movedBits, ln, rn, current, sibling, keyL, 
                                   keyR, buf, bufLink, p_d, tempL, tempR, 
                                   bits_d, depth_d, key_i, stackData, keyRNext, 
                                   oldCurrent, parent, keyLNext, links_, 
                                   oldSibling, current_, rev, bits_i, depth_i, 
                                   p_i, temp, scanRes, t, array, split, answer, 
                                   tmp, tmp2, l_, r_, mid, sibling_, keyL_, 
                                   keyR_, recStack1, key_g, startValue, 
                                   depth_g, bits_g, key, start, p, bits, depth, 
                                   states >>

goToLeft(self) == /\ pc[self] = "goToLeft"
                  /\ p_i' = [p_i EXCEPT ![self] = nodeArr[p_i[self]].left]
                  /\ pc' = [pc EXCEPT ![self] = "checkLeft"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  temp, scanRes, t, array, split, recStack, 
                                  answer, tmp, tmp2, l_, r_, mid, sibling_, 
                                  keyL_, keyR_, recStack1, key_g, startValue, 
                                  depth_g, bits_g, key, start, p, bits, depth, 
                                  states >>

saveAnswer2(self) == /\ pc[self] = "saveAnswer2"
                     /\ IF ~nodeArr[p_i[self]].deleted
                           THEN /\ pc' = [pc EXCEPT ![self] = "saveAnswer"]
                           ELSE /\ pc' = [pc EXCEPT ![self] = "checkRight"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

saveAnswer(self) == /\ pc[self] = "saveAnswer"
                    /\ answer' = [answer EXCEPT ![self] = Append(answer[self], nodeArr[p_i[self]].value)]
                    /\ pc' = [pc EXCEPT ![self] = "checkRight"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, p1, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, recStack, tmp, tmp2, l_, r_, mid, 
                                    sibling_, keyL_, keyR_, recStack1, key_g, 
                                    startValue, depth_g, bits_g, key, start, p, 
                                    bits, depth, states >>

checkRight(self) == /\ pc[self] = "checkRight"
                    /\ IF nodeArr[p_i[self]].hasRight
                          THEN /\ recStack' = [recStack EXCEPT ![self] = Append(recStack[self], <<p_i[self], TRUE>>)]
                               /\ pc' = [pc EXCEPT ![self] = "goToRight"]
                          ELSE /\ pc' = [pc EXCEPT ![self] = "checkRecSize"]
                               /\ UNCHANGED recStack
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, p1, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, answer, tmp, tmp2, l_, r_, mid, 
                                    sibling_, keyL_, keyR_, recStack1, key_g, 
                                    startValue, depth_g, bits_g, key, start, p, 
                                    bits, depth, states >>

goToRight(self) == /\ pc[self] = "goToRight"
                   /\ p_i' = [p_i EXCEPT ![self] = nodeArr[p_i[self]].right]
                   /\ pc' = [pc EXCEPT ![self] = "checkLeft"]
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, resultsForGet, 
                                   deleteRes, processWriterState, 
                                   processReaderState, values, stack, i, cur, 
                                   istack, current_d, key_, tmp_, tmp1, mid_, 
                                   p_, p1, recStack_, recStack1_, ret, depth_, 
                                   res, calc, maxDepth, l, r, gNode, buf_, 
                                   rmax, links, recStack_s, mid_s, val, child, 
                                   lastNode, bits_, depth_s, newBits, 
                                   movedBits, ln, rn, current, sibling, keyL, 
                                   keyR, buf, bufLink, p_d, tempL, tempR, 
                                   bits_d, depth_d, key_i, stackData, keyRNext, 
                                   oldCurrent, parent, keyLNext, links_, 
                                   oldSibling, current_, rev, bits_i, depth_i, 
                                   temp, scanRes, t, array, split, recStack, 
                                   answer, tmp, tmp2, l_, r_, mid, sibling_, 
                                   keyL_, keyR_, recStack1, key_g, startValue, 
                                   depth_g, bits_g, key, start, p, bits, depth, 
                                   states >>

checkRecSize(self) == /\ pc[self] = "checkRecSize"
                      /\ IF Len(recStack[self]) = 0
                            THEN /\ pc' = [pc EXCEPT ![self] = "startUpdateCur"]
                            ELSE /\ pc' = [pc EXCEPT ![self] = "goToRet"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

goToRet(self) == /\ pc[self] = "goToRet"
                 /\ tmp' = [tmp EXCEPT ![self] = Tail(recStack[self])]
                 /\ recStack' = [recStack EXCEPT ![self] = SubSeq(recStack[self], 0, Len(recStack[self] - 1))]
                 /\ p_i' = [p_i EXCEPT ![self] = tmp'[self][0]]
                 /\ IF tmp'[self][1]
                       THEN /\ pc' = [pc EXCEPT ![self] = "checkRecSize"]
                       ELSE /\ pc' = [pc EXCEPT ![self] = "saveAnswer"]
                 /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                 insertRes, resultsForGet, deleteRes, 
                                 processWriterState, processReaderState, 
                                 values, stack, i, cur, istack, current_d, 
                                 key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                 recStack1_, ret, depth_, res, calc, maxDepth, 
                                 l, r, gNode, buf_, rmax, links, recStack_s, 
                                 mid_s, val, child, lastNode, bits_, depth_s, 
                                 newBits, movedBits, ln, rn, current, sibling, 
                                 keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                 bits_d, depth_d, key_i, stackData, keyRNext, 
                                 oldCurrent, parent, keyLNext, links_, 
                                 oldSibling, current_, rev, bits_i, depth_i, 
                                 temp, scanRes, t, array, split, answer, tmp2, 
                                 l_, r_, mid, sibling_, keyL_, keyR_, 
                                 recStack1, key_g, startValue, depth_g, bits_g, 
                                 key, start, p, bits, depth, states >>

startUpdateCur(self) == /\ pc[self] = "startUpdateCur"
                        /\ gNodeArr' = [gNodeArr EXCEPT ![current_[self]].rev = gNodeArr[current_[self]]]
                        /\ split' = [split EXCEPT ![self] = Len(answer[self]) \div 2]
                        /\ p_i' = [p_i EXCEPT ![self] = gNodeArr'[current_[self]].root]
                        /\ l_' = [l_ EXCEPT ![self] = 0]
                        /\ r_' = [r_ EXCEPT ![self] = split'[self] - 1]
                        /\ recStack' = [recStack EXCEPT ![self] = <<>>]
                        /\ pc' = [pc EXCEPT ![self] = "startFillVal"]
                        /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                        insertRes, resultsForGet, deleteRes, 
                                        processWriterState, processReaderState, 
                                        values, stack, i, cur, istack, 
                                        current_d, key_, tmp_, tmp1, mid_, p_, 
                                        p1, recStack_, recStack1_, ret, depth_, 
                                        res, calc, maxDepth, l, r, gNode, buf_, 
                                        rmax, links, recStack_s, mid_s, val, 
                                        child, lastNode, bits_, depth_s, 
                                        newBits, movedBits, ln, rn, current, 
                                        sibling, keyL, keyR, buf, bufLink, p_d, 
                                        tempL, tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        current_, rev, bits_i, depth_i, temp, 
                                        scanRes, t, array, answer, tmp, tmp2, 
                                        mid, sibling_, keyL_, keyR_, recStack1, 
                                        key_g, startValue, depth_g, bits_g, 
                                        key, start, p, bits, depth, states >>

startFillVal(self) == /\ pc[self] = "startFillVal"
                      /\ IF r_[self] < l_[self] \/ l_[self] = split[self]
                            THEN /\ pc' = [pc EXCEPT ![self] = "checkRecSize2"]
                            ELSE /\ pc' = [pc EXCEPT ![self] = "getMid"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

getMid(self) == /\ pc[self] = "getMid"
                /\ mid' = [mid EXCEPT ![self] = (l_[self] + r_[self]) \div 2]
                /\ pc' = [pc EXCEPT ![self] = "checkLeft2"]
                /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                insertRes, resultsForGet, deleteRes, 
                                processWriterState, processReaderState, values, 
                                stack, i, cur, istack, current_d, key_, tmp_, 
                                tmp1, mid_, p_, p1, recStack_, recStack1_, ret, 
                                depth_, res, calc, maxDepth, l, r, gNode, buf_, 
                                rmax, links, recStack_s, mid_s, val, child, 
                                lastNode, bits_, depth_s, newBits, movedBits, 
                                ln, rn, current, sibling, keyL, keyR, buf, 
                                bufLink, p_d, tempL, tempR, bits_d, depth_d, 
                                key_i, stackData, keyRNext, oldCurrent, parent, 
                                keyLNext, links_, oldSibling, current_, rev, 
                                bits_i, depth_i, p_i, temp, scanRes, t, array, 
                                split, recStack, answer, tmp, tmp2, l_, r_, 
                                sibling_, keyL_, keyR_, recStack1, key_g, 
                                startValue, depth_g, bits_g, key, start, p, 
                                bits, depth, states >>

checkLeft2(self) == /\ pc[self] = "checkLeft2"
                    /\ IF nodeArr[p_i[self]].hasLeft
                          THEN /\ recStack' = [recStack EXCEPT ![self] = Append(recStack[self], <<p_i[self], FALSE>>)]
                               /\ pc' = [pc EXCEPT ![self] = "goToLeft2"]
                          ELSE /\ pc' = [pc EXCEPT ![self] = "updateValue"]
                               /\ UNCHANGED recStack
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, p1, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, answer, tmp, tmp2, l_, r_, mid, 
                                    sibling_, keyL_, keyR_, recStack1, key_g, 
                                    startValue, depth_g, bits_g, key, start, p, 
                                    bits, depth, states >>

goToLeft2(self) == /\ pc[self] = "goToLeft2"
                   /\ p_i' = [p_i EXCEPT ![self] = nodeArr[p_i[self]].left]
                   /\ pc' = [pc EXCEPT ![self] = "startFillVal"]
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, resultsForGet, 
                                   deleteRes, processWriterState, 
                                   processReaderState, values, stack, i, cur, 
                                   istack, current_d, key_, tmp_, tmp1, mid_, 
                                   p_, p1, recStack_, recStack1_, ret, depth_, 
                                   res, calc, maxDepth, l, r, gNode, buf_, 
                                   rmax, links, recStack_s, mid_s, val, child, 
                                   lastNode, bits_, depth_s, newBits, 
                                   movedBits, ln, rn, current, sibling, keyL, 
                                   keyR, buf, bufLink, p_d, tempL, tempR, 
                                   bits_d, depth_d, key_i, stackData, keyRNext, 
                                   oldCurrent, parent, keyLNext, links_, 
                                   oldSibling, current_, rev, bits_i, depth_i, 
                                   temp, scanRes, t, array, split, recStack, 
                                   answer, tmp, tmp2, l_, r_, mid, sibling_, 
                                   keyL_, keyR_, recStack1, key_g, startValue, 
                                   depth_g, bits_g, key, start, p, bits, depth, 
                                   states >>

updateValue(self) == /\ pc[self] = "updateValue"
                     /\ nodeArr' = [nodeArr EXCEPT ![p_i[self]].value = array[self][mid[self]]]
                     /\ pc' = [pc EXCEPT ![self] = "saveAnswerHasValue7"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     insertRes, resultsForGet, deleteRes, 
                                     processWriterState, processReaderState, 
                                     values, stack, i, cur, istack, current_d, 
                                     key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                     recStack1_, ret, depth_, res, calc, 
                                     maxDepth, l, r, gNode, buf_, rmax, links, 
                                     recStack_s, mid_s, val, child, lastNode, 
                                     bits_, depth_s, newBits, movedBits, ln, 
                                     rn, current, sibling, keyL, keyR, buf, 
                                     bufLink, p_d, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

saveAnswerHasValue7(self) == /\ pc[self] = "saveAnswerHasValue7"
                             /\ nodeArr' = [nodeArr EXCEPT ![p_i[self]].hasValue = TRUE]
                             /\ pc' = [pc EXCEPT ![self] = "checkRight2"]
                             /\ UNCHANGED << root, hasRoot, globalLock, 
                                             gNodeArr, insertRes, 
                                             resultsForGet, deleteRes, 
                                             processWriterState, 
                                             processReaderState, values, stack, 
                                             i, cur, istack, current_d, key_, 
                                             tmp_, tmp1, mid_, p_, p1, 
                                             recStack_, recStack1_, ret, 
                                             depth_, res, calc, maxDepth, l, r, 
                                             gNode, buf_, rmax, links, 
                                             recStack_s, mid_s, val, child, 
                                             lastNode, bits_, depth_s, newBits, 
                                             movedBits, ln, rn, current, 
                                             sibling, keyL, keyR, buf, bufLink, 
                                             p_d, tempL, tempR, bits_d, 
                                             depth_d, key_i, stackData, 
                                             keyRNext, oldCurrent, parent, 
                                             keyLNext, links_, oldSibling, 
                                             current_, rev, bits_i, depth_i, 
                                             p_i, temp, scanRes, t, array, 
                                             split, recStack, answer, tmp, 
                                             tmp2, l_, r_, mid, sibling_, 
                                             keyL_, keyR_, recStack1, key_g, 
                                             startValue, depth_g, bits_g, key, 
                                             start, p, bits, depth, states >>

checkRight2(self) == /\ pc[self] = "checkRight2"
                     /\ IF nodeArr[p_i[self]].hasRight
                           THEN /\ recStack' = [recStack EXCEPT ![self] = Append(recStack[self], <<p_i[self], TRUE>>)]
                                /\ pc' = [pc EXCEPT ![self] = "goToRight2"]
                           ELSE /\ pc' = [pc EXCEPT ![self] = "checkRecSize2"]
                                /\ UNCHANGED recStack
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, answer, tmp, tmp2, l_, r_, mid, 
                                     sibling_, keyL_, keyR_, recStack1, key_g, 
                                     startValue, depth_g, bits_g, key, start, 
                                     p, bits, depth, states >>

goToRight2(self) == /\ pc[self] = "goToRight2"
                    /\ p_i' = [p_i EXCEPT ![self] = nodeArr[p_i[self]].right]
                    /\ pc' = [pc EXCEPT ![self] = "startFillVal"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, p1, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, temp, scanRes, t, array, split, 
                                    recStack, answer, tmp, tmp2, l_, r_, mid, 
                                    sibling_, keyL_, keyR_, recStack1, key_g, 
                                    startValue, depth_g, bits_g, key, start, p, 
                                    bits, depth, states >>

checkRecSize2(self) == /\ pc[self] = "checkRecSize2"
                       /\ IF Len(recStack[self]) = 0
                             THEN /\ pc' = [pc EXCEPT ![self] = "createSibling"]
                             ELSE /\ pc' = [pc EXCEPT ![self] = "getRetValue"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

getRetValue(self) == /\ pc[self] = "getRetValue"
                     /\ tmp' = [tmp EXCEPT ![self] = Tail(recStack[self])]
                     /\ recStack' = [recStack EXCEPT ![self] = SubSeq(recStack[self], 0, Len(recStack[self]) - 1)]
                     /\ p_i' = [p_i EXCEPT ![self] = tmp'[self][0]]
                     /\ IF tmp'[self][1]
                           THEN /\ pc' = [pc EXCEPT ![self] = "checkRecSize2"]
                           ELSE /\ pc' = [pc EXCEPT ![self] = "updateValue"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, temp, scanRes, t, array, split, 
                                     answer, tmp2, l_, r_, mid, sibling_, 
                                     keyL_, keyR_, recStack1, key_g, 
                                     startValue, depth_g, bits_g, key, start, 
                                     p, bits, depth, states >>

createSibling(self) == /\ pc[self] = "createSibling"
                       /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "initLeaf",
                                                                pc        |->  "setSibling_",
                                                                i         |->  i[self],
                                                                cur       |->  cur[self],
                                                                istack    |->  istack[self] ] >>
                                                            \o stack[self]]
                       /\ i' = [i EXCEPT ![self] = defaultInitValue]
                       /\ cur' = [cur EXCEPT ![self] = defaultInitValue]
                       /\ istack' = [istack EXCEPT ![self] = defaultInitValue]
                       /\ pc' = [pc EXCEPT ![self] = "initLeaf_"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, current_d, 
                                       key_, tmp_, tmp1, mid_, p_, p1, 
                                       recStack_, recStack1_, ret, depth_, res, 
                                       calc, maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

setSibling_(self) == /\ pc[self] = "setSibling_"
                     /\ sibling_' = [sibling_ EXCEPT ![self] = Len(gNodeArr)]
                     /\ pc' = [pc EXCEPT ![self] = "startUpdateSibling2"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, keyL_, keyR_, recStack1, key_g, 
                                     startValue, depth_g, bits_g, key, start, 
                                     p, bits, depth, states >>

startUpdateSibling2(self) == /\ pc[self] = "startUpdateSibling2"
                             /\ gNodeArr' = [gNodeArr EXCEPT ![sibling_[self]].rev = gNodeArr[sibling_[self]]]
                             /\ pc' = [pc EXCEPT ![self] = "updateSiblingHighKey"]
                             /\ UNCHANGED << root, hasRoot, globalLock, 
                                             nodeArr, insertRes, resultsForGet, 
                                             deleteRes, processWriterState, 
                                             processReaderState, values, stack, 
                                             i, cur, istack, current_d, key_, 
                                             tmp_, tmp1, mid_, p_, p1, 
                                             recStack_, recStack1_, ret, 
                                             depth_, res, calc, maxDepth, l, r, 
                                             gNode, buf_, rmax, links, 
                                             recStack_s, mid_s, val, child, 
                                             lastNode, bits_, depth_s, newBits, 
                                             movedBits, ln, rn, current, 
                                             sibling, keyL, keyR, buf, bufLink, 
                                             p_d, tempL, tempR, bits_d, 
                                             depth_d, key_i, stackData, 
                                             keyRNext, oldCurrent, parent, 
                                             keyLNext, links_, oldSibling, 
                                             current_, rev, bits_i, depth_i, 
                                             p_i, temp, scanRes, t, array, 
                                             split, recStack, answer, tmp, 
                                             tmp2, l_, r_, mid, sibling_, 
                                             keyL_, keyR_, recStack1, key_g, 
                                             startValue, depth_g, bits_g, key, 
                                             start, p, bits, depth, states >>

updateSiblingHighKey(self) == /\ pc[self] = "updateSiblingHighKey"
                              /\ gNodeArr' = [gNodeArr EXCEPT ![sibling_[self]].highKey = gNodeArr[current_[self]].highKey]
                              /\ pc' = [pc EXCEPT ![self] = "updateSibling"]
                              /\ UNCHANGED << root, hasRoot, globalLock, 
                                              nodeArr, insertRes, 
                                              resultsForGet, deleteRes, 
                                              processWriterState, 
                                              processReaderState, values, 
                                              stack, i, cur, istack, current_d, 
                                              key_, tmp_, tmp1, mid_, p_, p1, 
                                              recStack_, recStack1_, ret, 
                                              depth_, res, calc, maxDepth, l, 
                                              r, gNode, buf_, rmax, links, 
                                              recStack_s, mid_s, val, child, 
                                              lastNode, bits_, depth_s, 
                                              newBits, movedBits, ln, rn, 
                                              current, sibling, keyL, keyR, 
                                              buf, bufLink, p_d, tempL, tempR, 
                                              bits_d, depth_d, key_i, 
                                              stackData, keyRNext, oldCurrent, 
                                              parent, keyLNext, links_, 
                                              oldSibling, current_, rev, 
                                              bits_i, depth_i, p_i, temp, 
                                              scanRes, t, array, split, 
                                              recStack, answer, tmp, tmp2, l_, 
                                              r_, mid, sibling_, keyL_, keyR_, 
                                              recStack1, key_g, startValue, 
                                              depth_g, bits_g, key, start, p, 
                                              bits, depth, states >>

updateSibling(self) == /\ pc[self] = "updateSibling"
                       /\ gNodeArr' = [gNodeArr EXCEPT ![sibling_[self]].sibling = gNodeArr[sibling_[self]].sibling]
                       /\ pc' = [pc EXCEPT ![self] = "updateCurrentHighKey"]
                       /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                       insertRes, resultsForGet, deleteRes, 
                                       processWriterState, processReaderState, 
                                       values, stack, i, cur, istack, 
                                       current_d, key_, tmp_, tmp1, mid_, p_, 
                                       p1, recStack_, recStack1_, ret, depth_, 
                                       res, calc, maxDepth, l, r, gNode, buf_, 
                                       rmax, links, recStack_s, mid_s, val, 
                                       child, lastNode, bits_, depth_s, 
                                       newBits, movedBits, ln, rn, current, 
                                       sibling, keyL, keyR, buf, bufLink, p_d, 
                                       tempL, tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

updateCurrentHighKey(self) == /\ pc[self] = "updateCurrentHighKey"
                              /\ gNodeArr' = [gNodeArr EXCEPT ![current_[self]].highKey = answer[self][split[self]]]
                              /\ pc' = [pc EXCEPT ![self] = "updateCurrentSibling"]
                              /\ UNCHANGED << root, hasRoot, globalLock, 
                                              nodeArr, insertRes, 
                                              resultsForGet, deleteRes, 
                                              processWriterState, 
                                              processReaderState, values, 
                                              stack, i, cur, istack, current_d, 
                                              key_, tmp_, tmp1, mid_, p_, p1, 
                                              recStack_, recStack1_, ret, 
                                              depth_, res, calc, maxDepth, l, 
                                              r, gNode, buf_, rmax, links, 
                                              recStack_s, mid_s, val, child, 
                                              lastNode, bits_, depth_s, 
                                              newBits, movedBits, ln, rn, 
                                              current, sibling, keyL, keyR, 
                                              buf, bufLink, p_d, tempL, tempR, 
                                              bits_d, depth_d, key_i, 
                                              stackData, keyRNext, oldCurrent, 
                                              parent, keyLNext, links_, 
                                              oldSibling, current_, rev, 
                                              bits_i, depth_i, p_i, temp, 
                                              scanRes, t, array, split, 
                                              recStack, answer, tmp, tmp2, l_, 
                                              r_, mid, sibling_, keyL_, keyR_, 
                                              recStack1, key_g, startValue, 
                                              depth_g, bits_g, key, start, p, 
                                              bits, depth, states >>

updateCurrentSibling(self) == /\ pc[self] = "updateCurrentSibling"
                              /\ gNodeArr' = [gNodeArr EXCEPT ![current_[self]].sibling = sibling_[self]]
                              /\ pc' = [pc EXCEPT ![self] = "finishUpdateCurrent4"]
                              /\ UNCHANGED << root, hasRoot, globalLock, 
                                              nodeArr, insertRes, 
                                              resultsForGet, deleteRes, 
                                              processWriterState, 
                                              processReaderState, values, 
                                              stack, i, cur, istack, current_d, 
                                              key_, tmp_, tmp1, mid_, p_, p1, 
                                              recStack_, recStack1_, ret, 
                                              depth_, res, calc, maxDepth, l, 
                                              r, gNode, buf_, rmax, links, 
                                              recStack_s, mid_s, val, child, 
                                              lastNode, bits_, depth_s, 
                                              newBits, movedBits, ln, rn, 
                                              current, sibling, keyL, keyR, 
                                              buf, bufLink, p_d, tempL, tempR, 
                                              bits_d, depth_d, key_i, 
                                              stackData, keyRNext, oldCurrent, 
                                              parent, keyLNext, links_, 
                                              oldSibling, current_, rev, 
                                              bits_i, depth_i, p_i, temp, 
                                              scanRes, t, array, split, 
                                              recStack, answer, tmp, tmp2, l_, 
                                              r_, mid, sibling_, keyL_, keyR_, 
                                              recStack1, key_g, startValue, 
                                              depth_g, bits_g, key, start, p, 
                                              bits, depth, states >>

finishUpdateCurrent4(self) == /\ pc[self] = "finishUpdateCurrent4"
                              /\ gNodeArr' = [gNodeArr EXCEPT ![current_[self]].rev = gNodeArr[current_[self]]]
                              /\ p_i' = [p_i EXCEPT ![self] = gNodeArr'[sibling_[self]].root]
                              /\ l_' = [l_ EXCEPT ![self] = split[self]]
                              /\ r_' = [r_ EXCEPT ![self] = Len(answer[self]) - 1]
                              /\ recStack' = [recStack EXCEPT ![self] = <<>>]
                              /\ pc' = [pc EXCEPT ![self] = "startFillVal2"]
                              /\ UNCHANGED << root, hasRoot, globalLock, 
                                              nodeArr, insertRes, 
                                              resultsForGet, deleteRes, 
                                              processWriterState, 
                                              processReaderState, values, 
                                              stack, i, cur, istack, current_d, 
                                              key_, tmp_, tmp1, mid_, p_, p1, 
                                              recStack_, recStack1_, ret, 
                                              depth_, res, calc, maxDepth, l, 
                                              r, gNode, buf_, rmax, links, 
                                              recStack_s, mid_s, val, child, 
                                              lastNode, bits_, depth_s, 
                                              newBits, movedBits, ln, rn, 
                                              current, sibling, keyL, keyR, 
                                              buf, bufLink, p_d, tempL, tempR, 
                                              bits_d, depth_d, key_i, 
                                              stackData, keyRNext, oldCurrent, 
                                              parent, keyLNext, links_, 
                                              oldSibling, current_, rev, 
                                              bits_i, depth_i, temp, scanRes, 
                                              t, array, split, answer, tmp, 
                                              tmp2, mid, sibling_, keyL_, 
                                              keyR_, recStack1, key_g, 
                                              startValue, depth_g, bits_g, key, 
                                              start, p, bits, depth, states >>

startFillVal2(self) == /\ pc[self] = "startFillVal2"
                       /\ IF r_[self] < l_[self] \/ l_[self] = Len(answer[self])
                             THEN /\ pc' = [pc EXCEPT ![self] = "checkRecSize3"]
                             ELSE /\ pc' = [pc EXCEPT ![self] = "getMid2"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

getMid2(self) == /\ pc[self] = "getMid2"
                 /\ mid' = [mid EXCEPT ![self] = (l_[self] + r_[self]) \div 2]
                 /\ pc' = [pc EXCEPT ![self] = "checkLeft3"]
                 /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                 insertRes, resultsForGet, deleteRes, 
                                 processWriterState, processReaderState, 
                                 values, stack, i, cur, istack, current_d, 
                                 key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                 recStack1_, ret, depth_, res, calc, maxDepth, 
                                 l, r, gNode, buf_, rmax, links, recStack_s, 
                                 mid_s, val, child, lastNode, bits_, depth_s, 
                                 newBits, movedBits, ln, rn, current, sibling, 
                                 keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                 bits_d, depth_d, key_i, stackData, keyRNext, 
                                 oldCurrent, parent, keyLNext, links_, 
                                 oldSibling, current_, rev, bits_i, depth_i, 
                                 p_i, temp, scanRes, t, array, split, recStack, 
                                 answer, tmp, tmp2, l_, r_, sibling_, keyL_, 
                                 keyR_, recStack1, key_g, startValue, depth_g, 
                                 bits_g, key, start, p, bits, depth, states >>

checkLeft3(self) == /\ pc[self] = "checkLeft3"
                    /\ IF nodeArr[p_i[self]].hasLeft
                          THEN /\ recStack' = [recStack EXCEPT ![self] = Append(recStack[self], <<p_i[self], l_[self], r_[self], FALSE>>)]
                               /\ pc' = [pc EXCEPT ![self] = "goToLeft3"]
                          ELSE /\ pc' = [pc EXCEPT ![self] = "saveAnswer3"]
                               /\ UNCHANGED recStack
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, p1, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, answer, tmp, tmp2, l_, r_, mid, 
                                    sibling_, keyL_, keyR_, recStack1, key_g, 
                                    startValue, depth_g, bits_g, key, start, p, 
                                    bits, depth, states >>

goToLeft3(self) == /\ pc[self] = "goToLeft3"
                   /\ p_i' = [p_i EXCEPT ![self] = nodeArr[p_i[self]].left]
                   /\ r_' = [r_ EXCEPT ![self] = mid[self] - 1]
                   /\ pc' = [pc EXCEPT ![self] = "startFillVal2"]
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, resultsForGet, 
                                   deleteRes, processWriterState, 
                                   processReaderState, values, stack, i, cur, 
                                   istack, current_d, key_, tmp_, tmp1, mid_, 
                                   p_, p1, recStack_, recStack1_, ret, depth_, 
                                   res, calc, maxDepth, l, r, gNode, buf_, 
                                   rmax, links, recStack_s, mid_s, val, child, 
                                   lastNode, bits_, depth_s, newBits, 
                                   movedBits, ln, rn, current, sibling, keyL, 
                                   keyR, buf, bufLink, p_d, tempL, tempR, 
                                   bits_d, depth_d, key_i, stackData, keyRNext, 
                                   oldCurrent, parent, keyLNext, links_, 
                                   oldSibling, current_, rev, bits_i, depth_i, 
                                   temp, scanRes, t, array, split, recStack, 
                                   answer, tmp, tmp2, l_, mid, sibling_, keyL_, 
                                   keyR_, recStack1, key_g, startValue, 
                                   depth_g, bits_g, key, start, p, bits, depth, 
                                   states >>

saveAnswer3(self) == /\ pc[self] = "saveAnswer3"
                     /\ nodeArr' = [nodeArr EXCEPT ![p_i[self]].value = array[self][mid[self]]]
                     /\ pc' = [pc EXCEPT ![self] = "saveAnswerHasValue2"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     insertRes, resultsForGet, deleteRes, 
                                     processWriterState, processReaderState, 
                                     values, stack, i, cur, istack, current_d, 
                                     key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                     recStack1_, ret, depth_, res, calc, 
                                     maxDepth, l, r, gNode, buf_, rmax, links, 
                                     recStack_s, mid_s, val, child, lastNode, 
                                     bits_, depth_s, newBits, movedBits, ln, 
                                     rn, current, sibling, keyL, keyR, buf, 
                                     bufLink, p_d, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

saveAnswerHasValue2(self) == /\ pc[self] = "saveAnswerHasValue2"
                             /\ nodeArr' = [nodeArr EXCEPT ![p_i[self]].hasValue = TRUE]
                             /\ pc' = [pc EXCEPT ![self] = "checkRight3"]
                             /\ UNCHANGED << root, hasRoot, globalLock, 
                                             gNodeArr, insertRes, 
                                             resultsForGet, deleteRes, 
                                             processWriterState, 
                                             processReaderState, values, stack, 
                                             i, cur, istack, current_d, key_, 
                                             tmp_, tmp1, mid_, p_, p1, 
                                             recStack_, recStack1_, ret, 
                                             depth_, res, calc, maxDepth, l, r, 
                                             gNode, buf_, rmax, links, 
                                             recStack_s, mid_s, val, child, 
                                             lastNode, bits_, depth_s, newBits, 
                                             movedBits, ln, rn, current, 
                                             sibling, keyL, keyR, buf, bufLink, 
                                             p_d, tempL, tempR, bits_d, 
                                             depth_d, key_i, stackData, 
                                             keyRNext, oldCurrent, parent, 
                                             keyLNext, links_, oldSibling, 
                                             current_, rev, bits_i, depth_i, 
                                             p_i, temp, scanRes, t, array, 
                                             split, recStack, answer, tmp, 
                                             tmp2, l_, r_, mid, sibling_, 
                                             keyL_, keyR_, recStack1, key_g, 
                                             startValue, depth_g, bits_g, key, 
                                             start, p, bits, depth, states >>

checkRight3(self) == /\ pc[self] = "checkRight3"
                     /\ IF nodeArr[p_i[self]].hasRight
                           THEN /\ recStack' = [recStack EXCEPT ![self] = Append(recStack[self], <<p_i[self], l_[self], r_[self], TRUE>>)]
                                /\ pc' = [pc EXCEPT ![self] = "goToRight3"]
                           ELSE /\ pc' = [pc EXCEPT ![self] = "checkRecSize3"]
                                /\ UNCHANGED recStack
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, answer, tmp, tmp2, l_, r_, mid, 
                                     sibling_, keyL_, keyR_, recStack1, key_g, 
                                     startValue, depth_g, bits_g, key, start, 
                                     p, bits, depth, states >>

goToRight3(self) == /\ pc[self] = "goToRight3"
                    /\ p_i' = [p_i EXCEPT ![self] = nodeArr[p_i[self]].right]
                    /\ l_' = [l_ EXCEPT ![self] = mid[self] + 1]
                    /\ pc' = [pc EXCEPT ![self] = "startFillVal2"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, p1, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, temp, scanRes, t, array, split, 
                                    recStack, answer, tmp, tmp2, r_, mid, 
                                    sibling_, keyL_, keyR_, recStack1, key_g, 
                                    startValue, depth_g, bits_g, key, start, p, 
                                    bits, depth, states >>

checkRecSize3(self) == /\ pc[self] = "checkRecSize3"
                       /\ IF Len(recStack[self]) = 0
                             THEN /\ pc' = [pc EXCEPT ![self] = "finishUpdateSibling2"]
                             ELSE /\ pc' = [pc EXCEPT ![self] = "goToRet3"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

goToRet3(self) == /\ pc[self] = "goToRet3"
                  /\ tmp' = [tmp EXCEPT ![self] = Tail(recStack[self])]
                  /\ recStack' = [recStack EXCEPT ![self] = SubSeq(recStack[self], 0, Len(recStack[self]) - 1)]
                  /\ p_i' = [p_i EXCEPT ![self] = tmp'[self][0]]
                  /\ l_' = [l_ EXCEPT ![self] = tmp'[self][1]]
                  /\ r_' = [r_ EXCEPT ![self] = tmp'[self][2]]
                  /\ IF tmp'[self][3]
                        THEN /\ pc' = [pc EXCEPT ![self] = "checkRecSize3"]
                        ELSE /\ pc' = [pc EXCEPT ![self] = "saveAnswer2"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  temp, scanRes, t, array, split, answer, tmp2, 
                                  mid, sibling_, keyL_, keyR_, recStack1, 
                                  key_g, startValue, depth_g, bits_g, key, 
                                  start, p, bits, depth, states >>

finishUpdateSibling2(self) == /\ pc[self] = "finishUpdateSibling2"
                              /\ gNodeArr' = [gNodeArr EXCEPT ![sibling_[self]].rev = gNodeArr[sibling_[self]]]
                              /\ pc' = [pc EXCEPT ![self] = "updateCurrentCount"]
                              /\ UNCHANGED << root, hasRoot, globalLock, 
                                              nodeArr, insertRes, 
                                              resultsForGet, deleteRes, 
                                              processWriterState, 
                                              processReaderState, values, 
                                              stack, i, cur, istack, current_d, 
                                              key_, tmp_, tmp1, mid_, p_, p1, 
                                              recStack_, recStack1_, ret, 
                                              depth_, res, calc, maxDepth, l, 
                                              r, gNode, buf_, rmax, links, 
                                              recStack_s, mid_s, val, child, 
                                              lastNode, bits_, depth_s, 
                                              newBits, movedBits, ln, rn, 
                                              current, sibling, keyL, keyR, 
                                              buf, bufLink, p_d, tempL, tempR, 
                                              bits_d, depth_d, key_i, 
                                              stackData, keyRNext, oldCurrent, 
                                              parent, keyLNext, links_, 
                                              oldSibling, current_, rev, 
                                              bits_i, depth_i, p_i, temp, 
                                              scanRes, t, array, split, 
                                              recStack, answer, tmp, tmp2, l_, 
                                              r_, mid, sibling_, keyL_, keyR_, 
                                              recStack1, key_g, startValue, 
                                              depth_g, bits_g, key, start, p, 
                                              bits, depth, states >>

updateCurrentCount(self) == /\ pc[self] = "updateCurrentCount"
                            /\ gNodeArr' = [gNodeArr EXCEPT ![current_[self]].countNode = split[self]]
                            /\ pc' = [pc EXCEPT ![self] = "updateSiblingCount"]
                            /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                            insertRes, resultsForGet, 
                                            deleteRes, processWriterState, 
                                            processReaderState, values, stack, 
                                            i, cur, istack, current_d, key_, 
                                            tmp_, tmp1, mid_, p_, p1, 
                                            recStack_, recStack1_, ret, depth_, 
                                            res, calc, maxDepth, l, r, gNode, 
                                            buf_, rmax, links, recStack_s, 
                                            mid_s, val, child, lastNode, bits_, 
                                            depth_s, newBits, movedBits, ln, 
                                            rn, current, sibling, keyL, keyR, 
                                            buf, bufLink, p_d, tempL, tempR, 
                                            bits_d, depth_d, key_i, stackData, 
                                            keyRNext, oldCurrent, parent, 
                                            keyLNext, links_, oldSibling, 
                                            current_, rev, bits_i, depth_i, 
                                            p_i, temp, scanRes, t, array, 
                                            split, recStack, answer, tmp, tmp2, 
                                            l_, r_, mid, sibling_, keyL_, 
                                            keyR_, recStack1, key_g, 
                                            startValue, depth_g, bits_g, key, 
                                            start, p, bits, depth, states >>

updateSiblingCount(self) == /\ pc[self] = "updateSiblingCount"
                            /\ gNodeArr' = [gNodeArr EXCEPT ![sibling_[self]].countNode = Len(answer[self]) - split[self]]
                            /\ keyR_' = [keyR_ EXCEPT ![self] = answer[self][split[self]]]
                            /\ keyL_' = [keyL_ EXCEPT ![self] = answer[self][0]]
                            /\ pc' = [pc EXCEPT ![self] = "checkCurrentHighKey"]
                            /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                            insertRes, resultsForGet, 
                                            deleteRes, processWriterState, 
                                            processReaderState, values, stack, 
                                            i, cur, istack, current_d, key_, 
                                            tmp_, tmp1, mid_, p_, p1, 
                                            recStack_, recStack1_, ret, depth_, 
                                            res, calc, maxDepth, l, r, gNode, 
                                            buf_, rmax, links, recStack_s, 
                                            mid_s, val, child, lastNode, bits_, 
                                            depth_s, newBits, movedBits, ln, 
                                            rn, current, sibling, keyL, keyR, 
                                            buf, bufLink, p_d, tempL, tempR, 
                                            bits_d, depth_d, key_i, stackData, 
                                            keyRNext, oldCurrent, parent, 
                                            keyLNext, links_, oldSibling, 
                                            current_, rev, bits_i, depth_i, 
                                            p_i, temp, scanRes, t, array, 
                                            split, recStack, answer, tmp, tmp2, 
                                            l_, r_, mid, sibling_, recStack1, 
                                            key_g, startValue, depth_g, bits_g, 
                                            key, start, p, bits, depth, states >>

checkCurrentHighKey(self) == /\ pc[self] = "checkCurrentHighKey"
                             /\ IF key_i[self] >= gNodeArr[current_[self]].highKey
                                   THEN /\ pc' = [pc EXCEPT ![self] = "insertLeaf"]
                                   ELSE /\ pc' = [pc EXCEPT ![self] = "insertLeaf2"]
                             /\ UNCHANGED << root, hasRoot, globalLock, 
                                             gNodeArr, nodeArr, insertRes, 
                                             resultsForGet, deleteRes, 
                                             processWriterState, 
                                             processReaderState, values, stack, 
                                             i, cur, istack, current_d, key_, 
                                             tmp_, tmp1, mid_, p_, p1, 
                                             recStack_, recStack1_, ret, 
                                             depth_, res, calc, maxDepth, l, r, 
                                             gNode, buf_, rmax, links, 
                                             recStack_s, mid_s, val, child, 
                                             lastNode, bits_, depth_s, newBits, 
                                             movedBits, ln, rn, current, 
                                             sibling, keyL, keyR, buf, bufLink, 
                                             p_d, tempL, tempR, bits_d, 
                                             depth_d, key_i, stackData, 
                                             keyRNext, oldCurrent, parent, 
                                             keyLNext, links_, oldSibling, 
                                             current_, rev, bits_i, depth_i, 
                                             p_i, temp, scanRes, t, array, 
                                             split, recStack, answer, tmp, 
                                             tmp2, l_, r_, mid, sibling_, 
                                             keyL_, keyR_, recStack1, key_g, 
                                             startValue, depth_g, bits_g, key, 
                                             start, p, bits, depth, states >>

insertLeaf(self) == /\ pc[self] = "insertLeaf"
                    /\ current_' = [current_ EXCEPT ![self] = gNodeArr[current_[self]].sibling]
                    /\ /\ current_d' = [current_d EXCEPT ![self] = current_'[self]]
                       /\ key_' = [key_ EXCEPT ![self] = key_i[self]]
                       /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "doInsertLeaf",
                                                                pc        |->  "rootRebalance",
                                                                tmp_      |->  tmp_[self],
                                                                tmp1      |->  tmp1[self],
                                                                mid_      |->  mid_[self],
                                                                p_        |->  p_[self],
                                                                p1        |->  p1[self],
                                                                recStack_ |->  recStack_[self],
                                                                recStack1_ |->  recStack1_[self],
                                                                ret       |->  ret[self],
                                                                depth_    |->  depth_[self],
                                                                res       |->  res[self],
                                                                calc      |->  calc[self],
                                                                maxDepth  |->  maxDepth[self],
                                                                current_d |->  current_d[self],
                                                                key_      |->  key_[self] ] >>
                                                            \o stack[self]]
                    /\ tmp_' = [tmp_ EXCEPT ![self] = defaultInitValue]
                    /\ tmp1' = [tmp1 EXCEPT ![self] = defaultInitValue]
                    /\ mid_' = [mid_ EXCEPT ![self] = defaultInitValue]
                    /\ p_' = [p_ EXCEPT ![self] = defaultInitValue]
                    /\ p1' = [p1 EXCEPT ![self] = defaultInitValue]
                    /\ recStack_' = [recStack_ EXCEPT ![self] = defaultInitValue]
                    /\ recStack1_' = [recStack1_ EXCEPT ![self] = defaultInitValue]
                    /\ ret' = [ret EXCEPT ![self] = defaultInitValue]
                    /\ depth_' = [depth_ EXCEPT ![self] = defaultInitValue]
                    /\ res' = [res EXCEPT ![self] = defaultInitValue]
                    /\ calc' = [calc EXCEPT ![self] = defaultInitValue]
                    /\ maxDepth' = [maxDepth EXCEPT ![self] = defaultInitValue]
                    /\ pc' = [pc EXCEPT ![self] = "getCurrentRoot"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, i, cur, istack, 
                                    l, r, gNode, buf_, rmax, links, recStack_s, 
                                    mid_s, val, child, lastNode, bits_, 
                                    depth_s, newBits, movedBits, ln, rn, 
                                    current, sibling, keyL, keyR, buf, bufLink, 
                                    p_d, tempL, tempR, bits_d, depth_d, key_i, 
                                    stackData, keyRNext, oldCurrent, parent, 
                                    keyLNext, links_, oldSibling, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, recStack, answer, tmp, tmp2, l_, r_, 
                                    mid, sibling_, keyL_, keyR_, recStack1, 
                                    key_g, startValue, depth_g, bits_g, key, 
                                    start, p, bits, depth, states >>

insertLeaf2(self) == /\ pc[self] = "insertLeaf2"
                     /\ /\ current_d' = [current_d EXCEPT ![self] = current_[self]]
                        /\ key_' = [key_ EXCEPT ![self] = key_i[self]]
                        /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "doInsertLeaf",
                                                                 pc        |->  "rootRebalance",
                                                                 tmp_      |->  tmp_[self],
                                                                 tmp1      |->  tmp1[self],
                                                                 mid_      |->  mid_[self],
                                                                 p_        |->  p_[self],
                                                                 p1        |->  p1[self],
                                                                 recStack_ |->  recStack_[self],
                                                                 recStack1_ |->  recStack1_[self],
                                                                 ret       |->  ret[self],
                                                                 depth_    |->  depth_[self],
                                                                 res       |->  res[self],
                                                                 calc      |->  calc[self],
                                                                 maxDepth  |->  maxDepth[self],
                                                                 current_d |->  current_d[self],
                                                                 key_      |->  key_[self] ] >>
                                                             \o stack[self]]
                     /\ tmp_' = [tmp_ EXCEPT ![self] = defaultInitValue]
                     /\ tmp1' = [tmp1 EXCEPT ![self] = defaultInitValue]
                     /\ mid_' = [mid_ EXCEPT ![self] = defaultInitValue]
                     /\ p_' = [p_ EXCEPT ![self] = defaultInitValue]
                     /\ p1' = [p1 EXCEPT ![self] = defaultInitValue]
                     /\ recStack_' = [recStack_ EXCEPT ![self] = defaultInitValue]
                     /\ recStack1_' = [recStack1_ EXCEPT ![self] = defaultInitValue]
                     /\ ret' = [ret EXCEPT ![self] = defaultInitValue]
                     /\ depth_' = [depth_ EXCEPT ![self] = defaultInitValue]
                     /\ res' = [res EXCEPT ![self] = defaultInitValue]
                     /\ calc' = [calc EXCEPT ![self] = defaultInitValue]
                     /\ maxDepth' = [maxDepth EXCEPT ![self] = defaultInitValue]
                     /\ pc' = [pc EXCEPT ![self] = "getCurrentRoot"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, i, cur, 
                                     istack, l, r, gNode, buf_, rmax, links, 
                                     recStack_s, mid_s, val, child, lastNode, 
                                     bits_, depth_s, newBits, movedBits, ln, 
                                     rn, current, sibling, keyL, keyR, buf, 
                                     bufLink, p_d, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

insertLeaf3(self) == /\ pc[self] = "insertLeaf3"
                     /\ /\ current_d' = [current_d EXCEPT ![self] = current_[self]]
                        /\ key_' = [key_ EXCEPT ![self] = key_i[self]]
                        /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "doInsertLeaf",
                                                                 pc        |->  "releaseGNodelock2",
                                                                 tmp_      |->  tmp_[self],
                                                                 tmp1      |->  tmp1[self],
                                                                 mid_      |->  mid_[self],
                                                                 p_        |->  p_[self],
                                                                 p1        |->  p1[self],
                                                                 recStack_ |->  recStack_[self],
                                                                 recStack1_ |->  recStack1_[self],
                                                                 ret       |->  ret[self],
                                                                 depth_    |->  depth_[self],
                                                                 res       |->  res[self],
                                                                 calc      |->  calc[self],
                                                                 maxDepth  |->  maxDepth[self],
                                                                 current_d |->  current_d[self],
                                                                 key_      |->  key_[self] ] >>
                                                             \o stack[self]]
                     /\ tmp_' = [tmp_ EXCEPT ![self] = defaultInitValue]
                     /\ tmp1' = [tmp1 EXCEPT ![self] = defaultInitValue]
                     /\ mid_' = [mid_ EXCEPT ![self] = defaultInitValue]
                     /\ p_' = [p_ EXCEPT ![self] = defaultInitValue]
                     /\ p1' = [p1 EXCEPT ![self] = defaultInitValue]
                     /\ recStack_' = [recStack_ EXCEPT ![self] = defaultInitValue]
                     /\ recStack1_' = [recStack1_ EXCEPT ![self] = defaultInitValue]
                     /\ ret' = [ret EXCEPT ![self] = defaultInitValue]
                     /\ depth_' = [depth_ EXCEPT ![self] = defaultInitValue]
                     /\ res' = [res EXCEPT ![self] = defaultInitValue]
                     /\ calc' = [calc EXCEPT ![self] = defaultInitValue]
                     /\ maxDepth' = [maxDepth EXCEPT ![self] = defaultInitValue]
                     /\ pc' = [pc EXCEPT ![self] = "getCurrentRoot"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, i, cur, 
                                     istack, l, r, gNode, buf_, rmax, links, 
                                     recStack_s, mid_s, val, child, lastNode, 
                                     bits_, depth_s, newBits, movedBits, ln, 
                                     rn, current, sibling, keyL, keyR, buf, 
                                     bufLink, p_d, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

releaseGNodelock2(self) == /\ pc[self] = "releaseGNodelock2"
                           /\ gNodeArr' = [gNodeArr EXCEPT ![current_[self]].lock = TRUE]
                           /\ pc' = [pc EXCEPT ![self] = "rerturnInsert2"]
                           /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                           insertRes, resultsForGet, deleteRes, 
                                           processWriterState, 
                                           processReaderState, values, stack, 
                                           i, cur, istack, current_d, key_, 
                                           tmp_, tmp1, mid_, p_, p1, recStack_, 
                                           recStack1_, ret, depth_, res, calc, 
                                           maxDepth, l, r, gNode, buf_, rmax, 
                                           links, recStack_s, mid_s, val, 
                                           child, lastNode, bits_, depth_s, 
                                           newBits, movedBits, ln, rn, current, 
                                           sibling, keyL, keyR, buf, bufLink, 
                                           p_d, tempL, tempR, bits_d, depth_d, 
                                           key_i, stackData, keyRNext, 
                                           oldCurrent, parent, keyLNext, 
                                           links_, oldSibling, current_, rev, 
                                           bits_i, depth_i, p_i, temp, scanRes, 
                                           t, array, split, recStack, answer, 
                                           tmp, tmp2, l_, r_, mid, sibling_, 
                                           keyL_, keyR_, recStack1, key_g, 
                                           startValue, depth_g, bits_g, key, 
                                           start, p, bits, depth, states >>

rerturnInsert2(self) == /\ pc[self] = "rerturnInsert2"
                        /\ insertRes' = [insertRes EXCEPT ![self] = TRUE]
                        /\ pc' = [pc EXCEPT ![self] = Head(stack[self]).pc]
                        /\ stackData' = [stackData EXCEPT ![self] = Head(stack[self]).stackData]
                        /\ keyRNext' = [keyRNext EXCEPT ![self] = Head(stack[self]).keyRNext]
                        /\ oldCurrent' = [oldCurrent EXCEPT ![self] = Head(stack[self]).oldCurrent]
                        /\ parent' = [parent EXCEPT ![self] = Head(stack[self]).parent]
                        /\ keyLNext' = [keyLNext EXCEPT ![self] = Head(stack[self]).keyLNext]
                        /\ links_' = [links_ EXCEPT ![self] = Head(stack[self]).links_]
                        /\ oldSibling' = [oldSibling EXCEPT ![self] = Head(stack[self]).oldSibling]
                        /\ current_' = [current_ EXCEPT ![self] = Head(stack[self]).current_]
                        /\ rev' = [rev EXCEPT ![self] = Head(stack[self]).rev]
                        /\ bits_i' = [bits_i EXCEPT ![self] = Head(stack[self]).bits_i]
                        /\ depth_i' = [depth_i EXCEPT ![self] = Head(stack[self]).depth_i]
                        /\ p_i' = [p_i EXCEPT ![self] = Head(stack[self]).p_i]
                        /\ temp' = [temp EXCEPT ![self] = Head(stack[self]).temp]
                        /\ scanRes' = [scanRes EXCEPT ![self] = Head(stack[self]).scanRes]
                        /\ t' = [t EXCEPT ![self] = Head(stack[self]).t]
                        /\ array' = [array EXCEPT ![self] = Head(stack[self]).array]
                        /\ split' = [split EXCEPT ![self] = Head(stack[self]).split]
                        /\ recStack' = [recStack EXCEPT ![self] = Head(stack[self]).recStack]
                        /\ answer' = [answer EXCEPT ![self] = Head(stack[self]).answer]
                        /\ tmp' = [tmp EXCEPT ![self] = Head(stack[self]).tmp]
                        /\ tmp2' = [tmp2 EXCEPT ![self] = Head(stack[self]).tmp2]
                        /\ l_' = [l_ EXCEPT ![self] = Head(stack[self]).l_]
                        /\ r_' = [r_ EXCEPT ![self] = Head(stack[self]).r_]
                        /\ mid' = [mid EXCEPT ![self] = Head(stack[self]).mid]
                        /\ sibling_' = [sibling_ EXCEPT ![self] = Head(stack[self]).sibling_]
                        /\ keyL_' = [keyL_ EXCEPT ![self] = Head(stack[self]).keyL_]
                        /\ keyR_' = [keyR_ EXCEPT ![self] = Head(stack[self]).keyR_]
                        /\ recStack1' = [recStack1 EXCEPT ![self] = Head(stack[self]).recStack1]
                        /\ key_i' = [key_i EXCEPT ![self] = Head(stack[self]).key_i]
                        /\ stack' = [stack EXCEPT ![self] = Tail(stack[self])]
                        /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                        nodeArr, resultsForGet, deleteRes, 
                                        processWriterState, processReaderState, 
                                        values, i, cur, istack, current_d, 
                                        key_, tmp_, tmp1, mid_, p_, p1, 
                                        recStack_, recStack1_, ret, depth_, 
                                        res, calc, maxDepth, l, r, gNode, buf_, 
                                        rmax, links, recStack_s, mid_s, val, 
                                        child, lastNode, bits_, depth_s, 
                                        newBits, movedBits, ln, rn, current, 
                                        sibling, keyL, keyR, buf, bufLink, p_d, 
                                        tempL, tempR, bits_d, depth_d, key_g, 
                                        startValue, depth_g, bits_g, key, 
                                        start, p, bits, depth, states >>

startFillBufLo(self) == /\ pc[self] = "startFillBufLo"
                        /\ p_i' = [p_i EXCEPT ![self] = gNodeArr[current_[self]].root]
                        /\ answer' = [answer EXCEPT ![self] = <<>>]
                        /\ pc' = [pc EXCEPT ![self] = "fboCheckLeftRight"]
                        /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                        nodeArr, insertRes, resultsForGet, 
                                        deleteRes, processWriterState, 
                                        processReaderState, values, stack, i, 
                                        cur, istack, current_d, key_, tmp_, 
                                        tmp1, mid_, p_, p1, recStack_, 
                                        recStack1_, ret, depth_, res, calc, 
                                        maxDepth, l, r, gNode, buf_, rmax, 
                                        links, recStack_s, mid_s, val, child, 
                                        lastNode, bits_, depth_s, newBits, 
                                        movedBits, ln, rn, current, sibling, 
                                        keyL, keyR, buf, bufLink, p_d, tempL, 
                                        tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        current_, rev, bits_i, depth_i, temp, 
                                        scanRes, t, array, split, recStack, 
                                        tmp, tmp2, l_, r_, mid, sibling_, 
                                        keyL_, keyR_, recStack1, key_g, 
                                        startValue, depth_g, bits_g, key, 
                                        start, p, bits, depth, states >>

fboCheckLeftRight(self) == /\ pc[self] = "fboCheckLeftRight"
                           /\ IF nodeArr[p_i[self]].hasLeft /\ nodeArr[p_i[self]].hasRight
                                 THEN /\ pc' = [pc EXCEPT ![self] = "fboGetleft"]
                                 ELSE /\ pc' = [pc EXCEPT ![self] = "fboIncValue"]
                           /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                           nodeArr, insertRes, resultsForGet, 
                                           deleteRes, processWriterState, 
                                           processReaderState, values, stack, 
                                           i, cur, istack, current_d, key_, 
                                           tmp_, tmp1, mid_, p_, p1, recStack_, 
                                           recStack1_, ret, depth_, res, calc, 
                                           maxDepth, l, r, gNode, buf_, rmax, 
                                           links, recStack_s, mid_s, val, 
                                           child, lastNode, bits_, depth_s, 
                                           newBits, movedBits, ln, rn, current, 
                                           sibling, keyL, keyR, buf, bufLink, 
                                           p_d, tempL, tempR, bits_d, depth_d, 
                                           key_i, stackData, keyRNext, 
                                           oldCurrent, parent, keyLNext, 
                                           links_, oldSibling, current_, rev, 
                                           bits_i, depth_i, p_i, temp, scanRes, 
                                           t, array, split, recStack, answer, 
                                           tmp, tmp2, l_, r_, mid, sibling_, 
                                           keyL_, keyR_, recStack1, key_g, 
                                           startValue, depth_g, bits_g, key, 
                                           start, p, bits, depth, states >>

fboGetleft(self) == /\ pc[self] = "fboGetleft"
                    /\ l_' = [l_ EXCEPT ![self] = nodeArr[p_i[self]].left]
                    /\ pc' = [pc EXCEPT ![self] = "fboGetRight"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, p1, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, recStack, answer, tmp, tmp2, r_, 
                                    mid, sibling_, keyL_, keyR_, recStack1, 
                                    key_g, startValue, depth_g, bits_g, key, 
                                    start, p, bits, depth, states >>

fboGetRight(self) == /\ pc[self] = "fboGetRight"
                     /\ r_' = [r_ EXCEPT ![self] = nodeArr[p_i[self]].right]
                     /\ pc' = [pc EXCEPT ![self] = "fboCheckLeftRight2"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     mid, sibling_, keyL_, keyR_, recStack1, 
                                     key_g, startValue, depth_g, bits_g, key, 
                                     start, p, bits, depth, states >>

fboCheckLeftRight2(self) == /\ pc[self] = "fboCheckLeftRight2"
                            /\ recStack' = [recStack EXCEPT ![self] = Append(recStack[self], <<p_i[self], FALSE>>)]
                            /\ p_i' = [p_i EXCEPT ![self] = l_[self]]
                            /\ pc' = [pc EXCEPT ![self] = "fboCheckLeftRight"]
                            /\ UNCHANGED << root, hasRoot, globalLock, 
                                            gNodeArr, nodeArr, insertRes, 
                                            resultsForGet, deleteRes, 
                                            processWriterState, 
                                            processReaderState, values, stack, 
                                            i, cur, istack, current_d, key_, 
                                            tmp_, tmp1, mid_, p_, p1, 
                                            recStack_, recStack1_, ret, depth_, 
                                            res, calc, maxDepth, l, r, gNode, 
                                            buf_, rmax, links, recStack_s, 
                                            mid_s, val, child, lastNode, bits_, 
                                            depth_s, newBits, movedBits, ln, 
                                            rn, current, sibling, keyL, keyR, 
                                            buf, bufLink, p_d, tempL, tempR, 
                                            bits_d, depth_d, key_i, stackData, 
                                            keyRNext, oldCurrent, parent, 
                                            keyLNext, links_, oldSibling, 
                                            current_, rev, bits_i, depth_i, 
                                            temp, scanRes, t, array, split, 
                                            answer, tmp, tmp2, l_, r_, mid, 
                                            sibling_, keyL_, keyR_, recStack1, 
                                            key_g, startValue, depth_g, bits_g, 
                                            key, start, p, bits, depth, states >>

fboCheckLeftRight3(self) == /\ pc[self] = "fboCheckLeftRight3"
                            /\ recStack' = [recStack EXCEPT ![self] = Append(recStack[self], <<p_i[self], TRUE>>)]
                            /\ p_i' = [p_i EXCEPT ![self] = r_[self]]
                            /\ pc' = [pc EXCEPT ![self] = "fboCheckLeftRight"]
                            /\ UNCHANGED << root, hasRoot, globalLock, 
                                            gNodeArr, nodeArr, insertRes, 
                                            resultsForGet, deleteRes, 
                                            processWriterState, 
                                            processReaderState, values, stack, 
                                            i, cur, istack, current_d, key_, 
                                            tmp_, tmp1, mid_, p_, p1, 
                                            recStack_, recStack1_, ret, depth_, 
                                            res, calc, maxDepth, l, r, gNode, 
                                            buf_, rmax, links, recStack_s, 
                                            mid_s, val, child, lastNode, bits_, 
                                            depth_s, newBits, movedBits, ln, 
                                            rn, current, sibling, keyL, keyR, 
                                            buf, bufLink, p_d, tempL, tempR, 
                                            bits_d, depth_d, key_i, stackData, 
                                            keyRNext, oldCurrent, parent, 
                                            keyLNext, links_, oldSibling, 
                                            current_, rev, bits_i, depth_i, 
                                            temp, scanRes, t, array, split, 
                                            answer, tmp, tmp2, l_, r_, mid, 
                                            sibling_, keyL_, keyR_, recStack1, 
                                            key_g, startValue, depth_g, bits_g, 
                                            key, start, p, bits, depth, states >>

fboIncValue(self) == /\ pc[self] = "fboIncValue"
                     /\ IF ~nodeArr[p_i[self]].deleted
                           THEN /\ answer' = [answer EXCEPT ![self] = Append(answer[self], nodeArr[p_i[self]].value)]
                           ELSE /\ TRUE
                                /\ UNCHANGED answer
                     /\ pc' = [pc EXCEPT ![self] = "fboCheckRetSize"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, tmp, tmp2, l_, r_, mid, 
                                     sibling_, keyL_, keyR_, recStack1, key_g, 
                                     startValue, depth_g, bits_g, key, start, 
                                     p, bits, depth, states >>

fboCheckRetSize(self) == /\ pc[self] = "fboCheckRetSize"
                         /\ IF Len(recStack[self]) = 0
                               THEN /\ pc' = [pc EXCEPT ![self] = "copyLinks"]
                               ELSE /\ pc' = [pc EXCEPT ![self] = "fboGoToRet3"]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, answer, tmp, tmp2, l_, r_, 
                                         mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, start, p, bits, depth, 
                                         states >>

fboGoToRet3(self) == /\ pc[self] = "fboGoToRet3"
                     /\ tmp' = [tmp EXCEPT ![self] = Tail(recStack[self])]
                     /\ recStack' = [recStack EXCEPT ![self] = SubSeq(recStack[self], 0, Len(recStack[self]) - 1)]
                     /\ p_i' = [p_i EXCEPT ![self] = tmp'[self][0]]
                     /\ IF tmp'[self][1]
                           THEN /\ pc' = [pc EXCEPT ![self] = "fboCheckRetSize"]
                           ELSE /\ pc' = [pc EXCEPT ![self] = "fboCheckLeftRight3"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, temp, scanRes, t, array, split, 
                                     answer, tmp2, l_, r_, mid, sibling_, 
                                     keyL_, keyR_, recStack1, key_g, 
                                     startValue, depth_g, bits_g, key, start, 
                                     p, bits, depth, states >>

copyLinks(self) == /\ pc[self] = "copyLinks"
                   /\ links_' = [links_ EXCEPT ![self] = gNodeArr[current_[self]].links]
                   /\ pc' = [pc EXCEPT ![self] = "startUpdateCurrent"]
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, resultsForGet, 
                                   deleteRes, processWriterState, 
                                   processReaderState, values, stack, i, cur, 
                                   istack, current_d, key_, tmp_, tmp1, mid_, 
                                   p_, p1, recStack_, recStack1_, ret, depth_, 
                                   res, calc, maxDepth, l, r, gNode, buf_, 
                                   rmax, links, recStack_s, mid_s, val, child, 
                                   lastNode, bits_, depth_s, newBits, 
                                   movedBits, ln, rn, current, sibling, keyL, 
                                   keyR, buf, bufLink, p_d, tempL, tempR, 
                                   bits_d, depth_d, key_i, stackData, keyRNext, 
                                   oldCurrent, parent, keyLNext, oldSibling, 
                                   current_, rev, bits_i, depth_i, p_i, temp, 
                                   scanRes, t, array, split, recStack, answer, 
                                   tmp, tmp2, l_, r_, mid, sibling_, keyL_, 
                                   keyR_, recStack1, key_g, startValue, 
                                   depth_g, bits_g, key, start, p, bits, depth, 
                                   states >>

startUpdateCurrent(self) == /\ pc[self] = "startUpdateCurrent"
                            /\ gNodeArr' = [gNodeArr EXCEPT ![current_[self]].rev = gNodeArr[current_[self]]]
                            /\ split' = [split EXCEPT ![self] = Len(answer[self]) \div 2]
                            /\ mid' = [mid EXCEPT ![self] = split'[self] \div 2]
                            /\ pc' = [pc EXCEPT ![self] = "insertValues1"]
                            /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                            insertRes, resultsForGet, 
                                            deleteRes, processWriterState, 
                                            processReaderState, values, stack, 
                                            i, cur, istack, current_d, key_, 
                                            tmp_, tmp1, mid_, p_, p1, 
                                            recStack_, recStack1_, ret, depth_, 
                                            res, calc, maxDepth, l, r, gNode, 
                                            buf_, rmax, links, recStack_s, 
                                            mid_s, val, child, lastNode, bits_, 
                                            depth_s, newBits, movedBits, ln, 
                                            rn, current, sibling, keyL, keyR, 
                                            buf, bufLink, p_d, tempL, tempR, 
                                            bits_d, depth_d, key_i, stackData, 
                                            keyRNext, oldCurrent, parent, 
                                            keyLNext, links_, oldSibling, 
                                            current_, rev, bits_i, depth_i, 
                                            p_i, temp, scanRes, t, array, 
                                            recStack, answer, tmp, tmp2, l_, 
                                            r_, sibling_, keyL_, keyR_, 
                                            recStack1, key_g, startValue, 
                                            depth_g, bits_g, key, start, p, 
                                            bits, depth, states >>

insertValues1(self) == /\ pc[self] = "insertValues1"
                       /\ gNodeArr' = [gNodeArr EXCEPT ![current_[self]].root = answer[self][mid[self]]]
                       /\ pc' = [pc EXCEPT ![self] = "insertValues2"]
                       /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                       insertRes, resultsForGet, deleteRes, 
                                       processWriterState, processReaderState, 
                                       values, stack, i, cur, istack, 
                                       current_d, key_, tmp_, tmp1, mid_, p_, 
                                       p1, recStack_, recStack1_, ret, depth_, 
                                       res, calc, maxDepth, l, r, gNode, buf_, 
                                       rmax, links, recStack_s, mid_s, val, 
                                       child, lastNode, bits_, depth_s, 
                                       newBits, movedBits, ln, rn, current, 
                                       sibling, keyL, keyR, buf, bufLink, p_d, 
                                       tempL, tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

insertValues2(self) == /\ pc[self] = "insertValues2"
                       /\ gNodeArr' = [gNodeArr EXCEPT ![current_[self]].links[0] = links_[self][mid[self]]]
                       /\ pc' = [pc EXCEPT ![self] = "callCurrentFill1"]
                       /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                       insertRes, resultsForGet, deleteRes, 
                                       processWriterState, processReaderState, 
                                       values, stack, i, cur, istack, 
                                       current_d, key_, tmp_, tmp1, mid_, p_, 
                                       p1, recStack_, recStack1_, ret, depth_, 
                                       res, calc, maxDepth, l, r, gNode, buf_, 
                                       rmax, links, recStack_s, mid_s, val, 
                                       child, lastNode, bits_, depth_s, 
                                       newBits, movedBits, ln, rn, current, 
                                       sibling, keyL, keyR, buf, bufLink, p_d, 
                                       tempL, tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

callCurrentFill1(self) == /\ pc[self] = "callCurrentFill1"
                          /\ /\ buf_' = [buf_ EXCEPT ![self] = answer[self]]
                             /\ gNode' = [gNode EXCEPT ![self] = current_[self]]
                             /\ l' = [l EXCEPT ![self] = 0]
                             /\ links' = [links EXCEPT ![self] = links_[self]]
                             /\ r' = [r EXCEPT ![self] = mid[self] - 1]
                             /\ rmax' = [rmax EXCEPT ![self] = split[self]]
                             /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "smartFillValParentLo",
                                                                      pc        |->  "callCurrentFill2",
                                                                      recStack_s |->  recStack_s[self],
                                                                      mid_s     |->  mid_s[self],
                                                                      val       |->  val[self],
                                                                      child     |->  child[self],
                                                                      lastNode  |->  lastNode[self],
                                                                      bits_     |->  bits_[self],
                                                                      depth_s   |->  depth_s[self],
                                                                      newBits   |->  newBits[self],
                                                                      movedBits |->  movedBits[self],
                                                                      ln        |->  ln[self],
                                                                      rn        |->  rn[self],
                                                                      l         |->  l[self],
                                                                      r         |->  r[self],
                                                                      gNode     |->  gNode[self],
                                                                      buf_      |->  buf_[self],
                                                                      rmax      |->  rmax[self],
                                                                      links     |->  links[self] ] >>
                                                                  \o stack[self]]
                          /\ recStack_s' = [recStack_s EXCEPT ![self] = defaultInitValue]
                          /\ mid_s' = [mid_s EXCEPT ![self] = defaultInitValue]
                          /\ val' = [val EXCEPT ![self] = defaultInitValue]
                          /\ child' = [child EXCEPT ![self] = defaultInitValue]
                          /\ lastNode' = [lastNode EXCEPT ![self] = defaultInitValue]
                          /\ bits_' = [bits_ EXCEPT ![self] = defaultInitValue]
                          /\ depth_s' = [depth_s EXCEPT ![self] = defaultInitValue]
                          /\ newBits' = [newBits EXCEPT ![self] = defaultInitValue]
                          /\ movedBits' = [movedBits EXCEPT ![self] = defaultInitValue]
                          /\ ln' = [ln EXCEPT ![self] = defaultInitValue]
                          /\ rn' = [rn EXCEPT ![self] = defaultInitValue]
                          /\ pc' = [pc EXCEPT ![self] = "startFillValParentLo"]
                          /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                          nodeArr, insertRes, resultsForGet, 
                                          deleteRes, processWriterState, 
                                          processReaderState, values, i, cur, 
                                          istack, current_d, key_, tmp_, tmp1, 
                                          mid_, p_, p1, recStack_, recStack1_, 
                                          ret, depth_, res, calc, maxDepth, 
                                          current, sibling, keyL, keyR, buf, 
                                          bufLink, p_d, tempL, tempR, bits_d, 
                                          depth_d, key_i, stackData, keyRNext, 
                                          oldCurrent, parent, keyLNext, links_, 
                                          oldSibling, current_, rev, bits_i, 
                                          depth_i, p_i, temp, scanRes, t, 
                                          array, split, recStack, answer, tmp, 
                                          tmp2, l_, r_, mid, sibling_, keyL_, 
                                          keyR_, recStack1, key_g, startValue, 
                                          depth_g, bits_g, key, start, p, bits, 
                                          depth, states >>

callCurrentFill2(self) == /\ pc[self] = "callCurrentFill2"
                          /\ /\ buf_' = [buf_ EXCEPT ![self] = answer[self]]
                             /\ gNode' = [gNode EXCEPT ![self] = current_[self]]
                             /\ l' = [l EXCEPT ![self] = mid[self] + 1]
                             /\ links' = [links EXCEPT ![self] = links_[self]]
                             /\ r' = [r EXCEPT ![self] = split[self]]
                             /\ rmax' = [rmax EXCEPT ![self] = split[self]]
                             /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "smartFillValParentLo",
                                                                      pc        |->  "createSibling2",
                                                                      recStack_s |->  recStack_s[self],
                                                                      mid_s     |->  mid_s[self],
                                                                      val       |->  val[self],
                                                                      child     |->  child[self],
                                                                      lastNode  |->  lastNode[self],
                                                                      bits_     |->  bits_[self],
                                                                      depth_s   |->  depth_s[self],
                                                                      newBits   |->  newBits[self],
                                                                      movedBits |->  movedBits[self],
                                                                      ln        |->  ln[self],
                                                                      rn        |->  rn[self],
                                                                      l         |->  l[self],
                                                                      r         |->  r[self],
                                                                      gNode     |->  gNode[self],
                                                                      buf_      |->  buf_[self],
                                                                      rmax      |->  rmax[self],
                                                                      links     |->  links[self] ] >>
                                                                  \o stack[self]]
                          /\ recStack_s' = [recStack_s EXCEPT ![self] = defaultInitValue]
                          /\ mid_s' = [mid_s EXCEPT ![self] = defaultInitValue]
                          /\ val' = [val EXCEPT ![self] = defaultInitValue]
                          /\ child' = [child EXCEPT ![self] = defaultInitValue]
                          /\ lastNode' = [lastNode EXCEPT ![self] = defaultInitValue]
                          /\ bits_' = [bits_ EXCEPT ![self] = defaultInitValue]
                          /\ depth_s' = [depth_s EXCEPT ![self] = defaultInitValue]
                          /\ newBits' = [newBits EXCEPT ![self] = defaultInitValue]
                          /\ movedBits' = [movedBits EXCEPT ![self] = defaultInitValue]
                          /\ ln' = [ln EXCEPT ![self] = defaultInitValue]
                          /\ rn' = [rn EXCEPT ![self] = defaultInitValue]
                          /\ pc' = [pc EXCEPT ![self] = "startFillValParentLo"]
                          /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                          nodeArr, insertRes, resultsForGet, 
                                          deleteRes, processWriterState, 
                                          processReaderState, values, i, cur, 
                                          istack, current_d, key_, tmp_, tmp1, 
                                          mid_, p_, p1, recStack_, recStack1_, 
                                          ret, depth_, res, calc, maxDepth, 
                                          current, sibling, keyL, keyR, buf, 
                                          bufLink, p_d, tempL, tempR, bits_d, 
                                          depth_d, key_i, stackData, keyRNext, 
                                          oldCurrent, parent, keyLNext, links_, 
                                          oldSibling, current_, rev, bits_i, 
                                          depth_i, p_i, temp, scanRes, t, 
                                          array, split, recStack, answer, tmp, 
                                          tmp2, l_, r_, mid, sibling_, keyL_, 
                                          keyR_, recStack1, key_g, startValue, 
                                          depth_g, bits_g, key, start, p, bits, 
                                          depth, states >>

createSibling2(self) == /\ pc[self] = "createSibling2"
                        /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "initLeaf",
                                                                 pc        |->  "getSibling",
                                                                 i         |->  i[self],
                                                                 cur       |->  cur[self],
                                                                 istack    |->  istack[self] ] >>
                                                             \o stack[self]]
                        /\ i' = [i EXCEPT ![self] = defaultInitValue]
                        /\ cur' = [cur EXCEPT ![self] = defaultInitValue]
                        /\ istack' = [istack EXCEPT ![self] = defaultInitValue]
                        /\ pc' = [pc EXCEPT ![self] = "initLeaf_"]
                        /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                        nodeArr, insertRes, resultsForGet, 
                                        deleteRes, processWriterState, 
                                        processReaderState, values, current_d, 
                                        key_, tmp_, tmp1, mid_, p_, p1, 
                                        recStack_, recStack1_, ret, depth_, 
                                        res, calc, maxDepth, l, r, gNode, buf_, 
                                        rmax, links, recStack_s, mid_s, val, 
                                        child, lastNode, bits_, depth_s, 
                                        newBits, movedBits, ln, rn, current, 
                                        sibling, keyL, keyR, buf, bufLink, p_d, 
                                        tempL, tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        current_, rev, bits_i, depth_i, p_i, 
                                        temp, scanRes, t, array, split, 
                                        recStack, answer, tmp, tmp2, l_, r_, 
                                        mid, sibling_, keyL_, keyR_, recStack1, 
                                        key_g, startValue, depth_g, bits_g, 
                                        key, start, p, bits, depth, states >>

getSibling(self) == /\ pc[self] = "getSibling"
                    /\ sibling_' = [sibling_ EXCEPT ![self] = Len(gNodeArr)]
                    /\ pc' = [pc EXCEPT ![self] = "startUpdateSibling"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, p1, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, recStack, answer, tmp, tmp2, l_, r_, 
                                    mid, keyL_, keyR_, recStack1, key_g, 
                                    startValue, depth_g, bits_g, key, start, p, 
                                    bits, depth, states >>

startUpdateSibling(self) == /\ pc[self] = "startUpdateSibling"
                            /\ gNodeArr' = [gNodeArr EXCEPT ![sibling_[self]].rev = gNodeArr[sibling_[self]]]
                            /\ pc' = [pc EXCEPT ![self] = "updateSiblingValues1"]
                            /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                            insertRes, resultsForGet, 
                                            deleteRes, processWriterState, 
                                            processReaderState, values, stack, 
                                            i, cur, istack, current_d, key_, 
                                            tmp_, tmp1, mid_, p_, p1, 
                                            recStack_, recStack1_, ret, depth_, 
                                            res, calc, maxDepth, l, r, gNode, 
                                            buf_, rmax, links, recStack_s, 
                                            mid_s, val, child, lastNode, bits_, 
                                            depth_s, newBits, movedBits, ln, 
                                            rn, current, sibling, keyL, keyR, 
                                            buf, bufLink, p_d, tempL, tempR, 
                                            bits_d, depth_d, key_i, stackData, 
                                            keyRNext, oldCurrent, parent, 
                                            keyLNext, links_, oldSibling, 
                                            current_, rev, bits_i, depth_i, 
                                            p_i, temp, scanRes, t, array, 
                                            split, recStack, answer, tmp, tmp2, 
                                            l_, r_, mid, sibling_, keyL_, 
                                            keyR_, recStack1, key_g, 
                                            startValue, depth_g, bits_g, key, 
                                            start, p, bits, depth, states >>

updateSiblingValues1(self) == /\ pc[self] = "updateSiblingValues1"
                              /\ gNodeArr' = [gNodeArr EXCEPT ![sibling_[self]].highKey = gNodeArr[current_[self]].highKey]
                              /\ pc' = [pc EXCEPT ![self] = "updateSiblingValues2"]
                              /\ UNCHANGED << root, hasRoot, globalLock, 
                                              nodeArr, insertRes, 
                                              resultsForGet, deleteRes, 
                                              processWriterState, 
                                              processReaderState, values, 
                                              stack, i, cur, istack, current_d, 
                                              key_, tmp_, tmp1, mid_, p_, p1, 
                                              recStack_, recStack1_, ret, 
                                              depth_, res, calc, maxDepth, l, 
                                              r, gNode, buf_, rmax, links, 
                                              recStack_s, mid_s, val, child, 
                                              lastNode, bits_, depth_s, 
                                              newBits, movedBits, ln, rn, 
                                              current, sibling, keyL, keyR, 
                                              buf, bufLink, p_d, tempL, tempR, 
                                              bits_d, depth_d, key_i, 
                                              stackData, keyRNext, oldCurrent, 
                                              parent, keyLNext, links_, 
                                              oldSibling, current_, rev, 
                                              bits_i, depth_i, p_i, temp, 
                                              scanRes, t, array, split, 
                                              recStack, answer, tmp, tmp2, l_, 
                                              r_, mid, sibling_, keyL_, keyR_, 
                                              recStack1, key_g, startValue, 
                                              depth_g, bits_g, key, start, p, 
                                              bits, depth, states >>

updateSiblingValues2(self) == /\ pc[self] = "updateSiblingValues2"
                              /\ gNodeArr' = [gNodeArr EXCEPT ![sibling_[self]].sibling = gNodeArr[current_[self]].sibling]
                              /\ pc' = [pc EXCEPT ![self] = "updateCurrentValues1"]
                              /\ UNCHANGED << root, hasRoot, globalLock, 
                                              nodeArr, insertRes, 
                                              resultsForGet, deleteRes, 
                                              processWriterState, 
                                              processReaderState, values, 
                                              stack, i, cur, istack, current_d, 
                                              key_, tmp_, tmp1, mid_, p_, p1, 
                                              recStack_, recStack1_, ret, 
                                              depth_, res, calc, maxDepth, l, 
                                              r, gNode, buf_, rmax, links, 
                                              recStack_s, mid_s, val, child, 
                                              lastNode, bits_, depth_s, 
                                              newBits, movedBits, ln, rn, 
                                              current, sibling, keyL, keyR, 
                                              buf, bufLink, p_d, tempL, tempR, 
                                              bits_d, depth_d, key_i, 
                                              stackData, keyRNext, oldCurrent, 
                                              parent, keyLNext, links_, 
                                              oldSibling, current_, rev, 
                                              bits_i, depth_i, p_i, temp, 
                                              scanRes, t, array, split, 
                                              recStack, answer, tmp, tmp2, l_, 
                                              r_, mid, sibling_, keyL_, keyR_, 
                                              recStack1, key_g, startValue, 
                                              depth_g, bits_g, key, start, p, 
                                              bits, depth, states >>

updateCurrentValues1(self) == /\ pc[self] = "updateCurrentValues1"
                              /\ gNodeArr' = [gNodeArr EXCEPT ![current_[self]].highKey = answer[self][split[self]]]
                              /\ pc' = [pc EXCEPT ![self] = "updateCurrentValues2"]
                              /\ UNCHANGED << root, hasRoot, globalLock, 
                                              nodeArr, insertRes, 
                                              resultsForGet, deleteRes, 
                                              processWriterState, 
                                              processReaderState, values, 
                                              stack, i, cur, istack, current_d, 
                                              key_, tmp_, tmp1, mid_, p_, p1, 
                                              recStack_, recStack1_, ret, 
                                              depth_, res, calc, maxDepth, l, 
                                              r, gNode, buf_, rmax, links, 
                                              recStack_s, mid_s, val, child, 
                                              lastNode, bits_, depth_s, 
                                              newBits, movedBits, ln, rn, 
                                              current, sibling, keyL, keyR, 
                                              buf, bufLink, p_d, tempL, tempR, 
                                              bits_d, depth_d, key_i, 
                                              stackData, keyRNext, oldCurrent, 
                                              parent, keyLNext, links_, 
                                              oldSibling, current_, rev, 
                                              bits_i, depth_i, p_i, temp, 
                                              scanRes, t, array, split, 
                                              recStack, answer, tmp, tmp2, l_, 
                                              r_, mid, sibling_, keyL_, keyR_, 
                                              recStack1, key_g, startValue, 
                                              depth_g, bits_g, key, start, p, 
                                              bits, depth, states >>

updateCurrentValues2(self) == /\ pc[self] = "updateCurrentValues2"
                              /\ gNodeArr' = [gNodeArr EXCEPT ![current_[self]].sibling = sibling_[self]]
                              /\ pc' = [pc EXCEPT ![self] = "finishUpdateCurrent"]
                              /\ UNCHANGED << root, hasRoot, globalLock, 
                                              nodeArr, insertRes, 
                                              resultsForGet, deleteRes, 
                                              processWriterState, 
                                              processReaderState, values, 
                                              stack, i, cur, istack, current_d, 
                                              key_, tmp_, tmp1, mid_, p_, p1, 
                                              recStack_, recStack1_, ret, 
                                              depth_, res, calc, maxDepth, l, 
                                              r, gNode, buf_, rmax, links, 
                                              recStack_s, mid_s, val, child, 
                                              lastNode, bits_, depth_s, 
                                              newBits, movedBits, ln, rn, 
                                              current, sibling, keyL, keyR, 
                                              buf, bufLink, p_d, tempL, tempR, 
                                              bits_d, depth_d, key_i, 
                                              stackData, keyRNext, oldCurrent, 
                                              parent, keyLNext, links_, 
                                              oldSibling, current_, rev, 
                                              bits_i, depth_i, p_i, temp, 
                                              scanRes, t, array, split, 
                                              recStack, answer, tmp, tmp2, l_, 
                                              r_, mid, sibling_, keyL_, keyR_, 
                                              recStack1, key_g, startValue, 
                                              depth_g, bits_g, key, start, p, 
                                              bits, depth, states >>

finishUpdateCurrent(self) == /\ pc[self] = "finishUpdateCurrent"
                             /\ gNodeArr' = [gNodeArr EXCEPT ![current_[self]].rev = gNodeArr[current_[self]]]
                             /\ mid' = [mid EXCEPT ![self] = split[self] \div 2 + split[self]]
                             /\ pc' = [pc EXCEPT ![self] = "insertValues3"]
                             /\ UNCHANGED << root, hasRoot, globalLock, 
                                             nodeArr, insertRes, resultsForGet, 
                                             deleteRes, processWriterState, 
                                             processReaderState, values, stack, 
                                             i, cur, istack, current_d, key_, 
                                             tmp_, tmp1, mid_, p_, p1, 
                                             recStack_, recStack1_, ret, 
                                             depth_, res, calc, maxDepth, l, r, 
                                             gNode, buf_, rmax, links, 
                                             recStack_s, mid_s, val, child, 
                                             lastNode, bits_, depth_s, newBits, 
                                             movedBits, ln, rn, current, 
                                             sibling, keyL, keyR, buf, bufLink, 
                                             p_d, tempL, tempR, bits_d, 
                                             depth_d, key_i, stackData, 
                                             keyRNext, oldCurrent, parent, 
                                             keyLNext, links_, oldSibling, 
                                             current_, rev, bits_i, depth_i, 
                                             p_i, temp, scanRes, t, array, 
                                             split, recStack, answer, tmp, 
                                             tmp2, l_, r_, sibling_, keyL_, 
                                             keyR_, recStack1, key_g, 
                                             startValue, depth_g, bits_g, key, 
                                             start, p, bits, depth, states >>

insertValues3(self) == /\ pc[self] = "insertValues3"
                       /\ gNodeArr' = [gNodeArr EXCEPT ![sibling_[self]].root = answer[self][mid[self]]]
                       /\ pc' = [pc EXCEPT ![self] = "insertValues4"]
                       /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                       insertRes, resultsForGet, deleteRes, 
                                       processWriterState, processReaderState, 
                                       values, stack, i, cur, istack, 
                                       current_d, key_, tmp_, tmp1, mid_, p_, 
                                       p1, recStack_, recStack1_, ret, depth_, 
                                       res, calc, maxDepth, l, r, gNode, buf_, 
                                       rmax, links, recStack_s, mid_s, val, 
                                       child, lastNode, bits_, depth_s, 
                                       newBits, movedBits, ln, rn, current, 
                                       sibling, keyL, keyR, buf, bufLink, p_d, 
                                       tempL, tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

insertValues4(self) == /\ pc[self] = "insertValues4"
                       /\ gNodeArr' = [gNodeArr EXCEPT ![sibling_[self]].links[0] = links_[self][mid[self]]]
                       /\ pc' = [pc EXCEPT ![self] = "callCurrentFill3"]
                       /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                       insertRes, resultsForGet, deleteRes, 
                                       processWriterState, processReaderState, 
                                       values, stack, i, cur, istack, 
                                       current_d, key_, tmp_, tmp1, mid_, p_, 
                                       p1, recStack_, recStack1_, ret, depth_, 
                                       res, calc, maxDepth, l, r, gNode, buf_, 
                                       rmax, links, recStack_s, mid_s, val, 
                                       child, lastNode, bits_, depth_s, 
                                       newBits, movedBits, ln, rn, current, 
                                       sibling, keyL, keyR, buf, bufLink, p_d, 
                                       tempL, tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

callCurrentFill3(self) == /\ pc[self] = "callCurrentFill3"
                          /\ /\ buf_' = [buf_ EXCEPT ![self] = answer[self]]
                             /\ gNode' = [gNode EXCEPT ![self] = sibling_[self]]
                             /\ l' = [l EXCEPT ![self] = split[self]]
                             /\ links' = [links EXCEPT ![self] = links_[self]]
                             /\ r' = [r EXCEPT ![self] = mid[self] - 1]
                             /\ rmax' = [rmax EXCEPT ![self] = Len(answer[self])]
                             /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "smartFillValParentLo",
                                                                      pc        |->  "callCurrentFill4",
                                                                      recStack_s |->  recStack_s[self],
                                                                      mid_s     |->  mid_s[self],
                                                                      val       |->  val[self],
                                                                      child     |->  child[self],
                                                                      lastNode  |->  lastNode[self],
                                                                      bits_     |->  bits_[self],
                                                                      depth_s   |->  depth_s[self],
                                                                      newBits   |->  newBits[self],
                                                                      movedBits |->  movedBits[self],
                                                                      ln        |->  ln[self],
                                                                      rn        |->  rn[self],
                                                                      l         |->  l[self],
                                                                      r         |->  r[self],
                                                                      gNode     |->  gNode[self],
                                                                      buf_      |->  buf_[self],
                                                                      rmax      |->  rmax[self],
                                                                      links     |->  links[self] ] >>
                                                                  \o stack[self]]
                          /\ recStack_s' = [recStack_s EXCEPT ![self] = defaultInitValue]
                          /\ mid_s' = [mid_s EXCEPT ![self] = defaultInitValue]
                          /\ val' = [val EXCEPT ![self] = defaultInitValue]
                          /\ child' = [child EXCEPT ![self] = defaultInitValue]
                          /\ lastNode' = [lastNode EXCEPT ![self] = defaultInitValue]
                          /\ bits_' = [bits_ EXCEPT ![self] = defaultInitValue]
                          /\ depth_s' = [depth_s EXCEPT ![self] = defaultInitValue]
                          /\ newBits' = [newBits EXCEPT ![self] = defaultInitValue]
                          /\ movedBits' = [movedBits EXCEPT ![self] = defaultInitValue]
                          /\ ln' = [ln EXCEPT ![self] = defaultInitValue]
                          /\ rn' = [rn EXCEPT ![self] = defaultInitValue]
                          /\ pc' = [pc EXCEPT ![self] = "startFillValParentLo"]
                          /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                          nodeArr, insertRes, resultsForGet, 
                                          deleteRes, processWriterState, 
                                          processReaderState, values, i, cur, 
                                          istack, current_d, key_, tmp_, tmp1, 
                                          mid_, p_, p1, recStack_, recStack1_, 
                                          ret, depth_, res, calc, maxDepth, 
                                          current, sibling, keyL, keyR, buf, 
                                          bufLink, p_d, tempL, tempR, bits_d, 
                                          depth_d, key_i, stackData, keyRNext, 
                                          oldCurrent, parent, keyLNext, links_, 
                                          oldSibling, current_, rev, bits_i, 
                                          depth_i, p_i, temp, scanRes, t, 
                                          array, split, recStack, answer, tmp, 
                                          tmp2, l_, r_, mid, sibling_, keyL_, 
                                          keyR_, recStack1, key_g, startValue, 
                                          depth_g, bits_g, key, start, p, bits, 
                                          depth, states >>

callCurrentFill4(self) == /\ pc[self] = "callCurrentFill4"
                          /\ /\ buf_' = [buf_ EXCEPT ![self] = answer[self]]
                             /\ gNode' = [gNode EXCEPT ![self] = sibling_[self]]
                             /\ l' = [l EXCEPT ![self] = mid[self] + 1]
                             /\ links' = [links EXCEPT ![self] = links_[self]]
                             /\ r' = [r EXCEPT ![self] = Len(answer[self])]
                             /\ rmax' = [rmax EXCEPT ![self] = Len(answer[self])]
                             /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "smartFillValParentLo",
                                                                      pc        |->  "finishUpdateSibling",
                                                                      recStack_s |->  recStack_s[self],
                                                                      mid_s     |->  mid_s[self],
                                                                      val       |->  val[self],
                                                                      child     |->  child[self],
                                                                      lastNode  |->  lastNode[self],
                                                                      bits_     |->  bits_[self],
                                                                      depth_s   |->  depth_s[self],
                                                                      newBits   |->  newBits[self],
                                                                      movedBits |->  movedBits[self],
                                                                      ln        |->  ln[self],
                                                                      rn        |->  rn[self],
                                                                      l         |->  l[self],
                                                                      r         |->  r[self],
                                                                      gNode     |->  gNode[self],
                                                                      buf_      |->  buf_[self],
                                                                      rmax      |->  rmax[self],
                                                                      links     |->  links[self] ] >>
                                                                  \o stack[self]]
                          /\ recStack_s' = [recStack_s EXCEPT ![self] = defaultInitValue]
                          /\ mid_s' = [mid_s EXCEPT ![self] = defaultInitValue]
                          /\ val' = [val EXCEPT ![self] = defaultInitValue]
                          /\ child' = [child EXCEPT ![self] = defaultInitValue]
                          /\ lastNode' = [lastNode EXCEPT ![self] = defaultInitValue]
                          /\ bits_' = [bits_ EXCEPT ![self] = defaultInitValue]
                          /\ depth_s' = [depth_s EXCEPT ![self] = defaultInitValue]
                          /\ newBits' = [newBits EXCEPT ![self] = defaultInitValue]
                          /\ movedBits' = [movedBits EXCEPT ![self] = defaultInitValue]
                          /\ ln' = [ln EXCEPT ![self] = defaultInitValue]
                          /\ rn' = [rn EXCEPT ![self] = defaultInitValue]
                          /\ pc' = [pc EXCEPT ![self] = "startFillValParentLo"]
                          /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                          nodeArr, insertRes, resultsForGet, 
                                          deleteRes, processWriterState, 
                                          processReaderState, values, i, cur, 
                                          istack, current_d, key_, tmp_, tmp1, 
                                          mid_, p_, p1, recStack_, recStack1_, 
                                          ret, depth_, res, calc, maxDepth, 
                                          current, sibling, keyL, keyR, buf, 
                                          bufLink, p_d, tempL, tempR, bits_d, 
                                          depth_d, key_i, stackData, keyRNext, 
                                          oldCurrent, parent, keyLNext, links_, 
                                          oldSibling, current_, rev, bits_i, 
                                          depth_i, p_i, temp, scanRes, t, 
                                          array, split, recStack, answer, tmp, 
                                          tmp2, l_, r_, mid, sibling_, keyL_, 
                                          keyR_, recStack1, key_g, startValue, 
                                          depth_g, bits_g, key, start, p, bits, 
                                          depth, states >>

finishUpdateSibling(self) == /\ pc[self] = "finishUpdateSibling"
                             /\ gNodeArr' = [gNodeArr EXCEPT ![sibling_[self]].rev = gNodeArr[sibling_[self]]]
                             /\ pc' = [pc EXCEPT ![self] = "updateCountNodes1"]
                             /\ UNCHANGED << root, hasRoot, globalLock, 
                                             nodeArr, insertRes, resultsForGet, 
                                             deleteRes, processWriterState, 
                                             processReaderState, values, stack, 
                                             i, cur, istack, current_d, key_, 
                                             tmp_, tmp1, mid_, p_, p1, 
                                             recStack_, recStack1_, ret, 
                                             depth_, res, calc, maxDepth, l, r, 
                                             gNode, buf_, rmax, links, 
                                             recStack_s, mid_s, val, child, 
                                             lastNode, bits_, depth_s, newBits, 
                                             movedBits, ln, rn, current, 
                                             sibling, keyL, keyR, buf, bufLink, 
                                             p_d, tempL, tempR, bits_d, 
                                             depth_d, key_i, stackData, 
                                             keyRNext, oldCurrent, parent, 
                                             keyLNext, links_, oldSibling, 
                                             current_, rev, bits_i, depth_i, 
                                             p_i, temp, scanRes, t, array, 
                                             split, recStack, answer, tmp, 
                                             tmp2, l_, r_, mid, sibling_, 
                                             keyL_, keyR_, recStack1, key_g, 
                                             startValue, depth_g, bits_g, key, 
                                             start, p, bits, depth, states >>

updateCountNodes1(self) == /\ pc[self] = "updateCountNodes1"
                           /\ gNodeArr' = [gNodeArr EXCEPT ![current_[self]].countNode = split[self] * 2]
                           /\ pc' = [pc EXCEPT ![self] = "updateCountNodes2"]
                           /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                           insertRes, resultsForGet, deleteRes, 
                                           processWriterState, 
                                           processReaderState, values, stack, 
                                           i, cur, istack, current_d, key_, 
                                           tmp_, tmp1, mid_, p_, p1, recStack_, 
                                           recStack1_, ret, depth_, res, calc, 
                                           maxDepth, l, r, gNode, buf_, rmax, 
                                           links, recStack_s, mid_s, val, 
                                           child, lastNode, bits_, depth_s, 
                                           newBits, movedBits, ln, rn, current, 
                                           sibling, keyL, keyR, buf, bufLink, 
                                           p_d, tempL, tempR, bits_d, depth_d, 
                                           key_i, stackData, keyRNext, 
                                           oldCurrent, parent, keyLNext, 
                                           links_, oldSibling, current_, rev, 
                                           bits_i, depth_i, p_i, temp, scanRes, 
                                           t, array, split, recStack, answer, 
                                           tmp, tmp2, l_, r_, mid, sibling_, 
                                           keyL_, keyR_, recStack1, key_g, 
                                           startValue, depth_g, bits_g, key, 
                                           start, p, bits, depth, states >>

updateCountNodes2(self) == /\ pc[self] = "updateCountNodes2"
                           /\ gNodeArr' = [gNodeArr EXCEPT ![sibling_[self]].countNode = (Len(answer[self]) - split[self]) * 2]
                           /\ keyRNext' = [keyRNext EXCEPT ![self] = answer[self][split[self]]]
                           /\ keyLNext' = [keyLNext EXCEPT ![self] = answer[self][0]]
                           /\ pc' = [pc EXCEPT ![self] = "insertNodeIf"]
                           /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                           insertRes, resultsForGet, deleteRes, 
                                           processWriterState, 
                                           processReaderState, values, stack, 
                                           i, cur, istack, current_d, key_, 
                                           tmp_, tmp1, mid_, p_, p1, recStack_, 
                                           recStack1_, ret, depth_, res, calc, 
                                           maxDepth, l, r, gNode, buf_, rmax, 
                                           links, recStack_s, mid_s, val, 
                                           child, lastNode, bits_, depth_s, 
                                           newBits, movedBits, ln, rn, current, 
                                           sibling, keyL, keyR, buf, bufLink, 
                                           p_d, tempL, tempR, bits_d, depth_d, 
                                           key_i, stackData, oldCurrent, 
                                           parent, links_, oldSibling, 
                                           current_, rev, bits_i, depth_i, p_i, 
                                           temp, scanRes, t, array, split, 
                                           recStack, answer, tmp, tmp2, l_, r_, 
                                           mid, sibling_, keyL_, keyR_, 
                                           recStack1, key_g, startValue, 
                                           depth_g, bits_g, key, start, p, 
                                           bits, depth, states >>

insertNodeIf(self) == /\ pc[self] = "insertNodeIf"
                      /\ IF keyR_[self] >= gNodeArr[current_[self]].highKey
                            THEN /\ pc' = [pc EXCEPT ![self] = "doInsertNode1"]
                            ELSE /\ pc' = [pc EXCEPT ![self] = "doInsertNode2"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

doInsertNode1(self) == /\ pc[self] = "doInsertNode1"
                       /\ /\ buf' = [buf EXCEPT ![self] = answer[self]]
                          /\ bufLink' = [bufLink EXCEPT ![self] = links_[self]]
                          /\ current' = [current EXCEPT ![self] = gNodeArr[current_[self]].sibling]
                          /\ keyL' = [keyL EXCEPT ![self] = keyL_[self]]
                          /\ keyR' = [keyR EXCEPT ![self] = keyR_[self]]
                          /\ sibling' = [sibling EXCEPT ![self] = oldSibling[self]]
                          /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "doInsertNode",
                                                                   pc        |->  "calcNextKeys",
                                                                   p_d       |->  p_d[self],
                                                                   tempL     |->  tempL[self],
                                                                   tempR     |->  tempR[self],
                                                                   bits_d    |->  bits_d[self],
                                                                   depth_d   |->  depth_d[self],
                                                                   current   |->  current[self],
                                                                   sibling   |->  sibling[self],
                                                                   keyL      |->  keyL[self],
                                                                   keyR      |->  keyR[self],
                                                                   buf       |->  buf[self],
                                                                   bufLink   |->  bufLink[self] ] >>
                                                               \o stack[self]]
                       /\ p_d' = [p_d EXCEPT ![self] = defaultInitValue]
                       /\ tempL' = [tempL EXCEPT ![self] = defaultInitValue]
                       /\ tempR' = [tempR EXCEPT ![self] = defaultInitValue]
                       /\ bits_d' = [bits_d EXCEPT ![self] = defaultInitValue]
                       /\ depth_d' = [depth_d EXCEPT ![self] = defaultInitValue]
                       /\ pc' = [pc EXCEPT ![self] = "detailedNodeSearchStart2"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, i, cur, 
                                       istack, current_d, key_, tmp_, tmp1, 
                                       mid_, p_, p1, recStack_, recStack1_, 
                                       ret, depth_, res, calc, maxDepth, l, r, 
                                       gNode, buf_, rmax, links, recStack_s, 
                                       mid_s, val, child, lastNode, bits_, 
                                       depth_s, newBits, movedBits, ln, rn, 
                                       key_i, stackData, keyRNext, oldCurrent, 
                                       parent, keyLNext, links_, oldSibling, 
                                       current_, rev, bits_i, depth_i, p_i, 
                                       temp, scanRes, t, array, split, 
                                       recStack, answer, tmp, tmp2, l_, r_, 
                                       mid, sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

doInsertNode2(self) == /\ pc[self] = "doInsertNode2"
                       /\ /\ buf' = [buf EXCEPT ![self] = answer[self]]
                          /\ bufLink' = [bufLink EXCEPT ![self] = links_[self]]
                          /\ current' = [current EXCEPT ![self] = current_[self]]
                          /\ keyL' = [keyL EXCEPT ![self] = keyL_[self]]
                          /\ keyR' = [keyR EXCEPT ![self] = keyR_[self]]
                          /\ sibling' = [sibling EXCEPT ![self] = oldSibling[self]]
                          /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "doInsertNode",
                                                                   pc        |->  "calcNextKeys",
                                                                   p_d       |->  p_d[self],
                                                                   tempL     |->  tempL[self],
                                                                   tempR     |->  tempR[self],
                                                                   bits_d    |->  bits_d[self],
                                                                   depth_d   |->  depth_d[self],
                                                                   current   |->  current[self],
                                                                   sibling   |->  sibling[self],
                                                                   keyL      |->  keyL[self],
                                                                   keyR      |->  keyR[self],
                                                                   buf       |->  buf[self],
                                                                   bufLink   |->  bufLink[self] ] >>
                                                               \o stack[self]]
                       /\ p_d' = [p_d EXCEPT ![self] = defaultInitValue]
                       /\ tempL' = [tempL EXCEPT ![self] = defaultInitValue]
                       /\ tempR' = [tempR EXCEPT ![self] = defaultInitValue]
                       /\ bits_d' = [bits_d EXCEPT ![self] = defaultInitValue]
                       /\ depth_d' = [depth_d EXCEPT ![self] = defaultInitValue]
                       /\ pc' = [pc EXCEPT ![self] = "detailedNodeSearchStart2"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, i, cur, 
                                       istack, current_d, key_, tmp_, tmp1, 
                                       mid_, p_, p1, recStack_, recStack1_, 
                                       ret, depth_, res, calc, maxDepth, l, r, 
                                       gNode, buf_, rmax, links, recStack_s, 
                                       mid_s, val, child, lastNode, bits_, 
                                       depth_s, newBits, movedBits, ln, rn, 
                                       key_i, stackData, keyRNext, oldCurrent, 
                                       parent, keyLNext, links_, oldSibling, 
                                       current_, rev, bits_i, depth_i, p_i, 
                                       temp, scanRes, t, array, split, 
                                       recStack, answer, tmp, tmp2, l_, r_, 
                                       mid, sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

calcNextKeys(self) == /\ pc[self] = "calcNextKeys"
                      /\ keyR_' = [keyR_ EXCEPT ![self] = keyRNext[self]]
                      /\ keyL_' = [keyL_ EXCEPT ![self] = keyLNext[self]]
                      /\ pc' = [pc EXCEPT ![self] = "rootRebalance"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, recStack1, 
                                      key_g, startValue, depth_g, bits_g, key, 
                                      start, p, bits, depth, states >>

doInsertNode3(self) == /\ pc[self] = "doInsertNode3"
                       /\ /\ buf' = [buf EXCEPT ![self] = answer[self]]
                          /\ bufLink' = [bufLink EXCEPT ![self] = links_[self]]
                          /\ current' = [current EXCEPT ![self] = current_[self]]
                          /\ keyL' = [keyL EXCEPT ![self] = keyL_[self]]
                          /\ keyR' = [keyR EXCEPT ![self] = keyR_[self]]
                          /\ sibling' = [sibling EXCEPT ![self] = sibling_[self]]
                          /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "doInsertNode",
                                                                   pc        |->  "releaseCurrentLock",
                                                                   p_d       |->  p_d[self],
                                                                   tempL     |->  tempL[self],
                                                                   tempR     |->  tempR[self],
                                                                   bits_d    |->  bits_d[self],
                                                                   depth_d   |->  depth_d[self],
                                                                   current   |->  current[self],
                                                                   sibling   |->  sibling[self],
                                                                   keyL      |->  keyL[self],
                                                                   keyR      |->  keyR[self],
                                                                   buf       |->  buf[self],
                                                                   bufLink   |->  bufLink[self] ] >>
                                                               \o stack[self]]
                       /\ p_d' = [p_d EXCEPT ![self] = defaultInitValue]
                       /\ tempL' = [tempL EXCEPT ![self] = defaultInitValue]
                       /\ tempR' = [tempR EXCEPT ![self] = defaultInitValue]
                       /\ bits_d' = [bits_d EXCEPT ![self] = defaultInitValue]
                       /\ depth_d' = [depth_d EXCEPT ![self] = defaultInitValue]
                       /\ pc' = [pc EXCEPT ![self] = "detailedNodeSearchStart2"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, i, cur, 
                                       istack, current_d, key_, tmp_, tmp1, 
                                       mid_, p_, p1, recStack_, recStack1_, 
                                       ret, depth_, res, calc, maxDepth, l, r, 
                                       gNode, buf_, rmax, links, recStack_s, 
                                       mid_s, val, child, lastNode, bits_, 
                                       depth_s, newBits, movedBits, ln, rn, 
                                       key_i, stackData, keyRNext, oldCurrent, 
                                       parent, keyLNext, links_, oldSibling, 
                                       current_, rev, bits_i, depth_i, p_i, 
                                       temp, scanRes, t, array, split, 
                                       recStack, answer, tmp, tmp2, l_, r_, 
                                       mid, sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

releaseCurrentLock(self) == /\ pc[self] = "releaseCurrentLock"
                            /\ gNodeArr' = [gNodeArr EXCEPT ![current_[self]].lock = TRUE]
                            /\ pc' = [pc EXCEPT ![self] = "insertRes_"]
                            /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                            insertRes, resultsForGet, 
                                            deleteRes, processWriterState, 
                                            processReaderState, values, stack, 
                                            i, cur, istack, current_d, key_, 
                                            tmp_, tmp1, mid_, p_, p1, 
                                            recStack_, recStack1_, ret, depth_, 
                                            res, calc, maxDepth, l, r, gNode, 
                                            buf_, rmax, links, recStack_s, 
                                            mid_s, val, child, lastNode, bits_, 
                                            depth_s, newBits, movedBits, ln, 
                                            rn, current, sibling, keyL, keyR, 
                                            buf, bufLink, p_d, tempL, tempR, 
                                            bits_d, depth_d, key_i, stackData, 
                                            keyRNext, oldCurrent, parent, 
                                            keyLNext, links_, oldSibling, 
                                            current_, rev, bits_i, depth_i, 
                                            p_i, temp, scanRes, t, array, 
                                            split, recStack, answer, tmp, tmp2, 
                                            l_, r_, mid, sibling_, keyL_, 
                                            keyR_, recStack1, key_g, 
                                            startValue, depth_g, bits_g, key, 
                                            start, p, bits, depth, states >>

insertRes_(self) == /\ pc[self] = "insertRes_"
                    /\ insertRes' = [insertRes EXCEPT ![self] = TRUE]
                    /\ pc' = [pc EXCEPT ![self] = Head(stack[self]).pc]
                    /\ stackData' = [stackData EXCEPT ![self] = Head(stack[self]).stackData]
                    /\ keyRNext' = [keyRNext EXCEPT ![self] = Head(stack[self]).keyRNext]
                    /\ oldCurrent' = [oldCurrent EXCEPT ![self] = Head(stack[self]).oldCurrent]
                    /\ parent' = [parent EXCEPT ![self] = Head(stack[self]).parent]
                    /\ keyLNext' = [keyLNext EXCEPT ![self] = Head(stack[self]).keyLNext]
                    /\ links_' = [links_ EXCEPT ![self] = Head(stack[self]).links_]
                    /\ oldSibling' = [oldSibling EXCEPT ![self] = Head(stack[self]).oldSibling]
                    /\ current_' = [current_ EXCEPT ![self] = Head(stack[self]).current_]
                    /\ rev' = [rev EXCEPT ![self] = Head(stack[self]).rev]
                    /\ bits_i' = [bits_i EXCEPT ![self] = Head(stack[self]).bits_i]
                    /\ depth_i' = [depth_i EXCEPT ![self] = Head(stack[self]).depth_i]
                    /\ p_i' = [p_i EXCEPT ![self] = Head(stack[self]).p_i]
                    /\ temp' = [temp EXCEPT ![self] = Head(stack[self]).temp]
                    /\ scanRes' = [scanRes EXCEPT ![self] = Head(stack[self]).scanRes]
                    /\ t' = [t EXCEPT ![self] = Head(stack[self]).t]
                    /\ array' = [array EXCEPT ![self] = Head(stack[self]).array]
                    /\ split' = [split EXCEPT ![self] = Head(stack[self]).split]
                    /\ recStack' = [recStack EXCEPT ![self] = Head(stack[self]).recStack]
                    /\ answer' = [answer EXCEPT ![self] = Head(stack[self]).answer]
                    /\ tmp' = [tmp EXCEPT ![self] = Head(stack[self]).tmp]
                    /\ tmp2' = [tmp2 EXCEPT ![self] = Head(stack[self]).tmp2]
                    /\ l_' = [l_ EXCEPT ![self] = Head(stack[self]).l_]
                    /\ r_' = [r_ EXCEPT ![self] = Head(stack[self]).r_]
                    /\ mid' = [mid EXCEPT ![self] = Head(stack[self]).mid]
                    /\ sibling_' = [sibling_ EXCEPT ![self] = Head(stack[self]).sibling_]
                    /\ keyL_' = [keyL_ EXCEPT ![self] = Head(stack[self]).keyL_]
                    /\ keyR_' = [keyR_ EXCEPT ![self] = Head(stack[self]).keyR_]
                    /\ recStack1' = [recStack1 EXCEPT ![self] = Head(stack[self]).recStack1]
                    /\ key_i' = [key_i EXCEPT ![self] = Head(stack[self]).key_i]
                    /\ stack' = [stack EXCEPT ![self] = Tail(stack[self])]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, resultsForGet, deleteRes, 
                                    processWriterState, processReaderState, 
                                    values, i, cur, istack, current_d, key_, 
                                    tmp_, tmp1, mid_, p_, p1, recStack_, 
                                    recStack1_, ret, depth_, res, calc, 
                                    maxDepth, l, r, gNode, buf_, rmax, links, 
                                    recStack_s, mid_s, val, child, lastNode, 
                                    bits_, depth_s, newBits, movedBits, ln, rn, 
                                    current, sibling, keyL, keyR, buf, bufLink, 
                                    p_d, tempL, tempR, bits_d, depth_d, key_g, 
                                    startValue, depth_g, bits_g, key, start, p, 
                                    bits, depth, states >>

rootRebalance(self) == /\ pc[self] = "rootRebalance"
                       /\ IF Len(stackData[self]) = 0
                             THEN /\ pc' = [pc EXCEPT ![self] = "initNewRoot"]
                             ELSE /\ pc' = [pc EXCEPT ![self] = "setOldCurrent"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

initNewRoot(self) == /\ pc[self] = "initNewRoot"
                     /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "initLeaf",
                                                              pc        |->  "initNewRoot2",
                                                              i         |->  i[self],
                                                              cur       |->  cur[self],
                                                              istack    |->  istack[self] ] >>
                                                          \o stack[self]]
                     /\ i' = [i EXCEPT ![self] = defaultInitValue]
                     /\ cur' = [cur EXCEPT ![self] = defaultInitValue]
                     /\ istack' = [istack EXCEPT ![self] = defaultInitValue]
                     /\ pc' = [pc EXCEPT ![self] = "initLeaf_"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, current_d, 
                                     key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                     recStack1_, ret, depth_, res, calc, 
                                     maxDepth, l, r, gNode, buf_, rmax, links, 
                                     recStack_s, mid_s, val, child, lastNode, 
                                     bits_, depth_s, newBits, movedBits, ln, 
                                     rn, current, sibling, keyL, keyR, buf, 
                                     bufLink, p_d, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

initNewRoot2(self) == /\ pc[self] = "initNewRoot2"
                      /\ parent' = [parent EXCEPT ![self] = Len(gNodeArr)]
                      /\ pc' = [pc EXCEPT ![self] = "setRootValue"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

setRootValue(self) == /\ pc[self] = "setRootValue"
                      /\ nodeArr' = [nodeArr EXCEPT ![gNodeArr[parent[self]].root].value = keyR_[self]]
                      /\ pc' = [pc EXCEPT ![self] = "setRootHasValue"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      insertRes, resultsForGet, deleteRes, 
                                      processWriterState, processReaderState, 
                                      values, stack, i, cur, istack, current_d, 
                                      key_, tmp_, tmp1, mid_, p_, p1, 
                                      recStack_, recStack1_, ret, depth_, res, 
                                      calc, maxDepth, l, r, gNode, buf_, rmax, 
                                      links, recStack_s, mid_s, val, child, 
                                      lastNode, bits_, depth_s, newBits, 
                                      movedBits, ln, rn, current, sibling, 
                                      keyL, keyR, buf, bufLink, p_d, tempL, 
                                      tempR, bits_d, depth_d, key_i, stackData, 
                                      keyRNext, oldCurrent, parent, keyLNext, 
                                      links_, oldSibling, current_, rev, 
                                      bits_i, depth_i, p_i, temp, scanRes, t, 
                                      array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

setRootHasValue(self) == /\ pc[self] = "setRootHasValue"
                         /\ nodeArr' = [nodeArr EXCEPT ![gNodeArr[parent[self]].root].hasValue = TRUE]
                         /\ pc' = [pc EXCEPT ![self] = "setRootLinks"]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         insertRes, resultsForGet, deleteRes, 
                                         processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, answer, tmp, tmp2, l_, r_, 
                                         mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, start, p, bits, depth, 
                                         states >>

setRootLinks(self) == /\ pc[self] = "setRootLinks"
                      /\ gNodeArr' = [gNodeArr EXCEPT ![parent[self]].links[0] = current_[self]]
                      /\ pc' = [pc EXCEPT ![self] = "getLeftFromRoot"]
                      /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                      insertRes, resultsForGet, deleteRes, 
                                      processWriterState, processReaderState, 
                                      values, stack, i, cur, istack, current_d, 
                                      key_, tmp_, tmp1, mid_, p_, p1, 
                                      recStack_, recStack1_, ret, depth_, res, 
                                      calc, maxDepth, l, r, gNode, buf_, rmax, 
                                      links, recStack_s, mid_s, val, child, 
                                      lastNode, bits_, depth_s, newBits, 
                                      movedBits, ln, rn, current, sibling, 
                                      keyL, keyR, buf, bufLink, p_d, tempL, 
                                      tempR, bits_d, depth_d, key_i, stackData, 
                                      keyRNext, oldCurrent, parent, keyLNext, 
                                      links_, oldSibling, current_, rev, 
                                      bits_i, depth_i, p_i, temp, scanRes, t, 
                                      array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

getLeftFromRoot(self) == /\ pc[self] = "getLeftFromRoot"
                         /\ tempL' = [tempL EXCEPT ![self] = nodeArr[gNodeArr[parent[self]].root].left]
                         /\ pc' = [pc EXCEPT ![self] = "getRightFromRoot"]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempR, 
                                         bits_d, depth_d, key_i, stackData, 
                                         keyRNext, oldCurrent, parent, 
                                         keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, answer, tmp, tmp2, l_, r_, 
                                         mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, start, p, bits, depth, 
                                         states >>

getRightFromRoot(self) == /\ pc[self] = "getRightFromRoot"
                          /\ tempR' = [tempR EXCEPT ![self] = nodeArr[gNodeArr[parent[self]].root].right]
                          /\ pc' = [pc EXCEPT ![self] = "setTempLValue"]
                          /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                          nodeArr, insertRes, resultsForGet, 
                                          deleteRes, processWriterState, 
                                          processReaderState, values, stack, i, 
                                          cur, istack, current_d, key_, tmp_, 
                                          tmp1, mid_, p_, p1, recStack_, 
                                          recStack1_, ret, depth_, res, calc, 
                                          maxDepth, l, r, gNode, buf_, rmax, 
                                          links, recStack_s, mid_s, val, child, 
                                          lastNode, bits_, depth_s, newBits, 
                                          movedBits, ln, rn, current, sibling, 
                                          keyL, keyR, buf, bufLink, p_d, tempL, 
                                          bits_d, depth_d, key_i, stackData, 
                                          keyRNext, oldCurrent, parent, 
                                          keyLNext, links_, oldSibling, 
                                          current_, rev, bits_i, depth_i, p_i, 
                                          temp, scanRes, t, array, split, 
                                          recStack, answer, tmp, tmp2, l_, r_, 
                                          mid, sibling_, keyL_, keyR_, 
                                          recStack1, key_g, startValue, 
                                          depth_g, bits_g, key, start, p, bits, 
                                          depth, states >>

setTempLValue(self) == /\ pc[self] = "setTempLValue"
                       /\ nodeArr' = [nodeArr EXCEPT ![tempL[self]].value = keyL_[self]]
                       /\ pc' = [pc EXCEPT ![self] = "setTempRValue"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       insertRes, resultsForGet, deleteRes, 
                                       processWriterState, processReaderState, 
                                       values, stack, i, cur, istack, 
                                       current_d, key_, tmp_, tmp1, mid_, p_, 
                                       p1, recStack_, recStack1_, ret, depth_, 
                                       res, calc, maxDepth, l, r, gNode, buf_, 
                                       rmax, links, recStack_s, mid_s, val, 
                                       child, lastNode, bits_, depth_s, 
                                       newBits, movedBits, ln, rn, current, 
                                       sibling, keyL, keyR, buf, bufLink, p_d, 
                                       tempL, tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

setTempRValue(self) == /\ pc[self] = "setTempRValue"
                       /\ nodeArr' = [nodeArr EXCEPT ![tempR[self]].value = keyR_[self]]
                       /\ pc' = [pc EXCEPT ![self] = "setSecondChild"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       insertRes, resultsForGet, deleteRes, 
                                       processWriterState, processReaderState, 
                                       values, stack, i, cur, istack, 
                                       current_d, key_, tmp_, tmp1, mid_, p_, 
                                       p1, recStack_, recStack1_, ret, depth_, 
                                       res, calc, maxDepth, l, r, gNode, buf_, 
                                       rmax, links, recStack_s, mid_s, val, 
                                       child, lastNode, bits_, depth_s, 
                                       newBits, movedBits, ln, rn, current, 
                                       sibling, keyL, keyR, buf, bufLink, p_d, 
                                       tempL, tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

setSecondChild(self) == /\ pc[self] = "setSecondChild"
                        /\ gNodeArr' = [gNodeArr EXCEPT ![parent[self]].links[shiftL(1, maxDepth[self] - 2)] = sibling_[self]]
                        /\ pc' = [pc EXCEPT ![self] = "setParentCountNode"]
                        /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                        insertRes, resultsForGet, deleteRes, 
                                        processWriterState, processReaderState, 
                                        values, stack, i, cur, istack, 
                                        current_d, key_, tmp_, tmp1, mid_, p_, 
                                        p1, recStack_, recStack1_, ret, depth_, 
                                        res, calc, maxDepth, l, r, gNode, buf_, 
                                        rmax, links, recStack_s, mid_s, val, 
                                        child, lastNode, bits_, depth_s, 
                                        newBits, movedBits, ln, rn, current, 
                                        sibling, keyL, keyR, buf, bufLink, p_d, 
                                        tempL, tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        current_, rev, bits_i, depth_i, p_i, 
                                        temp, scanRes, t, array, split, 
                                        recStack, answer, tmp, tmp2, l_, r_, 
                                        mid, sibling_, keyL_, keyR_, recStack1, 
                                        key_g, startValue, depth_g, bits_g, 
                                        key, start, p, bits, depth, states >>

setParentCountNode(self) == /\ pc[self] = "setParentCountNode"
                            /\ gNodeArr' = [gNodeArr EXCEPT ![parent[self]].countNode = 3]
                            /\ pc' = [pc EXCEPT ![self] = "setNewRoot"]
                            /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                            insertRes, resultsForGet, 
                                            deleteRes, processWriterState, 
                                            processReaderState, values, stack, 
                                            i, cur, istack, current_d, key_, 
                                            tmp_, tmp1, mid_, p_, p1, 
                                            recStack_, recStack1_, ret, depth_, 
                                            res, calc, maxDepth, l, r, gNode, 
                                            buf_, rmax, links, recStack_s, 
                                            mid_s, val, child, lastNode, bits_, 
                                            depth_s, newBits, movedBits, ln, 
                                            rn, current, sibling, keyL, keyR, 
                                            buf, bufLink, p_d, tempL, tempR, 
                                            bits_d, depth_d, key_i, stackData, 
                                            keyRNext, oldCurrent, parent, 
                                            keyLNext, links_, oldSibling, 
                                            current_, rev, bits_i, depth_i, 
                                            p_i, temp, scanRes, t, array, 
                                            split, recStack, answer, tmp, tmp2, 
                                            l_, r_, mid, sibling_, keyL_, 
                                            keyR_, recStack1, key_g, 
                                            startValue, depth_g, bits_g, key, 
                                            start, p, bits, depth, states >>

setNewRoot(self) == /\ pc[self] = "setNewRoot"
                    /\ root' = parent[self]
                    /\ pc' = [pc EXCEPT ![self] = "releaseCurrentLock2"]
                    /\ UNCHANGED << hasRoot, globalLock, gNodeArr, nodeArr, 
                                    insertRes, resultsForGet, deleteRes, 
                                    processWriterState, processReaderState, 
                                    values, stack, i, cur, istack, current_d, 
                                    key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                    recStack1_, ret, depth_, res, calc, 
                                    maxDepth, l, r, gNode, buf_, rmax, links, 
                                    recStack_s, mid_s, val, child, lastNode, 
                                    bits_, depth_s, newBits, movedBits, ln, rn, 
                                    current, sibling, keyL, keyR, buf, bufLink, 
                                    p_d, tempL, tempR, bits_d, depth_d, key_i, 
                                    stackData, keyRNext, oldCurrent, parent, 
                                    keyLNext, links_, oldSibling, current_, 
                                    rev, bits_i, depth_i, p_i, temp, scanRes, 
                                    t, array, split, recStack, answer, tmp, 
                                    tmp2, l_, r_, mid, sibling_, keyL_, keyR_, 
                                    recStack1, key_g, startValue, depth_g, 
                                    bits_g, key, start, p, bits, depth, states >>

releaseCurrentLock2(self) == /\ pc[self] = "releaseCurrentLock2"
                             /\ gNodeArr' = [gNodeArr EXCEPT ![current_[self]].lock = TRUE]
                             /\ pc' = [pc EXCEPT ![self] = "setInsertResult"]
                             /\ UNCHANGED << root, hasRoot, globalLock, 
                                             nodeArr, insertRes, resultsForGet, 
                                             deleteRes, processWriterState, 
                                             processReaderState, values, stack, 
                                             i, cur, istack, current_d, key_, 
                                             tmp_, tmp1, mid_, p_, p1, 
                                             recStack_, recStack1_, ret, 
                                             depth_, res, calc, maxDepth, l, r, 
                                             gNode, buf_, rmax, links, 
                                             recStack_s, mid_s, val, child, 
                                             lastNode, bits_, depth_s, newBits, 
                                             movedBits, ln, rn, current, 
                                             sibling, keyL, keyR, buf, bufLink, 
                                             p_d, tempL, tempR, bits_d, 
                                             depth_d, key_i, stackData, 
                                             keyRNext, oldCurrent, parent, 
                                             keyLNext, links_, oldSibling, 
                                             current_, rev, bits_i, depth_i, 
                                             p_i, temp, scanRes, t, array, 
                                             split, recStack, answer, tmp, 
                                             tmp2, l_, r_, mid, sibling_, 
                                             keyL_, keyR_, recStack1, key_g, 
                                             startValue, depth_g, bits_g, key, 
                                             start, p, bits, depth, states >>

setInsertResult(self) == /\ pc[self] = "setInsertResult"
                         /\ insertRes' = [insertRes EXCEPT ![self] = TRUE]
                         /\ pc' = [pc EXCEPT ![self] = Head(stack[self]).pc]
                         /\ stackData' = [stackData EXCEPT ![self] = Head(stack[self]).stackData]
                         /\ keyRNext' = [keyRNext EXCEPT ![self] = Head(stack[self]).keyRNext]
                         /\ oldCurrent' = [oldCurrent EXCEPT ![self] = Head(stack[self]).oldCurrent]
                         /\ parent' = [parent EXCEPT ![self] = Head(stack[self]).parent]
                         /\ keyLNext' = [keyLNext EXCEPT ![self] = Head(stack[self]).keyLNext]
                         /\ links_' = [links_ EXCEPT ![self] = Head(stack[self]).links_]
                         /\ oldSibling' = [oldSibling EXCEPT ![self] = Head(stack[self]).oldSibling]
                         /\ current_' = [current_ EXCEPT ![self] = Head(stack[self]).current_]
                         /\ rev' = [rev EXCEPT ![self] = Head(stack[self]).rev]
                         /\ bits_i' = [bits_i EXCEPT ![self] = Head(stack[self]).bits_i]
                         /\ depth_i' = [depth_i EXCEPT ![self] = Head(stack[self]).depth_i]
                         /\ p_i' = [p_i EXCEPT ![self] = Head(stack[self]).p_i]
                         /\ temp' = [temp EXCEPT ![self] = Head(stack[self]).temp]
                         /\ scanRes' = [scanRes EXCEPT ![self] = Head(stack[self]).scanRes]
                         /\ t' = [t EXCEPT ![self] = Head(stack[self]).t]
                         /\ array' = [array EXCEPT ![self] = Head(stack[self]).array]
                         /\ split' = [split EXCEPT ![self] = Head(stack[self]).split]
                         /\ recStack' = [recStack EXCEPT ![self] = Head(stack[self]).recStack]
                         /\ answer' = [answer EXCEPT ![self] = Head(stack[self]).answer]
                         /\ tmp' = [tmp EXCEPT ![self] = Head(stack[self]).tmp]
                         /\ tmp2' = [tmp2 EXCEPT ![self] = Head(stack[self]).tmp2]
                         /\ l_' = [l_ EXCEPT ![self] = Head(stack[self]).l_]
                         /\ r_' = [r_ EXCEPT ![self] = Head(stack[self]).r_]
                         /\ mid' = [mid EXCEPT ![self] = Head(stack[self]).mid]
                         /\ sibling_' = [sibling_ EXCEPT ![self] = Head(stack[self]).sibling_]
                         /\ keyL_' = [keyL_ EXCEPT ![self] = Head(stack[self]).keyL_]
                         /\ keyR_' = [keyR_ EXCEPT ![self] = Head(stack[self]).keyR_]
                         /\ recStack1' = [recStack1 EXCEPT ![self] = Head(stack[self]).recStack1]
                         /\ key_i' = [key_i EXCEPT ![self] = Head(stack[self]).key_i]
                         /\ stack' = [stack EXCEPT ![self] = Tail(stack[self])]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, resultsForGet, deleteRes, 
                                         processWriterState, 
                                         processReaderState, values, i, cur, 
                                         istack, current_d, key_, tmp_, tmp1, 
                                         mid_, p_, p1, recStack_, recStack1_, 
                                         ret, depth_, res, calc, maxDepth, l, 
                                         r, gNode, buf_, rmax, links, 
                                         recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_g, 
                                         startValue, depth_g, bits_g, key, 
                                         start, p, bits, depth, states >>

setOldCurrent(self) == /\ pc[self] = "setOldCurrent"
                       /\ oldCurrent' = [oldCurrent EXCEPT ![self] = current_[self]]
                       /\ current_' = [current_ EXCEPT ![self] = Tail(stackData[self])]
                       /\ stackData' = [stackData EXCEPT ![self] = SubSeq(stackData[self], 0, Len(stackData[self]) - 1)]
                       /\ pc' = [pc EXCEPT ![self] = "releaseCurrentLock3"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, keyRNext, 
                                       parent, keyLNext, links_, oldSibling, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

releaseCurrentLock3(self) == /\ pc[self] = "releaseCurrentLock3"
                             /\ gNodeArr[current_[self]].lock
                             /\ gNodeArr' = [gNodeArr EXCEPT ![current_[self]].lock = FALSE]
                             /\ pc' = [pc EXCEPT ![self] = "startMoveRight2"]
                             /\ UNCHANGED << root, hasRoot, globalLock, 
                                             nodeArr, insertRes, resultsForGet, 
                                             deleteRes, processWriterState, 
                                             processReaderState, values, stack, 
                                             i, cur, istack, current_d, key_, 
                                             tmp_, tmp1, mid_, p_, p1, 
                                             recStack_, recStack1_, ret, 
                                             depth_, res, calc, maxDepth, l, r, 
                                             gNode, buf_, rmax, links, 
                                             recStack_s, mid_s, val, child, 
                                             lastNode, bits_, depth_s, newBits, 
                                             movedBits, ln, rn, current, 
                                             sibling, keyL, keyR, buf, bufLink, 
                                             p_d, tempL, tempR, bits_d, 
                                             depth_d, key_i, stackData, 
                                             keyRNext, oldCurrent, parent, 
                                             keyLNext, links_, oldSibling, 
                                             current_, rev, bits_i, depth_i, 
                                             p_i, temp, scanRes, t, array, 
                                             split, recStack, answer, tmp, 
                                             tmp2, l_, r_, mid, sibling_, 
                                             keyL_, keyR_, recStack1, key_g, 
                                             startValue, depth_g, bits_g, key, 
                                             start, p, bits, depth, states >>

startMoveRight2(self) == /\ pc[self] = "startMoveRight2"
                         /\ IF gNodeArr[t[self]].hasSibling /\ gNodeArr[t[self]].highKey > 0 /\ gNodeArr[t[self]].highKey <= keyR_[self]
                               THEN /\ pc' = [pc EXCEPT ![self] = "saveSibling2"]
                                    /\ UNCHANGED current_
                               ELSE /\ current_' = [current_ EXCEPT ![self] = t[self]]
                                    /\ pc' = [pc EXCEPT ![self] = "relOldCurLock"]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         rev, bits_i, depth_i, p_i, temp, 
                                         scanRes, t, array, split, recStack, 
                                         answer, tmp, tmp2, l_, r_, mid, 
                                         sibling_, keyL_, keyR_, recStack1, 
                                         key_g, startValue, depth_g, bits_g, 
                                         key, start, p, bits, depth, states >>

saveSibling2(self) == /\ pc[self] = "saveSibling2"
                      /\ current_' = [current_ EXCEPT ![self] = gNodeArr[t[self]].sibling]
                      /\ pc' = [pc EXCEPT ![self] = "getNewLock2"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, rev, 
                                      bits_i, depth_i, p_i, temp, scanRes, t, 
                                      array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

getNewLock2(self) == /\ pc[self] = "getNewLock2"
                     /\ gNodeArr[current_[self]].lock
                     /\ gNodeArr' = [gNodeArr EXCEPT ![current_[self]].lock = FALSE]
                     /\ pc' = [pc EXCEPT ![self] = "releaseOldLock2"]
                     /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                     insertRes, resultsForGet, deleteRes, 
                                     processWriterState, processReaderState, 
                                     values, stack, i, cur, istack, current_d, 
                                     key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                     recStack1_, ret, depth_, res, calc, 
                                     maxDepth, l, r, gNode, buf_, rmax, links, 
                                     recStack_s, mid_s, val, child, lastNode, 
                                     bits_, depth_s, newBits, movedBits, ln, 
                                     rn, current, sibling, keyL, keyR, buf, 
                                     bufLink, p_d, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

releaseOldLock2(self) == /\ pc[self] = "releaseOldLock2"
                         /\ gNodeArr' = [gNodeArr EXCEPT ![t[self]].lock = TRUE]
                         /\ t' = [t EXCEPT ![self] = current_[self]]
                         /\ pc' = [pc EXCEPT ![self] = "startMoveRight2"]
                         /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                         insertRes, resultsForGet, deleteRes, 
                                         processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, array, split, recStack, 
                                         answer, tmp, tmp2, l_, r_, mid, 
                                         sibling_, keyL_, keyR_, recStack1, 
                                         key_g, startValue, depth_g, bits_g, 
                                         key, start, p, bits, depth, states >>

relOldCurLock(self) == /\ pc[self] = "relOldCurLock"
                       /\ gNodeArr' = [gNodeArr EXCEPT ![oldCurrent[self]].lock = TRUE]
                       /\ pc' = [pc EXCEPT ![self] = "startInsertCycle"]
                       /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                       insertRes, resultsForGet, deleteRes, 
                                       processWriterState, processReaderState, 
                                       values, stack, i, cur, istack, 
                                       current_d, key_, tmp_, tmp1, mid_, p_, 
                                       p1, recStack_, recStack1_, ret, depth_, 
                                       res, calc, maxDepth, l, r, gNode, buf_, 
                                       rmax, links, recStack_s, mid_s, val, 
                                       child, lastNode, bits_, depth_s, 
                                       newBits, movedBits, ln, rn, current, 
                                       sibling, keyL, keyR, buf, bufLink, p_d, 
                                       tempL, tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

insert(self) == checkHasRoot(self) \/ hasRootLock(self)
                   \/ checkHasRoot2(self) \/ initLeaf_i(self)
                   \/ setRoot(self) \/ releaseLock(self)
                   \/ returnInsertRes(self) \/ releaseLock2(self)
                   \/ startFindGNode(self) \/ startFindGNode2(self)
                   \/ startCycle(self) \/ startScanNode(self)
                   \/ updating(self) \/ getRoot(self)
                   \/ startSearchInGNode(self) \/ checkIsLeft(self)
                   \/ goLeft(self) \/ goRight(self) \/ preparePath(self)
                   \/ checkHighKey(self) \/ checkRev(self)
                   \/ getChildGNode(self) \/ checkRev2(self)
                   \/ prepareStack(self) \/ getGNodeLock(self)
                   \/ startMoveRight(self) \/ saveSibling(self)
                   \/ getNewLock(self) \/ releaseOldLock(self)
                   \/ startInsertCycle(self) \/ checkIsLeaf(self)
                   \/ checkGNodeSize(self) \/ startFillBuf(self)
                   \/ checkLeft(self) \/ goToLeft(self)
                   \/ saveAnswer2(self) \/ saveAnswer(self)
                   \/ checkRight(self) \/ goToRight(self)
                   \/ checkRecSize(self) \/ goToRet(self)
                   \/ startUpdateCur(self) \/ startFillVal(self)
                   \/ getMid(self) \/ checkLeft2(self) \/ goToLeft2(self)
                   \/ updateValue(self) \/ saveAnswerHasValue7(self)
                   \/ checkRight2(self) \/ goToRight2(self)
                   \/ checkRecSize2(self) \/ getRetValue(self)
                   \/ createSibling(self) \/ setSibling_(self)
                   \/ startUpdateSibling2(self)
                   \/ updateSiblingHighKey(self) \/ updateSibling(self)
                   \/ updateCurrentHighKey(self)
                   \/ updateCurrentSibling(self)
                   \/ finishUpdateCurrent4(self) \/ startFillVal2(self)
                   \/ getMid2(self) \/ checkLeft3(self) \/ goToLeft3(self)
                   \/ saveAnswer3(self) \/ saveAnswerHasValue2(self)
                   \/ checkRight3(self) \/ goToRight3(self)
                   \/ checkRecSize3(self) \/ goToRet3(self)
                   \/ finishUpdateSibling2(self)
                   \/ updateCurrentCount(self) \/ updateSiblingCount(self)
                   \/ checkCurrentHighKey(self) \/ insertLeaf(self)
                   \/ insertLeaf2(self) \/ insertLeaf3(self)
                   \/ releaseGNodelock2(self) \/ rerturnInsert2(self)
                   \/ startFillBufLo(self) \/ fboCheckLeftRight(self)
                   \/ fboGetleft(self) \/ fboGetRight(self)
                   \/ fboCheckLeftRight2(self) \/ fboCheckLeftRight3(self)
                   \/ fboIncValue(self) \/ fboCheckRetSize(self)
                   \/ fboGoToRet3(self) \/ copyLinks(self)
                   \/ startUpdateCurrent(self) \/ insertValues1(self)
                   \/ insertValues2(self) \/ callCurrentFill1(self)
                   \/ callCurrentFill2(self) \/ createSibling2(self)
                   \/ getSibling(self) \/ startUpdateSibling(self)
                   \/ updateSiblingValues1(self)
                   \/ updateSiblingValues2(self)
                   \/ updateCurrentValues1(self)
                   \/ updateCurrentValues2(self)
                   \/ finishUpdateCurrent(self) \/ insertValues3(self)
                   \/ insertValues4(self) \/ callCurrentFill3(self)
                   \/ callCurrentFill4(self) \/ finishUpdateSibling(self)
                   \/ updateCountNodes1(self) \/ updateCountNodes2(self)
                   \/ insertNodeIf(self) \/ doInsertNode1(self)
                   \/ doInsertNode2(self) \/ calcNextKeys(self)
                   \/ doInsertNode3(self) \/ releaseCurrentLock(self)
                   \/ insertRes_(self) \/ rootRebalance(self)
                   \/ initNewRoot(self) \/ initNewRoot2(self)
                   \/ setRootValue(self) \/ setRootHasValue(self)
                   \/ setRootLinks(self) \/ getLeftFromRoot(self)
                   \/ getRightFromRoot(self) \/ setTempLValue(self)
                   \/ setTempRValue(self) \/ setSecondChild(self)
                   \/ setParentCountNode(self) \/ setNewRoot(self)
                   \/ releaseCurrentLock2(self) \/ setInsertResult(self)
                   \/ setOldCurrent(self) \/ releaseCurrentLock3(self)
                   \/ startMoveRight2(self) \/ saveSibling2(self)
                   \/ getNewLock2(self) \/ releaseOldLock2(self)
                   \/ relOldCurLock(self)

checkRoot(self) == /\ pc[self] = "checkRoot"
                   /\ IF ~hasRoot
                         THEN /\ resultsForGet' = [resultsForGet EXCEPT ![self] = <<-1, key_g[self]>>]
                              /\ pc' = [pc EXCEPT ![self] = Head(stack[self]).pc]
                              /\ startValue' = [startValue EXCEPT ![self] = Head(stack[self]).startValue]
                              /\ depth_g' = [depth_g EXCEPT ![self] = Head(stack[self]).depth_g]
                              /\ bits_g' = [bits_g EXCEPT ![self] = Head(stack[self]).bits_g]
                              /\ key_g' = [key_g EXCEPT ![self] = Head(stack[self]).key_g]
                              /\ stack' = [stack EXCEPT ![self] = Tail(stack[self])]
                         ELSE /\ pc' = [pc EXCEPT ![self] = "startSearchGNode"]
                              /\ UNCHANGED << resultsForGet, stack, key_g, 
                                              startValue, depth_g, bits_g >>
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, deleteRes, 
                                   processWriterState, processReaderState, 
                                   values, i, cur, istack, current_d, key_, 
                                   tmp_, tmp1, mid_, p_, p1, recStack_, 
                                   recStack1_, ret, depth_, res, calc, 
                                   maxDepth, l, r, gNode, buf_, rmax, links, 
                                   recStack_s, mid_s, val, child, lastNode, 
                                   bits_, depth_s, newBits, movedBits, ln, rn, 
                                   current, sibling, keyL, keyR, buf, bufLink, 
                                   p_d, tempL, tempR, bits_d, depth_d, key_i, 
                                   stackData, keyRNext, oldCurrent, parent, 
                                   keyLNext, links_, oldSibling, current_, rev, 
                                   bits_i, depth_i, p_i, temp, scanRes, t, 
                                   array, split, recStack, answer, tmp, tmp2, 
                                   l_, r_, mid, sibling_, keyL_, keyR_, 
                                   recStack1, key, start, p, bits, depth, 
                                   states >>

startSearchGNode(self) == /\ pc[self] = "startSearchGNode"
                          /\ startValue' = [startValue EXCEPT ![self] = root]
                          /\ pc' = [pc EXCEPT ![self] = "sgnStartCycle"]
                          /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                          nodeArr, insertRes, resultsForGet, 
                                          deleteRes, processWriterState, 
                                          processReaderState, values, stack, i, 
                                          cur, istack, current_d, key_, tmp_, 
                                          tmp1, mid_, p_, p1, recStack_, 
                                          recStack1_, ret, depth_, res, calc, 
                                          maxDepth, l, r, gNode, buf_, rmax, 
                                          links, recStack_s, mid_s, val, child, 
                                          lastNode, bits_, depth_s, newBits, 
                                          movedBits, ln, rn, current, sibling, 
                                          keyL, keyR, buf, bufLink, p_d, tempL, 
                                          tempR, bits_d, depth_d, key_i, 
                                          stackData, keyRNext, oldCurrent, 
                                          parent, keyLNext, links_, oldSibling, 
                                          current_, rev, bits_i, depth_i, p_i, 
                                          temp, scanRes, t, array, split, 
                                          recStack, answer, tmp, tmp2, l_, r_, 
                                          mid, sibling_, keyL_, keyR_, 
                                          recStack1, key_g, depth_g, bits_g, 
                                          key, start, p, bits, depth, states >>

sgnStartCycle(self) == /\ pc[self] = "sgnStartCycle"
                       /\ IF gNodeArr[startValue[self]].isLeaf
                             THEN /\ pc' = [pc EXCEPT ![self] = "getStartRoot_"]
                             ELSE /\ pc' = [pc EXCEPT ![self] = "startItSearchLo"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

getStartRoot_(self) == /\ pc[self] = "getStartRoot_"
                       /\ p' = [p EXCEPT ![self] = gNodeArr[startValue[self]].root]
                       /\ bits_g' = [bits_g EXCEPT ![self] = 1]
                       /\ depth_g' = [depth_g EXCEPT ![self] = 0]
                       /\ pc' = [pc EXCEPT ![self] = "checkNodeCycle"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, key, start, bits, 
                                       depth, states >>

checkNodeCycle(self) == /\ pc[self] = "checkNodeCycle"
                        /\ IF p[self].hasValue
                              THEN /\ depth_g' = [depth_g EXCEPT ![self] = depth_g[self] + 1]
                                   /\ bits_g' = [bits_g EXCEPT ![self] = bits_g[self] * 2]
                                   /\ pc' = [pc EXCEPT ![self] = "checkKeyValue"]
                              ELSE /\ pc' = [pc EXCEPT ![self] = "prepareBits"]
                                   /\ UNCHANGED << depth_g, bits_g >>
                        /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                        nodeArr, insertRes, resultsForGet, 
                                        deleteRes, processWriterState, 
                                        processReaderState, values, stack, i, 
                                        cur, istack, current_d, key_, tmp_, 
                                        tmp1, mid_, p_, p1, recStack_, 
                                        recStack1_, ret, depth_, res, calc, 
                                        maxDepth, l, r, gNode, buf_, rmax, 
                                        links, recStack_s, mid_s, val, child, 
                                        lastNode, bits_, depth_s, newBits, 
                                        movedBits, ln, rn, current, sibling, 
                                        keyL, keyR, buf, bufLink, p_d, tempL, 
                                        tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        current_, rev, bits_i, depth_i, p_i, 
                                        temp, scanRes, t, array, split, 
                                        recStack, answer, tmp, tmp2, l_, r_, 
                                        mid, sibling_, keyL_, keyR_, recStack1, 
                                        key_g, startValue, key, start, p, bits, 
                                        depth, states >>

checkKeyValue(self) == /\ pc[self] = "checkKeyValue"
                       /\ IF key_g[self] < nodeArr[p[self]].value
                             THEN /\ pc' = [pc EXCEPT ![self] = "setLeftP"]
                             ELSE /\ pc' = [pc EXCEPT ![self] = "setRightP"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

setLeftP(self) == /\ pc[self] = "setLeftP"
                  /\ p' = [p EXCEPT ![self] = nodeArr[p[self]].left]
                  /\ pc' = [pc EXCEPT ![self] = "checkNodeCycle"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  p_i, temp, scanRes, t, array, split, 
                                  recStack, answer, tmp, tmp2, l_, r_, mid, 
                                  sibling_, keyL_, keyR_, recStack1, key_g, 
                                  startValue, depth_g, bits_g, key, start, 
                                  bits, depth, states >>

setRightP(self) == /\ pc[self] = "setRightP"
                   /\ p' = [p EXCEPT ![self] = nodeArr[p[self]].right]
                   /\ bits_g' = [bits_g EXCEPT ![self] = bits_g[self] + 1]
                   /\ pc' = [pc EXCEPT ![self] = "checkNodeCycle"]
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, resultsForGet, 
                                   deleteRes, processWriterState, 
                                   processReaderState, values, stack, i, cur, 
                                   istack, current_d, key_, tmp_, tmp1, mid_, 
                                   p_, p1, recStack_, recStack1_, ret, depth_, 
                                   res, calc, maxDepth, l, r, gNode, buf_, 
                                   rmax, links, recStack_s, mid_s, val, child, 
                                   lastNode, bits_, depth_s, newBits, 
                                   movedBits, ln, rn, current, sibling, keyL, 
                                   keyR, buf, bufLink, p_d, tempL, tempR, 
                                   bits_d, depth_d, key_i, stackData, keyRNext, 
                                   oldCurrent, parent, keyLNext, links_, 
                                   oldSibling, current_, rev, bits_i, depth_i, 
                                   p_i, temp, scanRes, t, array, split, 
                                   recStack, answer, tmp, tmp2, l_, r_, mid, 
                                   sibling_, keyL_, keyR_, recStack1, key_g, 
                                   startValue, depth_g, key, start, bits, 
                                   depth, states >>

prepareBits(self) == /\ pc[self] = "prepareBits"
                     /\ bits_g' = [bits_g EXCEPT ![self] = bits_g[self] \div 2]
                     /\ pc' = [pc EXCEPT ![self] = "prepareBits2"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     key, start, p, bits, depth, states >>

prepareBits2(self) == /\ pc[self] = "prepareBits2"
                      /\ bits_g' = [bits_g EXCEPT ![self] = shiftL(bits_g[self], GNodeDepth - depth_g[self])]
                      /\ pc' = [pc EXCEPT ![self] = "checkHighKey2"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, key, start, p, bits, depth, 
                                      states >>

checkHighKey2(self) == /\ pc[self] = "checkHighKey2"
                       /\ IF gNodeArr[startValue[self]].hasSibling /\ gNodeArr[startValue[self]].highKey > 0 /\ gNodeArr[startValue[self]].highKey <= key_g[self]
                             THEN /\ pc' = [pc EXCEPT ![self] = "setSibling"]
                                  /\ UNCHANGED startValue
                             ELSE /\ IF gNodeArr[startValue[self]].hasLinks[bits_g[self]]
                                        THEN /\ startValue' = [startValue EXCEPT ![self] = gNodeArr[startValue[self]].links[bits_g[self]]]
                                             /\ pc' = [pc EXCEPT ![self] = "sgnStartCycle"]
                                        ELSE /\ pc' = [pc EXCEPT ![self] = "startItSearchLo"]
                                             /\ UNCHANGED startValue
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, depth_g, bits_g, key, start, p, 
                                       bits, depth, states >>

setSibling(self) == /\ pc[self] = "setSibling"
                    /\ startValue' = [startValue EXCEPT ![self] = gNodeArr[startValue[self]].sibling]
                    /\ pc' = [pc EXCEPT ![self] = "sgnStartCycle"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, p1, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, recStack, answer, tmp, tmp2, l_, r_, 
                                    mid, sibling_, keyL_, keyR_, recStack1, 
                                    key_g, depth_g, bits_g, key, start, p, 
                                    bits, depth, states >>

startItSearchLo(self) == /\ pc[self] = "startItSearchLo"
                         /\ p' = [p EXCEPT ![self] = gNodeArr[startValue[self]].root]
                         /\ lastNode' = [lastNode EXCEPT ![self] = p'[self]]
                         /\ pc' = [pc EXCEPT ![self] = "cycleItSearchLo"]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         bits_, depth_s, newBits, movedBits, 
                                         ln, rn, current, sibling, keyL, keyR, 
                                         buf, bufLink, p_d, tempL, tempR, 
                                         bits_d, depth_d, key_i, stackData, 
                                         keyRNext, oldCurrent, parent, 
                                         keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, answer, tmp, tmp2, l_, r_, 
                                         mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, start, bits, depth, 
                                         states >>

cycleItSearchLo(self) == /\ pc[self] = "cycleItSearchLo"
                         /\ IF nodeArr[p[self]].hasValue
                               THEN /\ pc' = [pc EXCEPT ![self] = "saveLastNode"]
                               ELSE /\ pc' = [pc EXCEPT ![self] = "returnGet"]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, answer, tmp, tmp2, l_, r_, 
                                         mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, start, p, bits, depth, 
                                         states >>

saveLastNode(self) == /\ pc[self] = "saveLastNode"
                      /\ lastNode' = [lastNode EXCEPT ![self] = p[self]]
                      /\ pc' = [pc EXCEPT ![self] = "smislCheckValue"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, bits_, depth_s, newBits, 
                                      movedBits, ln, rn, current, sibling, 
                                      keyL, keyR, buf, bufLink, p_d, tempL, 
                                      tempR, bits_d, depth_d, key_i, stackData, 
                                      keyRNext, oldCurrent, parent, keyLNext, 
                                      links_, oldSibling, current_, rev, 
                                      bits_i, depth_i, p_i, temp, scanRes, t, 
                                      array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, p, bits, 
                                      depth, states >>

smislCheckValue(self) == /\ pc[self] = "smislCheckValue"
                         /\ IF key_g[self] < nodeArr[p[self]].value
                               THEN /\ IF ~nodeArr[p[self]].hasLeft
                                          THEN /\ pc' = [pc EXCEPT ![self] = "returnGet"]
                                          ELSE /\ pc' = [pc EXCEPT ![self] = "smislSetLeft"]
                               ELSE /\ IF ~nodeArr[p[self]].hasRight
                                          THEN /\ pc' = [pc EXCEPT ![self] = "returnGet"]
                                          ELSE /\ pc' = [pc EXCEPT ![self] = "smislSetRight"]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, answer, tmp, tmp2, l_, r_, 
                                         mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, start, p, bits, depth, 
                                         states >>

smislSetLeft(self) == /\ pc[self] = "smislSetLeft"
                      /\ p' = [p EXCEPT ![self] = nodeArr[p[self]].left]
                      /\ pc' = [pc EXCEPT ![self] = "cycleItSearchLo"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, bits, depth, 
                                      states >>

smislSetRight(self) == /\ pc[self] = "smislSetRight"
                       /\ p' = [p EXCEPT ![self] = nodeArr[p[self]].right]
                       /\ pc' = [pc EXCEPT ![self] = "cycleItSearchLo"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, bits, depth, states >>

returnGet(self) == /\ pc[self] = "returnGet"
                   /\ resultsForGet' = [resultsForGet EXCEPT ![self] = <<nodeArr[lastNode[self]].value, key_g[self]>>]
                   /\ pc' = [pc EXCEPT ![self] = "Error"]
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, deleteRes, 
                                   processWriterState, processReaderState, 
                                   values, stack, i, cur, istack, current_d, 
                                   key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                   recStack1_, ret, depth_, res, calc, 
                                   maxDepth, l, r, gNode, buf_, rmax, links, 
                                   recStack_s, mid_s, val, child, lastNode, 
                                   bits_, depth_s, newBits, movedBits, ln, rn, 
                                   current, sibling, keyL, keyR, buf, bufLink, 
                                   p_d, tempL, tempR, bits_d, depth_d, key_i, 
                                   stackData, keyRNext, oldCurrent, parent, 
                                   keyLNext, links_, oldSibling, current_, rev, 
                                   bits_i, depth_i, p_i, temp, scanRes, t, 
                                   array, split, recStack, answer, tmp, tmp2, 
                                   l_, r_, mid, sibling_, keyL_, keyR_, 
                                   recStack1, key_g, startValue, depth_g, 
                                   bits_g, key, start, p, bits, depth, states >>

get(self) == checkRoot(self) \/ startSearchGNode(self)
                \/ sgnStartCycle(self) \/ getStartRoot_(self)
                \/ checkNodeCycle(self) \/ checkKeyValue(self)
                \/ setLeftP(self) \/ setRightP(self) \/ prepareBits(self)
                \/ prepareBits2(self) \/ checkHighKey2(self)
                \/ setSibling(self) \/ startItSearchLo(self)
                \/ cycleItSearchLo(self) \/ saveLastNode(self)
                \/ smislCheckValue(self) \/ smislSetLeft(self)
                \/ smislSetRight(self) \/ returnGet(self)

startDelete(self) == /\ pc[self] = "startDelete"
                     /\ IF ~hasRoot
                           THEN /\ deleteRes' = [deleteRes EXCEPT ![self] = 0]
                                /\ pc' = [pc EXCEPT ![self] = "returnDel"]
                           ELSE /\ pc' = [pc EXCEPT ![self] = "getStart"]
                                /\ UNCHANGED deleteRes
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     processWriterState, processReaderState, 
                                     values, stack, i, cur, istack, current_d, 
                                     key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                     recStack1_, ret, depth_, res, calc, 
                                     maxDepth, l, r, gNode, buf_, rmax, links, 
                                     recStack_s, mid_s, val, child, lastNode, 
                                     bits_, depth_s, newBits, movedBits, ln, 
                                     rn, current, sibling, keyL, keyR, buf, 
                                     bufLink, p_d, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

returnDel(self) == /\ pc[self] = "returnDel"
                   /\ pc' = [pc EXCEPT ![self] = Head(stack[self]).pc]
                   /\ start' = [start EXCEPT ![self] = Head(stack[self]).start]
                   /\ p' = [p EXCEPT ![self] = Head(stack[self]).p]
                   /\ bits' = [bits EXCEPT ![self] = Head(stack[self]).bits]
                   /\ depth' = [depth EXCEPT ![self] = Head(stack[self]).depth]
                   /\ key' = [key EXCEPT ![self] = Head(stack[self]).key]
                   /\ stack' = [stack EXCEPT ![self] = Tail(stack[self])]
                   /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                   nodeArr, insertRes, resultsForGet, 
                                   deleteRes, processWriterState, 
                                   processReaderState, values, i, cur, istack, 
                                   current_d, key_, tmp_, tmp1, mid_, p_, p1, 
                                   recStack_, recStack1_, ret, depth_, res, 
                                   calc, maxDepth, l, r, gNode, buf_, rmax, 
                                   links, recStack_s, mid_s, val, child, 
                                   lastNode, bits_, depth_s, newBits, 
                                   movedBits, ln, rn, current, sibling, keyL, 
                                   keyR, buf, bufLink, p_d, tempL, tempR, 
                                   bits_d, depth_d, key_i, stackData, keyRNext, 
                                   oldCurrent, parent, keyLNext, links_, 
                                   oldSibling, current_, rev, bits_i, depth_i, 
                                   p_i, temp, scanRes, t, array, split, 
                                   recStack, answer, tmp, tmp2, l_, r_, mid, 
                                   sibling_, keyL_, keyR_, recStack1, key_g, 
                                   startValue, depth_g, bits_g, states >>

getStart(self) == /\ pc[self] = "getStart"
                  /\ start' = [start EXCEPT ![self] = root]
                  /\ pc' = [pc EXCEPT ![self] = "sslStartCycle"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, stack, i, cur, istack, current_d, 
                                  key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  p_i, temp, scanRes, t, array, split, 
                                  recStack, answer, tmp, tmp2, l_, r_, mid, 
                                  sibling_, keyL_, keyR_, recStack1, key_g, 
                                  startValue, depth_g, bits_g, key, p, bits, 
                                  depth, states >>

sslStartCycle(self) == /\ pc[self] = "sslStartCycle"
                       /\ IF gNodeArr[start[self]].isLeaf
                             THEN /\ pc' = [pc EXCEPT ![self] = "getStartRoot"]
                             ELSE /\ pc' = [pc EXCEPT ![self] = "lockGettedGNode"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

getStartRoot(self) == /\ pc[self] = "getStartRoot"
                      /\ p' = [p EXCEPT ![self] = gNodeArr[start[self]].root]
                      /\ bits' = [bits EXCEPT ![self] = 1]
                      /\ depth' = [depth EXCEPT ![self] = 0]
                      /\ pc' = [pc EXCEPT ![self] = "startInnerCycle"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, start, states >>

startInnerCycle(self) == /\ pc[self] = "startInnerCycle"
                         /\ IF nodeArr[p[self]].hasValue
                               THEN /\ depth' = [depth EXCEPT ![self] = depth[self] + 1]
                                    /\ bits' = [bits EXCEPT ![self] = bits[self] * 2]
                                    /\ pc' = [pc EXCEPT ![self] = "sslCheckKey"]
                               ELSE /\ pc' = [pc EXCEPT ![self] = "sslPrepareBits"]
                                    /\ UNCHANGED << bits, depth >>
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, answer, tmp, tmp2, l_, r_, 
                                         mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, start, p, states >>

sslCheckKey(self) == /\ pc[self] = "sslCheckKey"
                     /\ IF key[self] < nodeArr[p[self]].value
                           THEN /\ pc' = [pc EXCEPT ![self] = "sslSetLeft"]
                           ELSE /\ pc' = [pc EXCEPT ![self] = "sslSetRight"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

sslSetLeft(self) == /\ pc[self] = "sslSetLeft"
                    /\ p' = [p EXCEPT ![self] = nodeArr[p[self]].left]
                    /\ pc' = [pc EXCEPT ![self] = "startInnerCycle"]
                    /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                    nodeArr, insertRes, resultsForGet, 
                                    deleteRes, processWriterState, 
                                    processReaderState, values, stack, i, cur, 
                                    istack, current_d, key_, tmp_, tmp1, mid_, 
                                    p_, p1, recStack_, recStack1_, ret, depth_, 
                                    res, calc, maxDepth, l, r, gNode, buf_, 
                                    rmax, links, recStack_s, mid_s, val, child, 
                                    lastNode, bits_, depth_s, newBits, 
                                    movedBits, ln, rn, current, sibling, keyL, 
                                    keyR, buf, bufLink, p_d, tempL, tempR, 
                                    bits_d, depth_d, key_i, stackData, 
                                    keyRNext, oldCurrent, parent, keyLNext, 
                                    links_, oldSibling, current_, rev, bits_i, 
                                    depth_i, p_i, temp, scanRes, t, array, 
                                    split, recStack, answer, tmp, tmp2, l_, r_, 
                                    mid, sibling_, keyL_, keyR_, recStack1, 
                                    key_g, startValue, depth_g, bits_g, key, 
                                    start, bits, depth, states >>

sslSetRight(self) == /\ pc[self] = "sslSetRight"
                     /\ p' = [p EXCEPT ![self] = nodeArr[p[self]].right]
                     /\ bits' = [bits EXCEPT ![self] = bits[self] + 1]
                     /\ pc' = [pc EXCEPT ![self] = "startInnerCycle"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, stack, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_i, stackData, 
                                     keyRNext, oldCurrent, parent, keyLNext, 
                                     links_, oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, depth, states >>

sslPrepareBits(self) == /\ pc[self] = "sslPrepareBits"
                        /\ bits' = [bits EXCEPT ![self] = bits[self] \div 2]
                        /\ pc' = [pc EXCEPT ![self] = "sslPrepareBits2"]
                        /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                        nodeArr, insertRes, resultsForGet, 
                                        deleteRes, processWriterState, 
                                        processReaderState, values, stack, i, 
                                        cur, istack, current_d, key_, tmp_, 
                                        tmp1, mid_, p_, p1, recStack_, 
                                        recStack1_, ret, depth_, res, calc, 
                                        maxDepth, l, r, gNode, buf_, rmax, 
                                        links, recStack_s, mid_s, val, child, 
                                        lastNode, bits_, depth_s, newBits, 
                                        movedBits, ln, rn, current, sibling, 
                                        keyL, keyR, buf, bufLink, p_d, tempL, 
                                        tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        current_, rev, bits_i, depth_i, p_i, 
                                        temp, scanRes, t, array, split, 
                                        recStack, answer, tmp, tmp2, l_, r_, 
                                        mid, sibling_, keyL_, keyR_, recStack1, 
                                        key_g, startValue, depth_g, bits_g, 
                                        key, start, p, depth, states >>

sslPrepareBits2(self) == /\ pc[self] = "sslPrepareBits2"
                         /\ bits' = [bits EXCEPT ![self] = shiftL(bits[self], GNodeDepth - depth[self])]
                         /\ pc' = [pc EXCEPT ![self] = "sslCheckGNode"]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, answer, tmp, tmp2, l_, r_, 
                                         mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, start, p, depth, states >>

sslCheckGNode(self) == /\ pc[self] = "sslCheckGNode"
                       /\ IF gNodeArr[start[self]].hasSibling /\ gNodeArr[start[self]].highKey <= key[self]
                             THEN /\ pc' = [pc EXCEPT ![self] = "setStartSibling"]
                             ELSE /\ pc' = [pc EXCEPT ![self] = "sslCheckLinks"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

setStartSibling(self) == /\ pc[self] = "setStartSibling"
                         /\ start' = [start EXCEPT ![self] = gNodeArr[start[self]].sibling]
                         /\ pc' = [pc EXCEPT ![self] = "sslStartCycle"]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, answer, tmp, tmp2, l_, r_, 
                                         mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, p, bits, depth, states >>

sslCheckLinks(self) == /\ pc[self] = "sslCheckLinks"
                       /\ IF gNodeArr[start[self]].hasLinks[bits[self]]
                             THEN /\ pc' = [pc EXCEPT ![self] = "sslGoToLinks"]
                             ELSE /\ pc' = [pc EXCEPT ![self] = "lockGettedGNode"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, p, bits, depth, states >>

sslGoToLinks(self) == /\ pc[self] = "sslGoToLinks"
                      /\ start' = [start EXCEPT ![self] = gNodeArr[start[self]].links[bits[self]]]
                      /\ pc' = [pc EXCEPT ![self] = "sslStartCycle"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, p, bits, depth, 
                                      states >>

lockGettedGNode(self) == /\ pc[self] = "lockGettedGNode"
                         /\ gNodeArr[start[self]].lock
                         /\ gNodeArr' = [gNodeArr EXCEPT ![start[self]].lock = FALSE]
                         /\ t' = [t EXCEPT ![self] = start[self]]
                         /\ pc' = [pc EXCEPT ![self] = "startMoveRight3"]
                         /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                         insertRes, resultsForGet, deleteRes, 
                                         processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, array, split, recStack, 
                                         answer, tmp, tmp2, l_, r_, mid, 
                                         sibling_, keyL_, keyR_, recStack1, 
                                         key_g, startValue, depth_g, bits_g, 
                                         key, start, p, bits, depth, states >>

startMoveRight3(self) == /\ pc[self] = "startMoveRight3"
                         /\ IF gNodeArr[t[self]].hasSibling /\ gNodeArr[t[self]].highKey > 0 /\ gNodeArr[t[self]].highKey <= key[self]
                               THEN /\ pc' = [pc EXCEPT ![self] = "saveSibling3"]
                                    /\ start' = start
                               ELSE /\ start' = [start EXCEPT ![self] = t[self]]
                                    /\ pc' = [pc EXCEPT ![self] = "startItSearchLo2"]
                         /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                         nodeArr, insertRes, resultsForGet, 
                                         deleteRes, processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, t, array, split, 
                                         recStack, answer, tmp, tmp2, l_, r_, 
                                         mid, sibling_, keyL_, keyR_, 
                                         recStack1, key_g, startValue, depth_g, 
                                         bits_g, key, p, bits, depth, states >>

saveSibling3(self) == /\ pc[self] = "saveSibling3"
                      /\ start' = [start EXCEPT ![self] = gNodeArr[t[self]].sibling]
                      /\ pc' = [pc EXCEPT ![self] = "getNewLock3"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, 
                                      processReaderState, values, stack, i, 
                                      cur, istack, current_d, key_, tmp_, tmp1, 
                                      mid_, p_, p1, recStack_, recStack1_, ret, 
                                      depth_, res, calc, maxDepth, l, r, gNode, 
                                      buf_, rmax, links, recStack_s, mid_s, 
                                      val, child, lastNode, bits_, depth_s, 
                                      newBits, movedBits, ln, rn, current, 
                                      sibling, keyL, keyR, buf, bufLink, p_d, 
                                      tempL, tempR, bits_d, depth_d, key_i, 
                                      stackData, keyRNext, oldCurrent, parent, 
                                      keyLNext, links_, oldSibling, current_, 
                                      rev, bits_i, depth_i, p_i, temp, scanRes, 
                                      t, array, split, recStack, answer, tmp, 
                                      tmp2, l_, r_, mid, sibling_, keyL_, 
                                      keyR_, recStack1, key_g, startValue, 
                                      depth_g, bits_g, key, p, bits, depth, 
                                      states >>

getNewLock3(self) == /\ pc[self] = "getNewLock3"
                     /\ gNodeArr[start[self]].lock
                     /\ gNodeArr' = [gNodeArr EXCEPT ![start[self]].lock = FALSE]
                     /\ pc' = [pc EXCEPT ![self] = "releaseOldLock3"]
                     /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                     insertRes, resultsForGet, deleteRes, 
                                     processWriterState, processReaderState, 
                                     values, stack, i, cur, istack, current_d, 
                                     key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                     recStack1_, ret, depth_, res, calc, 
                                     maxDepth, l, r, gNode, buf_, rmax, links, 
                                     recStack_s, mid_s, val, child, lastNode, 
                                     bits_, depth_s, newBits, movedBits, ln, 
                                     rn, current, sibling, keyL, keyR, buf, 
                                     bufLink, p_d, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

releaseOldLock3(self) == /\ pc[self] = "releaseOldLock3"
                         /\ gNodeArr' = [gNodeArr EXCEPT ![t[self]].lock = TRUE]
                         /\ t' = [t EXCEPT ![self] = start[self]]
                         /\ pc' = [pc EXCEPT ![self] = "startMoveRight3"]
                         /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                         insertRes, resultsForGet, deleteRes, 
                                         processWriterState, 
                                         processReaderState, values, stack, i, 
                                         cur, istack, current_d, key_, tmp_, 
                                         tmp1, mid_, p_, p1, recStack_, 
                                         recStack1_, ret, depth_, res, calc, 
                                         maxDepth, l, r, gNode, buf_, rmax, 
                                         links, recStack_s, mid_s, val, child, 
                                         lastNode, bits_, depth_s, newBits, 
                                         movedBits, ln, rn, current, sibling, 
                                         keyL, keyR, buf, bufLink, p_d, tempL, 
                                         tempR, bits_d, depth_d, key_i, 
                                         stackData, keyRNext, oldCurrent, 
                                         parent, keyLNext, links_, oldSibling, 
                                         current_, rev, bits_i, depth_i, p_i, 
                                         temp, scanRes, array, split, recStack, 
                                         answer, tmp, tmp2, l_, r_, mid, 
                                         sibling_, keyL_, keyR_, recStack1, 
                                         key_g, startValue, depth_g, bits_g, 
                                         key, start, p, bits, depth, states >>

startItSearchLo2(self) == /\ pc[self] = "startItSearchLo2"
                          /\ p' = [p EXCEPT ![self] = gNodeArr[start[self]].root]
                          /\ lastNode' = [lastNode EXCEPT ![self] = p'[self]]
                          /\ pc' = [pc EXCEPT ![self] = "cycleItSearchLo2"]
                          /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                          nodeArr, insertRes, resultsForGet, 
                                          deleteRes, processWriterState, 
                                          processReaderState, values, stack, i, 
                                          cur, istack, current_d, key_, tmp_, 
                                          tmp1, mid_, p_, p1, recStack_, 
                                          recStack1_, ret, depth_, res, calc, 
                                          maxDepth, l, r, gNode, buf_, rmax, 
                                          links, recStack_s, mid_s, val, child, 
                                          bits_, depth_s, newBits, movedBits, 
                                          ln, rn, current, sibling, keyL, keyR, 
                                          buf, bufLink, p_d, tempL, tempR, 
                                          bits_d, depth_d, key_i, stackData, 
                                          keyRNext, oldCurrent, parent, 
                                          keyLNext, links_, oldSibling, 
                                          current_, rev, bits_i, depth_i, p_i, 
                                          temp, scanRes, t, array, split, 
                                          recStack, answer, tmp, tmp2, l_, r_, 
                                          mid, sibling_, keyL_, keyR_, 
                                          recStack1, key_g, startValue, 
                                          depth_g, bits_g, key, start, bits, 
                                          depth, states >>

cycleItSearchLo2(self) == /\ pc[self] = "cycleItSearchLo2"
                          /\ IF nodeArr[p[self]].hasValue
                                THEN /\ pc' = [pc EXCEPT ![self] = "saveLastNode2"]
                                ELSE /\ pc' = [pc EXCEPT ![self] = "deleteValue"]
                          /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                          nodeArr, insertRes, resultsForGet, 
                                          deleteRes, processWriterState, 
                                          processReaderState, values, stack, i, 
                                          cur, istack, current_d, key_, tmp_, 
                                          tmp1, mid_, p_, p1, recStack_, 
                                          recStack1_, ret, depth_, res, calc, 
                                          maxDepth, l, r, gNode, buf_, rmax, 
                                          links, recStack_s, mid_s, val, child, 
                                          lastNode, bits_, depth_s, newBits, 
                                          movedBits, ln, rn, current, sibling, 
                                          keyL, keyR, buf, bufLink, p_d, tempL, 
                                          tempR, bits_d, depth_d, key_i, 
                                          stackData, keyRNext, oldCurrent, 
                                          parent, keyLNext, links_, oldSibling, 
                                          current_, rev, bits_i, depth_i, p_i, 
                                          temp, scanRes, t, array, split, 
                                          recStack, answer, tmp, tmp2, l_, r_, 
                                          mid, sibling_, keyL_, keyR_, 
                                          recStack1, key_g, startValue, 
                                          depth_g, bits_g, key, start, p, bits, 
                                          depth, states >>

saveLastNode2(self) == /\ pc[self] = "saveLastNode2"
                       /\ lastNode' = [lastNode EXCEPT ![self] = p[self]]
                       /\ pc' = [pc EXCEPT ![self] = "smislCheckValue2"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       bits_, depth_s, newBits, movedBits, ln, 
                                       rn, current, sibling, keyL, keyR, buf, 
                                       bufLink, p_d, tempL, tempR, bits_d, 
                                       depth_d, key_i, stackData, keyRNext, 
                                       oldCurrent, parent, keyLNext, links_, 
                                       oldSibling, current_, rev, bits_i, 
                                       depth_i, p_i, temp, scanRes, t, array, 
                                       split, recStack, answer, tmp, tmp2, l_, 
                                       r_, mid, sibling_, keyL_, keyR_, 
                                       recStack1, key_g, startValue, depth_g, 
                                       bits_g, key, start, p, bits, depth, 
                                       states >>

smislCheckValue2(self) == /\ pc[self] = "smislCheckValue2"
                          /\ IF key[self] < nodeArr[p[self]].value
                                THEN /\ IF ~nodeArr[p[self]].hasLeft
                                           THEN /\ pc' = [pc EXCEPT ![self] = "deleteValue"]
                                           ELSE /\ pc' = [pc EXCEPT ![self] = "smislSetLeft2"]
                                ELSE /\ IF ~nodeArr[p[self]].hasRight
                                           THEN /\ pc' = [pc EXCEPT ![self] = "deleteValue"]
                                           ELSE /\ pc' = [pc EXCEPT ![self] = "smislSetRight2"]
                          /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                          nodeArr, insertRes, resultsForGet, 
                                          deleteRes, processWriterState, 
                                          processReaderState, values, stack, i, 
                                          cur, istack, current_d, key_, tmp_, 
                                          tmp1, mid_, p_, p1, recStack_, 
                                          recStack1_, ret, depth_, res, calc, 
                                          maxDepth, l, r, gNode, buf_, rmax, 
                                          links, recStack_s, mid_s, val, child, 
                                          lastNode, bits_, depth_s, newBits, 
                                          movedBits, ln, rn, current, sibling, 
                                          keyL, keyR, buf, bufLink, p_d, tempL, 
                                          tempR, bits_d, depth_d, key_i, 
                                          stackData, keyRNext, oldCurrent, 
                                          parent, keyLNext, links_, oldSibling, 
                                          current_, rev, bits_i, depth_i, p_i, 
                                          temp, scanRes, t, array, split, 
                                          recStack, answer, tmp, tmp2, l_, r_, 
                                          mid, sibling_, keyL_, keyR_, 
                                          recStack1, key_g, startValue, 
                                          depth_g, bits_g, key, start, p, bits, 
                                          depth, states >>

smislSetLeft2(self) == /\ pc[self] = "smislSetLeft2"
                       /\ p' = [p EXCEPT ![self] = nodeArr[p[self]].left]
                       /\ pc' = [pc EXCEPT ![self] = "cycleItSearchLo2"]
                       /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                       nodeArr, insertRes, resultsForGet, 
                                       deleteRes, processWriterState, 
                                       processReaderState, values, stack, i, 
                                       cur, istack, current_d, key_, tmp_, 
                                       tmp1, mid_, p_, p1, recStack_, 
                                       recStack1_, ret, depth_, res, calc, 
                                       maxDepth, l, r, gNode, buf_, rmax, 
                                       links, recStack_s, mid_s, val, child, 
                                       lastNode, bits_, depth_s, newBits, 
                                       movedBits, ln, rn, current, sibling, 
                                       keyL, keyR, buf, bufLink, p_d, tempL, 
                                       tempR, bits_d, depth_d, key_i, 
                                       stackData, keyRNext, oldCurrent, parent, 
                                       keyLNext, links_, oldSibling, current_, 
                                       rev, bits_i, depth_i, p_i, temp, 
                                       scanRes, t, array, split, recStack, 
                                       answer, tmp, tmp2, l_, r_, mid, 
                                       sibling_, keyL_, keyR_, recStack1, 
                                       key_g, startValue, depth_g, bits_g, key, 
                                       start, bits, depth, states >>

smislSetRight2(self) == /\ pc[self] = "smislSetRight2"
                        /\ p' = [p EXCEPT ![self] = nodeArr[p[self]].right]
                        /\ pc' = [pc EXCEPT ![self] = "cycleItSearchLo2"]
                        /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                        nodeArr, insertRes, resultsForGet, 
                                        deleteRes, processWriterState, 
                                        processReaderState, values, stack, i, 
                                        cur, istack, current_d, key_, tmp_, 
                                        tmp1, mid_, p_, p1, recStack_, 
                                        recStack1_, ret, depth_, res, calc, 
                                        maxDepth, l, r, gNode, buf_, rmax, 
                                        links, recStack_s, mid_s, val, child, 
                                        lastNode, bits_, depth_s, newBits, 
                                        movedBits, ln, rn, current, sibling, 
                                        keyL, keyR, buf, bufLink, p_d, tempL, 
                                        tempR, bits_d, depth_d, key_i, 
                                        stackData, keyRNext, oldCurrent, 
                                        parent, keyLNext, links_, oldSibling, 
                                        current_, rev, bits_i, depth_i, p_i, 
                                        temp, scanRes, t, array, split, 
                                        recStack, answer, tmp, tmp2, l_, r_, 
                                        mid, sibling_, keyL_, keyR_, recStack1, 
                                        key_g, startValue, depth_g, bits_g, 
                                        key, start, bits, depth, states >>

deleteValue(self) == /\ pc[self] = "deleteValue"
                     /\ gNodeArr' = [gNodeArr EXCEPT ![lastNode[self]].delete = TRUE]
                     /\ pc' = [pc EXCEPT ![self] = "releaseStartLock2"]
                     /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                     insertRes, resultsForGet, deleteRes, 
                                     processWriterState, processReaderState, 
                                     values, stack, i, cur, istack, current_d, 
                                     key_, tmp_, tmp1, mid_, p_, p1, recStack_, 
                                     recStack1_, ret, depth_, res, calc, 
                                     maxDepth, l, r, gNode, buf_, rmax, links, 
                                     recStack_s, mid_s, val, child, lastNode, 
                                     bits_, depth_s, newBits, movedBits, ln, 
                                     rn, current, sibling, keyL, keyR, buf, 
                                     bufLink, p_d, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

releaseStartLock2(self) == /\ pc[self] = "releaseStartLock2"
                           /\ gNodeArr' = [gNodeArr EXCEPT ![start[self]].lock = FALSE]
                           /\ deleteRes' = [deleteRes EXCEPT ![self] = 1]
                           /\ pc' = [pc EXCEPT ![self] = "Error"]
                           /\ UNCHANGED << root, hasRoot, globalLock, nodeArr, 
                                           insertRes, resultsForGet, 
                                           processWriterState, 
                                           processReaderState, values, stack, 
                                           i, cur, istack, current_d, key_, 
                                           tmp_, tmp1, mid_, p_, p1, recStack_, 
                                           recStack1_, ret, depth_, res, calc, 
                                           maxDepth, l, r, gNode, buf_, rmax, 
                                           links, recStack_s, mid_s, val, 
                                           child, lastNode, bits_, depth_s, 
                                           newBits, movedBits, ln, rn, current, 
                                           sibling, keyL, keyR, buf, bufLink, 
                                           p_d, tempL, tempR, bits_d, depth_d, 
                                           key_i, stackData, keyRNext, 
                                           oldCurrent, parent, keyLNext, 
                                           links_, oldSibling, current_, rev, 
                                           bits_i, depth_i, p_i, temp, scanRes, 
                                           t, array, split, recStack, answer, 
                                           tmp, tmp2, l_, r_, mid, sibling_, 
                                           keyL_, keyR_, recStack1, key_g, 
                                           startValue, depth_g, bits_g, key, 
                                           start, p, bits, depth, states >>

delete(self) == startDelete(self) \/ returnDel(self) \/ getStart(self)
                   \/ sslStartCycle(self) \/ getStartRoot(self)
                   \/ startInnerCycle(self) \/ sslCheckKey(self)
                   \/ sslSetLeft(self) \/ sslSetRight(self)
                   \/ sslPrepareBits(self) \/ sslPrepareBits2(self)
                   \/ sslCheckGNode(self) \/ setStartSibling(self)
                   \/ sslCheckLinks(self) \/ sslGoToLinks(self)
                   \/ lockGettedGNode(self) \/ startMoveRight3(self)
                   \/ saveSibling3(self) \/ getNewLock3(self)
                   \/ releaseOldLock3(self) \/ startItSearchLo2(self)
                   \/ cycleItSearchLo2(self) \/ saveLastNode2(self)
                   \/ smislCheckValue2(self) \/ smislSetLeft2(self)
                   \/ smislSetRight2(self) \/ deleteValue(self)
                   \/ releaseStartLock2(self)

readerStart(self) == /\ pc[self] = "readerStart"
                     /\ processReaderState' = [processReaderState EXCEPT ![self] = "started"]
                     /\ pc' = [pc EXCEPT ![self] = "startGet"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, values, 
                                     stack, i, cur, istack, current_d, key_, 
                                     tmp_, tmp1, mid_, p_, p1, recStack_, 
                                     recStack1_, ret, depth_, res, calc, 
                                     maxDepth, l, r, gNode, buf_, rmax, links, 
                                     recStack_s, mid_s, val, child, lastNode, 
                                     bits_, depth_s, newBits, movedBits, ln, 
                                     rn, current, sibling, keyL, keyR, buf, 
                                     bufLink, p_d, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

startGet(self) == /\ pc[self] = "startGet"
                  /\ \E v \in VALUES:
                       /\ /\ key_g' = [key_g EXCEPT ![self] = v]
                          /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "get",
                                                                   pc        |->  "readerFinish",
                                                                   startValue |->  startValue[self],
                                                                   depth_g   |->  depth_g[self],
                                                                   bits_g    |->  bits_g[self],
                                                                   key_g     |->  key_g[self] ] >>
                                                               \o stack[self]]
                       /\ startValue' = [startValue EXCEPT ![self] = defaultInitValue]
                       /\ depth_g' = [depth_g EXCEPT ![self] = defaultInitValue]
                       /\ bits_g' = [bits_g EXCEPT ![self] = defaultInitValue]
                       /\ pc' = [pc EXCEPT ![self] = "checkRoot"]
                  /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, nodeArr, 
                                  insertRes, resultsForGet, deleteRes, 
                                  processWriterState, processReaderState, 
                                  values, i, cur, istack, current_d, key_, 
                                  tmp_, tmp1, mid_, p_, p1, recStack_, 
                                  recStack1_, ret, depth_, res, calc, maxDepth, 
                                  l, r, gNode, buf_, rmax, links, recStack_s, 
                                  mid_s, val, child, lastNode, bits_, depth_s, 
                                  newBits, movedBits, ln, rn, current, sibling, 
                                  keyL, keyR, buf, bufLink, p_d, tempL, tempR, 
                                  bits_d, depth_d, key_i, stackData, keyRNext, 
                                  oldCurrent, parent, keyLNext, links_, 
                                  oldSibling, current_, rev, bits_i, depth_i, 
                                  p_i, temp, scanRes, t, array, split, 
                                  recStack, answer, tmp, tmp2, l_, r_, mid, 
                                  sibling_, keyL_, keyR_, recStack1, key, 
                                  start, p, bits, depth, states >>

readerFinish(self) == /\ pc[self] = "readerFinish"
                      /\ processReaderState' = [processReaderState EXCEPT ![self] = "finished"]
                      /\ pc' = [pc EXCEPT ![self] = "Done"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processWriterState, values, 
                                      stack, i, cur, istack, current_d, key_, 
                                      tmp_, tmp1, mid_, p_, p1, recStack_, 
                                      recStack1_, ret, depth_, res, calc, 
                                      maxDepth, l, r, gNode, buf_, rmax, links, 
                                      recStack_s, mid_s, val, child, lastNode, 
                                      bits_, depth_s, newBits, movedBits, ln, 
                                      rn, current, sibling, keyL, keyR, buf, 
                                      bufLink, p_d, tempL, tempR, bits_d, 
                                      depth_d, key_i, stackData, keyRNext, 
                                      oldCurrent, parent, keyLNext, links_, 
                                      oldSibling, current_, rev, bits_i, 
                                      depth_i, p_i, temp, scanRes, t, array, 
                                      split, recStack, answer, tmp, tmp2, l_, 
                                      r_, mid, sibling_, keyL_, keyR_, 
                                      recStack1, key_g, startValue, depth_g, 
                                      bits_g, key, start, p, bits, depth, 
                                      states >>

procR(self) == readerStart(self) \/ startGet(self) \/ readerFinish(self)

writerStart(self) == /\ pc[self] = "writerStart"
                     /\ processWriterState' = [processWriterState EXCEPT ![self] = "started"]
                     /\ pc' = [pc EXCEPT ![self] = "startInsert"]
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processReaderState, values, 
                                     stack, i, cur, istack, current_d, key_, 
                                     tmp_, tmp1, mid_, p_, p1, recStack_, 
                                     recStack1_, ret, depth_, res, calc, 
                                     maxDepth, l, r, gNode, buf_, rmax, links, 
                                     recStack_s, mid_s, val, child, lastNode, 
                                     bits_, depth_s, newBits, movedBits, ln, 
                                     rn, current, sibling, keyL, keyR, buf, 
                                     bufLink, p_d, tempL, tempR, bits_d, 
                                     depth_d, key_i, stackData, keyRNext, 
                                     oldCurrent, parent, keyLNext, links_, 
                                     oldSibling, current_, rev, bits_i, 
                                     depth_i, p_i, temp, scanRes, t, array, 
                                     split, recStack, answer, tmp, tmp2, l_, 
                                     r_, mid, sibling_, keyL_, keyR_, 
                                     recStack1, key_g, startValue, depth_g, 
                                     bits_g, key, start, p, bits, depth, 
                                     states >>

startInsert(self) == /\ pc[self] = "startInsert"
                     /\ \/ /\ \E v \in VALUES:
                                /\ /\ key_i' = [key_i EXCEPT ![self] = v]
                                   /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "insert",
                                                                            pc        |->  "writerFinish",
                                                                            stackData |->  stackData[self],
                                                                            keyRNext  |->  keyRNext[self],
                                                                            oldCurrent |->  oldCurrent[self],
                                                                            parent    |->  parent[self],
                                                                            keyLNext  |->  keyLNext[self],
                                                                            links_    |->  links_[self],
                                                                            oldSibling |->  oldSibling[self],
                                                                            current_  |->  current_[self],
                                                                            rev       |->  rev[self],
                                                                            bits_i    |->  bits_i[self],
                                                                            depth_i   |->  depth_i[self],
                                                                            p_i       |->  p_i[self],
                                                                            temp      |->  temp[self],
                                                                            scanRes   |->  scanRes[self],
                                                                            t         |->  t[self],
                                                                            array     |->  array[self],
                                                                            split     |->  split[self],
                                                                            recStack  |->  recStack[self],
                                                                            answer    |->  answer[self],
                                                                            tmp       |->  tmp[self],
                                                                            tmp2      |->  tmp2[self],
                                                                            l_        |->  l_[self],
                                                                            r_        |->  r_[self],
                                                                            mid       |->  mid[self],
                                                                            sibling_  |->  sibling_[self],
                                                                            keyL_     |->  keyL_[self],
                                                                            keyR_     |->  keyR_[self],
                                                                            recStack1 |->  recStack1[self],
                                                                            key_i     |->  key_i[self] ] >>
                                                                        \o stack[self]]
                                /\ stackData' = [stackData EXCEPT ![self] = <<>>]
                                /\ keyRNext' = [keyRNext EXCEPT ![self] = defaultInitValue]
                                /\ oldCurrent' = [oldCurrent EXCEPT ![self] = defaultInitValue]
                                /\ parent' = [parent EXCEPT ![self] = defaultInitValue]
                                /\ keyLNext' = [keyLNext EXCEPT ![self] = defaultInitValue]
                                /\ links_' = [links_ EXCEPT ![self] = defaultInitValue]
                                /\ oldSibling' = [oldSibling EXCEPT ![self] = defaultInitValue]
                                /\ current_' = [current_ EXCEPT ![self] = defaultInitValue]
                                /\ rev' = [rev EXCEPT ![self] = defaultInitValue]
                                /\ bits_i' = [bits_i EXCEPT ![self] = defaultInitValue]
                                /\ depth_i' = [depth_i EXCEPT ![self] = defaultInitValue]
                                /\ p_i' = [p_i EXCEPT ![self] = defaultInitValue]
                                /\ temp' = [temp EXCEPT ![self] = defaultInitValue]
                                /\ scanRes' = [scanRes EXCEPT ![self] = defaultInitValue]
                                /\ t' = [t EXCEPT ![self] = defaultInitValue]
                                /\ array' = [array EXCEPT ![self] = defaultInitValue]
                                /\ split' = [split EXCEPT ![self] = defaultInitValue]
                                /\ recStack' = [recStack EXCEPT ![self] = defaultInitValue]
                                /\ answer' = [answer EXCEPT ![self] = defaultInitValue]
                                /\ tmp' = [tmp EXCEPT ![self] = defaultInitValue]
                                /\ tmp2' = [tmp2 EXCEPT ![self] = defaultInitValue]
                                /\ l_' = [l_ EXCEPT ![self] = defaultInitValue]
                                /\ r_' = [r_ EXCEPT ![self] = defaultInitValue]
                                /\ mid' = [mid EXCEPT ![self] = defaultInitValue]
                                /\ sibling_' = [sibling_ EXCEPT ![self] = defaultInitValue]
                                /\ keyL_' = [keyL_ EXCEPT ![self] = defaultInitValue]
                                /\ keyR_' = [keyR_ EXCEPT ![self] = defaultInitValue]
                                /\ recStack1' = [recStack1 EXCEPT ![self] = defaultInitValue]
                                /\ pc' = [pc EXCEPT ![self] = "checkHasRoot"]
                           /\ UNCHANGED <<key, start, p, bits, depth>>
                        \/ /\ \E v \in VALUES:
                                /\ /\ key' = [key EXCEPT ![self] = v]
                                   /\ stack' = [stack EXCEPT ![self] = << [ procedure |->  "delete",
                                                                            pc        |->  "writerFinish",
                                                                            start     |->  start[self],
                                                                            p         |->  p[self],
                                                                            bits      |->  bits[self],
                                                                            depth     |->  depth[self],
                                                                            key       |->  key[self] ] >>
                                                                        \o stack[self]]
                                /\ start' = [start EXCEPT ![self] = defaultInitValue]
                                /\ p' = [p EXCEPT ![self] = defaultInitValue]
                                /\ bits' = [bits EXCEPT ![self] = defaultInitValue]
                                /\ depth' = [depth EXCEPT ![self] = defaultInitValue]
                                /\ pc' = [pc EXCEPT ![self] = "startDelete"]
                           /\ UNCHANGED <<key_i, stackData, keyRNext, oldCurrent, parent, keyLNext, links_, oldSibling, current_, rev, bits_i, depth_i, p_i, temp, scanRes, t, array, split, recStack, answer, tmp, tmp2, l_, r_, mid, sibling_, keyL_, keyR_, recStack1>>
                     /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                     nodeArr, insertRes, resultsForGet, 
                                     deleteRes, processWriterState, 
                                     processReaderState, values, i, cur, 
                                     istack, current_d, key_, tmp_, tmp1, mid_, 
                                     p_, p1, recStack_, recStack1_, ret, 
                                     depth_, res, calc, maxDepth, l, r, gNode, 
                                     buf_, rmax, links, recStack_s, mid_s, val, 
                                     child, lastNode, bits_, depth_s, newBits, 
                                     movedBits, ln, rn, current, sibling, keyL, 
                                     keyR, buf, bufLink, p_d, tempL, tempR, 
                                     bits_d, depth_d, key_g, startValue, 
                                     depth_g, bits_g, states >>

writerFinish(self) == /\ pc[self] = "writerFinish"
                      /\ processWriterState' = [processWriterState EXCEPT ![self] = "finished"]
                      /\ pc' = [pc EXCEPT ![self] = "Done"]
                      /\ UNCHANGED << root, hasRoot, globalLock, gNodeArr, 
                                      nodeArr, insertRes, resultsForGet, 
                                      deleteRes, processReaderState, values, 
                                      stack, i, cur, istack, current_d, key_, 
                                      tmp_, tmp1, mid_, p_, p1, recStack_, 
                                      recStack1_, ret, depth_, res, calc, 
                                      maxDepth, l, r, gNode, buf_, rmax, links, 
                                      recStack_s, mid_s, val, child, lastNode, 
                                      bits_, depth_s, newBits, movedBits, ln, 
                                      rn, current, sibling, keyL, keyR, buf, 
                                      bufLink, p_d, tempL, tempR, bits_d, 
                                      depth_d, key_i, stackData, keyRNext, 
                                      oldCurrent, parent, keyLNext, links_, 
                                      oldSibling, current_, rev, bits_i, 
                                      depth_i, p_i, temp, scanRes, t, array, 
                                      split, recStack, answer, tmp, tmp2, l_, 
                                      r_, mid, sibling_, keyL_, keyR_, 
                                      recStack1, key_g, startValue, depth_g, 
                                      bits_g, key, start, p, bits, depth, 
                                      states >>

procW(self) == writerStart(self) \/ startInsert(self) \/ writerFinish(self)

(* Allow infinite stuttering to prevent deadlock on termination. *)
Terminating == /\ \A self \in ProcSet: pc[self] = "Done"
               /\ UNCHANGED vars

Next == (\E self \in ProcSet:  \/ initLeaf(self) \/ doInsertLeaf(self)
                               \/ smartFillValParentLo(self)
                               \/ doInsertNode(self) \/ insert(self)
                               \/ get(self) \/ delete(self))
           \/ (\E self \in R: procR(self))
           \/ (\E self \in P: procW(self))
           \/ Terminating

Spec == Init /\ [][Next]_vars

Termination == <>(\A self \in ProcSet: pc[self] = "Done")

\* END TRANSLATION 


=============================================================================
\* Modification History
\* Last modified Tue Jun 27 08:56:15 MSK 2023 by green-tea
\* Created Sun May 28 11:21:31 MSK 2023 by green-tea
